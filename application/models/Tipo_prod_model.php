<?php
	if (!defined('BASEPATH')) exit ('No direct script access allowed');

	class Tipo_prod_model extends CI_Model{

		public function iniciar_sesion($login,$clave){
			$this->db->where('login',$login);
			$this->db->where('clave',$clave);
			$this->db->where('estatus','1');
			$this->db->select('*');
			$this->db->from(' usuarios u');
			return $this->db->count_all_results();
		}

		public function consultarCatProd($data){
			if($data["id_idioma"]!=""){
				$this->db->where('a.id_idioma', $data["id_idioma"]);
			}
			$this->db->order_by('a.id','DESC');
	        $this->db->where('a.estatus!=',2);
			$this->db->select('a.id, a.descripcion, a.titulo');
			$this->db->from('categoria_producto a');
			$res = $this->db->get();

			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function guardartipoProd($datos){
			if ($this->db->insert("tipo_producto", $datos)) {
				return true;
			}else {
				return false;
			}
		}

		public function consultartipoProd($data){

			if($data["id_tipo_prod"]!=""){
				$this->db->where('a.id', $data["id_tipo_prod"]);
			}
			$this->db->order_by('a.id');
	        $this->db->where('a.estatus!=',2);
			$this->db->select('a.id, a.titulo, a.descripcion,b.id as id_idioma, b.descripcion as descripcion_idioma,a.estatus as estatus');
			//,c.id as categoria_prod, c.titulo as titulo_catprod
			$this->db->from('tipo_producto a');
			$this->db->join('idioma b', 'b.id = a.id_idioma');
			//$this->db->join('categoria_producto c', 'c.id = a.id_categoria_prod');
			$res = $this->db->get();
			//print_r($this->db->last_query());die;

			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultarExiste($id){
			if($id!=""){
				$this->db->where('a.id', $id);
			}
			$this->db->select('a.*');
			$this->db->from('tipo_producto a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function modificarTipoProd($data){
			$this->db->where('id', $data["id"]);
			if($this->db->update("tipo_producto", $data)){
				return true;
			}else{
				return false;
			}
		}

		public function modificar_estatus($data){
			$this->db->where('id', $data["id"]);
			if($this->db->update("tipo_producto", $data)){
				return true;
			}else{
				return false;
			}
		}

		public function consultarDetProd($data){
			//print_r($data);die;
			if($data["id_idioma"]!=""){
				$this->db->where('a.id_idioma', $data["id_idioma"]);
			}
			$this->db->order_by('a.id','DESC');
	        $this->db->where('a.estatus!=',2);
			$this->db->select('a.id, a.descripcion, a.titulo');
			$this->db->from('tipo_producto a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
	}

?>
