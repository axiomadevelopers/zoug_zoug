<?php
	if (!defined('BASEPATH')) exit ('No direct script access allowed');

	class Tallas_model extends CI_Model{

		public function iniciar_sesion($login,$clave){
			$this->db->where('login',$login);
			$this->db->where('clave',$clave);
			$this->db->where('estatus','1');
			$this->db->select('*');
			$this->db->from(' usuarios u');
			return $this->db->count_all_results();
		}

		public function guardarTallas($data){
			if($this->db->insert("tallas",$data)){
				return true;
			}else{
				return false;
			}
		}

		public function consultarTallas($data){
			if($data["id_talla"]!=""){
				$this->db->where('a.id', $data["id_talla"]);
			}
			$this->db->order_by('a.id','DESC');
	        $this->db->where('a.estatus!=',2);
			//$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen');
			$this->db->select('a.*');
			$this->db->from('tallas a');
	        //$this->db->join('galeria c', 'c.id = a.id_imagen');
			$res = $this->db->get();

			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultarExiste($id){
			if($id!=""){
				$this->db->where('a.id', $id);
			}
			$this->db->select('a.*');
			$this->db->from('tallas a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function modificarTallas($data){
			$this->db->where('id', $data["id"]);
	        if($this->db->update("tallas", $data)){
	        	return true;
	        }else{
	        	return false;
	        }
		}

		public function modificarTallasEstatus($data){
			$this->db->where('id', $data["id"]);
	        if($this->db->update("tallas", $data)){
	        	return true;
	        }else{
	        	return false;
	        }
		}

		public function consultarMarcas_idioma($data){
			if($data["id_idioma"]!=""){
				$this->db->where('a.id_idioma', $data["id_idioma"]);
			}
			$this->db->order_by('a.id','DESC');
	        $this->db->where('a.estatus!=',2);
			$this->db->select('a.id, a.descripcion, a.titulo');
			$this->db->from('marcas a');
			$res = $this->db->get();

			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
	}

?>
