<?php
	if (!defined('BASEPATH')) exit ('No direct script access allowed');

	class OrdenCompra_model extends CI_Model{

		public function consultar_orden_compra(){
			
			$this->db->order_by('a.id','desc');
			$this->db->select('a.id,a.numero_orden_compra,a.fecha,a.payment_id,a.payer_id,a.payment_token,a.id_carrito,b.id as id_usuario, b.nombre_apellido as usuario');
			$this->db->from('orden_compra a');
 			 $this->db->join('social_login b', 'b.id = a.id_usuario'); 
			 /* 
			 ,c.id_orden as id, c.monto_total as monto_total,d.id_orden as id, d.monto as monto_individual,e.id_orden as id, e.cantidad as cantidad
			 
			 $this->db->join('orden_compra_detalle c', 'c.id_orden = a.id');
 			$this->db->join('orden_compra_detalle d', 'd.id_orden = a.id');
 			$this->db->join('orden_compra_detalle e', 'e.id_orden = a.id');  */
 			$res = $this->db->get();

			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultar_detalles($id){
			
			$this->db->order_by('a.id','desc');
			$this->db->where('a.id_orden',$id);
			$this->db->select('a.id_producto,a.cantidad,a.monto,a.monto_total,a.id_orden, e.titulo as producto,d.id_color as id_color,d.id_talla as id_talla');
			$this->db->join('detalle_productos e', 'e.id = a.id_producto'); 
			$this->db->join('cantidad_producto d', 'd.id = a.id_cantidad_producto'); 
 			$this->db->from('orden_compra_detalle a');
 			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
		public function consultar_img_prod($id_prod){

			$this->db->order_by('a.id','ASC');
			$this->db->where('a.id_det_prod',$id_prod);
			$this->db->select('c.ruta as ruta');
	    	$this->db->join('galeria c', 'c.id = a.id_imagen');
			$this->db->from('detalle_productos_imag a');
			$this->db->limit('1');

 			$res = $this->db->get();
			if($res){
				return $res->row_array();
			}else{
				return false;
			}
		}

		public function guardarFooter($data){
			if($this->db->insert("footer",$data)){
				return true;
			}else{
				return false;
			}
		}

		public function consultarFooter($data){
			$this->db->order_by('id','asc');
			if($data["id_footer"]!=""){
				$this->db->where('a.id', $data["id_footer"]);
			}
	        $this->db->where('a.estatus!=',2);
			$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma');
			$this->db->from('footer a');
			$this->db->join('idioma b', 'b.id = a.id_idioma');
			$res = $this->db->get();
	        
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultarExiste($id){
			$this->db->where('n.id',$id);
			$this->db->select('*');
			$this->db->from(' footer n');
			return $this->db->count_all_results();
		}

		public function modificarFooter($data){
		$this->db->where('id', $data["id"]);
        if($this->db->update("footer", $data)){
        	return true;
        }else{
        	return false;
        }
	} 
	public function consultar_color($id){
			
		$this->db->where('a.id',$id);
		$this->db->select('a.descripcion');
		$this->db->from('colores a');
		 $res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	public function consultar_talla($id){
			
		$this->db->where('a.id',$id);
		$this->db->select('a.descripcion');
		$this->db->from('tallas a');
		 $res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
}	