<?php
	if (!defined('BASEPATH')) exit ('No direct script access allowed');

	class Producto_idioma_model extends CI_Model{

		public function iniciar_sesion($login,$clave){
			$this->db->where('login',$login);
			$this->db->where('clave',$clave);
			$this->db->where('estatus','1');
			$this->db->select('*');
			$this->db->from(' usuarios u');
			return $this->db->count_all_results();
		}

		public function guardarColores($data){
			//print_r ($data);die;
			if($this->db->insert("colores",$data)){
				return true;
			}else{
				return false;
			}
		}

		public function consultarColores($data){
			if($data["id_color"]!=""){
				$this->db->where('a.id', $data["id_color"]);
			}
			$this->db->order_by('a.id','DESC');
	        $this->db->where('a.estatus!=',2);
			//$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen');
			$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma');
			$this->db->from('colores a');
			$this->db->join('idioma b', 'b.id = a.id_idioma');
	        //$this->db->join('galeria c', 'c.id = a.id_imagen');
			$res = $this->db->get();

			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultarExiste($id){
			if($id!=""){
				$this->db->where('a.id', $id);
			}
			$this->db->select('a.*');
			$this->db->from('colores a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function modificarColores($data){
			$this->db->where('id', $data["id"]);
	        if($this->db->update("colores", $data)){
	        	return true;
	        }else{
	        	return false;
	        }
		}

		public function modificarColoresEstatus($data){
			$this->db->where('id', $data["id"]);
	        if($this->db->update("colores", $data)){
	        	return true;
	        }else{
	        	return false;
	        }
		}
		
		
		public function dasd($id,$descripcion){
			$this->db->where('n.id !=',$id);
			$this->db->where('n.descripcion',$descripcion);
			$this->db->select('*');
			$this->db->from(' colores n');

		}
		public function existeColores($id,$descripcion){
			
			
			$this->db->where('a.id!=', $id);
			$this->db->where('a.estatus!=', 2);
			$this->db->where('a.descripcion',$descripcion);
			$this->db->select('a.*');
			$this->db->from('colores a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
		public function consultarColorEspanol($data){
		
		
			$this->db->where('a.id_idioma', 1);
			$this->db->order_by('a.id');
			$this->db->select('a.id,a.descripcion');
			$this->db->from('colores a');
			$res = $this->db->get();
			if($res){
				return $res->result_array();
			}else{
				return false;
			}
			
		}
	}

?>
