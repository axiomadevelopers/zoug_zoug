<?php

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class Palabras_claves_model extends CI_Model{

	public function guardarDescripcion($data){
		if($this->db->insert("palabras_claves", $data)){
			return true;
		}else{
			return false;
		}
	}
	/*
	*	consultarIdDescripcion
	*/
	public function consultarIdDescripcion(){
		$this->db->select_max('id');
		$res1 = $this->db->get("palabras_claves");
		if ($res1->num_rows() > 0){
	        $res2 = $res1->result();
	        return $res2[0]->id;
		}else{
			return 0;
		}
	}
	/*
	*	existeDescripcion
	*/
	public function existeDescripcion($id){

		if($id!=""){
			$this->db->where('a.id', $id);
		}

		$this->db->select('a.id');
		$this->db->from('palabras_claves a');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	*	modificarDescripcion
	*/	
	public function modificarDescripcion($id,$data){
		$this->db->where('id', $id);
        if($this->db->update("palabras_claves", $data)){
        	return true;
        }else{
        	return false;
        }

	}
	/*
	*	consultarDescripcion
	*/
	public function consultarDescripcion(){
		$this->db->select('a.id,a.descripcion,a.estatus');
		$this->db->from('palabras_claves a');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
}	