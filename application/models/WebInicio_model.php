<?php

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class WebInicio_model extends CI_Model{

	public function consultarSlider($data){
		//var_dump($data);echo "<br>";
		$this->db->where('a.id_idioma', $data["id_idioma"]);
		$this->db->order_by('a.orden');
        $this->db->where('a.estatus',1);
		$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen');
		$this->db->from('slider a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
        $this->db->join('galeria c', 'c.id = a.id_imagen');
		$res = $this->db->get();
		/*var_dump($res);echo"<br>";
		var_dump($this->db->last_query());echo"<br>";*/
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function consultarNosotros($data){
		//var_dump($data);echo"<br>";
		$this->db->where('a.id_idioma', $data["id_idioma"]);
		$this->db->order_by('a.id');
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma,  c.ruta as ruta, c.id as id_imagen');
		$this->db->from('empresa_nosotros a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
		$this->db->join('galeria c', 'c.id = a.id_imagen');
		$res = $this->db->get();
		//var_dump($this->db->last_query());echo"<br>";
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function guardarContactos($data){
		if($this->db->insert("contactos", $data)){
			return true;
		}else{
			return false;
		}
	}

	public function consultarRedes($data){
		$this->db->order_by('a.id');
		$this->db->select('a.id, a.url_red, c.descripcion');
		$this->db->from('red_social a');
		$this->db->join('tipo_red c', 'c.id = a.id_tipo_red');

		$res = $this->db->get();
        //print_r($this->db->last_query());die;

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function consultarFooter($data){
		$this->db->where('a.id_idioma', $data["id_idioma"]);
		$this->db->order_by('a.id');
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.id, a.correo, a.descripcion');
		$this->db->from('footer a');

		$res = $this->db->get();
        //print_r($this->db->last_query());die;

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function consultarproductos($datos){
		$this->db->order_by('c.orden','DESC');
		$this->db->order_by('d.id','ASC');
        $this->db->where('a.estatus!=',2);
        $this->db->where('a.id_idioma',$datos["id_idioma"]);
		$this->db->select('a.id');
		$this->db->from('detalle_productos a');
		$this->db->limit(6);
		$this->db->join('idioma b', 'b.id = a.id_idioma');
		$this->db->join('marcas c', 'c.id = a.id_marca');
		$this->db->join('categoria_producto d', 'd.id = a.id_categoria_prod');

		$res = $this->db->get();

		//print_r($this->db->last_query());die("x");

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function consultarimg($id){

		$this->db->order_by('a.id', 'DESC');

		if($id!=""){
			$this->db->where('a.id_det_prod', $id);
		}
		$this->db->select('a.id_det_prod, GROUP_CONCAT(DISTINCT a.id_imagen ORDER BY a.id ASC SEPARATOR ",") as imagen',FALSE);
		$this->db->from('detalle_productos_imag a');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	*	consultar_ingles
	*/
	public function consultar_ingles($id_producto){
		$this->db->where('a.id_original_clonado', $id_producto);
		$this->db->select('a.titulo');
		$this->db->from('detalle_productos a');
		$res = $this->db->get();

		//print_r($this->db->last_query());die("x");

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/***/
	/*
	*	consultar_ingles_color($arreglo_registro->id_color)
	*/
	public function consultar_ingles_color($id_color){
		$this->db->where('a.nombre_tabla', 'Colores');
		$this->db->where('a.id_categoria_es',$id_color); 
		$this->db->select('a.id_categoria_en,b.descripcion as titulo_color');
		$this->db->from('categorias_productos_idiomas a');
		$this->db->join('colores b', 'b.id = a.id_categoria_en');

		$res = $this->db->get();

		//print_r($this->db->last_query());die("x");

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/***/
	public function consultarimg_sola($id_final,$id_producto,$id_cantidad_producto){

		$this->db->order_by('b.id','ASC');

		if(($id_final!="")&&($id_final!="0")){
			$this->db->where('a.id', $id_final);
		}

		if($id_producto!=""){
			$this->db->where('c.id', $id_producto);
		}

		if($id_cantidad_producto!=""){
			$this->db->where('d.id', $id_cantidad_producto);
			$x = $this->db->select('a.ruta, c.titulo, c.precio, c.id, c.slug,e.descripcion as color, f.descripcion as talla, e.id as id_color, f.id as id_talla');
		}else{
			$x = $this->db->select('a.ruta, c.titulo, c.precio, c.id, c.slug');
		}

		$this->db->from('galeria a');
		$this->db->join('detalle_productos_imag b', 'b.id_imagen = a.id');
		$this->db->join('detalle_productos c', 'c.id = b.id_det_prod');
		
		if($id_cantidad_producto!=""){
			$this->db->join('cantidad_producto d', 'd.id_det_prod = c.id');
			$this->db->join('colores e', 'e.id = d.id_color');
			$this->db->join('tallas f','f.id = d.id_talla'); 
		}
		
		$res = $this->db->get();

		//print_r($this->db->last_query());die("x");

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function consultarNoticiasFiltros($datos){
		/*if($data["id_noticias"]!=""){
			$this->db->where('a.id', $data["id_noticias"]);
		}*/
		$this->db->order_by('a.id');
        $this->db->where('a.estatus!=',2);
        $this->db->where('a.id_idioma',$datos["id_idioma"]);
		$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen,d.login as usuario');
		$this->db->from('seccion_noticias a');
		//$this->db->limit($data["limit"],$data["offset"]);
		$this->db->join('idioma b', 'b.id = a.id_idioma');
        $this->db->join('galeria c', 'c.id = a.id_imagen');
        $this->db->join('usuarios d', 'd.id = a.id_usuario');
		$res = $this->db->get();
		//print_r($res);die;
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	*	Consultar carrito segun usuario 
	*/
	public function consultarCarrito($datos){
		//var_dump($datos);die;
		if($datos["id_usuario"]!=""){
			$this->db->where('c.id_usuario', $datos["id_usuario"]);
		}
		$this->db->where('b.estatus !=',"2");
		$this->db->where('a.id_idioma', $datos["id_idioma"]);
		$this->db->order_by('a.id','ASC');
		//$this->db->limit('2');
		$this->db->select('a.*,b.cantidad,b.monto,b.monto_total, b.id as id_producto_carrito,b.id_carrito,b.id_producto,b.id_cantidad_producto');
		//,d.id_color as id_color,d.id_talla as id_talla,e.descripcion as color, f.descripcion as talla
		$this->db->from('detalle_productos a');
		
		//Para español
		if($datos["id_idioma"]==1){
			$this->db->join('carrito_productos b', 'b.id_producto = a.id');
		}else{
		//Para ingles
			$this->db->join('carrito_productos b', 'b.id_producto = a.id_original_clonado');
		}
		$this->db->join('carrito_encabezado c', 'c.id = b.id_carrito');
		//--
		$this->db->join('cantidad_producto d', 'd.id = b.id_cantidad_producto');
		$this->db->join('colores e', 'e.id = d.id_color');
		$this->db->join('tallas f','f.id = d.id_talla'); 
		$res = $this->db->get();

		//print_r($this->db->last_query());die("x");

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/***/
	public function consultarLinea($datos){
		$this->db->where('a.estatus', '1');
		if($datos['id_idioma']!=""){
			$this->db->where('a.id_idioma', $datos['id_idioma']);
		}
		$this->db->order_by('a.orden', 'DESC');
		$this->db->select('a.id, a.titulo');
		$this->db->from('marcas a');;
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	
	/*
	*	consultarPalabrasClaves
	*/
	public function consultarPalabrasClaves(){
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.descripcion as palabras_claves');
		$this->db->from('palabras_claves a');

		$res = $this->db->get();
        //print_r($this->db->last_query());die;

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	/*
	*	consultarDescripcion
	*/
	public function consultarDescripcion(){
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.descripcion as descripcion');
		$this->db->from('descripcion a');

		$res = $this->db->get();
        //print_r($this->db->last_query());die;

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
}
