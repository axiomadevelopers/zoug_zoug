<?php

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class WebCheckOut_model extends CI_Model{
	//--------------------------------------
	public function consultarCarritoInfo($id_usuario,$id_carrito){
		//--Paso1 consulto los datos del carrito de ese usuario
		/*if($id_usuario!=""){
			$this->db->where('a.id_usuario', $id_usuario);
		}*/
		$this->db->where('a.id_carrito', $id_carrito);
		$this->db->where('a.id_usuario', $id_usuario);
		$this->db->where('a.estatus','1');
		$this->db->order_by('a.id','DESC');
		$this->db->select('a.*');
		$this->db->from('carrito_productos a');
		$res = $this->db->get();
		//print_r($this->db->last_query());die("x");
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	*	generarOrdenEncabezado

	*/
	public function generarOrdenEncabezado($id_usuario,$id_carrito,$paymentID,$payerID,$paymentToken){
		
		$data = array(
						"id_usuario"=>$id_usuario,
						"fecha"=>date("Y-m-d"),
						"numero_orden_compra"=> md5(uniqid($id_usuario, true)), 
						"payment_id"=>$paymentID,
						"payer_id"=>$payerID,
						"payment_token"=>$paymentToken,
						"estatus"=>1,
						"id_carrito"=>$id_carrito
		);

		$datosOrden = [];
		$this->db->insert("orden_compra",$data);
		$id_orden_compra = $this->db->insert_id();
		#Consulto el numero de la orden 
		$this->db->order_by('a.id','DESC');
		$this->db->select('a.*');
		$this->db->from('orden_compra a');
		$this->db->limit('1');
		$res = $this->db->get();
		//print_r($this->db->last_query());die("x");
		if($res){
			$recordOrden = $res->result();
			$datosOrden = array(
									"id_orden_compra"=>$id_orden_compra,
									"numero_orden_compra"=>$recordOrden[0]->numero_orden_compra
			);
		}
		return $datosOrden;
	}
	/*
	*	generarOrdenDetalle
	*/
	public function generarOrdenDetalle($idOrden,$res_info_carrito,$id_carrito){
		$data_update = array(
								"estatus"=>2
		);
		foreach ($res_info_carrito as $clave_carrito => $valor_carrito) {
			$data = array(
							"id_producto"=>$valor_carrito->id_producto,
							"cantidad"=>$valor_carrito->cantidad,
							"monto"=>$valor_carrito->monto,
							"monto_total"=>$valor_carrito->monto_total,
							"id_orden"=>$idOrden,
							"estatus"=>1,
							"id_cantidad_producto"=>$valor_carrito->id_cantidad_producto 	

							
			);
			$this->db->insert("orden_compra_detalle",$data);
			//----
			//Modificar estatus de producto en carrito...
			$this->db->where('id_producto',$valor_carrito->id_producto);
			$this->db->where('id_carrito', $valor_carrito->id_carrito);
			$modificacion = $this->db->update("carrito_productos", $data_update);
			//
			//$id_carrito =$valor_carrito->id_carrito;
			
			//
		}
		$this->db->where('id', $id_carrito);
		$modificacion = $this->db->update("carrito_encabezado", $data_update);
		return true;
		
	}
	/*
	*	Trae cantidad en inventario Ordenes
	*/
	public function consultarCantidad($id){
		
		$this->db->where('a.id',$id);
		$this->db->where('a.estatus','1');
		$this->db->order_by('a.id','DESC');
		$this->db->select('a.*');
		$this->db->from('inventario_ordenes a');
		$res = $this->db->get();
		//print_r($this->db->last_query());die("x");
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	*	Cambia cantidad en inventario Ordenes
	*/
	public function modificarTablaCantidad($id,$data){
		$this->db->where('id', $id);
		if($this->db->update("inventario_ordenes", $data)){
			return true;
		}else{
			return false;
		}
	}
	/*
	*	existeOrden
	*/
	public function existeOrden($id_usuario,$id_carrito){
		$this->db->where('id_usuario',$id_usuario);
		$this->db->where('id_carrito',$id_carrito);
		$this->db->select('*');
		$this->db->from('orden_compra');
		return $this->db->count_all_results();
	}
	/*
	*	consultarOrden
	*/
	public function consultarOrden($id_carrito){
		$this->db->order_by('a.id','DESC');
		$this->db->where('id_carrito',$id_carrito);
		$this->db->select('a.*');
		$this->db->from('orden_compra a');
		$this->db->limit('1');
		$res = $this->db->get();
		//print_r($this->db->last_query());die("x");
		if($res){
			$recordOrden = $res->result();
			$datosOrden = array(
									"id_orden_compra"=>$recordOrden[0]->id,
									"numero_orden_compra"=>$recordOrden[0]->numero_orden_compra
			);
		}
		return $datosOrden;
	}
	/*
	*	Consultar existe carrito...
	*/
	public function consultarCarritoExiste($id_carrito){
		$this->db->where('id',$id_carrito);
		$this->db->select('*');
		$this->db->from('carrito_encabezado');
		return $this->db->count_all_results();
	}
	//--------------------------------------
	/*
	*	COnsulto el titulo del producto en ingles
	*/
	public function consultarClonadoEmail($id){
			
		$this->db->order_by('a.id','DESC');
		$this->db->where('a.estatus!=',2);
		$this->db->where('a.id_original_clonado', $id);
		$this->db->select('a.titulo as producto, c.descripcion as color, d.descripcion as talla, c.id as id_color, d.id as id_talla');
		$this->db->from('detalle_productos a');
		$this->db->join('cantidad_producto b','b.id_det_prod = a.id_original_clonado');
		$this->db->join('colores c', 'c.id = b.id_color');
		$this->db->join('tallas d', 'd.id = b.id_talla');
		$res = $this->db->get();
		//print_r($this->db->last_query());die("x");
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	//-------------------------------------
	/*
	*	Para consultar colores en el idioma ingles
	*/
	public function consultarColoresIngles($id_color){
		$this->db->where('b.id_categoria_es',$id_color);
		$this->db->where('b.nombre_tabla', 'Colores');
		$this->db->select('a.descripcion as descripcion_colores');
		$this->db->from('colores a');
		$this->db->join('categorias_productos_idiomas b','b.id_categoria_en = a.id');
	
		$res = $this->db->get();
		//print_r($this->db->last_query());die("x");
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	//--------------------------------------
}