<?php

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class WebOrdenUsuario_model extends CI_Model{
	/*
	*	Consultar info de Orden
	*/
	public function consultarOrden($datos = "",$id_usuario){
		//if($id_usuario!=""){
		$this->db->where('a.id_usuario', $id_usuario);
		//}
		$this->db->where('a.estatus !=',"2");
		$this->db->order_by('a.id','DESC');
		$this->db->select('a.*');
		$this->db->from('orden_compra a');
		//
		$res = $this->db->get();

		//print_r($this->db->last_query());die("x");

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	*	Consultar info del carrito de compra para x usuario
	*/
	public function consultarCarrito($datos = "",$id){
		if($id!=""){
			$this->db->where('a.id', $id);
		}
		$this->db->where('c.estatus !=',"2");
		$this->db->order_by('a.id','DESC');
		$this->db->select('a.*,b.cantidad,b.monto,b.monto_total, a.id as id_producto_orden,c.titulo,b.id_producto as id_producto_real, b.id_cantidad_producto as id_cantidad_producto');
		$this->db->from('orden_compra a');
		$this->db->join('orden_compra_detalle b', 'b.id_orden	 = a.id');
		//Para español
		if($datos["id_idioma"]==1){
			$this->db->join('detalle_productos c', 'c.id = b.id_producto');
		}else{
		//Para ingles
			$this->db->join('detalle_productos c', 'c.id_original_clonado = b.id_producto');
		}

		$res = $this->db->get();

		//print_r($this->db->last_query());die("x");

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	*
	*/
}