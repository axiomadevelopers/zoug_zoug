<?php
	if (!defined('BASEPATH')) exit ('No direct script access allowed');

	class Contactos_model extends CI_Model{

		public function consultar_contactos(){
			
			$this->db->order_by('a.id','desc');
			$this->db->select('a.id, a.nombres, a.telefono, a.email, a.mensaje');
			$this->db->from('contactos a');
			$res = $this->db->get();

			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function guardarFooter($data){
			if($this->db->insert("footer",$data)){
				return true;
			}else{
				return false;
			}
		}

		public function consultarFooter($data){
			$this->db->order_by('id','asc');
			if($data["id_footer"]!=""){
				$this->db->where('a.id', $data["id_footer"]);
			}
	        $this->db->where('a.estatus!=',2);
			$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma');
			$this->db->from('footer a');
			$this->db->join('idioma b', 'b.id = a.id_idioma');
			$res = $this->db->get();
	        
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultarExiste($id){
			$this->db->where('n.id',$id);
			$this->db->select('*');
			$this->db->from(' footer n');
			return $this->db->count_all_results();
		}

		public function modificarFooter($data){
		$this->db->where('id', $data["id"]);
        if($this->db->update("footer", $data)){
        	return true;
        }else{
        	return false;
        }
	} 
	}	