<?php

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class Descripcion_model extends CI_Model{

	public function guardarDescripcion($data){
		if($this->db->insert("descripcion", $data)){
			return true;
		}else{
			return false;
		}
	}
	/*
	*	consultarIdDescripcion
	*/
	public function consultarIdDescripcion(){
		$this->db->select_max('id');
		$res1 = $this->db->get("descripcion");
		if ($res1->num_rows() > 0){
	        $res2 = $res1->result();
	        return $res2[0]->id;
		}else{
			return 0;
		}
	}
	/*
	*	existeDescripcion
	*/
	public function existeDescripcion($id){

		if($id!=""){
			$this->db->where('a.id', $id);
		}

		$this->db->select('a.id');
		$this->db->from('descripcion a');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	*	modificarDescripcion
	*/	
	public function modificarDescripcion($id,$data){
		$this->db->where('id', $id);
        if($this->db->update("descripcion", $data)){
        	return true;
        }else{
        	return false;
        }

	}
	/*
	*	consultarDescripcion
	*/
	public function consultarDescripcion(){
		$this->db->select('a.id,a.descripcion,a.estatus');
		$this->db->from('descripcion a');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
}	