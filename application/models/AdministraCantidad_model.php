<?php
	if (!defined('BASEPATH')) exit ('No direct script access allowed');

	class AdministraCantidad_model extends CI_Model{

		public function consultarProducos($data){
	
		$this->db->order_by('a.id');
		$this->db->where('a.id_idioma=',1);
		$this->db->where('a.estatus!=',2);
		$this->db->select('a.id,a.titulo');
		$this->db->from('detalle_productos a');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
		public function consultaExistente($datos){
			$this->db->where('a.id_det_prod',$datos['id_producto']);
			$this->db->where('a.id_color',$datos['id_color']);
			$this->db->where('a.id_talla',$datos['id_talla']);
			$this->db->where('a.estatus',1);
			$this->db->select('cantidad');
			$this->db->from('cantidad_producto a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
		public function TraeID($datos){
			$this->db->where('a.id_det_prod',$datos['id_det_prod']);
			$this->db->where('a.id_color',$datos['id_color']);
			$this->db->where('a.id_talla',$datos['id_talla']);
			$this->db->where('a.estatus',1);
			$this->db->select('id,id_det_prod');
			$this->db->from('cantidad_producto a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultaExistenteTodos($data){
			
			$this->db->order_by('a.id','DESC');
	        $this->db->where('a.estatus!=',2);
			$this->db->select('a.*,b.id as id_det_prod, b.titulo as titulo_prod,c.id as id_color, c.descripcion as color_prod,d.id as id_talla, d.descripcion as talla_prod');
			$this->db->from('cantidad_producto a');
			$this->db->join('detalle_productos b', 'b.id = a.id_det_prod');
			$this->db->join('colores c', 'c.id = a.id_color');
			$this->db->join('tallas d', 'd.id = a.id_talla');
			$res = $this->db->get();

			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function existeProducto($id_prod,$id_color,$id_talla){

			$this->db->where('a.id_det_prod', $id_prod	);
			$this->db->where('a.id_color', $id_color);
			$this->db->where('a.id_talla', $id_talla);
			$this->db->where('a.estatus',1);
			$this->db->select('a.*');
			$this->db->from('cantidad_producto a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function cambiarCantidad($data){
			$this->db->where('id', $data["id"]);
			$this->db->where('id_det_prod', $data["id_det_prod"]);
			$this->db->where('id_color', $data["id_color"]);
			$this->db->where('id_talla', $data["id_talla"]);
			$this->db->where('estatus','1');
	        if($this->db->update("cantidad_producto", $data)){
	        	return true;
	        }else{
	        	return false;
	        }
		}
		public function cambiarCantidadCarrito($data){
			$this->db->where('id', $data["id"]);
			$this->db->where('id_det_prod', $data["id_det_prod"]);
			$this->db->where('id_color', $data["id_color"]);
			$this->db->where('id_talla', $data["id_talla"]);
			$this->db->where('estatus','1');
	        if($this->db->update("inventario_carrito", $data)){
	        	return true;
	        }else{
	        	return false;
	        }
		}
		public function cambiarCantidadOrdenes($data){
			$this->db->where('id', $data["id"]);
			$this->db->where('id_det_prod', $data["id_det_prod"]);
			$this->db->where('id_color', $data["id_color"]);
			$this->db->where('id_talla', $data["id_talla"]);
			$this->db->where('estatus','1');
	        if($this->db->update("inventario_ordenes", $data)){
	        	return true;
	        }else{
	        	return false;
	        }
		}
		

		public function RegistroCantidad($data){
			//print_r ($data);die;
			if($this->db->insert("cantidad_producto",$data)){
				return true;
			}else{
				return false;
			}
		}

		public function RegistroCantidadCarrito($data){
			//print_r ($data);die;
			if($this->db->insert("inventario_carrito",$data)){
				return true;
			}else{
				return false;
			}
		}

		public function RegistroCantidadOrdenes($data){
			//print_r ($data);die;
			if($this->db->insert("inventario_ordenes",$data)){
				return true;
			}else{
				return false;
			}
		}
		

		public function modificarEstatusCantidad($data){
			$this->db->where('id', $data["id"]);
	        if($this->db->update("cantidad_producto", $data)){
	        	return true;
	        }else{
	        	return false;
	        }
		}
		public function modificarEstatusCarrito($data){
			$this->db->where('id', $data["id"]);
	        if($this->db->update("inventario_carrito", $data)){
	        	return true;
	        }else{
	        	return false;
	        }
		}
		public function modificarEstatusOrden($data){
			$this->db->where('id', $data["id"]);
	        if($this->db->update("inventario_ordenes", $data)){
	        	return true;
	        }else{
	        	return false;
	        }
		}
		public function existeColores($id,$descripcion){	
			$this->db->where('a.id!=', $id);
			$this->db->where('a.estatus!=', 2);
			$this->db->where('a.descripcion',$descripcion);
			$this->db->select('a.*');
			$this->db->from('colores a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
	}

?>
