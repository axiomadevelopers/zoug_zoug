<?php

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class WebNoticias_model extends CI_Model{
	public function consultarNoticias($datos){
		/*if($data["id_noticias"]!=""){
			$this->db->where('a.id', $data["id_noticias"]);
		}*/
		$this->db->order_by('a.id desc');
        $this->db->where('a.estatus!=',2);
        $this->db->where('a.estatus!=',0);
        $this->db->where('a.id_idioma',$datos["id_idioma"]);
		$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen,d.login as usuario');
		$this->db->from('seccion_noticias a');
		//$this->db->limit($data["limit"],$data["offset"]);
		$this->db->join('idioma b', 'b.id = a.id_idioma');
        $this->db->join('galeria c', 'c.id = a.id_imagen');
        $this->db->join('usuarios d', 'd.id = a.id_usuario');
		$res = $this->db->get();
		//print_r($res);die;
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	*	Metodo que consulta los card inferiores de noticias
	*/
	public function consultarSubNoticias($datos){
		

		$this->db->order_by('a.id desc');
		
		if($datos["start"]==''){
			$datos["start"] = 0;
		}

		if($datos["limit"]!=''){

	       $this->db->limit($datos["limit"], $datos["start"]);
	    }
	    
	    $this->db->where('a.id!=',$datos["id_noticias"]);
        $this->db->where('a.estatus!=',2);
        $this->db->where('a.id_idioma',$datos["id_idioma"]);
		$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen,d.login as usuario');
		$this->db->from('seccion_noticias a');
		//$this->db->limit($data["limit"],$data["offset"]);
		$this->db->join('idioma b', 'b.id = a.id_idioma');
        $this->db->join('galeria c', 'c.id = a.id_imagen');
        $this->db->join('usuarios d', 'd.id = a.id_usuario');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	*	Metodo que consulta  la noticia segun su slug...
	*/
	public function consultarNoticiasSlug($datos){
	    $this->db->where('a.slug=',$datos["id_noticias"]);
		$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen,d.login as usuario');
		$this->db->from('seccion_noticias a');
		//$this->db->limit($data["limit"],$data["offset"]);
		$this->db->join('idioma b', 'b.id = a.id_idioma');
        $this->db->join('galeria c', 'c.id = a.id_imagen');
        $this->db->join('usuarios d', 'd.id = a.id_usuario');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	
}
