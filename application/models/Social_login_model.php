<?php 

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class Social_login_model extends CI_Model{

	public function iniciar_sesion($login,$clave){
		$this->db->where('login',$login);
		$this->db->where('clave',$clave);
		$this->db->where('estatus','1');
		$this->db->select('*');
		$this->db->from(' usuarios u');
		return $this->db->count_all_results();
	}

	public function consultar_usuario($login,$clave){
		$this->db->where('login',$login);
		$this->db->where('clave',$clave);
		$this->db->where('u.estatus','1');
		$this->db->select('u.*,b.nombres_apellidos');
		$this->db->from(' usuarios u');
		$this->db->join('personas b', 'b.id = u.id_persona');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	public function existe($email){
		$this->db->where('u.correo',$email);
		$this->db->select('u.*');
		$this->db->from('social_login u');
		return $this->db->count_all_results();
	}
	public function existe_email($email){
		$this->db->where('u.correo',$email);
		$this->db->select('u.*');
		$this->db->from('social_login u');
		$res = $this->db->get();
		if($res){
			return $res->row_array();
		}else{
			return false;
		}
	}
	public function registro($data){
		//var_dump($data);die;
		if($this->db->insert("social_login",$data)){
			return true;
		}else{
			return false;
		}
	}
	public function buscarID($code){
		//var_dump($data);die;
		$this->db->where('codigo',$code);
		$this->db->where('estatus','2');
		$this->db->select('u.id');
		$this->db->from('social_login u');
		$res = $this->db->get();
		if($res){
			return $res->row_array();
		}else{
			return false;
		}
	}
	public function guardar_codigo($data){
		//var_dump($data);die;
		if($this->db->insert("cambio_contraseña",$data)){
			return true;
		}else{
			return false;
		}
	}
	
	public function identificar_usuario($code){
		//var_dump($data);die;
		$this->db->where('id_identificador',$code);
		$this->db->select('u.id_usuario');
		$this->db->from('cambio_contraseña u');
		$res = $this->db->get();
		
		if($res){
			return $res->row_array();
		}else{
			return false;
		}
	}
	public function buscarUser($correo){
		//var_dump($data);die;
		$this->db->where('correo',$correo);
		$this->db->where('estatus','1');
		$this->db->select('u.id,u.tp_login');
		$this->db->from('social_login u');
		$res = $this->db->get();
		if($res){
			return $res->row_array();
		}else{
			return false;
		}
	}
	public function codigo_contraseña($data,$codigo){
			$this->db->where('id_identificador', $codigo);
			if($this->db->update("cambio_contraseña", $data)){
				return true;
			}else{
				return false;
			}
		}
	
	public function login_social($nombre,$correo){
		$this->db->where('nombre_apellido',$nombre);
		$this->db->where('correo',$correo);
		$this->db->where('u.estatus','1');
		$this->db->select('u.*');
		$this->db->from(' social_login u');
		$res = $this->db->get();
		
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	public function inicioSesion($correo,$clave){
		$this->db->where('clave',$clave);
		$this->db->where('correo',$correo);
		$this->db->where('u.estatus','1');
		$this->db->select('u.*');
		$this->db->from('social_login u');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	public function buscarInactivo($correo,$clave){
		$this->db->where('clave',$clave);
		$this->db->where('correo',$correo);
		$this->db->where('u.estatus','2');
		$this->db->select('u.*');
		$this->db->from('social_login u');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	public function buscarCorreo($correo){
		$this->db->where('u.correo',$correo);
		$this->db->where('u.estatus','1');
		$this->db->select('u.*');
		$this->db->from('social_login u');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	public function identificar($id,$codigo){
		$this->db->where('id',$id);
		$this->db->where('codigo',$codigo);
		$this->db->where('u.estatus','2');
		$this->db->select('u.id,u.nombre_apellido,codigo,u.correo');
		$this->db->from(' social_login u');
		$res = $this->db->get();
		if($res){
			return $res->row_array();
		}else{
			return false;
		}
	}
	public function verActivacion($id,$codigo){
		$this->db->where('id',$id);
		$this->db->where('codigo',$codigo);
		$this->db->where('u.estatus','1');
		$this->db->select('u.id,u.nombre_apellido,codigo,u.correo');
		$this->db->from(' social_login u');
		$res = $this->db->get();
		//print_r($this->db->last_query());die;
		if($res){
			return $res->row_array();
		}else{
			return false;
		}
	}
	public function activar_user($data){
		$this->db->where('id', $data["id"]);
		if($this->db->update("social_login", $data)){
			return true;
		}else{
			return false;
		}
	}
	public function identificar_cambio($id,$codigo){
		$this->db->where('u.id_usuario',$id);
		$this->db->where('u.codigo',$codigo);
		$this->db->where('u.estatus','1');
		$this->db->select('u.id,b.*');
		$this->db->from(' cambio_contraseña u');
		$this->db->join('social_login b', 'b.id = u.id_usuario');

		$res = $this->db->get();
		
		if($res){
			return $res->row_array();
		}else{
			return false;
		}
	}
	public function buscar_peticion($id,$codigo){
		$this->db->where('u.id_usuario',$id);
		$this->db->where('u.codigo',$codigo);
		$this->db->where('u.estatus','1');
		$this->db->select('u.id_usuario,u.codigo as codigo_cambio,b.correo,b.nombre_apellido,b.codigo,u.id as id_tabla');
		$this->db->from('cambio_contraseña u');
		$this->db->join('social_login b', 'b.id = u.id_usuario');

		$res = $this->db->get();
		
		if($res){
			return $res->row_array();
		}else{
			return false;
		}
	}
	public function desactivar_cambio($data){
		$this->db->where('id', $data["id"]);
		$this->db->where('estatus','1');
		if($this->db->update("cambio_contraseña", $data)){
			return true;
		}else{
			return false;
		}
	}public function cambio_clave($data){
		$this->db->where('id', $data["id"]);
		if($this->db->update("social_login", $data)){
			return true;
		}else{
			return false;
		}
	}

}