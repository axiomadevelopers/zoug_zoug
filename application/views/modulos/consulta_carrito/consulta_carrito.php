<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid" ng-controller="ConsultaCarritoController">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center titulo-dashboard">
                <h3 class="text-themecolor">Productos</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Productos</li>
                    <li class="breadcrumb-item active">Carrito de compra</li>
                </ol>
            </div>
            <!--<div class="col-md-7 col-4 align-self-center">
                <div class="d-flex m-t-10 justify-content-end">
                    <div class="">
                        <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i
                                class="ti-settings text-white"></i></button>
                    </div>
                </div>
            </div>-->
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{{titulo_pagina}}</h4>
                        <div class="table-responsive m-t-40">
                            <form id="formConsultaNoticias" method="POST" target="_self">
                                <input type="hidden" id="id_noticias" name="id_noticias" ng-model="id_noticias">
                                <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                               
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th style="width:100px">#Carrito</th>
                                            <th>Acciones</th>
                                            <th style="width:400px;">Usuario</th>
                                            <th>Fecha</th>
                                            <th>Total</th>
                                            <th>Estatus</th>


                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="carritoCompra in carritoCompra track by $index">
                                            <td class="">{{carritoCompra.numero_carrito}}</td>
                                            <td class="centrado">
                                                <div class="form-group">
                                                    <div ng-click="ver_detalle($index)"
                                                        class="btn btn-danger flotar_izquierda2" title="Ver detalle"
                                                        style="float:left">
                                                        <i class="fa fa-file" aria-hidden="true"></i>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="">{{carritoCompra.usuario}}</td>
                                            <td class="">{{carritoCompra.fecha}}</td>
                                            <td class="float-right-carrito">{{carritoCompra.total}}</td>
                                            <td class="">{{carritoCompra.estatus}}</td>
                                            <!-- width="20%"  -->

                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!--Bloque modal del sistema -->
        <div class="modal fade" id="modal_mensaje" name="modal_mensaje" tabindex="-1" role="dialog"
            aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-md .modal-sm">
                <div class="modal-content">
                    <div class="modal-header header_conf">
                        <h2>
                            <p id="cabecera_mensaje" name="cabecera_mensaje">{{titulo_mensaje}}</p>

                        </h2>
                        <button type="button" id="cerrar_mensaje2" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body padding40" id="cuerpo_mensaje" name="cuerpo_mensaje">
                        <!--Modal body -->
                        <div ng-class="{visible:total.estatus=='Paso a orden',invisible:total.estatus!='Paso a orden'}"> 
                        <label class="label label-rounded label-success">{{total.estatus}}</label></div>
                        <div ng-class="{visible:total.estatus=='Compra pendente',invisible:total.estatus!='Compra pendente'}"> 
                        <label class="label label-rounded label-warning">{{total.estatus}}</label></div>

                        <div class="row padding40" ng-repeat="detalle in detalle track by $index">
                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <h3>
                                    Producto: {{detalle.producto}}
                                </h3>
                            </div>
                            <div class="mensaje_img_principal">
                                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                    <img class="img_noticias" id="img_seleccionada"
                                        ng-src="{{base_url}}{{detalle.ruta.ruta}}" height="115" data="">
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <div>
                                        <strong><label>Color:</label>
                                            <span class="float-right">{{detalle.color}}</span></strong>
                                    </div>
                                    <div>
                                        <strong><label>Talla:</label>
                                            <span class="float-right">{{detalle.talla}}</span></strong>
                                    </div>
                                    <div>
                                        <strong><label>Cantidad:</label>
                                            <span class="float-right">{{detalle.cantidad}}</span></strong>
                                    </div>
                                    <div>
                                        <strong><label>Monto Individual:</label>
                                            <span class="float-right">{{detalle.monto}}</span></strong>
                                    </div>
                                    <div>
                                        <strong>
                                                <label>Monto Total:</label>
                                            <span class="float-right">{{detalle.monto_total}}</span></strong>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6">
                                <div class="form-group">
                                    <label>Monto Individual:</label>
                                    <span class="float-right">{{detalle.monto}}</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <label>Monto Total:</label>
                                    <span class="float-right">{{detalle.monto_total}}</span>
                                </div>
                            </div> -->
                        </div>
                        <div class="">
                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <div class="float-right">
                                        <strong> <label>Total a pagar: </label>
                                            <span>{{total.total}}</span></strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- modal body-->
                    </div>
                    <div class="modal-footer footer_conf">
                        <!-- Footter del modal -->
                        <button type="button" name="modal_reporte_salir" id="modal_reporte_salir" class="btn btn-danger"
                            data-dismiss="modal">Cerrar</button>
                        <!-- Fin footter del modal -->
                    </div>
                </div>
            </div>
        </div>
        <!-- -->
    </div>
</div>