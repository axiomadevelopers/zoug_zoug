3<div class="page-wrapper">
	<div class="container-fluid" ng-controller="Detalle_prodController">
		<div class="row page-titles">
			<div class="col-md-5 col-8 align-self-center titulo-dashboard">
				<h3 class="text-themecolor">Productos</h3>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
					<li class="breadcrumb-item active">Productos</li>
					<li class="breadcrumb-item active">Detalle de Productos</li>
				</ol>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">{{titulo_pagina}}</h4>
						<h6 class="card-subtitle">{{subtitulo_pagina}}</h6>
						<ul class="nav nav-tabs customtab2" role="tablist">
							<li class="nav-item" ng-class="{'active': currentTab === 'datos_basicos'}"
								ng-click="currentTab = 'datos_basicos';">
								<a class="nav-link active" data-toggle="tab" href="ui-tab.html#home7" role="tab">
									<span class="hidden-sm-up">
										<i class="ti-pencil-alt"></i>
									</span>
									<span class="hidden-xs-down">Datos Básicos</span>
								</a>
							</li>
							<li class="nav-item" ng-class="{'active': currentTab === 'imagenes'}"
								ng-click="currentTab = 'imagenes'">
								<a class="nav-link" data-toggle="tab" href="ui-tab.html#profile7" role="tab">
									<span class="hidden-sm-up">
										<i class="ti-image"></i>
									</span>
									<span class="hidden-xs-down">Imagenes</span></a>
							</li>
						</ul>
						<form class="form-material m-t-40" name="formularioDetalleprod" id="formularioDetalleprod">
							<div class="tab-content">
								<div class="tab-content">
									<div id="datos_basicos" class="tab-pane active" role="tabpanel"
										ng-class="{'active':currentTab === 'datos_basicos'}">
										<div class="row p-20">
											<div class="col-lg-4 col-md-6 col-xs-12 col-sm-12 padding0_min "
												style="padding-top: 5px; ">
												<div class="form-group">
													<div class='asterisco_rojo'><i class='fa fa-asterisk'
															aria-hidden='true'></i></div>
													<label class="">Idioma:</label>
													<!-- -->
													<select name="idioma" id="idioma"
														class="form-control m-bot15 select-picker" data-done-button="true"
														data-actions-box="true" data-style="btn-fff "
														data-live-search="true" data-size="3" disabled>
													</select>
													<!--  -->
													<!-- 
														ng-change="cargar_catprod_idioma(); cargar_marca_idioma(); cargar_color_idioma();"ng-options="option.descripcion for option in idioma track by option.id"-->
												</div>		
											</div>
										</div>
										<div class="row p-20">
											<div class="col-lg-4 col-md-6 col-xs-12 col-sm-12 padding0_min "
												style="padding-top: 5px; ">
												<div class="form-group">
													<div class='asterisco_rojo'><i class='fa fa-asterisk'
															aria-hidden='true'></i></div>
													<label class="">Línea del Producto:</label>
													<select name="marca" id="marca"
														class="form-control m-bot15 select-picker" data-done-button="true"
														data-actions-box="true"
														data-live-search-placeholder="Seleccione una categoría"
														data-style="btn-fff " data-live-search="true"
														ng-model="detalle_prod.marca" data-size="3">
														<option value="">--Seleccione una Línea--</option>
													</select>
													<!-- ng-options="option.titulo for option in marca track by option.id" -->
												</div>	
											</div>
											<div class="col-lg-4 col-md-6 col-xs-12 col-sm-12 padding0_min "
												style="padding-top: 5px; ">
												<div class="form-group">
													<div class='asterisco_rojo'><i class='fa fa-asterisk'
															aria-hidden='true'></i></div>
													<label class="">Género:</label>
													<select name="categoria_prod" id="categoria_prod"
														class="form-control m-bot15 select-picker" data-done-button="true"
														data-actions-box="true"
														data-live-search-placeholder="Seleccione una categoría"
														data-style="btn-fff " data-live-search="true"
														ng-model="detalle_prod.categoria_prod" data-size="3">
														<option value="">--Seleccione un Género--</option>
													</select>
													<!--ng-options="option.titulo for option in catprod track by option.id"-->
												</div>	
											</div>
											<div class="col-lg-3 col-md-6 col-xs-12 col-sm-12 padding0_min "
												style="padding-top: 5px; ">
												<div class="form-group">
													<div class='asterisco_rojo'><i class='fa fa-asterisk'
															aria-hidden='true'></i></div>
													<label class="">Tipo del Producto:</label>
													<select name="tipo_producto" id="tipo_producto"
														class="form-control m-bot15 select-picker" data-done-button="true"
														data-actions-box="true" data-style="btn-fff "
														data-live-search="true" ng-model="detalle_prod.tipo_prod"
														data-size="3">
														<option value="">--Seleccione un Género--</option>
													</select>
													<!-- ng-options="option.titulo for option in detprod track by option.id"  -->
												</div>	
											</div>
										</div>
										<div class="row p-20">
											<!-- <div class="col-lg-3 col-md-6 col-xs-12 col-sm-12 padding0_min " style="padding-top: 5px; ">
												<div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
												<label class="">Color:</label>
												<select name="color" id="color" class="form-control m-bot15 select-picker" data-done-button="true" data-actions-box="true"  data-live-search-placeholder="Seleccione un Producto" data-style="btn-fff " data-live-search="true"  ng-model="detalle_prod.color" data-size="3">
													<option value="">--Seleccione un Color--</option>
												</select>
											
											</div>
											<div class="col-lg-3 col-md-6 col-xs-12 col-sm-12 padding0_min " style="padding-top: 5px; ">
												<div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
												<label class="">Talla:</label>
												<select name="talla" id="talla" class="form-control m-bot15 select-picker" data-done-button="true" data-actions-box="true"  data-live-search-placeholder="Seleccione una talla" placholder="Seleccione un Producto" data-style="btn-fff " data-live-search="true"  ng-model="detalle_prod.talla" data-size="3">
													<option value="">--Seleccione una talla--</option>
												</select>
											
											</div> -->
											<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 ">
												<div class="form-group">
													<div class='asterisco_rojo'><i class='fa fa-asterisk'
															aria-hidden='true'></i></div>

													<label>Código: <span class="help"></span></label>
													<input name="codigo" id="codigo" type="text"
														class="form-control form-control-line"
														placeholder="Ingrese el código del Producto"
														ng-model="detalle_prod.codigo"
														onKeyPress="return valida(event,this,2,6)"
														onBlur="valida2(this,2,6);" required>
												</div>		
											</div>
											<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 ">
												<div class="form-group">
													<label>Título: <span class="help"></span></label>
													<input name="titulo" id="titulo" type="text"
														class="form-control form-control-line"
														placeholder="Ingrese el título del tipo de Producto"
														ng-model="detalle_prod.titulo" required
														onKeyPress="return valida(event,this,3,12)"
														onBlur="valida2(this,3,12);">
												</div>		
											</div>
											<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 ">
												<div class="form-group">
													<div class='asterisco_rojo'><i class='fa fa-asterisk'
															aria-hidden='true'></i></div>
													<label>Precio: <span class="help"></span></label>
													<!-- <input name="precio" id="precio" type="text"   class="form-control form-control-line" placeholder="Ingrese el Precio"  ng-model="detalle_prod.precio"  required> -->
													<input name="precio" id="precio" type="text"
														class="form-control form-control-line"
														data-bts-button-down-class="btn btn-secondary btn-outline"
														data-bts-button-up-class="btn btn-secondary btn-outline"
														ng-model="detalle_prod.precio" required>
												</div>		
											</div>
											<!-- <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 ">
												<label>Cantidad: <span class="help"></span></label>
												<input name="cantidad" id="cantidad" type="text" class="form-control form-control-line" placeholder="Ingrese Cantidad"  ng-model="detalle_prod.cantidad"  required>
											</div> -->
										</div>
										<div class="row p-20">

											<div class="col-lg-8 col-md-6 col-sm-12 col-xs-12">
												<div class="form-group">
													<div class='asterisco_rojo'><i class='fa fa-asterisk'
															aria-hidden='true'></i></div>
													<label>Detalles del Producto:</label>
													<div class="div_wisig" id="div_descripcion" name="div_descripcion"
														data-toggle="modal" data-target="#wisiModal" data-whatever="@mdo"
														ng-click="wisi_modal('1')">
														Pulse aquí para ingresas el contenido
													</div>
												</div>	
											</div>
										</div>
									</div>

									<div class="tab-pane p-20" id="imagenes" role="tabpanel"
										ng-class="{'active':currentTab === 'imagenes'}">
										<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
											<div class="img_galeria" ng-click="seleccione_img_marcas()">
												<div ng-class="{'invisible':activo_img_soportes==='activo','visible':activo_img_soportes==='inactivo'}"
													style="line-height:250px;">
													<span><i class="fas fa-image" aria-hidden="true"></i> Imagenes
														Productos </span>
												</div>
												<div id="masonrry" padding="0px;" class="">
													<div class="mensaje_img_principal grid"
														ng-class="{'visible':activo_img_soportes==='activo','invisible':activo_img_soportes==='inactivo'}"
														id="cuerpo_img_soportes">
														<div class="row">
															<div class="col-lg-4"
																ng-repeat="imag in galeria_marca track by $index">
																<img class="img_soportes" id="img_seleccionada"
																	ng-src="{{base_url}}{{imag}}" data="">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row button-group">
								<div class="col-lg-6">
									<div class="row">
										<div id="div_mensaje"></div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="row">
										<div class="col-lg-4 col-md-4" ng-if="id_detalle_prod!=''">
											<a href="<?=base_url();?>cms/detalle_prod">
												<button id="btn-nuevo" type="button"
													class="btn waves-effect waves-light btn-block btn-success">Nuevo</button>
											</a>
										</div>
										<div class="col-lg-4 col-md-4" ng-if="id_detalle_prod==''">
											<button id="btn-limpiar" type="button" id="btn-limpiar"
												class="btn waves-effect waves-light btn-block btn-success"
												ng-click="limpiar_cajas_detProd()">Limpiar</button>
										</div>

										<div class="col-lg-4 col-md-4">
											<a href="<?=base_url();?>cms/detalle_prod/consultar_detProd">
												<button id="btn-consultar" type="button"
													class="btn waves-effect waves-light btn-block btn-danger">{{titulo_cons}}</button>
											</a>
										</div>
										<div class="col-lg-4 col-md-4">
											<button id="btn-registrar" type="button" id="btn-registrar"
												class="btn waves-effect waves-light btn-block btn-info"
												ng-click="registrardetProd()">{{titulo_registrar}}</button>
										</div>
										<input type="hidden" name="id_detalle_prod" id="id_detalle_prod"
											value="<?php if(isset($id)){echo $id;}?>">
										<input type="hidden" name="clonar" id="clonar"
											value="<?php if(isset($clonar)){echo $clonar;}?>">

										<input type="hidden" name="base_url" id="base_url"
											value="<?php echo base_url(); ?>">
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="wisiModal" name="wisiModal" tabindex="-1" role="dialog"
			aria-labelledby="exampleModalLabel1">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="exampleModalLabel1">Ingrese el contenido</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">&times;</span></button>
					</div>
					<div class="modal-body">
						<form>
							<div class="card-body">
								<form method="post">
									<div class="form-group">
										<textarea id="textarea_editor" name="textarea_editor"
											class="textarea_editor form-control" rows="15"
											placeholder="Ingrese texto..."></textarea>
									</div>
								</form>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button id="cerrarModal" type="button" class="btn btn-danger"
							data-dismiss="modal">Cerrar</button>
						<button type="button" class="btn btn-primary" ng-click="agregarWisi()">Agregar</button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="modal_img2" name="modal_img2" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
			<div class="modal-dialog modal-lg .modal-sm">
				<div class="modal-content">
					<div class="modal-header header_conf">
						<p id="cabecera_mensaje" name="cabecera_mensaje"><b>Información:</b> Seleccione imagen productos
						</p>
						<button type="button" id="cerrar_mensaje2" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body" id="cuerpo_mensaje" name="cuerpo_mensaje">
						<div class="centrar_galeria" ng-show="galeria_m.length ==0">No se han cargado imagenes</div>
						<div id="cuerpo-img-galeria" ng-show="galeria_m.length!=0" class="fade-in-out">
							<div class="form-group">
								<input type='text' name='filtro_soportes' id='filtro_soportes'
									placeholder='Ingrese el valor a filtrar' class="form-control"
									ng-model="searchSoportes" ng-click="limpiar_arreglos()">
							</div>
							<div>
								<div class="col-lg-12" padding="0px;" class="img_galeria_soportes">
									<div id="masonrry2" padding="0px;">
										<div class="row">
											<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 divbiblioteca"
												ng-repeat="imagen in galeria_m | filter : searchSoportes track by $index">
												<div class="grid-item3">
													<img class="imgbiblioteca" id="img_soporte{{$index}}"
														ng-src="{{base_url}}/{{imagen.ruta}}" height="115"
														data-ng-click="seleccionar_imagen_soportes($event)"
														data="{{imagen.id}}|{{imagen.ruta}}">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div style="clear:both"></div>
					</div>
					<div class="modal-footer footer_conf">
						<button type="button" name="modal_reporte_salir" id="modal_reporte_salir"
							class="btn btn-primary" data-dismiss="modal">Aceptar</button>
					</div>
					<div style="clear:both"></div>
				</div>
			</div>
		</div>
	</div>
</div>