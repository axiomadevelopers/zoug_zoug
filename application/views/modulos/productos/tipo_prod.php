<div class="page-wrapper">
	<div class="container-fluid" ng-controller="Tipo_prodController">
		<div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center titulo-dashboard">
                <h3 class="text-themecolor">Productos</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Productos</li>
                    <li class="breadcrumb-item active">Tipo de Productos</li>
                </ol>
            </div>
        </div>

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">{{titulo_pagina}}</h4>
						<h6 class="card-subtitle">{{subtitulo_pagina}}</h6>
						<hr>
                        <form class="form-material m-t-40" name="formularioTipoprod" id="formularioTipoprod">
							<div class="row p-20">
								<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 padding0_min " style="padding-top: 5px; ">
                                    <div class="form-group">
    									<div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
    									<label class="">Idioma:</label>
    									<select name="idioma" id="idioma" class="form-control m-bot15 select-picker" data-done-button="true" data-actions-box="true"  data-live-search-placeholder="Seleccione tipo de idioma" placholder="Seleccione tipo de idioma" data-style="btn-fff " data-live-search="true" ng-options="option.descripcion for option in idioma track by option.id" ng-model="tipo_prod.id_idioma" data-size="3" ng-change="cargar_catprod_idioma();">
    										<option value="">--Seleccione un idioma--</option>
    									</select>
                                    </div>    
								</div>
								<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 padding0_min " style="display:none;padding-top: 5px; ">
                                    <div class="form-group">
    									<div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
    									<label class="">Género:</label>
    									<select name="categoria_prod" id="categoria_prod" class="form-control m-bot15 select-picker" data-done-button="true" data-actions-box="true"  data-live-search-placeholder="Seleccione una categoría" placholder="Seleccione una categoría" data-style="btn-fff " data-live-search="true" ng-options="option.titulo for option in catprod track by option.id" ng-model="tipo_prod.categoria_prod" data-size="3">
    										<option value="">--Seleccione un Género--</option>
    									</select>
                                    </div>    
								</div>
							</div>
							<div class="row p-20">
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                                    <div class="form-group">
                                        <div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
    									<label>Título: <span class="help"></span></label>
    									<input name="titulo" id="titulo" type="text" class="form-control form-control-line" placeholder="Ingrese el título"  ng-model="tipo_prod.titulo"  required>
                                    </div>        
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="form-group">
                                        <div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
    									<label>Descripción:</label>
    									<div class="div_wisig" id="div_descripcion" name="div_descripcion" data-toggle="modal" data-target="#wisiModal" data-whatever="@mdo" ng-click="wisi_modal('1')">
    										Pulse aquí para ingresas el contenido
    									</div>
                                     </div>   
								</div>
							</div>
							<div class="row button-group">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div id="div_mensaje"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4" ng-if="id_tipo_prod!=''">
                                            <a href="<?=base_url();?>cms/tipo_prod">
                                                <button id="btn-nuevo" type="button" class="btn waves-effect waves-light btn-block btn-success">Nuevo</button>
                                            </a>
                                        </div>
                                        <div class="col-lg-4 col-md-4" ng-if="id_tipo_prod==''">
                                            <button id="btn-limpiar" type="button" class="btn waves-effect waves-light btn-block btn-success" ng-click="limpiar_cajas_tipoProd()">Limpiar</button>
                                        </div>

                                        <div class="col-lg-4 col-md-4">
                                            <a href="<?=base_url();?>cms/tipo_prod/consultar_tipoProd">
                                                <button id="btn-consultar" type="button" class="btn waves-effect waves-light btn-block btn-danger" >{{titulo_cons}}</button>
                                            </a>
                                        </div>
                                        <div class="col-lg-4 col-md-4">
                                                <button id="btn-registrar" type="button" class="btn waves-effect waves-light btn-block btn-info" ng-click="registrarTipoProd()">{{titulo_registrar}}</button>
                                        </div>
                                        <input type="hidden" name="id_tipo_prod" id="id_tipo_prod" value="<?php if(isset($id)){echo $id;}?>">
                                        <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                                    </div>
                                </div>
                            </div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="wisiModal" name="wisiModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel1">Ingrese el contenido</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="card-body">
                                <form method="post">
                                    <div class="form-group">
											<textarea id="textarea_editor" name="textarea_editor" class="textarea_editor form-control" rows="15" placeholder="Ingrese texto..."></textarea>
                                    </div>
                                </form>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="cerrarModal" type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" ng-click="agregarWisi()">Agregar</button>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>
