<div class="page-wrapper">
	<div class="container-fluid" ng-controller="detalleProd_consultaController">
		<div class="row page-titles">
			<div class="col-md-6 col-8 align-self-center titulo-dashboard">
				<h3 class="text-themecolor">Productos</h3>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
					<li class="breadcrumb-item active">Productos</li>
					<li class="breadcrumb-item active">Detalle de Producto</li>
					<li class="breadcrumb-item active">Consultar</li>
				</ol>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">{{titulo_pagina}}</h4>
						<div class="table-responsive m-t-40">
							<form id="formConsultadetalleProd" method="POST" target="_self">
								<input type="hidden" id="id_detalle_prod" name="id_detalle_prod" ng-model="id_detalle_prod" >
								<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
								<div class="col-lg-12 mg-b30">
									<a href="{{base_url}}cms/detalle_prod">
										<div class="btn btn-success btn-new" >Nuevo</div>
									</a>
								</div>
								<table id="myTable" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>#</th>
											
											<th>Acciones</th>
											<th>Idioma</th>
											<th>Categoría Producto</th>
											<th>Tipo de Producto</th>
											<th>Marca</th>
											<th>Título</th>
											<th>Descripción</th>
											<!-- <th>Color</th>
											<th>Talla</th> -->
											<th>Precio</th>
											<th>Código</th>
											<!-- <th>Cantidad</th> -->
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat = "detalle_prod in detalle_prod track by $index">
											<td class="">{{detalle_prod.id}}</td>
											<td class="centrado">
												<div class="form-group flotar_izquierda">
													<div id="ver{{$index}}" ng-click="ver_detalleprod($index)" data="{{detalle_prod.id}}" class="btn btn-primary flotar_izquierda2" title="modificar">
														<i class="fas fa-pencil-alt" aria-hidden="true"></i>
													</div>
													<div id="clonar{{$index}}" ng-click="ver_detalleprodClonar($index)" data="{{detalle_prod.id}}|{{detalle_prod.clonado}}" class="btn btn-info flotar_izquierda2" title="Clonar" ng-class="{visible:detalle_prod.id_idioma=='1',invisible:detalle_prod.id_idioma=='2'}">
														<i class="fa fa-clone" aria-hidden="true"></i>
													</div>
													<div id="btn_activar{{$index}}_a" name="btn_activar{{$index}}_a" class="btn btn-warning flotar_izquierda2" ng-class="{visible:detalle_prod.estatus=='1',invisible:detalle_prod.estatus!=1}" title="Inactivar" data-ng-click="activar_registro($event)" data="{{detalle_prod.id}}|{{detalle_prod.estatus}}">
													<i class="fa fa-lock" aria-hidden="true"></i>
													</div>
													<div id="btn_activar{{$index}}_b" name="btn_activar{{$index}}_b"  class="btn btn-success flotar_izquierda2" ng-class="{visible:detalle_prod.estatus!='1',invisible:detalle_prod.estatus==1}" title="Publicar" data-ng-click="activar_registro($event)" data="{{detalle_prod.id}}|{{detalle_prod.estatus}}">
													<i class="fa fa-check" aria-hidden="true"></i>
													</div>
													<div id="btn_eliminar{{$index}}" name="btn_eliminar{{$index}}"  class="btn btn-danger flotar_izquierda2" title="Eliminar" data-ng-click="eliminar_registro($event)" data="{{detalle_prod.id}}|2">
													<i class="fa fa-window-close" aria-hidden="true"></i>
													</div>
												</div>
											</td>
											<td class="">{{detalle_prod.descripcion_idioma}}</td>
											<td class="">{{detalle_prod.titulo_catprod}}</td>
											<td class="">{{detalle_prod.titulo_tipoprod}}</td>
											<td class="">{{detalle_prod.titulo_marca}}</td>
											<td class="">{{detalle_prod.titulo}}</td>
											<td class="">{{detalle_prod.descripcion_sin_html}}</td>
											<!-- <td class="">{{detalle_prod.descripcion_color}}</td>
											<td class="">{{detalle_prod.descripcion_talla}}</td> -->
											<td class="">{{detalle_prod.precio}}</td>
											<td class="">{{detalle_prod.codigo}}</td>

											<!-- <td class="">{{detalle_prod.cantidad}}</td> -->
											
									</tbody>
								</table>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
