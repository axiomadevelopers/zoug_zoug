<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">Configuración</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Configuración</li>
                     <li class="breadcrumb-item active">Categorías</li>
                </ol>
            </div>
            <div class="col-md-7 col-4 align-self-center">
                <div class="d-flex m-t-10 justify-content-end">
                    <div class="">
                        <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i
                                class="ti-settings text-white"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== --> 
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Categorías</h4>
                        <h6 class="card-subtitle"></h6>
                        <form class="form-material m-t-40">
                            <div class="form-group">
                                <label>Descripción <span class="help"></span></label>
                                <input type="text" class="form-control form-control-line" placeholder="Ingrese la descripción de la categoría"> 
                            </div>
                            <div class="row button-group">
                                <div class="col-lg-6">
                                    <div class="row">
                                    </div>
                                </div>    
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4">
                                            <button type="button" class="btn waves-effect waves-light btn-block btn-success">Limpiar</button>
                                        </div>
                                        <div class="col-lg-4 col-md-4">
                                            <button type="button" class="btn waves-effect waves-light btn-block btn-warning">Consultar</button>
                                        </div>
                                        <div class="col-lg-4 col-md-4">
                                            <button type="button" class="btn waves-effect waves-light btn-block btn-info">Registrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                  
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
</div>        