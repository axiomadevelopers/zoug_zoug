<div class="page-wrapper">
	<div class="container-fluid" ng-controller="AdministracionCantidadController">
		<div class="row page-titles">
			<div class="col-md-7 col-8 align-self-center titulo-dashboard">
				<h3 class="text-themecolor">Productos</h3>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
					<li class="breadcrumb-item active">Productos</li>
					<li class="breadcrumb-item active">Administración de cantidades de producto</li>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">{{titulo_pagina}}</h4>
						<h6 class="card-subtitle">{{subtitulo_pagina}}</h6>
						<hr>
						<form class="form-material m-t-40" name="formularioMarca" id="formularioMarca">
							<div class="row p-20">
								<div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 padding0_min "
									style="padding-top: 5px; ">
									<div class="form-group">
										<div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
										<label class="">Productos:</label>
										<select name="producto" id="producto"
											class="form-control m-bot15 select-picker" data-done-button="true"
											data-actions-box="true" data-live-search-placeholder="Seleccione tipo de idioma"
											placholder="Seleccione tipo de idioma" data-style="btn-fff "
											data-live-search="true"
											ng-options="option.titulo for option in productos track by option.id"
											ng-model="administra.productos" data-size="3" ng-click="TraerExistentes()">
											<option value="">--Seleccione un Producto--</option>
										</select>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 padding0_min "
									style="padding-top: 5px; ">
									<div class="form-group">
										<div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
										<label class="">Color:</label>
										<select name="color" id="color" class="form-control m-bot15 select-picker"
											data-done-button="true" data-actions-box="true"
											data-live-search-placeholder="Seleccione tipo de idioma"
											placholder="Seleccione tipo de idioma" data-style="btn-fff "
											data-live-search="true"
											ng-options="option.descripcion for option in color track by option.id"
											ng-model="administra.color" data-size="3" ng-click="TraerExistentes()">
											<option value="">--Seleccione una color--</option>
										</select>
									</div>	
								</div>
								<div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 padding0_min "
									style="padding-top: 5px; ">
									<div class="form-group">
										<div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
										<label class="">Talla:</label>
										<select name="talla" id="talla" class="form-control m-bot15 select-picker"
											data-done-button="true" data-actions-box="true"
											data-live-search-placeholder="Seleccione tipo de idioma"
											placholder="Seleccione tipo de idioma" data-style="btn-fff "
											data-live-search="true"
											ng-options="option.descripcion for option in talla track by option.id"
											ng-model="administra.talla" data-size="3" ng-click="TraerExistentes()">
											<option value="">--Seleccione un color--</option>
										</select>
									</div>	
								</div>

							</div>
							<div class="row p-20">
								<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 padding0_min "
									style="padding-top: 5px; ">
									<div class="form-group">
										<div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
										<label class="">Cantidad:</label>
										<input type="text" name="titulo" id="titulo" class="form-control form-control-line"
											ng-model="administra.cantidad" placeholder="Ingrese la cantidad del producto"
											onKeyPress="return valida(event,this,13,100)" onBlur="valida2(this,13,100);">
									</div>


								</div>
								<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 padding0_min "
									style="padding-top: 5px; ">
									<div class="form-group">
										<div class=''><i class='' aria-hidden='true'></i></div>
										<label class="">Existente:</label>
										<input type="text" name="existente" id="existente"
											class="form-control form-control-line" ng-model="administra.existente"
											placeholder="Número de existentes" onKeyPress="return valida(event,this,10,100)"
											onBlur="valida2(this,10,100);">
									</div>
								</div>

							</div>



					<div class="row button-group">
						<div class="col-lg-6">
							<div class="row">
								<div id="div_mensaje"></div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="row">
								<div class="col-lg-4 col-md-4">
									<button id="btn-nuevo" type="button"
										class="btn waves-effect waves-light btn-block btn-success"
										ng-click="limpiar_cantidad()">Limpiar</button>
								</div>

								<div class="col-lg-4 col-md-4">
									<a href="<?=base_url();?>cms/administra_cantidad/consultar">
										<button id="btn-consultar" type="button"
											class="btn waves-effect waves-light btn-block btn-danger">Consultar</button>
									</a>
								</div>
								<div class="col-lg-4 col-md-4">
									<button id="btn-registrar" type="button"
										class="btn waves-effect waves-light btn-block btn-info"
										ng-click="registrar()">{{titulo_registrar}}</button>
								</div>
								<input type="hidden" name="id_color" id="id_color"
									value="<?php if(isset($id)){echo $id;}?>">
								<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
</div>