<div class="page-wrapper">
	<div class="container-fluid" ng-controller="AdmCantidadConsulta">
		<div class="row page-titles">
			<div class="col-md-7 col-8 align-self-center titulo-dashboard">
				<h3 class="text-themecolor">Productos</h3>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
					<li class="breadcrumb-item active">Productos</li>
					<li class="breadcrumb-item active">Administración de cantidades de producto</li>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">{{titulo_pagina}}</h4>
						<div class="table-responsive m-t-40">
                            <form id="formConsultaColores" name="formConsultaColores" method="POST" target="_self">
                                <input type="hidden" id="id_color" name="id_color" ng-model="id_color" >
                                 <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                                <div class="col-lg-12 mg-b30">
                                    <a href="{{base_url}}cms/administra_cantidad/">
                                        <div class="btn btn-success btn-new">Nuevo</div>
                                    </a>
                                </div>
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Acciones</th>
                                            <th>Producto</th>
                                            <th>Color</th>
                                            <th>Talla</th>
                                            <th>Cantidad</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat = "inventario in inventario track by $index">
                                            <td class="">{{inventario.id}}</td>
                                            <td class="centrado">
                                                <div class="form-group flotar_izquierda">
                                                    <div id="btn_activar{{$index}}_a" name="btn_activar{{$index}}_a" class="btn btn-warning flotar_izquierda2" ng-class="{visible:inventario.estatus=='1',invisible:inventario.estatus!=1}" title="Inactivar" data-ng-click="activar_registro($event)" data="{{inventario.id}}|{{inventario.estatus}}">
                                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                                    </div> 
                                                    <div id="btn_activar{{$index}}_b" name="btn_activar{{$index}}_b"  class="btn btn-success flotar_izquierda2" ng-class="{visible:inventario.estatus!='1',invisible:inventario.estatus==1}" title="Publicar" data-ng-click="activar_registro($event)" data="{{inventario.id}}|{{inventario.estatus}}">
                                                    <i class="fa fa-check" aria-hidden="true"></i>
                                                    </div>
                                                    <div id="btn_eliminar{{$index}}" name="btn_eliminar{{$index}}"  class="btn btn-danger flotar_izquierda2" title="Eliminar" data-ng-click="eliminar_registro($event)" data="{{inventario.id}}|2">
                                                    <i class="fa fa-window-close" aria-hidden="true"></i>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="">{{inventario.titulo_prod}}</td>
                                            <td class="">{{inventario.color_prod}}</td>
                                            <td class="">{{inventario.talla_prod}}</td>
                                            <td class="">{{inventario.cantidad}}</td>
                                            
                                    </tbody>
                                </table>
                            </form>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
