<!-- -->
<div id="cuerpoProductos" ng-controller="privacyController">
    <!--PARALLAX-->
    <div id="prlx_como_funciona" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
        <div class="super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after" style="height: 130%; background-image: url(<?=base_url();?>assets/web/img/parallax/parallax9.jpg);"></div>

        <div class="container g-pt-100 g-pb-70">
          <div class="row">
            <div class="col-sm-6 col-lg-6 align-items-end mt-auto g-mb-50 texto_parallax">
              <div class="text-center">
                <h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1" style="color:#fff">PRIVACY POLICY</h1>
              </div>
            </div>
          </div>
        </div>
    </div>
    <!--Privacy-->
    <section id="" class="mainContent clearfix aboutUsInfo">
        <div class="container">
            <div class="row fadeInUp wow">
                <div class="col-lg-12 text-center">
                    
                    <div class="contenido-privacy">
                        <div>
                            <h3 class="titulo-privacy"><strong>Privacy Policy</strong></h3><br>
                            <p style="text-align: justify; ">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus risus quam, semper facilisis ligula vel, ornare eleifend elit. Mauris interdum velit eu pharetra pretium. Phasellus libero mauris, mollis vitae commodo vel, convallis nec quam</p><p><strong><br></strong></p>
                            <h3 class="titulo-privacy" ><strong>What information do we collect?</strong></h3><br>
                            <p style="text-align: justify"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus risus quam, semper facilisis ligula vel, ornare eleifend elit.</p><p><strong><br></strong></p>
                            <h3 class="titulo-privacy"><strong>What do we use your information?</strong></h3><br> 
                            <div style="text-align: justify;">
                                <p style="text-align: justify; ">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p><br>
                               
                            </div>
                            <br>
                            <h3 class="titulo-privacy"><strong>How do we protect your information?</strong></h3><br><p style="text-align: justify;">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.&nbsp;</p><br>
                            <h3 class="titulo-privacy"><strong>Do we use ‘cookies’?</strong></h3><p><strong><br></strong></p><p style="text-align: justify; ">Nulla facilisi. Fusce non sodales ipsum. Mauris malesuada posuere luctus. Etiam feugiat nisl eget felis consequat feugiat.&nbsp;</p><br><h3 class="titulo-privacy"><strong>Third-party disclosure</strong></h3><p><strong><br></strong></p><p style="text-align: justify">Donec eu sapien est. Fusce eget felis nisi. Donec faucibus magna vel urna lacinia ultricies.</p><br>
                            <h3 class="titulo-privacy"><strong>Third-party links</strong></h3><p><strong><br></strong></p><p style="text-align: justify">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p><p><strong><br></strong></p>
                            <h3 class="titulo-privacy"><strong>Children’s Online Privacy Protection Act Compliance</strong></h3><p><strong><br></strong></p><p style="text-align: justify;">Proin convallis orci vel ligula fermentum porttitor. Quisque volutpat imperdiet ex, in ultrices velit laoreet sed. Proin ut convallis quam.</p><p><strong><br><br></strong></p>
                            <h3 class="titulo-privacy"><strong>Terms &amp; Conditions</strong></h3><p><strong><br></strong></p><p  style="text-align: justify">Maecenas fringilla sapien erat, id lobortis risus cursus eget. Sed at imperdiet risus&nbsp;</p><p><strong><br></strong></p>
                            <h3 class="titulo-privacy"><strong>Your Consent</strong></h3><p><strong><br></strong></p><p  style="text-align: justify">Quisque volutpat imperdiet ex, in ultrices velit laoreet sed.</p><br>
                            <h3 class="titulo-privacy"><strong>Changes to This Privacy Policy</strong></h3><p><strong><br></strong></p><p style="text-align: justify" >Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla facilisi. Fusce non sodales ipsum. Mauris malesuada posuere luctus. Etiam feugiat nisl eget felis consequat feugiat.</p><br><p style="text-align: justify;">If we make any changes to this Privacy Policy, we will post those changes on this page.</p><p><strong><br></strong></p>
                            <h3 class="titulo-privacy"><strong>Contact Us</strong></h3><p><strong><br></strong></p><p style="text-align: justify">Donec eu sapien est. Fusce eget felis nisi. Donec faucibus magna vel urna lacinia ultricies.</p><br><p style="text-align: justify;"> Pellentesque habitant .&nbsp;</p><p style="text-align: justify;">Quisque volutpat imperdiet ex, in ultrices velit laoreet sed.</p><p style="text-align: justify;">Ultrices velit laoreet sed</p><p style="text-align: justify;">contacto@zougzoug.com</p><p style="text-align: justify;">+58 999 999 99 99 &nbsp;</p><br>
                        </div>    
                    </div>    
                </div>
            </div>
        </div>
    </section>
    <!-- -->
</div>
<!-- -->     