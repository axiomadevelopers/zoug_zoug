<div id="cuerpoNoticias" ng-controller="noticiasController">
	<!--PARALLAX-->
	<div id="prlx_como_funciona" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
		<!-- stylesheets -->
		<div class="super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after" style="height: 130%; background-image: url(<?=base_url();?>assets/web/img/parallax/parallax2.jpg);"></div>

		<div class="container g-pt-100 g-pb-70">
		  <div class="row">
		    <div class="col-sm-6 col-lg-6 align-items-end mt-auto g-mb-50 texto_parallax">
		      <div class="text-center">
		        <h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1" style="color:#fff">{{titulos_home.noticias}} </h1>
		      </div>
		    </div>
		  </div>
		</div>
	</div>
	<!-- -->
	<!-- MAIN CONTENT SECTION -->
	<section class="mainContent clearfix aboutUsInfo">
        <div class="container">
			<div class="row">
				<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
					<div class="card" style="width: 100%;">
						<img class="card-img-top  fadeInLeft wow"  ng-src="{{base_url}}{{noticia.ruta}}" alt="Card image cap">
						<div class="card-body fadeInLeft wow">
							<h3 class="card-title">{{noticia.titulo}}</h3>
							<span class="sub-notice col-xs-12">  
								<i class="fa fa-user info-news" aria-hidden="true"></i>
				    			<label class="pr-1 parrafos">{{noticia.usuario}}</label> 
				    	    </span>
				    	    <span class="sub-notice col-xs-12">  
								<i class="fa fa-calendar info-news" aria-hidden="true"></i>
				    			<label class="pr-1 parrafos">{{noticia.fecha}}</label> 
				    	    </span>
							<p class="card-text parrafos">{{noticia.descripcion_sin_html}}</p>
							<h4><a href="{{base_url}}{{url.menu4}}/{{noticia.slug}}" class="card-title btn-news">{{btn.leer_mas}}</a></h4>
							<div style="clear:both;"></div>
							<div style="clear: both"></div>
						</div>
					</div>
					<div style="clear:both;"></div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 fadeInRight wow cuerpo-noticias-productos">
					<div class="card" style="width: 100%;">
						<div class="card-header letra titulo_publicidad">
						{{titulos_home.nuestros_productos}}
						</div>
						  <div class="card-body">
						  	<img  class="card-img-top img-producto-noticias" src="{{base_url}}{{super_producto_noticias1.ruta}}" alt="article-image">
						    <h5 class="card-title">{{super_producto_noticias1.titulo}}</h5>
						    <p class="card-text"></p>
						    <a href="{{base_url}}{{url.menu3}}/{{super_producto_noticias1.slug}}" class="btn btn-primary btn-prod-noticias">{{btn.ver_producto}}</a>
						  </div>

						  <div class="card-body">
						  	<img class="card-img-top img-producto-noticias" src="{{base_url}}{{super_producto_noticias2.ruta}}" alt="article-image" style="height: 300px;">
						    <h5 class="card-title">{{super_producto_noticias2.titulo}}</h5>
						    <p class="card-text"></p>
						    <a href="{{base_url}}{{url.menu3}}/{{super_producto_noticias2.slug}}" class="btn btn-primary btn-prod-noticias">{{btn.ver_producto}}</a>
						  </div>
					</div>
					<div style="clear:both;"></div>
				</div>
			</div>
 			<div class="container">
				<div class="row latestArticles fadeInUp wow">
					<hr></hr>
					<div class="col-12 col-lg-12 col-md-12  col-xs-12 col-sm-12">
						<div class="card-header letra titulo_otras_noticias" >
							{{titulos_home.otras_noticias}}
						</div>
						<div style="clear: both"></div>
					</div>	
					<!--<div class="col-md-4" ng-repeat="notice in sub_noticias track by $index">
					  	<div class="thumbnail">
						    <a href="blog-single-right-sidebar.html">
						      <img src="<?=base_url();?>{{notice.ruta}}" alt="article-image" style="height: 233px;">
						    </a>
						    <div class="date-holder">
						      <p>{{notice.dias}}</p>
						      <span>{{notice.mes}}</span>
						    </div>
						    <h5>
						    	<a href="{{base_url}}{{url.menu4}}/{{notice.slug}}">{{notice.titulo}}</a>
						    </h5>
				    		<span class="meta"> by 
				    			<a class="pr-1">{{notice.usuario}}</a> 
				    		</span>
						    <div class="caption">
						      	<p ng-bind-html="notice.descripcion_sin_html"></p>
						    </div>
					  	</div>					  	
					</div>-->
					<otras-noticias class="col-lg-4 col-md-6 col-sm-12 col-xs-12 fadeInUp wow" ng-repeat="notice in sub_noticias track by $index"></otras-noticias>
				</div>
				<div class="col-lg-12">
					<div  id="campo_mensaje_noticias" name="campo_mensaje_noticias" ></div>
				</div>
				<div class="centrar-div fadeInUp wow" >
				    <button type="button" class="btn btn-primary btn-rounded centrar-div btn-about" ng-click="cargar_mas_subnoticias()">{{btn.ver_mas}}</button>
				</div>
			</div>	
        </div>
    </section>
</div>