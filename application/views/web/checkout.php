<div id="cuerpoProducto" ng-controller="checkoutController">
	<!--PARALLAX-->
	<div id="prlx_como_funciona" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
		<div
			class="super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after"
			style="height: 130%; background-image: url(<?=base_url();?>assets/web/img/parallax/parallax13.jpg);"></div>
		<div class="container g-pt-100 g-pb-70">
			<div class="row">
				<div class="col-sm-6 col-lg-6 align-items-end mt-auto g-mb-50 texto_parallax">
					<div class="text-center">
						<h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1"
							style="color:#fff">{{titulos_home.checkout}}</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- MAIN CONTENT SECTION -->
	<section class="mainContent clearfix stepsWrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="innerWrapper clearfix stepsPage">
						<div class="row justify-content-center order-confirm">
							<div class="col-md-8 col-lg-6 text-center">
								<h2>{{titulos_home.gracias_compra}}</h2>
								<span>{{titulos_home.info_email}}</span>
								<div class="orderInfo">
									{{titulos_home.info_orden1}}
									<span id="numero_orden">#{{numero_orden}}</span> <br>
									{{titulos_home.info_orden2}} <br>
									{{correoUsuario}}<br>
									<div>
										{{titulos_home.payment_id}}:
										<span id="payment_id">{{id_pago_paypal}}</span> <br>
									</div>
									<div>
										{{titulos_home.payer_id}}:
										<span id="payment_id">{{id_cliente_paypal}}</span> <br>
									</div>
								</div>
								<a href="{{base_url}}{{url.menu3}}" class="btn btn-primary btn-default">{{btn.volver_tienda}}</a>
								<div style="clear: both"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- -->
</div>
<div id="idOrdenCompra" name="idOrdenCompra" class="invisible"><?=$numero_orden_compra;?></div>
<div id="correoUsuario" name="correoUsuario" class="invisible"><?=$correo?></div>
<div id="payment_id_oculto" name="payment_id_oculto" class="invisible"><?=$id_pago_paypal;?></div>
<div id="payer_id_oculto" name="payer_id_oculto" class="invisible"><?=$id_cliente_paypal;?></div>