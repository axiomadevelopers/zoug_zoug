<div id="cuerpoNosotros" ng-controller="nosotrosController">
	<!--PARALLAX-->
	<div id="prlx_como_funciona" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
	<!-- stylesheets -->
	<div class="super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after" style="height: 130%; background-image: url(<?=base_url();?>assets/web/img/parallax/parallax5.jpg);"></div>

	<div class="container g-pt-100 g-pb-70">
	  <div class="row">
	    <div class="col-sm-6 col-lg-6 align-items-end mt-auto g-mb-50 texto_parallax">
	      <div class="text-center">
	        <h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1" style="color:#fff">{{titulos_home.nosotros}} </h1>
	      </div>
	    </div>
	  </div>
	</div>
	</div>
	<!-- -->
	<!-- MAIN CONTENT SECTION -->
	<section class="mainContent clearfix aboutUsInfo">
        <div class="container">
	          <div class="page-header texto-about-us fadeInUp wow">
	            <h1>{{titulos_home.somos1}}</h1>
	          </div>
	          <div class="row cuadro-nosotros">
	            <div class="col-md-6 order-sm-12 fadeInRight wow">
	              <img ng-src="{{base_url}}{{nosotros.ruta}}" alt="about-us-img">
	            </div>
	            <div class="col-md-6 order-sm-1 fadeInLeft wow">
	              <div class="lead parrafos" ng-bind-html="nosotros.somos"></div>
	            </div>
	          </div>
	          <div class="row cuerpo-mision-vision" >
				<!-- -->
				<div class="col-md-6 mg-bt-nosotros fadeInLeft wow">
					<div class="contenedor_icono_pasos">
					  <div class="iconos_pasos_us">
					    <span class="fa-stack fa-lg">
					      <i class="fa fa-circle fa-stack-2x"></i>
					      <i class="fa fa-spoon fa-stack-1x fa-inverse"></i>
					    </span>
					      <!--<i class="fa fa-area-chart" aria-hidden="true" style="color:#aa7ee7"></i>-->
					  </div>
					</div>
					<div class="feature">
						<div class="card-como_funciona">
						    <div class="titulo-mision">
							      <h3>
							       {{titulos_home.mision}}
							      </h3>
						    </div>
						    <hr class="fadeInDown wow hr1 hr_como_funciona">
						    <div style="clear:both"></div>  
						</div>  
					  	<p class="parrafos parrafo-mision" ng-bind-html="nosotros.mision">
					  	</p>
					</div>
				</div>
				<!-- -->
				<div class="col-md-6 mg-bt-nosotros fadeInRight wow">
					<div class="contenedor_icono_pasos">
					  <div class="iconos_pasos_us">
					    <span class="fa-stack fa-lg">
					      <i class="fa fa-circle fa-stack-2x"></i>
					      <i class="fa fa-eye fa-stack-1x fa-inverse"></i>
					    </span>
					      <!--<i class="fa fa-area-chart" aria-hidden="true" style="color:#aa7ee7"></i>-->
					  </div>
					</div>
					<div class="feature">
						<div class="card-como_funciona">
						    <div class="titulo-mision">
							      <h3>
							       {{titulos_home.vision}}
							      </h3>
						    </div>
						    <hr class="fadeInDown wow hr1 hr_como_funciona">
						    <div style="clear:both"></div>  
						</div>  
					  	<p class=" parrafos parrafo-mision" ng-bind-html="nosotros.vision">
					  	</p>
					</div>
				</div>
				<!-- -->
	          </div>
	          <div class="centrar-div fadeInUp wow">
	              	<a href="{{base_url}}{{url.menu5}}" target="_self" style="display: flex; margin: 0 auto;">
                    	<button type="button" class="btn btn-primary btn-rounded centrar-div btn-about">{{btn.contacto}}</button>
              		</a>
	          </div>
        </div>
    </section>
	<!-- -->    
</div>