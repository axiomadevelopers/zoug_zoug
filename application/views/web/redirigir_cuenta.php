<div id="cuerpoProducto" ng-controller="redirectAccountController">
  <!--PARALLAX-->
  <div id="prlx_como_funciona" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
    <div
      class="super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after"
      style="height: 130%; background-image: url(<?=base_url();?>assets/web/img/parallax/parallax13.jpg);"></div>

    <div class="container g-pt-100 g-pb-70">
      <div class="row">
        <div class="col-sm-6 col-lg-6 align-items-end mt-auto g-mb-50 texto_parallax">
          <div class="text-center">
            <h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1"
              style="color:#fff">{{titulos_home.mensaje_redirigir}}</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- MAIN CONTENT SECTION -->
  <section class="mainContent clearfix stepsWrapper">
    <div class="container">
      <div class="row">
       <div class="col-md-12">
          <div class="innerWrapper clearfix stepsPage">
            <div class="row justify-content-center order-confirm">
              <div class="col-md-8 col-lg-6 text-center">
                <div class="tab-content">
                  <div class="tab-pane p-20" id="email" role="tabpanel" ng-class="{'active':currentab === 'email'}">
                    <h2>{{titulos_home.mensaje_email}}</h2>
                    <a href="{{base_url}}{{url.menu1}}" class="btn btn-primary btn-default">{{btn.volver_inicio}}</a>
                    <div style="clear: both"></div>
                  </div>
                  <div class="tab-pane p-20" id="facebook" role="tabpanel" ng-class="{'active':currentab === 'facebook'}">
                    <h2>{{titulos_home.mensaje_facebook}}</h2>
                    <a href="{{base_url}}{{url.menu1}}" class="btn btn-primary btn-default">{{btn.volver_inicio}}</a>
                    <div style="clear: both"></div>
                  </div>
                  <div class="tab-pane p-20" id="google" role="tabpanel" ng-class="{'active':currentab === 'google'}">
                    <h2>{{titulos_home.mensaje_google}}</h2>
                    <a href="{{base_url}}{{url.menu1}}" class="btn btn-primary btn-default">{{btn.volver_inicio}}</a>
                    <div style="clear: both"></div>
                  </div>
                  <div class="tab-pane p-20" id="error" role="tabpanel" ng-class="{'active':currentab === 'error'}">
                    <h2>{{titulos_home.error_nada}}</h2>
                    <!-- <div class="orderInfo">
                      asdasdasdas<br>
                      {{correoUsuario}}<br>
                      <div>
                        {{titulos_home.activada}}:
                        <br>
                      </div>
                    </div> -->
                    <br>
                    <a href="{{base_url}}{{url.menu1}}" class="btn btn-primary btn-default">{{btn.volver_inicio}}</a>
                    <div style="clear: both"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </section>
  <!-- -->
</div>
<div id="tp_login" name="tp_login" class="invisible"><?=$tp_login;?></div>
