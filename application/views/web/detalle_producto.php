<div id="cuerpoProducto" ng-controller="productoDetallesController">
  <!--PARALLAX-->
  <div id="prlx_como_funciona" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
    <div class="super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after" style="height: 130%; background-image: url(<?=base_url();?>assets/web/img/parallax/parallax1.jpg);"></div>

    <div class="container g-pt-100 g-pb-70">
      <div class="row">
        <div class="col-sm-6 col-lg-6 align-items-end mt-auto g-mb-50 texto_parallax">
          <div class="text-center">
            <h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1" style="color:#fff">{{titulos_home.productos}}</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
	<!-- MAIN CONTENT SECTION -->
  	<section class="mainContent clearfix">
    	<div class="container">
      		<div class="row singleProduct">
        		<div class="col-md-12">
          			<div class="media flex-wrap">
                  
                  <div class="media-left productSlider">
                      <!-- Imagenes principal -->
                      <div id="slider-ppal">
                      <!-- -->
                          <div class="row">
                              <div class="large-12 columns">
                                <div class="col-lg-12">
                                  <div id="slider" class="">
                                      <div ng-repeat="imagen in sub_producto track by $index" class="item img-slider-ppal" id="div_imagen{{$index}}" ><!-- onmouseover="super_hover()"-->
                                          <img id="imagen{{$index}}" ng-src="{{base_url}}{{imagen.ruta}}" class="img-ppal-producto"  onload="super_hover()">
                                          <!-- height:501px;margin-right:2px onmouseover="super_hover()"-->
                                      </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                      <!-- -->  
                      </div>
                      <hr>
                      <!--Slider imagenes inferior -->
                      <div id="slider-inf" class="col-lg-12">
                          <!-- -->
                          <div class="row">
                              <div class="col-xs-2" style="padding:2px;" ng-repeat="imagen in sub_producto track by $index">
                                  <img id="imagen_selector{{$index}}" class="imagen_galeria imagen_selector img-barra-slider" dat="#div_imagen{{$index}}" ng-src="{{base_url}}{{imagen.ruta}}" >
                              </div>
                              <div style="clear: both"></div>
                          </div> 
                          <!-- -->
                      </div>
                      <hr>   
                  </div>
                   
                  <br>
              		<div class="media-body cuerpo-detalle-producto">
                			<form id="formProducto" name="formProducto" target="_self">
                				<h2>{{producto.titulo}}</h2>
                        <h3>{{producto.codigo}}</h3>
                				<h3>${{producto.precio}}</h3>
                				
                        <div>
                            <label>{{titulos_home.detalles_productos_talla}}:</label>
                            <div id="tallaDiv" name="tallaDiv" style="display: none">
                            </div>
                            <select id="tallaProd" name="tallaProd" ng-model="talla" class="form-control cantidad_select" placeholder="Cantidad" ng-options="option.descripcion_talla for option in tallas track by option.id_talla" ng-click="ColoresExistentes();VaciaCantidad();VaciaColor();">
                            <option value=""selected>{{titulos_home.detalles_productos_talla}}</option>
                            </select>
                        </div>
                        <div>
                            <label>{{titulos_home.detalles_productos_color}}:</label>
                            <div id="colorDiv" name="colorDiv" style="display: none">
                            </div>
                            <select id="colorProd" name="colorProd" ng-model="color" class="form-control cantidad_select" placeholder="Cantidad" ng-options="option.descripcion_color for option in colores track by option.id_color" ng-click="CantidadExistentes();VaciaCantidad();">
                            <option value=""selected>{{titulos_home.detalles_productos_color}}</option>
                            </select>
                        </div>
                        <div>
                            <label>{{titulos_home.detalles_productos_cantidad}}</label>
                            <div id="cantidadDiv" name="cantidadDiv" style="display: none">{{producto.cantidad}}
                            </div>
                            <select id="cantidadProducto" name="cantidadProducto" class="form-control cantidad_select" placeholder="Cantidad">
                                <option>0</option>
                            </select>
                        </div>
                				<div class="">
                      <?php
                      $correcto = $this->session->userdata('web');
                      if ($correcto) 
                        {
                      ?>
                        <div ng-click="agregarCarrito()" class="btn btn-primary btn-default">
                            {{titulos_home.detalles_productos_agregar}}
                            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                        </div>
                      <?php 
                      }
                        if (!$correcto) {
                      ?>
                        <div >
                        <a class="btn btn-primary btn-default" data-toggle="modal" data-target="#defaultSize"
                                              class="iniciar-sesion">{{titulos_home.detalles_productos_agregar}} <i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
                          
                         
                        </div>                    
                      <?php 
                        }
                       ?>
                  </div>
                				<div class="tabArea">
                    				<!--<ul class="nav nav-tabs bar-tabs">
                      					<li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="">{{titulos_home.detalles_productos}}</a>
                                </li>
                    				</ul>-->
                            <label>{{titulos_home.detalles_productos}}:</label>
                    				<div class="tab-content">
                      					<div id="details" class="tab-pane fade show active">
                      						<ul class="list-unstyled">
                                      <li ng-bind-html="producto.descripcion"></li>
                        							<li>{{titulos_home.detalles_productos_categoria}}: {{producto.categoria}}</li>
                        							<li>{{titulos_home.detalles_productos_tipo_producto}}: {{producto.tipo_producto}}</li>
                        							                                      <!--<li>PRECIO: ${{producto.precio}}</li>-->

                      						</ul>
                      					</div>
                    				</div>
                            <div class="col-lg-12">
                                <div id="campo_mensaje_productos" name="campo_mensaje_productos" ></div>
                            </div>
                				</div>
                      </form>  
              		</div>
          			</div>
        		</div>
      		</div>
          <!-- -->
          
          <!--
          <div class="row fadeInUp wow">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" ng-repeat="product in mas_productos track by $index">
                <div class="card card-producto">
                  <div class="card_img">
                    <a href="{{base_url}}{{url.menu3}}/{{product.slug}}" target="_self">
                      <img class="card-img-top card-mas-producto" src="{{base_url}}{{product.ruta}}" alt="card img">
                    </a>
                  </div>
                  <div class="card-block">
                    <a href="{{base_url}}{{url.menu3}}/{{product.slug}}"><h5 class="card-title title-producto">{{product.titulo}}</h5></a>
                    <div class="col-12 precio-mas-productos">
                      ${{product.precio}}
                    </div>   
                  </div>
                </div>
            </div>
          </div>  
          -->
          <section class="mainContent clearfix productsContent productos-detalle-p">

              <div class="container">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="page-header mb-4">
                        <h4>{{titulos_home.mas_productos}}</h4>
                      </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row" ng-hide="variable">
                          <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" ng-repeat="product in mas_productos track by $index">
                              <div class="productBox">
                                <!-- -->  
                                <div class="productImage clearfix">
                                    <img src="<?=base_url();?>{{product.ruta}}" alt="products-img" class="products-img products-img2" style="">
                                    <div class="productMasking">
                                      <ul class="list-inline btn-group" role="group">
                                          
                                          <li>
                                            <a href="{{base_url}}{{url.menu3}}/{{product.slug}}">
                                              <i class="fa fa-eye icono-productos"></i>
                                              <span class="btn-productos">
                                                {{btn.ver_producto}}
                                              </span> 
                                            </a>
                                          </li>
                                      </ul>
                                    </div>
                                </div>
                                <!-- -->
                                <div class="productCaption clearfix">
                                    <a href="{{base_url}}{{url.menu3}}/{{product.slug}}"><h5 class="card-title title-producto">{{product.titulo}}</h5></a>
                                    <div class="col-12 precio-mas-productos">
                                      ${{product.precio}}
                                    </div> 
                                </div>
                                <!-- -->
                                <div style="clear: both"></div>
                              </div>
                          </div>  
                        </div>
                    </div> 
                  </div>
              </div>
              <div style="clear: both"></div>
          </section> 
          <!--
          <section class="mainContent clearfix productsContent">
              <div class="container">
                <div class="row">
                  <div class="col-md-6 col-lg-4" ng-repeat="product in mas_productos track by $index">
                      <div class="productBox">
                        <div class="productImage clearfix">
                            <img src="<?=base_url();?>{{product.ruta}}" alt="products-img" class="card-img-top card-mas-producto" style="">
                            <div class="productMasking">
                              <ul class="list-inline btn-group" role="group">
                                  
                                  <li>
                                    <a href="{{base_url}}{{url.menu3}}/{{product.slug}}">
                                      <i class="fa fa-eye icono-productos"></i>
                                      <span class="btn-productos">
                                        {{btn.ver_producto}}
                                      </span> 
                                    </a>
                                  </li>
                              </ul>
                            </div>
                        </div>
                        <div class="productCaption clearfix">
                            <a href="{{base_url}}{{url.menu3}}/{{product.slug}}"><h5 class="card-title title-producto">{{product.titulo}}</h5></a>
                            <div class="col-12 precio-mas-productos">
                              ${{product.precio}}
                            </div> 
                        </div>
                      </div>
                  </div>
                  <div class="col-lg-12">
                    <div id="campo_mensaje_productos" name="campo_mensaje_productos" ></div>
                  </div>
                  <div class="centrar-div fadeInUp wow" >
                      <button type="button" id="cargar_mas" class="btn btn-primary btn-rounded centrar-div btn-about" ng-click="cargar_mas_productos()">{{btn.ver_mas}}</button>
                  </div>
                </div> 
              </div>
          </section>      
          -->
		<div id="slug_producto" name="slug_producto" style="display: none;"><?php echo $slug_producto; ?></div>
    <div id="id_producto" name="id_producto" style="display: none;">{{producto.id}}</div>

  	</section>
</div>




 <!-- Modal para editar -->
 <div class="modal fade text-left" id="defaultSize" tabindex="-1" role="dialog"
                              aria-labelledby="myModalLabel18" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel18"><i class="fa"></i>{{menu.titulo_inicio_sesion}}</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    <br>
                                    <div class="action-buttons mt-2 text-center">
                                      <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                                        <img alt="" src="{{base_url}}/assets/images/user.png" class="img-responsive img-circle profile-image">
                                        <div style="clear: both;">
                                      </div>  
                                      <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                                                      <?php

                                                    $data['google']=$this->google->get_login_url();
                                                    $data['facebook'] =  $this->facebook->login_url();

                                                    ?>


                                       <a href="<?=$data['facebook']?>"
                                          class="btn  btn-facebook btn-rounded centrar-div btn-about"><i
                                            class="fa fa-facebook iconos-is"></i>Facebook</a>


                                        <a href="<?=$data['google']?>"
                                          class="btn btn-google btn-rounded centrar-div btn-about"><i
                                            class="fa fa-google iconos-is"></i>Google login</a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                          </div>
                          <!-- Fin Modal para editar -->