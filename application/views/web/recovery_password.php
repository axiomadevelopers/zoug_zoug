<div id="cuerpoProducto" ng-controller="recoveryPasswordController">
  <!--PARALLAX-->
  <div id="prlx_como_funciona" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
    <div
      class="super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after"
      style="height: 130%; background-image: url(<?=base_url();?>assets/web/img/parallax/parallax13.jpg);"></div>

    <div class="container g-pt-100 g-pb-70">
      <div class="row">
        <div class="col-sm-6 col-lg-6 align-items-end mt-auto g-mb-50 texto_parallax">
          <div class="text-center">
            <h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1"
              style="color:#fff">{{titulos_home.recuperar_cuenta}}</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- MAIN CONTENT SECTION -->
  <section class="mainContent clearfix stepsWrapper">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="innerWrapper clearfix stepsPage">
            <div class="row justify-content-center order-confirm">
              <div class="col-md-8 col-lg-6 text-center">
                <div class="tab-content">
                  <div class="tab-pane p-20" id="aprobado" role="tabpanel"
                    ng-class="{'active':currentTab1 === 'aprobado'}">
                    <h2>{{titulos_home.recuperar}}</h2>
                    <div class="orderInfo">
                      <label class="">{{titulos_home.nueva_clave}}:</label>
                      <div class="controls">
                        <input type="password" name="clave_cambio" id="clave_cambio" class="form-control form-control-line"
                          ng-model="clave" onKeyPress="return valida(event,this,2,8)" onBlur="valida2(this,2,8);">
                      </div>
                      <br>
                      <label class="">{{titulos_home.nueva_clave_r}}:</label>
                      <div class="controls">
                        <input type="password" name="clave_cambio_confirmar" id="clave_cambio_confirmar"
                          class="form-control form-control-line" ng-model="clave_r"
                          onKeyPress="return valida(event,this,2,8)" onBlur="valida2(this,2,8);">
                      </div>


                    </div>
                    <a ng-click="verificarClave()" class="btn btn-primary btn-default">{{btn.cambiar_clave}}</a>
                    <div style="clear: both"></div>
                  </div>

                  <div class="tab-pane p-20" id="cambio_logrado" role="tabpanel"
                    ng-class="{'active':currentTab1 === 'cambio_logrado'}">
                    <h2>{{titulos_home.cambio_realizado}}</h2>
                    <!-- <div class="orderInfo">
                      {{titulos_home.la_cuenta}} <br>
                      {{correoUsuario}}<br>
                      <div>
                        {{titulos_home.activada}}:
                        <br>
                      </div>
                    </div> -->
                    <a href="{{base_url}}{{url.menu1}}" class="btn btn-primary btn-default">{{btn.volver_inicio}}</a>
                    <div style="clear: both"></div>
                  </div>


                  <div class="tab-pane p-20" id="ya_activado" role="tabpanel"
                    ng-class="{'active':currentTab1 === 'ya_activado'}">
                    <h2>{{titulos_home.caducado}}</h2>
                    <!-- <div class="orderInfo">
                      asdasdasdas<br>
                      {{correoUsuario}}<br>
                      <div>
                        {{titulos_home.activada}}:
                        <br>
                      </div>
                    </div> -->
                    <br>
                    <a href="{{base_url}}{{url.menu1}}" class="btn btn-primary btn-default">{{btn.volver_inicio}}</a>
                    <div style="clear: both"></div>
                  </div>

                  <div class="tab-pane p-20" id="error" role="tabpanel" ng-class="{'active':currentTab1 === 'error'}">
                    <h2>{{titulos_home.error_nada}}</h2><br>
                    <!-- <div class="orderInfo">
                      asdasdasdas<br>
                      {{correoUsuario}}<br>
                      <div>
                        {{titulos_home.activada}}:
                        <br>
                      </div>
                    </div> -->
                    <br>
                    <a href="{{base_url}}{{url.menu1}}" class="btn btn-primary btn-default">{{btn.volver_inicio}}</a>
                    <div style="clear: both"></div>
                  </div>
                  <div class="col-lg-12">
                    <div id="campo_mensaje_cambio_clave" name="campo_mensaje_cambio_clave"></div>
                  </div>


                </div>




              </div>
            </div>
          </div>
        </div>
      </div>
  </section>
  <!-- -->
</div>
<div id="idUser" name="idUser" class="invisible"><?=$id;?></div>
<div id="codigo" name="codigo" class="invisible"><?=$codigo;?></div>