<div id="cuerpoContactanos" ng-controller="contactanosController">
	<!--PARALLAX-->
	<div id="prlx_como_funciona" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
		<!-- stylesheets -->
		<div class="super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after" style="height: 130%; background-image: url(<?=base_url();?>assets/web/img/parallax/parallax3.jpg);"></div>

		<div class="container g-pt-100 g-pb-70">
		  <div class="row">
		    <div class="col-sm-6 col-lg-6 align-items-end mt-auto g-mb-50 texto_parallax">
		      <div class="text-center">
		        <h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1" style="color:#fff">{{titulos_home.contactanos}}</h1>
		      </div>
		    </div>
		  </div>
		</div>
	</div>
	<!-- --> 
	<section class="mainContent clearfix">
          <div class="container">     
            <div class="page-header fadeInUp wow">
              <h4>{{titulos_home.contactanos2}}</h4>
            </div>
            <div class="display-single_element fadeInDown wow cuerpo-form-contactos">
                <form >
                  <div class="row">
                    <div class="form-group col-md-6">
                      <input type="text" class="form-control" id="exampleInputName" aria-describedby="userName" placeholder="{{btn.place_h1}}" ng-model="contactos.nombres" >
                    </div>
                    <div class="form-group col-md-6">
                      <input type="email" class="form-control" aria-describedby="emailHelp" placeholder="{{btn.place_h2}}" ng-model="contactos.email"  onKeyPress="return valida(event,this,5,50)" onBlur="valida2(this,5,50);correo(this,'campo_mensaje_clientes')" maxlength="50" id="contactos_email" name="contactos_email">
                    </div>
                    <div class="form-group col-md-12">
                      <input type="number" class="form-control" id="contactos_telefono" name="contactos_telefono" placeholder="{{btn.place_h3}}" ng-model="contactos.telefono" onKeyPress="return valida(event,this,21,14)" onBlur="valida2(this,21,14);" >
                    </div>
                    <div class="form-group col-md-12">
                       <textarea class="form-control" id="exampleTextarea" rows="5" placeholder="{{btn.place_h4}}" ng-model="contactos.mensaje" id="mensaje" name="mensaje"></textarea>
                    </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12" style="">
                      <div id="campo_mensaje_clientes" ></div>
                  </div>
                  <button type="submit" class="btn btn-default btn-primary fadeInUp wow" ng-click="registrar_contactos()">{{btn.enviar}}</button>
                </form>
            </div>
          </div>
    </section>
    <!--PARALLAX-->
    <div id="prlx_como_funciona" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
      <!-- stylesheets -->
        <div class="super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after" style="height: 130%; background-image: url(<?=base_url();?>assets/web/img/parallax/parallax4.jpg);"></div>

        <div class="container g-pt-100 g-pb-70">
          <div class="row">
            <div class="col-sm-6 col-lg-6 align-items-end mt-auto g-mb-50 texto_parallax">
              <div class="text-center">
                <h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1" style="color:#fff"> {{titulos_home.redes_sociales}} </h1>
              </div>
            </div>
          </div>
        </div>
    </div>  
	<section class="mainContent clearfix">
	  <div class="container">     
	    <div class="page-header fadeInUp wow">
	      <h4> {{titulos_home.redes_sociales2}} </h4>
	    </div>
	  </div>
	  <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 padding0 fadeInUp wow redes_sociales">
	    <div style="" class="cuerpo_redes col-lg-12 col-md-12 col-xs-12 col-sm-12">
	    	<div class="container">
		      <div class="col-4  padding0 redes_individual" style="float:left;display: flex; margin: 0 auto;" title="Facebook">
		        <a href="{{redes[1].url_red}}" target="_blank"  class="redes_individual" style="display: flex;margin: 0 auto">
		          <div class="contenedor_icono_pasos">
		            <div class="iconos_pasos iconos_redes">
		              <i class="fa fa-facebook icono-zoug" aria-hidden="true"></i>
		            </div>
		          </div>  
		        </a>
		        <div style="clear:both"></div>
		      </div>    
		      <div class="col-4 padding0 redes_individual" style="float:left;display: flex; margin: 0 auto;">  
		        <a href="{{redes[0].url_red}}"  target="_blank" class="redes_individual" style="display: flex;margin: 0 auto">
		          <div class="contenedor_icono_pasos">
		            <div class="iconos_pasos iconos_redes">
		              <i class="fa fa-instagram icono-zoug" aria-hidden="true" ></i>
		            </div>
		          </div>  
		        </a>
		        <div style="clear:both"></div>  
		      </div> 

		      <div class="col-4 padding0 redes_individual"  style="float:left;display: flex; margin: 0 auto;">
		        <a href="{{redes[2].url_red}}" target="_blank" class="redes_individual" style="float:left;display: flex; margin: 0 auto;">  
		          <div class="contenedor_icono_pasos">
		            <div class="iconos_pasos iconos_redes icono-zoug" >
		              <i class="fa fa-twitter" aria-hidden="true"></i>
		            </div>
		          </div>  
		        </a>
		        <div style="clear:both"></div>
		      </div>
			</div>
	      <div style="clear:both"></div>
	    </div> 
	    <!-- Bloque de facebook -->
	    <div class="container cuerpo-redes-soc">
        <!-- -->
            <div class="row">
              <div class="col-lg-12">
                  <div class="page-header2 fadeInUp wow">
                      <h4> {{titulos_home.redes_sociales3}} </h4>
                  </div>
              </div>
                <!-- LightWidget WIDGET --><script src="https://cdn.lightwidget.com/widgets/lightwidget.js"></script><iframe src="//lightwidget.com/widgets/f8cc972d1e47565d805133fd5e1476f0.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width:100%;border:0;overflow:hidden;"></iframe>
                <!-- -->
            </div>   
        </div>
        <div style="clear:both"></div>
      </div>	   
	</section>      
</div>