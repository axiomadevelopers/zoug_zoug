<div id="cuerpoProducto" ng-controller="orderController">
  <!--PARALLAX-->
  <div id="prlx_como_funciona" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
    <div class="super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after" style="height: 130%; background-image: url(<?=base_url();?>assets/web/img/parallax/parallax11.jpg);"></div>

    <div class="container g-pt-100 g-pb-70">
      <div class="row">
        <div class="col-sm-6 col-lg-6 align-items-end mt-auto g-mb-50 texto_parallax">
          <div class="text-center">
            <h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1" style="color:#fff">{{titulos_home.order_titulo}}</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
	<!-- MAIN CONTENT SECTION -->
  <section class="mainContent clearfix cartListWrapper">
    <div class="container">
      <!--
      <form id="realizarPago" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
        <input name="cmd" type="text" value="_cart" />
        <input name="upload" type="text" value="1" />
        <input name="business" type="text" ng-model="correo_paypal" />
        <input name="shopping_url" type="text" ng-model="url_shopping" />
        <input name="currency_code" type="text" value="EUR" />
        <input name="return" type="text" value="" ng-model="url_carrito" />
        <input name="notify_url" type="text" ng-model="url_ipn" />

        <input name="rm" type="text" value="2" />
        <input name="item_number_1" type="text" ng-model="id_carrito" />
        <input name="item_name_1" type="text" ng-model="monto_global_total" />
        <input name="amount_1" type="text" ng-model="monto_global_total" />
        <input name="quantity_1" type="text" value="1" /> 
      </form>
      -->
      <div class="row">
        <!--Titulo --->
        <div class="col-md-12">
            <div class="page-header mb-4">
              <h4>{{titulos_home.vista_orden}}</h4>
            </div>
        </div >
        <!-- -->
        <div class="col-lg-12">
            <div id="campo_mensaje_productos" name="campo_mensaje_productos" ></div>
        </div>
        <div class="col-md-8" ng-if="cuantos_productos>0">
          <div class="innerWrapper clearfix stepsPage">              
            <div class="cartListInner review-inner row">
              <form action=""  id="formOrderProducto" name="formOrderProducto" class="col-sm-12">
                <input  type="text" id="idCarrito" name="idCarrito" ng-model="id_carrito" class="invisible">
                <div class="table-responsive tabla-orden">
                  <table class="table table-productos">
                    <thead>
                      <tr>
                        <th></th>
                        <th>{{titulos_home.producto_nombre}}</th>
                        <!--<th>{{titulos_home.detalles_productos}}</th>-->
                        <th class="campo-precio">{{titulos_home.producto_precio}}</th>
                        <th class="campo-cantidad">
                            <span class="cantidad_completo">{{titulos_home.producto_cantidad}}</span>
                            <span class="cantidad_reducido">{{titulos_home.producto_cantidad_reducido}}</span>
                        </th>
                        <th>Sub Total</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="producto in productos track by $index" class="tabla_producto" data="{{$index}}">
                          <td class="">
                            <span class="cartImage">
                                <img src="{{base_url}}{{producto.imagenes[0].ruta}}" alt="image" class="img-carrito">
                            </span>
                          </td>
                          <td class="">
                              {{producto.titulo}}<br>
                              {{titulos_home.detalles_productos_talla}}:  {{producto.descripcion_talla}} <br>
                              {{titulos_home.detalles_productos_color}}:  {{producto.descripcion_color}} <br>
                          </td>
                          <!--
                          <td class="">                        {{titulos_home.detalles_productos_color}}:  {{producto.descripcion_color}} <br>
                        {{titulos_home.detalles_productos_talla}}:  {{producto.descripcion_talla}} <br></td>
                         -->
                          <td class=""> ${{producto.monto_individual}}</td>
                          <td class="count-input">
                            <input class="quantity" type="text" value="{{producto.cantidad}}" disabled>
                          </td>
                          <td class="">
                              ${{producto.monto_total}}
                              <div id="monto_total_oculto_{{$index}}"  class="invisible">{{producto.monto_total_oculto}}</div>
                          </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </form>
            </div>

            <div class="row shipping-info metodo-pago">
                <div class="col-md-4">
                  <h5>{{titulos_home.producto_metodo_pago}}:</h5>
                   Paypal
                </div>
            </div>
            
            <div class="well well-lg clearfix">
              <ul class="pager">
              <li class="previous float-left">
                  <div class="btn btn-secondary btn-default float-left" ng-click="backpageCart()">back</div>
              </li>
                <li class="next">
                    <!--<div class="btn btn-primary btn-default float-right" ng-click="FrompageCheckout()">
                        Continue <i class="fa fa-angle-right"></i>
                    </div>-->
                    <div id="paypal-button-container"></div>
                    <div id="paypal-button"></div>
                    <div style="clear: both"></div>
                </li>
              </ul>
            </div>

          </div>
        </div>
        <div class="col-md-4" ng-if="cuantos_productos>0">
          <div class="summery-box">
            <h4>{{titulos_home.resumen_orden}}</h4>
            <p></p>
            <ul class="list-unstyled">
              <li class="d-flex justify-content-between li-subtotal">
                <span class="tag"></span>
                <span class="val">${{productos[0].monto_global_total}}</span>
                <span id="monto_global_oculto" class="val invisible">${{productos[0].monto_global_total_oculto}}</span>
              </li>
              <!--<li class="d-flex justify-content-between">
                <span class="tag">Shipping & Handling</span>
                <span class="val">$12.00 </span>
              </li>
              <li class="d-flex justify-content-between">
                <span class="tag">Estimated Tax</span>
                <span class="val">$0.00 </span>
              </li>-->
              <li class="d-flex justify-content-between total-summary">
                <span class="tag">Total</span>
                <span class="val">USD  ${{productos[0].monto_global_total}} </span>
              </li>
            </ul>
          </div>
        </div>

      </div>
      <!-- -->	
    </div>
  </section>
  <!-- -->
</div>
<script src="https://www.paypalobjects.com/api/checkout.js"></script>