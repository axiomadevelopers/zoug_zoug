<script type="text/javascript">
  if (window.location.hash && window.location.hash == '#_=_') {
    window.location.hash = '';
  } else
  if (window.location.hash && window.location.hash == '#') {
    window.location.hash = '';
  } else
  if (window.location.hash && window.location.hash == '/#') {
    window.location.hash = '';
  }
</script>
<?php
$correcto = $this->session->userdata('web');
  if ($correcto) {
    $web = $_SESSION["web"];

  ?>

<body class="body-wrapper version1 loaded1" ng-controller="inicioController"
  ng-init="inicio('<?=$web['id']?>','<?=$web['nombre_persona']?>','<?=$web['correo']?>','<?=$web['estatus']?>')"> <?php 
  }
  if (!$correcto) {
    ?>

  <body class="body-wrapper version1 loaded1" ng-controller="inicioController">
    <?php 
            }
  ?>

    <div id="preloader_zougzoug" class="" data-loading>
      <div class="loading-spiner">
        <!--Servidor -->
        <!-- Local: -->
        <div class="" style="margin: 0 auto;display: flex;">
          <div id="loader-container"
            style="background-image:url(<?=base_url();?>assets/web/img/logo.png);background-position:center;background-repeat:no-repeat;">
            <div class="loader"></div>
            <div>
              <img src="<?=base_url();?>/assets/web/img/30.gif" class="img-responsive"
                style="margin: 0 auto;display: flex;">
            </div>
            <div style="clear:both"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- Inicio de main-wrapper -->
    <div id="header_menu" class="main-wrapper" style="display: none">

      <!-- HEADER -->
      <div class="header clearfix" ng-controlller="inicioController">

        <!-- TOPBAR -->
        <div class="topBar topBar-ini">
          <div class="container2">
            <div class="row">
              <div class="col-5 col-lg-5 col-md-6 col-sm-6 col-xs-6 d-md-block redes-nav">
                <ul class="list-inline">
                  <li id="nav-facebook"><a href="{{redes[1].url_red}}" target="_blank"><i
                        class="fa fa-facebook"></i></a></li>
                  <li id="nav-instagram"><a href="{{redes[0].url_red}}" target="_blank"><i
                        class="fa fa-instagram"></i></a></li>
                  <li id="nav-twitter"><a href="{{redes[2].url_red}}" target="_blank"><i class="fa fa-twitter"></i></a>
                  </li>
                  <li>
                    <a href="mailto:{{footer.correo}}">
                      <span class="barra-zugzug">|</span>
                      <i class="fa fa-envelope-o margen-correo" aria-hidden="true" alt="{{footer.correo}}"></i>
                      <span class="email-zugzug">{{footer.correo}}</span>
                    </a>
                  </li>
                </ul>
              </div>
              <div class="col-7 col-lg-7 col-md-6 col-sm-6 col-xs-6  p-right4 p-peque0">
                <ul class="list-inline float-right top-right p-right4">
                  <li class="account-login"><span>
                      <?php
                      $correcto = $this->session->userdata('web');
                      if ($correcto) 
                      {
                      ?>
                      <a class="iniciar-sesion">
                        {{inicio.nombre_apellido}} | <span class="span_correo"> {{inicio.correo}}</span>
                      </a>
                      <?php 
                      }
                      if (!$correcto) {
                        ?>
                      <a data-toggle="modal" data-target="#defaultSize" class="iniciar-sesion"
                        ng-click="currentTab = 'login_email';vaciarDatos();">{{menu.menu_iniciar}}
                      </a>
                      <?php 
                        }
                      ?>
                      <?php
                        $correcto = $this->session->userdata('web');
                        if ($correcto) 
                        {
                      ?>
                      <a class="barra-zugzug" id="tipo_prod" href="<?=base_url();?>salir">{{menu.menu_salir}}</a>
                      <?php 
                                             }
                                            ?>
                    </span>
                  </li>
                  <li class="">
                    <!--<a href="index.html#"><i class="fa fa-search"></i></a>-->
                    <ul class="dropdown-menu dropdown-menu-right">
                      <li>
                        <span class="input-group">
                          <input type="text" class="form-control" placeholder="Search…" aria-describedby="basic-addon2">
                          <button type="submit" class="input-group-addon">Submit</button>
                        </span>
                      </li>
                    </ul>
                  </li>
                  <li class="dropdown cart-dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle btn-buy" data-toggle="dropdown"><i
                        class="fa fa-shopping-cart"></i>
                      <span class="badge"><?=$cuantos ?></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right" style="background-color: #b9ca46;">
                      <li>{{menu.menu_items}}</li>
                      <?php foreach ($productos as $clave => $valor) { ?>
                      <!-- -->
                      <li>
                        <a href="single-product.html" class="barra-producto">
                          <div class="media">
                            <img class="media-left media-object" src="<?=base_url().'/'.$valor->ruta?>"
                              alt="cart-Image">
                            <div class="media-body">
                              <h5 class="media-heading">
                                <?=$valor->titulo ?>
                                <br>
                                <div>
                                  Talla: <?=$valor->talla?>
                                </div>
                                <div>
                                  Color: <?=ucfirst(strtolower($valor->color))?>
                                </div>
                                <span>
                                  $<?=$valor->precio?>
                                </span>
                              </h5>
                            </div>
                          </div>
                        </a>
                      </li>
                      <!-- -->
                      <?php } ?>
                      <li>
                        <div class="btn-group" role="group" aria-label="...">
                          <!--
                          <button type="button" class="btn btn-default"
                            onclick="location.href='<?=base_url().$ir_cart?>';">{{menu.menu_carritos}}</button>
                          <button type="button" class="btn btn-default"
                            onclick="location.href='<?=base_url().$ir_orden?>';">{{menu.menu_productos}}</button>
                          -->
                          <a href="<?=base_url().$ir_cart?>" target="_self">
                            <button type="button" class="btn btn-default">{{menu.menu_carritos}}</button>
                          </a>
                          <a href="<?=base_url().$ir_orden?>" target="_self">
                            <button type="button" class="btn btn-default">{{menu.menu_productos}}</button>
                          </a>
                        </div>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        <!-- NAVBAR -->
        <nav class="navbar navbar-main navbar-default navbar-expand-md" role="navigation">
          <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->

            <a class="navbar-brand" href="{{base_url}}{{url.menu1}}">
              <img src="<?=base_url();?>/assets/web/img/logo.png" class="logo-zugzug">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-ex1-collapse"
              aria-controls="navbar-ex1-collapse" aria-expanded="false" aria-label="Toggle navigation">
              <i class="fa fa-bars" aria-hidden="true"></i>

            </button>


            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
              <ul class="nav navbar-nav ml-auto">
                <li id="menu1" class="menu_web nav-item dropdown active ">
                  <a href="{{base_url}}{{url.menu1}}" class="dropdown-toggle nav-link" data-toggle="" role="button"
                    aria-haspopup="true" aria-expanded="false">{{menu.menu1}}</a>
                </li>
                <li id="menu2" class="menu_web nav-item dropdown megaDropMenu ">
                  <a href="{{base_url}}{{url.menu2}}" class="nav-link" data-toggle="" data-hover="" data-delay="300"
                    data-close-others="true" aria-expanded="false">{{menu.menu2}}</b></a>
                </li>
                <li id="menu3" class="menu_web nav-item dropdown megaDropMenu ">
                  <a href="{{base_url}}{{url.menu3}}" class="nav-link" data-toggle="" data-hover="" data-delay="300"
                    data-close-others="true" aria-expanded="false">{{menu.menu3}}</b></a>
                </li>
                <li id="menu4" class="menu_web nav-item dropdown megaDropMenu ">
                  <a href="{{base_url}}{{url.menu4}}" class="nav-link" data-toggle="" data-hover="" data-delay="300"
                    data-close-others="true" aria-expanded="false">{{menu.menu4}}</b></a>
                </li>
                <li id="menu5" class="menu_web nav-item dropdown megaDropMenu ">
                  <a href="{{base_url}}{{url.menu5}}" class="dropdown-toggle nav-link" data-toggle="" data-hover=""
                    data-delay="300" data-close-others="true" aria-expanded="false">{{menu.menu5}}</b></a>
                </li>
                <li class="nav-item dropdown megaDropMenu ">
                  <div id="idioma" name="idioma" class="esta_espaniol " title="Versión en Inglés"
                    ng-click="cambio_idioma()">
                    <img id="bandera" class="img-flag" src="<?=base_url();?>/assets/web/img/flags/uk.png">
                  </div>
                </li>
              </ul>
            </div><!-- /.navbar-collapse -->
            <div class="version2">
              <div class="dropdown cart-dropdown">
                <a href="javascript:void(0)" class="dropdown-toggle shop-cart" data-toggle="dropdown">
                  <i class="fa fa-shopping-cart"></i>
                  <span class="badge"><?=$cuantos?></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                  <li>{{menu.menu_items}}</li>
                  <?php foreach ($productos as $clave => $valor) { ?>
                  <li>
                    <a>
                      <div class="media">
                        <img class="media-left media-object" src="<?=base_url().'/'.$valor->ruta?>" alt="cart-Image">
                        <div class="media-body">
                          <h5 class="media-heading">
                            <?=$valor->titulo ?> <br>
                            <div>
                              Talla: <?=$valor->talla?>
                            </div>
                            <div>
                              Color: <?=ucfirst(strtolower($valor->color))?>
                            </div>
                            <span>$<?=$valor->precio ?></span>
                          </h5>
                        </div>
                      </div>
                    </a>
                  </li>
                  <?php }?>
                  <li>
                    <div class="btn-group" role="group" aria-label="...">
                      <!--
                      <button type="button" class="btn btn-defau  lt"
                        onclick="location.href='';">{{menu.menu_carritos}}</button>
                      <button type="button" class="btn btn-default"
                        onclick="location.href='';">{{menu.menu_productos}}</button>
                      -->
                      <a href="<?=base_url().$ir_cart?>" target="_self"> 
                        <button type="button" class="btn btn-default">
                          {{menu.menu_carritos}}
                        </button>
                      </a>
                      <a href="<?=base_url().$ir_orden?>" target="_self">    
                        <button type="button" class="btn btn-default">{{menu.menu_productos}}
                        </button>
                      </a>    
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </nav>
      </div>
      <!-- Fin del header -->



      <!-- Modal de registro -->
      <?php
      $data['google']=$this->google->get_login_url();
      $data['facebook'] =  $this->facebook->login_url();
      ?>
      <div class="modal fade text-left" id="defaultSize" tabindex="-1" role="dialog" aria-labelledby="myModalLabel18"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="myModalLabel18"><i class="fa"></i>{{menu.titulo_inicio_sesion}}
              </h4>

            </div>
            <div class="modal-body">
              <br>
              <div class="action-buttons mt-2 text-center">

                <!-- Tab panes -->
                <div class="tab-content">

                  <div class="tab-pane " id="login_email" role="tabpanel"
                    ng-class="{'active':currentTab === 'login_email'}">
                    <div class=" ">
                      <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                          <div class="form-group">
                            <div class="controls">
                              <input type="text" name="correo_inicio1" id="correo_inicio1"
                                class="form-control form-control-line" placeholder="{{menu.correo}}"
                                ng-model="registro.correo" onKeyPress="return valida(event,this,5,100)"
                                onBlur="valida2(this,5,100);">
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                          <div class="form-group">
                            <div class="controls">
                              <input type="password" name="clave_inicio1" id="clave_inicio1"
                                class="form-control form-control-line" placeholder="{{menu.clave}}"
                                ng-model="registro.clave" onKeyPress="return valida(event,this,2,8)"
                                onBlur="valida2(this,2,8);">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center ">
                          <div class="nav-item" ng-click="iniciarUsuario()">
                            <a class="col-12 btn  btn-primary super-btn btn-rounded centrar-div"><i
                                class=""></i>{{menu.iniciar}}</a>
                          </div>
                        </div>
                      </div>
                      <div class="row margin20">
                        <!--<div class="col-xs-6 col-sm-6 col-md-6 m-t-10 text-center">
                          <div class="nav-item" ng-class="{'active': currentTab === 'registro_email'}"
                            ng-click="currentTab = 'registro_email';vaciarDatos();">
                            <a class="col-12 btn  btn-info super-btn btn-rounded centrar-div"><i
                                class=""></i>{{menu.registrarse_email}}</a>
                          </div>
                        </div>-->
                        <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                          <div class="nav-item" ng-class="{'active': currentTab === 'olvide_clave'}"
                            ng-click="currentTab = 'olvide_clave';vaciarDatos();">
                            <a class="col-12 btn  btn-warning super-btn btn-rounded centrar-div"><i
                                class=""></i>{{menu.contrasena}}</a>
                          </div>
                        </div>
                      </div>
                      <div class="row margin20">
                        <div class="marco"></div>
                        <div class="col-xs-6 col-sm-6 col-md-6 m-t-10 text-center" href="<?=$data['facebook']?>">
                          <a href="<?=$data['facebook']?>" class=" col-12 btn  btn-facebook btn-rounded centrar-div texto-btn"><i
                              class="fa fa-facebook iconos-is"></i>{{menu.incio_facebook}}</a>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 m-t-10 ">
                          <a href="<?=$data['google']?>" class="col-12  btn btn-google btn-rounded centrar-div texto-btn"><i
                              class="fa fa-google iconos-is"></i>{{menu.incio_google}}</a>
                        </div>
                        <div class="marco"></div>
                      </div>
                      <!-- -->
                      <div class="row margin20">
                          <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center texto-registrar">
                              <div class="nav-item" ng-class="{'active': currentTab === 'registro_email'}"
                                ng-click="currentTab = 'registro_email';vaciarDatos();">
                                {{menu.no_tienes_cuenta}}
                                <a class="link_registro">{{menu.registrarse_email}}</a>
                              </div>
                          </div>
                      </div>    
                      <!-- -->
                    </div>
                    <div class="col-lg-12 margin20">
                      <div id="campo_mensaje_inicio" name="campo_mensaje_inicio"></div>
                    </div>
                  </div>
                  <div class="tab-pane " id="olvide_clave" role="tabpanel"
                    ng-class="{'active':currentTab === 'olvide_clave'}">
                    <div class="">
                      <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                          <div class="form-group">
                            <div class="controls">
                              <input type="text" name="correo_cambio" id="correo_cambio"
                                class="form-control form-control-line" placeholder="{{menu.correo}}"
                                ng-model="registro.correo" onKeyPress="return valida(event,this,5,100)"
                                onBlur="valida2(this,5,100);">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                          <div class="nav-item" ng-click="cambiarClave();">
                            <a class="col-12 btn  btn-primary super-btn btn-rounded centrar-div"><i
                                class=""></i>{{menu.change_clave}}</a>
                          </div>
                        </div>
                      </div>
                      <div class="row margin20">
                        <div class="col-xs-6 col-sm-6 col-md-6 m-t-10 text-center">
                          <div class="nav-item" ng-class="{'active': currentTab === 'registro_email'}"
                            ng-click="currentTab = 'registro_email';vaciarDatos();">
                            <a class="col-12 btn  btn-info super-btn btn-rounded centrar-div"><i
                                class=""></i>{{menu.registrarse_email}}</a>
                          </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 m-t-10 text-center">
                          <div class="nav-item" ng-class="{'active': currentTab === 'login_email'}"
                            ng-click="currentTab = 'login_email';vaciarDatos();">
                            <a class="col-12 btn btn-warning super-btn btn-rounded centrar-div"><i
                                class=""></i>{{menu.todas_opciones2}}</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 margin20">
                        <div id="campo_mensaje_clave" name="campo_mensaje_clave"></div>
                      </div>
                    </div>

                  </div>
                  <div class="tab-pane " id="registro_email" role="tabpanel"
                    ng-class="{'active':currentTab === 'registro_email'}">
                    <div class="">
                      <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                          <div class="form-group">
                            <div class="controls">
                              <input type="text" name="nombre" id="nombre" class="form-control form-control-line"
                                ng-model="registro.nombre" placeholder="{{menu.nombres_apellidos}}"
                                onKeyPress="return valida(event,this,4,100)" onBlur="valida2(this,4,100);">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                          <div class="form-group">
                            <div class="controls">
                              <input type="text" name="correo" id="correo" class="form-control form-control-line"
                                placeholder="{{menu.correo}}" ng-model="registro.correo"
                                onKeyPress="return valida(event,this,5,100)" onBlur="valida2(this,5,100);">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                          <div class="form-group">
                            <div class="controls">
                              <input type="password" name="clave" id="clave" class="form-control form-control-line"
                                placeholder="{{menu.clave}}" ng-model="registro.clave"
                                onKeyPress="return valida(event,this,2,8)" onBlur="valida2(this,2,8);">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                          <div class="form-group">
                            <div class="controls">
                              <input type="password" name="r_clave" id="r_clave" class="form-control form-control-line"
                                placeholder="{{menu.clave_confirm}}" ng-model="registro.r_clave"
                                onKeyPress="return valida(event,this,2,8)" onBlur="valida2(this,2,8);">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                          <div ng-click="registrarUsuario();">
                            <a class=" col-12 btn btn-primary super-btn btn-rounded centrar-div"><i
                                class=""></i>{{menu.registro}}</a>
                          </div>
                        </div>
                      </div><br>
                      <div class="row">
                      <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                        <div ng-click="currentTab = 'login_email';vaciarDatos();">
                          <a class="col-12 btn btn-info  super-btn btn-rounded centrar-div"><i
                              class=""></i>{{menu.todas_opciones}}</a>
                        </div>
                        </div>
                        </div>
                    </div><br>
                    <div class="col-lg-12">
                      <div id="campo_mensaje_registro" name="campo_mensaje_registro"></div>
                    </div>
                  </div>
                  <div class="tab-pane " id="aviso_codigo" role="tabpanel"
                    ng-class="{'active':currentTab === 'aviso_codigo'}">
                    <div class="">
                      <img alt="" ng-src="{{base_url}}/assets/images/icono-correo.png"
                        class="img-responsive img-circle profile-image"><br><br>
                      <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                        <div class="form-group">
                          <h4 class="">{{menu.envio_activacion}}</h4>
                          <div class="controls">
                          </div>
                        </div><br>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane " id="aviso_clave" role="tabpanel"
                    ng-class="{'active':currentTab === 'aviso_clave'}">
                    <div class="">
                      <img alt="" ng-src="{{base_url}}/assets/images/icono-correo.png"
                        class="img-responsive img-circle profile-image"><br><br>
                      <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                        <div class="form-group">
                          <h4 class="">{{menu.envio_cambio_contra}}</h4>
                          <div class="controls">
                          </div>
                        </div><br>
                      </div>
                    </div>
                  </div>
                </div>


              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Fin Modal para editar -->




      <div class="modal fade text-left" id="aviso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel18"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="myModalLabel18"><i class="fa"></i>{{<menu class="titulo_inicio_sesion"></menu>}}
              </h4>

            </div>
            <div class="modal-body">
              <br>
              <div class="action-buttons mt-2 text-center">

                <!-- Tab panes -->
                <div class="tab-content">

                  <div class="tab-pane " id="login_email" role="tabpanel"
                    ng-class="{'active':currentTab === 'login_email'}">
                    <div class=" ">

                      <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                        <div class="form-group">
                          <div class="controls">
                            <input type="text" name="correo_inicio2" id="correo_inicio2"
                              class="form-control form-control-line" placeholder="{{menu.correo}}"
                              ng-model="registro.correo" onKeyPress="return valida(event,this,5,100)"
                              onBlur="valida2(this,5,100);">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                        <div class="form-group">
                          <div class="controls">
                            <input type="password" name="clave_inicio2" id="clave_inicio2"
                              class="form-control form-control-line" placeholder="{{menu.clave}}"
                              ng-model="registro.clave" onKeyPress="return valida(event,this,2,100)"
                              onBlur="valida2(this,2,100);">
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center ">
                        <div class="nav-item" ng-click="iniciarUsuario()">
                          <a class="col-12 btn  btn-email btn-rounded centrar-div"><i class=""></i>{{menu.iniciar}}</a>

                        </div>
                      </div>
                      <div class="row margin20">
                        <div class="col-xs-6 col-sm-6 col-md-6 m-t-10 text-center">
                          <div class="nav-item" ng-class="{'active': currentTab === 'registro_email'}"
                            ng-click="currentTab = 'registro_email';vaciarDatos();">
                            <a class="col- btn  btn-email btn-rounded centrar-div"><i
                                class=""></i>{{menu.registrarse_email}}</a>
                          </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 m-t-10 text-center">
                          <div class="nav-item" ng-class="{'active': currentTab === 'olvide_clave'}"
                            ng-click="currentTab = 'olvide_clave';vaciarDatos();">
                            <a class="col- btn  btn-email btn-rounded centrar-div"><i
                                class=""></i>{{menu.contrasena}}</a>
                          </div>
                        </div>
                      </div>
                      <div class="row margin20">
                        <div class="col-xs-6 col-sm-6 col-md-6 m-t-10 text-center" href="<?=$data['facebook']?>">
                          <a href="<?=$data['facebook']?>" class=" col-12 btn  btn-facebook btn-rounded centrar-div"><i
                              class="cofa fa-facebook iconos-is"></i>Facebook</a>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 m-t-10">
                          <a href="<?=$data['google']?>" class="col-12  btn btn-google btn-rounded centrar-div "><i
                              class="fa fa-google iconos-is"></i>Google login</a>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-12 margin20">
                      <div id="campo_mensaje_inicio" name="campo_mensaje_inicio"></div>
                    </div>



                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Fin Modal para editar -->