<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class WebProductos extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('session');
      $this->load->model('WebProductos_model');
      $this->load->model('WebInicio_model');
      $this->load->model('Auditoria_model');
      $this->load->helper('meses');
      $this->load->library('google');
      $this->load->library('facebook');
    }

    public function index($idioma=1){
        $data = [
            'url' => $_SERVER['REQUEST_URI'],
            'localhost' => 'http://localhost'
        ];
        $this->session->set_userdata($data);
        $datos["idioma"] = $idioma;
        //-------------------------------------------------------
        $datos_menu["productos"] = $this->consultarCarritoUsuario($datos["idioma"]);
        $datos_menu["cuantos"] = count($datos_menu["productos"]);
        if($datos["idioma"] =="1"){
            $datos_menu["ir_cart"] ="carrito";
            $datos_menu["ir_orden"] ="orden_usuario";  
        }else{
            $datos_menu["ir_cart"] ="cart";
            $datos_menu["ir_orden"] ="order_us"; 
        }
        //-------------------------------------------------------
        $this->load->view('web/header');
        $this->load->view('web/menu',$datos_menu);
        $this->load->view('web/productos',$datos);
        $this->load->view('web/footer');
    }
    /*
    *
    */
    public function consultarCarritoUsuario($id_idioma){
        $listado = [];
        if ($this->session->userdata("web")) {
            $web = $_SESSION["web"]; 
            $id_suario = $web["id"];
            $datos = array(
                                "id_idioma"=>$id_idioma,
                                "id_usuario"=>$id_suario
            );
            $respuesta = $this->WebInicio_model->consultarCarrito($datos);
            foreach ($respuesta as $clave_carrito => $valor_carrito) {
                $registro_img = $this->WebInicio_model->consultarimg_sola("",$valor_carrito->id_producto,$valor_carrito->id_cantidad_producto);
                //var_dump($registro_img);die('');
                $arreglo_registro = $registro_img[0] ;
                //--Patch si idioma es 2 para que traiga los valores referentes al idioma
                if($id_idioma==2){
                    //--Titulo en ingles
                    $arreglo_titulo =  $this->WebInicio_model->consultar_ingles($arreglo_registro->id);
                    //--Color en ingles
                    $arreglo_color = $this->WebInicio_model->consultar_ingles_color($arreglo_registro->id_color);

                    $arreglo_registro->titulo = $arreglo_titulo[0]->titulo;
                    $arreglo_registro->color = $arreglo_color[0]->titulo_color;
                }
                //var_dump($registro_img);die('');
                $listado[] = $arreglo_registro;
            }
        }
        //---
        return $listado;
        //---

    }
    /*
    *
    */
    public function verProductoDetalle($idioma=1,$slug_producto){
        //print_r($slug_producto);die;
        $datos["idioma"] = $idioma;
        $data = [
            'url' => $_SERVER['REQUEST_URI'],
            'localhost' => 'http://localhost'
        ];
        $this->session->set_userdata($data);
        //-------------------------------------------------------
        $datos_menu["productos"] = $this->consultarCarritoUsuario($datos["idioma"]);
        $datos_menu["cuantos"] = count($datos_menu["productos"]);
        if($datos["idioma"] =="1"){
            $datos_menu["ir_cart"] ="carrito";
            $datos_menu["ir_orden"] ="orden";  
        }else{
            $datos_menu["ir_cart"] ="cart";
            $datos_menu["ir_orden"] ="order"; 
        }
        //-------------------------------------------------------
        $datos["slug_producto"] = $slug_producto;
        $this->load->view('web/header');
        $this->load->view('web/menu',$datos_menu);
        $this->load->view('web/detalle_producto',$datos);
        $this->load->view('web/footer');
    }

    public function consultarproductos_todos(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebProductos_model->consultarproductos($datos);
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            $img = $this->WebProductos_model->consultarimg($value->id);

            $ruta_imagen = "";
            $precio = "";
            $titulo = "";
                foreach ($img as $campo1) {
                    $valor1 = $campo1;
                    $id_producto = $valor1->id_det_prod;
                    $capturar_id = $valor1->imagen;
                    $separar_id = explode(",", $capturar_id);
                    $id_final = $separar_id[0];

                    $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                    foreach ($una_img as $value2) {
                        $ruta_imagen = $value2->ruta;
                        $precio =  $value2->precio;
                        $titulo =  $value2->titulo;
                    }
                }
                $valor->ruta = $ruta_imagen;
                $valor->precio = $precio;
                $valor->titulo = $titulo;
            $res[] = $valor;
        }
       
        $listado = (object)$res;
        die(json_encode($listado));
    }

    public function consultarmas_productos(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        //$id_recibido = $datos['id_producto'];
        $respuesta = $this->WebProductos_model->consultarmasProductos($datos);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }
    //--
    #Consultar productos segun filtro titulo
    public function consultar_producto_titulo(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebProductos_model->consultar_producto_titulo($datos);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }
    //--
    public function consultar_linea(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebProductos_model->consultar_linea($datos);
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $res[] = $valor;
            }
        $listado = (object)$res;
        die(json_encode($listado));
    }

    public function consultar_generos(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebProductos_model->consultar_generos($datos);

            foreach ($respuesta as $key => $value) {
                $valor = $value;
                //$valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
                $res[] = $valor;
            }
        $listado = (object)$res;
        die(json_encode($listado));
    }

    public function consultar_color(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebProductos_model->consultar_color($datos);

            foreach ($respuesta as $key => $value) {
                $valor = $value;
                //$valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
                $res[] = $valor;
            }
        $listado = (object)$res;
        die(json_encode($listado));
    }

    public function consultar_talla(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebProductos_model->consultar_talla($datos);
        //print_r($respuesta);die;
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                //$valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
                $res[] = $valor;
            }
        $listado = (object)$res;
        
        die(json_encode($listado));
    }

    public function consultar_img_linea(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebProductos_model->consultar_img_linea($datos);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }

    public function consultar_img_genero(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebProductos_model->consultar_img_genero($datos);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }

    public function consultar_img_color(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebProductos_model->consultar_img_color($datos);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }

    public function consultar_img_talla(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebProductos_model->consultar_img_talla($datos);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }

    public function consultar_img_precio(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebProductos_model->consultar_img_precio($datos);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }

    //X LINEA Y GENERO
    public function consultar(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $id_idioma = $datos['id_idioma'];
        $id_linea = $datos['id_buscar_linea'];
        $id_genero = $datos['id_buscar_genero'];
        $respuesta = $this->WebProductos_model->consultar_val1($id_idioma, $id_linea,$id_genero);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }

    //X LINEA Y PRECIO
    public function consultarfiltro2(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        //print_r($datos);die;
        $id_idioma = $datos['id_idioma'];
        $id_linea = $datos['id_buscar_linea'];
        $id_precio1 = $datos['id_buscar_precio1'];
        $id_precio2 = $datos['id_buscar_precio2'];
        $respuesta = $this->WebProductos_model->consultar_FIL2($id_idioma, $id_linea,$id_precio1, $id_precio2);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }

    //GENERO Y PRECIO
    public function consultarfiltro3(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $id_idioma = $datos['id_idioma'];
        $id_genero = $datos['id_buscar_genero'];
        $id_precio1 = $datos['id_buscar_precio1'];
        $id_precio2 = $datos['id_buscar_precio2'];
        $respuesta = $this->WebProductos_model->consultar_FIL3($id_idioma, $id_genero,$id_precio1, $id_precio2);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }

    //LINEA Y COLOR
    public function consultarfiltro4(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        //print_r($datos);die;
        $id_idioma = $datos['id_idioma'];
        $id_linea = $datos['id_buscar_linea'];
        $id_color = $datos['id_buscar_color'];
        $respuesta = $this->WebProductos_model->consultar_FIL4($id_idioma, $id_linea, $id_color);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }

    //LINEA Y TALLA
    public function consultarfiltro5(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $id_idioma = $datos['id_idioma'];
        $id_linea = $datos['id_buscar_linea'];
        $id_talla = $datos['id_buscar_talla'];
        $respuesta = $this->WebProductos_model->consultar_FIL5($id_idioma, $id_linea, $id_talla);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }



    //GENERO Y COLOR
    public function consultarfiltro6(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        //print_r($datos);die;
        $id_idioma = $datos['id_idioma'];
        $id_genero = $datos['id_buscar_genero'];
        $id_color = $datos['id_buscar_color'];
        $respuesta = $this->WebProductos_model->consultar_FIL6($id_idioma, $id_genero, $id_color);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }

    //GENERO Y TALLA
    public function consultarfiltro7(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        //print_r($datos);die;
        $id_idioma = $datos['id_idioma'];
        $id_genero = $datos['id_buscar_genero'];
        $id_talla = $datos['id_buscar_talla'];
        $respuesta = $this->WebProductos_model->consultar_FIL7($id_idioma, $id_genero, $id_talla);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }

    //COLOR Y PRECIO
    public function consultarfiltro8(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $id_idioma = $datos['id_idioma'];
        $id_color = $datos['id_buscar_color'];
        $id_precio1 = $datos['id_buscar_precio1'];
        $id_precio2 = $datos['id_buscar_precio2'];
        $respuesta = $this->WebProductos_model->consultar_FIL8($id_idioma, $id_color,$id_precio1, $id_precio2);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }

    //TALLA Y PRECIO
    public function consultarfiltro9(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        //print_r($datos);die;
        $id_idioma = $datos['id_idioma'];
        $id_talla = $datos['id_buscar_talla'];
        $id_precio1 = $datos['id_buscar_precio1'];
        $id_precio2 = $datos['id_buscar_precio2'];
        $respuesta = $this->WebProductos_model->consultar_FIL9($id_idioma, $id_talla,$id_precio1, $id_precio2);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }

    //COLOR Y TALLA
    public function consultarfiltro10(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $id_idioma = $datos['id_idioma'];
        $id_color = $datos['id_buscar_color'];
        $id_talla = $datos['id_buscar_talla'];
        $respuesta = $this->WebProductos_model->consultar_FIL10($id_idioma, $id_color, $id_talla);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }
    //LINEA, GENERO y PRECIO
    public function consultar_val2(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        //print_r($datos);die;
        $id_idioma = $datos['id_idioma'];
        $id_linea = $datos['id_buscar_linea'];
        $id_genero = $datos['id_buscar_genero'];
        $id_precio1 = $datos['id_buscar_precio1'];
        $id_precio2 = $datos['id_buscar_precio2'];
        $respuesta = $this->WebProductos_model->consultar_val2($id_idioma, $id_linea,$id_genero, $id_precio1, $id_precio2);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }
    //LINEA, GENERO y COLOR
    public function consultar_val3(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        //print_r($datos);die;
        $id_idioma = $datos['id_idioma'];
        $id_linea = $datos['id_buscar_linea'];
        $id_genero = $datos['id_buscar_genero'];
        $id_color = $datos['id_buscar_color'];
        $respuesta = $this->WebProductos_model->consultar_val3($id_idioma, $id_linea,$id_genero, $id_color);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }
    //LINEA, GENERO y TALLA
    public function consultar_val4(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        //print_r($datos);die;
        $id_idioma = $datos['id_idioma'];
        $id_linea = $datos['id_buscar_linea'];
        $id_genero = $datos['id_buscar_genero'];
        $id_talla = $datos['id_buscar_talla'];
        $respuesta = $this->WebProductos_model->consultar_val4($id_idioma, $id_linea,$id_genero, $id_talla);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }
    //LINEA, PRECIO y COLOR
    public function consultar_val5(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        //print_r($datos);die;
        $id_idioma = $datos['id_idioma'];
        $id_linea = $datos['id_buscar_linea'];
        $id_color = $datos['id_buscar_color'];
        $id_precio1 = $datos['id_buscar_precio1'];
        $id_precio2 = $datos['id_buscar_precio2'];
        $respuesta = $this->WebProductos_model->consultar_val5($id_idioma, $id_linea,$id_color, $id_precio1, $id_precio2);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }
    //LINEA, PRECIO y TALLA
    public function consultar_val6(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        //print_r($datos);die;
        $id_idioma  = $datos['id_idioma'];
        $id_linea   = $datos['id_buscar_linea'];
        $id_talla   = $datos['id_buscar_talla'];
        $id_precio1 = $datos['id_buscar_precio1'];
        $id_precio2 = $datos['id_buscar_precio2'];
        $respuesta  = $this->WebProductos_model->consultar_val6($id_idioma, $id_linea,$id_talla, $id_precio1, $id_precio2);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }
    //LINEA, COLOR y COLOR
    public function consultar_val7(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        //print_r($datos);die;
        $id_idioma  = $datos['id_idioma'];
        $id_linea   = $datos['id_buscar_linea'];
        $id_color   = $datos['id_buscar_color'];
        $id_talla   = $datos['id_buscar_talla'];
        $respuesta  = $this->WebProductos_model->consultar_val7($id_idioma, $id_linea,$id_color,$id_talla);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }
    //LINEA, PRECIO y TALLA
    public function consultar_val9(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        //print_r($datos);die;
        $id_idioma  = $datos['id_idioma'];
        $id_genero  = $datos['id_buscar_genero'];
        $id_precio1 = $datos['id_buscar_precio1'];
        $id_precio2 = $datos['id_buscar_precio2'];
        $id_talla   = $datos['id_buscar_talla'];
        $respuesta  = $this->WebProductos_model->consultar_val9($id_idioma,$id_genero,$id_precio1,$id_precio2,$id_talla);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }
    //PRECIO, COLOR y TALLA
    public function consultar_val10(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        //print_r($datos);die;
        $id_idioma  = $datos['id_idioma'];
        $id_precio1 = $datos['id_buscar_precio1'];
        $id_precio2 = $datos['id_buscar_precio2'];
        $id_color   = $datos['id_buscar_color'];
        $id_talla   = $datos['id_buscar_talla'];
        $respuesta  = $this->WebProductos_model->consultar_val10($id_idioma,$id_precio1,$id_precio2,$id_color,$id_talla);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }
    //LINEA, GENERO y PRECIO Y COLOR
    public function consultar_val11(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        //print_r($datos);die;
        $id_idioma  = $datos['id_idioma'];
        $id_linea   = $datos['id_buscar_linea'];
        $id_genero   = $datos['id_buscar_genero'];
        $id_precio1 = $datos['id_buscar_precio1'];
        $id_precio2 = $datos['id_buscar_precio2'];
        $id_color   = $datos['id_buscar_color'];

        $respuesta  = $this->WebProductos_model->consultar_val11($id_idioma,$id_linea,$id_genero,$id_precio1,$id_precio2,$id_color);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }
    //LINEA, GENERO y PRECIO Y COLOR
    public function consultar_val12(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        //print_r($datos);die;
        $id_idioma  = $datos['id_idioma'];
        $id_linea   = $datos['id_buscar_linea'];
        $id_genero  = $datos['id_buscar_genero'];
        $id_precio1 = $datos['id_buscar_precio1'];
        $id_precio2 = $datos['id_buscar_precio2'];
        $id_talla   = $datos['id_buscar_talla'];

        $respuesta  = $this->WebProductos_model->consultar_val12($id_idioma,$id_linea,$id_genero,$id_precio1,$id_precio2,$id_talla);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }
    //LINEA, GENERO y PRECIO Y COLOR
    public function consultar_val13(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        //print_r($datos);die;
        $id_idioma  = $datos['id_idioma'];
        $id_linea   = $datos['id_buscar_linea'];
        $id_precio1 = $datos['id_buscar_precio1'];
        $id_precio2 = $datos['id_buscar_precio2'];
        $id_talla   = $datos['id_buscar_talla'];
        $id_color   = $datos['id_buscar_color'];

        $respuesta  = $this->WebProductos_model->consultar_val13($id_idioma,$id_linea,$id_precio1,$id_precio2,$id_talla,$id_color);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }
    //PRECIO, TALLA, COLOR y GENERO
    public function consultar_val14(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        //print_r($datos);die;
        $id_idioma  = $datos['id_idioma'];
        $id_precio1 = $datos['id_buscar_precio1'];
        $id_precio2 = $datos['id_buscar_precio2'];
        $id_talla   = $datos['id_buscar_talla'];
        $id_color   = $datos['id_buscar_color'];
        $id_genero   = $datos['id_buscar_genero'];

        $respuesta  = $this->WebProductos_model->consultar_val14($id_idioma,$id_precio1,$id_precio2,$id_talla,$id_color,$id_genero);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }
    //LINEA,GENERO, PRECIO, COLOR Y TALLA
    public function consultar_val15(){
        $res=[];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        //print_r($datos);die;
        $id_idioma  = $datos['id_idioma'];
        $id_linea   = $datos['id_buscar_linea'];
        $id_genero  = $datos['id_buscar_genero'];
        $id_precio1 = $datos['id_buscar_precio1'];
        $id_precio2 = $datos['id_buscar_precio2'];
        $id_color   = $datos['id_buscar_color'];
        $id_talla   = $datos['id_buscar_talla'];

        $respuesta  = $this->WebProductos_model->consultar_val15($id_idioma,$id_linea,$id_genero,$id_precio1,$id_precio2,$id_color,$id_talla);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }

    public function consultarProducto(){
        $res = [];
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebProductos_model->consultarProductoSlug($datos);
        //print_r($respuesta);die;
        $c = 0;
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            $c++;
            $valor->numero = $c;
            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
            $valor->precio_oculto = $valor->precio;
            $valor->precio = number_format($valor->precio,2);
            //-----------------------------------------------------
            ($value->id_original_clonado==0) ? $idBuscarCantidad=$value->id: $idBuscarCantidad=$value->id_original_clonado;
            $value->cantidad = $this->WebProductos_model->consultarCantidad($idBuscarCantidad);
            //$value->cantidad = 100;
            //-----------------------------------------------------
            $res[] = $valor;
        }
        $listado = (object)$res;
        die(json_encode($listado));
    }
    public function consultarTallasProd(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $id_prod = $datos['id_prod'];
        $es_clonado =  $this->WebProductos_model->consultarClonado($id_prod);
        if($es_clonado[0]->id_original_clonado !=0){
         $id_prod = $es_clonado[0]->id_original_clonado;
            //var_dump($id_prod);die;
        }

        $respuesta = $this->WebProductos_model->consultarTallasProd($id_prod);
        $unicos = array_unique($respuesta, SORT_REGULAR);

        $a = 1;
        foreach ($unicos as $key => $value) {


            $valor = $value;
            //$valor->descripcion_sin_html = strip_tags($value->descripcion);
            //$valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
            
            $res[] = $valor;
            $a++;


        }

        $listado = (object)$res;

        die(json_encode($listado));
    }
    public function consultarColorProd(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);

        $id_prod = $datos['id_prod'];
        $es_clonado =  $this->WebProductos_model->consultarClonado($id_prod);
        if($es_clonado[0]->id_original_clonado !=0){
         $id_prod = $es_clonado[0]->id_original_clonado;
            //var_dump($id_prod);die;
        }
        $respuesta = $this->WebProductos_model->consultarColorProd($id_prod,$datos["talla_prod"]);
        //var_dump($respuesta);die;

        $a = 1;
        foreach ($respuesta as $key => $value) {
            $valor = $value;

            if($es_clonado[0]->id_original_clonado !=0){
                $color_en = $this->WebProductos_model->consultarColorIngles($value->id_color);
                $valor->descripcion_color = $color_en[0]->descripcion_color;
            }

            
            $res[] = $valor;
            $a++;
        }
        $listado = (object)$res;
       // var_dump($listado);die;

        die(json_encode($listado));
    }

    public function consultarCantidadProd(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        //var_dump($datos);die;
        $id_prod = $datos['id_prod'];
        $es_clonado =  $this->WebProductos_model->consultarClonado($id_prod);
        if($es_clonado[0]->id_original_clonado !=0){
             $id_prod = $es_clonado[0]->id_original_clonado;
      
        }
        $respuesta = $this->WebProductos_model->consultarCantidadProd($id_prod,$datos['talla_prod'],$datos['color_prod']);
        
        $a = 1;
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            //$valor->descripcion_sin_html = strip_tags($value->descripcion);
            //$valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
            $res[] = $valor;
            $a++;
        }
        $listado = (object)$res[0];
        die(json_encode($listado));
    }
    
    public function consultarSubProductos(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebProductos_model->consultarSubProductos($datos);
        //print_r($respuesta);die;
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value){
                $valor = $value;
                $cons_img = $this->WebProductos_model->consultarimgdetalleProd($value->id);//CONSULTA SECUNDARIA
                    foreach ($cons_img as $key => $value) {
                        $valor = $value;
                        $res[]=$valor;
                    }
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }
    /*
    *   crearCarrito
    */
    public function existeCarrito($id_usuario){
        $idCarrito = $this->WebProductos_model->consultarCarritoExiste($id_usuario);
        return $idCarrito;
    }
    /*
    *   CrearCArrito
    */
    public function crearCarrito($id_usuario){
        $datosCarrito = array(
                            "id_usuario"=>$id_usuario,
                            "estatus"=>1
        );
        $id_carrito = $this->WebProductos_model->crearCarrito($datosCarrito); 
        return $id_carrito;   
    }
    /*
    *
    */
    public function  agregarCarrito(){
        $mensajes = [];
        $fecha = date("Y-m-d");
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        //var_dump($datos);die('');
        $n_cantidad = $datos['cantidad_total'] - $datos['cantidad'];
        if ($this->session->userdata("web")) {
            $web = $_SESSION["web"];
        }
        $id_usuario = $web["id"];
        $existeCarrito = $this->existeCarrito($id_usuario);
        if($existeCarrito==0){
            $id_carrito = $this->crearCarrito($id_usuario);
        }else{
            $id_carrito = $existeCarrito;
        }
        //---
        if($datos["id_idioma"]==1){
            $id_producto = $datos["id"];
        }else if($datos["id_idioma"]==2){
            $id_producto = $datos["id_producto_clonado"]    ;
        }
        $id_prod = $datos['id'];
        $es_clonado =  $this->WebProductos_model->consultarClonado($id_prod);
        if($es_clonado[0]->id_original_clonado !=0){
         $id_prod = $es_clonado[0]->id_original_clonado;
        }
        $id_cantidad = $this->WebProductos_model->TraeIDcantidad($datos["id_color"],$datos["id_talla"],$id_prod);
        //var_dump($id_cantidad);die;

        $cambio = array(
            "cantidad" => $n_cantidad
        );
        //Debo descomentarlo
        $cambia_cantidad = $this->WebProductos_model->CambiaCantidad($cambio,$id_cantidad[0]->id);


        $datos2 = array(
                            "id_producto"=>$id_producto,
                            "cantidad"=>$datos["cantidad"],
                            "monto"=>$datos["monto_individual"],
                            "monto_total"=>$datos["monto_total"],
                            "fecha"=>$fecha,
                            "id_usuario"=>$id_usuario,
                            "id_idioma"=>$datos["id_idioma"],
                            "id_carrito"=>$id_carrito,
                            "estatus"=>1,
                            "id_cantidad_producto"=>$id_cantidad[0]->id,

        );
        //---
        #Valido si ya existe en el carrito..

        $res_existe_carrito = $this->WebProductos_model->existeEnCarrito($datos2["id_producto"],$datos2["id_carrito"],$datos2["id_cantidad_producto"]);
        if($res_existe_carrito){
            $existe_carrito_cantidad =$res_existe_carrito[0]->cantidad+$datos2["cantidad"];
            $cantidad_existencia = (integer)$datos["cantidad_total"];
            if($existe_carrito_cantidad>$cantidad_existencia){
                #Si existe en el carrito pero la cantidad excede
                $mensajes["mensaje"] = "excede_cantidad";
                die(json_encode($mensajes));
            }else{
                //----
                #Si existe en carrito pero la cantidad no excede
                $nuevo_monto_total = $existe_carrito_cantidad*$datos2["monto"];
                $datos3 = array( "cantidad"=>$existe_carrito_cantidad,"monto_total"=>$nuevo_monto_total );
                $respuesta = $this->WebProductos_model->modificarCantidad2($datos2["id_producto"],$datos2["id_carrito"],$datos3,$datos2["id_cantidad_producto"]);
                if($respuesta==true){
                    $mensajes["mensaje"] = "registro_procesado";

                }else{
                     $mensajes["mensaje"] = "error";
                }
                //die(json_encode($mensajes));
                //----
            }
        }else{
            //---
            $respuesta = $this->WebProductos_model->agregarCarrito($datos2);
            if($respuesta==true){
                $mensajes["mensaje"] = "registro_procesado";
            }else{
                 $mensajes["mensaje"] = "error";
            }
            //-- 
        }
            
        //--Bloque Auditoria
        if($mensajes["mensaje"] == "registro_procesado"){
            //------------------------------------------------------------------------------
            #Consulto los datos de titulo de producto, talla y color
            $datos_producto_detalle = $this->WebProductos_model->consultar_producto_auditoria($id_producto,$id_cantidad[0]->id);
            
            $accion = "Agrego al carrito producto: ".$datos_producto_detalle["0"]->descripcion_producto." Talla:".$datos_producto_detalle["0"]->descripcion_talla." Color:".$datos_producto_detalle["0"]->descripcion_color;
            if ($this->session->userdata("web")) {
                $web = $_SESSION["web"];
            }
            $data_auditoria = array(
                                    "id_usuario"=>$web["id"],
                                    "modulo"=>'2',
                                    "accion"=>$accion,
                                    "ip"=>$this->Auditoria_model->get_client_ip(),
                                    "fecha_hora"=> date("Y-m-d H:i:00")
            );
            $respuesta2 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
            //------------------------------------------------------------------------------
        }
        //--
        die(json_encode($mensajes));
    }
    /*
    *
    */
    public function verCarrito($idioma=1){
        $datos["idioma"] = $idioma;
        //Aqui debo pasarle el id del cliente...
        //$datos = $this->WebProductos_model->consultarCarrito();
        $this->load->view('web/header');
        $this->load->view('web/menu');
        $this->load->view('web/cart',$datos);
        $this->load->view('web/footer');
    }

    public function consultarCarrito($value=''){
        $res = [];
        $acumulador= 0;
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebProductos_model->consultarCarrito($datos);
        foreach ($respuesta as $clave => $valor) {
           $valores = $valor;
           $acumulador = $acumulador+$valores->monto_total;
           $valores->monto_oculto = $valores->monto;
           $valores->monto_total_oculto = $valores->monto_total;
           $valores->monto_individual= number_format($valores->monto,2);
           $valores->monto_total = number_format($valores->monto_total,2);
           //--Para consultar las imagenes
           $cons_img = $this->WebProductos_model->consultarimgdetalleProd($valor->id);
            foreach ($cons_img as $key => $value) {
                $valorImg = $value;
                $res[]=$valorImg;
            }
           $valores->imagenes= $res;
           $listado[] = $valores;
        }
        $listado[0]->monto_global_total_oculto = $acumulador;
        $listado[0]->monto_global_total = number_format($acumulador,2);
        $listado = (object)$respuesta;
        die(json_encode($listado));
    }

    public function eliminar_producto(){
        $mensajes = array("mensaje"=>"");
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $data = array(
                        'estatus' =>  '2',
                    ); 
        $res_eliminar = $this->WebProductos_model->eliminarProducto($datos["id"],$data,$datos["id_cantidad_producto"]);
        if($res_eliminar){
            $mensajes["mensaje"] = "modificacion_procesada";
        }else{
             $mensajes["mensaje"] = "error";
        }
        die(json_encode($mensajes));
    }

    public function modificar_cantidad_carrito_productos(){
        
        $mensajes = array("mensaje"=>"");
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $id = $datos["id"];
        $cantidad = $datos["cantidad"];
        $monto_total = $datos["monto_total"];

        $c = 0;
        
        foreach ($id as $clave => $valor) {
            //armo datos a modificar...
            $data = array(
                            'cantidad' =>  $cantidad[$c],
                            'monto_total'=> $monto_total[$c]
                        ); 
            $res_modificar_cantidad = $this->WebProductos_model->modificarCantidad($valor,$data); 
            $c++;      
        }

        if($c>0){
            $mensajes["mensaje"] = "modificacion_procesada";
        }else{
            $mensajes["mensaje"] = "error";
        }
        die(json_encode($mensajes));
    }
    /*
    *   ver orden
    */
    public function verOrden($idioma=1){
        $datos["idioma"] = $idioma;
        //Aqui debo pasarle el id del cliente...
        //$datos = $this->WebProductos_model->consultarCarrito();
        $this->load->view('web/header');
        $this->load->view('web/menu');
        $this->load->view('web/order',$datos);
        $this->load->view('web/footer');
    }
    /***/
    /*
    *   consultarMasProductos
    */
    public function consultarMasProductos(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        //$id_recibido = $datos['id_producto'];
        $respuesta = $this->WebProductos_model->consultarmasProductos($datos);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $img = $this->WebProductos_model->consultarimg($value->id);
                $ruta_imagen = "";
                $precio = "";
                $titulo = "";
                    foreach ($img as $campo1) {
                        $valor1 = $campo1;
                        $id_producto = $valor1->id_det_prod;
                        $capturar_id = $valor1->imagen;
                        $separar_id = explode(",", $capturar_id);
                        $id_final = $separar_id[0];

                        $una_img = $this->WebProductos_model->consultarimg_sola($id_final,$id_producto);

                        foreach ($una_img as $value2) {
                            $ruta_imagen = $value2->ruta;
                            $precio =  $value2->precio;
                            $titulo =  $value2->titulo;
                        }
                    }
                    $valor->ruta = $ruta_imagen;
                    $valor->precio = $precio;
                    $valor->titulo = $titulo;
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado ,TRUE));
        }else{
            die('null');
        }
    }
    // public function consultarSubProductos(){
    //     $datos= json_decode(file_get_contents('php://input'), TRUE);
    //     $res = [];
    //     $respuesta = $this->WebProductos_model->consultarSubProductos($datos);
    //     //print_r($respuesta);die;
    //     foreach ($respuesta as $key => $value){
    //         $valor = $value;
    //         $cons_img = $this->WebProductos_model->consultarimgdetalleProd($value->id);//CONSULTA SECUNDARIA
    //         $x = 0;
    //         $imagen = "";
    //         $id_imagen = "";
    //         foreach ($cons_img as $campo2){
    //             if ($x==0) {
    //                 $imagen.=$campo2->ruta;
    //                 $id_imagen.=$campo2->id;
    //             }else{
    //                 $imagen.="|".$campo2->ruta;
    //                 $id_imagen.="|".$campo2->id;
    //             }
    //             $x++;
    //         }
    //         $valor1->imagen[]= array('ruta' => $imagen , 'id_imagen'=> $id_imagen);
    //         $res[] = $valor1;
    //     }
    //     $listado = (object)$res;
    //     die(json_encode($listado));
    // }

}
