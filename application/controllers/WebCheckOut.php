<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class WebCheckOut extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('session');
      $this->load->model('WebCart_model');
      $this->load->model('WebCheckOut_model');
      $this->load->model('Auditoria_model');
      $this->load->model('WebProductos_model');
      $this->load->model('WebInicio_model');
      $this->load->helper('meses');
      $this->load->library('google');
      $this->load->library('facebook');
    }
    /*
    *
    */
    public function consultarCarritoUsuario($id_idioma){
        $listado = [];
        if ($this->session->userdata("web")) {
            $web = $_SESSION["web"]; 
            $id_suario = $web["id"];
            $datos = array(
                                "id_idioma"=>$id_idioma,
                                "id_usuario"=>$id_suario
            );
            $respuesta = $this->WebInicio_model->consultarCarrito($datos);
            foreach ($respuesta as $clave_carrito => $valor_carrito) {
                $registro_img = $this->WebInicio_model->consultarimg_sola("",$valor_carrito->id_producto,$valor_carrito->id_cantidad_producto);
                //var_dump($registro_img);die('');
                $arreglo_registro = $registro_img[0] ;
                //--Patch si idioma es 2 para que traiga los valores referentes al idioma
                if($id_idioma==2){
                    //--Titulo en ingles
                    $arreglo_titulo =  $this->WebInicio_model->consultar_ingles($arreglo_registro->id);
                    //--Color en ingles
                    $arreglo_color = $this->WebInicio_model->consultar_ingles_color($arreglo_registro->id_color);

                    $arreglo_registro->titulo = $arreglo_titulo[0]->titulo;
                    $arreglo_registro->color = $arreglo_color[0]->titulo_color;
                }
                //var_dump($registro_img);die('');
                $listado[] = $arreglo_registro;
            }
        }
        //---
        //var_dump($listado);die;
        return $listado;
        //---

    }
    /*
    *   ver orden
    */
    public function verCheckOut($idioma="",$id_carrito,$paymentID,$payerID,$paymentToken){

        $data = [
            'url' => $_SERVER['REQUEST_URI'],
            'localhost' => 'http://localhost'
        ];
        $this->session->set_userdata($data);
        //--
        $datos["idioma"] = $idioma;
        if ($this->session->userdata("web")) {
            $web = $_SESSION["web"];
        }
        $id_usuario = $web["id"];
        //-----------------------------------------------------
        #Genero regitro de la orden--------------------------------------------------
        #Consulto si exite el carrito
        $res_existe_carrito = $this->WebCheckOut_model->consultarCarritoExiste($id_carrito);
        if($res_existe_carrito==0){
            echo "Ocurrio un error el carrito no existe, contacte al administrador del sistema";die('');
        }
        #Consulto los datos del carrito de compra

        $res_info_carrito = $this->WebCheckOut_model->consultarCarritoInfo($id_usuario,$id_carrito);

        $existe_orden = $this->WebCheckOut_model->existeOrden($id_usuario,$id_carrito);
        if ($this->session->userdata("web")) {
            $web = $_SESSION["web"];
        }
        if($existe_orden==0){
            //----------------------
            //Bloque para definir titulos y subtitulos del email
            $arr_mensaje = $this->ArmarMensajeCompra($datos["idioma"]);
            //----------------------------------
            #Registro orden encabezado
            $datosOrdenCompra = $this->WebCheckOut_model->generarOrdenEncabezado($id_usuario,$id_carrito,$paymentID,$payerID,$paymentToken);
            #registro orden detalle
            $res_save_carrito = $this->WebCheckOut_model->generarOrdenDetalle($datosOrdenCompra["id_orden_compra"],$res_info_carrito,$id_carrito);
            $accion="Creación de orden #id:".$datosOrdenCompra["id_orden_compra"]." con los siguientes items: ";
            $tablaProductosEmail = "<table border='1' style='width:100%;border-spacing: 0;'>
                                            <thead>
                                                <tr>
                                                    <td style='padding: 1em;text-align: left;background-color: #b9ca46;font-weight: bold'>".$arr_mensaje["titulo_tabla_producto"]."</td>
                                                    <td style='padding: 1em;text-align: left;background-color: #b9ca46;font-weight: bold'>".$arr_mensaje["cantidad_tabla_producto"]."</td>
                                                    <td style='padding: 1em;text-align: left;background-color: #b9ca46;font-weight: bold'>".$arr_mensaje["monto_tabla_producto"]."</td>
                                                    <td style='padding: 1em;text-align: left;background-color: #b9ca46;font-weight: bold'>".$arr_mensaje["monto_total_producto_tabla_producto"]."</td>
                                                </tr>    
                                            </thead>

                                            <tbody>";
            $montoDef = 0;
            #Disminuyo el inventario en la tabla orden detalle
            foreach ($res_info_carrito as $key => $valor) {
                #Obtengo la cantidad del producto guardado
                $rs_carrito = $this->WebCheckOut_model->consultarCantidad($valor->id_cantidad_producto);
                #Resto la cantidad del carrito al inventario.
                $cantidad_nueva = $rs_carrito[0]->cantidad - $valor->cantidad;

                #Datos a guardar
    			$data = array( 
                    "cantidad" => $cantidad_nueva
                );
                $rs_carrito = $this->WebCheckOut_model->modificarTablaCantidad($valor->id_cantidad_producto,$data);
                #Consulto los datos de titulo de producto, talla y color
                $datos_producto_detalle = $this->WebProductos_model->consultar_producto_auditoria("",$valor->id_cantidad_producto);
                //--ALimento la variable de accion para la auditoria...
                $accion.=" producto: ".$datos_producto_detalle["0"]->descripcion_producto." Talla:".$datos_producto_detalle["0"]->descripcion_talla." Color:".$datos_producto_detalle["0"]->descripcion_color.",";
                //--------------------------------------------------------------
                #Si idioma=="2" busco en la tabla de productos el producto cloneado y armo la descripcion:
                if($datos["idioma"]=="2"){
                    $descripcion_producto_tabla = $this->armarDescripcionProductoIdioma($datos_producto_detalle["0"]->id_producto);
                }else{
                     $descripcion_producto_tabla = $datos_producto_detalle["0"]->descripcion_producto." Talla:".$datos_producto_detalle["0"]->descripcion_talla." Color:".$datos_producto_detalle["0"]->descripcion_color.",";
                }
                //--------------------------------------------------------------
                //---Alimento la estructura de la tabla informativa del email
                $tablaProductosEmail.="
                                                <tr>
                                                    <td style='padding: 1em;text-align: left;'>".$descripcion_producto_tabla."</td>
                                                    <td style='padding: 1em;text-align: center;'>".$valor->cantidad."</td>
                                                    <td style='padding: 1em;text-align: right;'>".number_format($valor->monto,2)."</td>
                                                    <td style='padding: 1em;text-align: right;'>".number_format($valor->monto_total,2)."</td>    
                                                </tr>";  
                $montoDef = $montoDef + $valor->monto_total;          
                // ------------- final del fore
            }
            //---
            #Pie de paghina de tabla de email
            $tablaProductosEmail.=" <tr>
                                        <td></td>
                                        <td></td>
                                        <td style='font-weight:bold;text-align:center'>Mo".$arr_mensaje["monto_total"]."</td>
                                        <td style='padding: 1em;text-align: right;background-color:#ebebeb;font-weight:bold'>".number_format($montoDef,2)."</td>
                                    </tr>

                </tbody>
                </table>";
            //---
            //-----------------------------------------------
            #Realizo el envío de email
            //-----------------------------------------------
            $usuario = "info@zougzoug.net";
            $clave="zougzoug.181931";
            $correo_remitente = $usuario;
            $destinatario = trim(strtolower($web['correo']));
            
            $this->load->library('email');
            
            //$config['protocol'] = 'smtp';
            $config['smtp_host'] = 'localhost';
            $config['smtp_user'] = $usuario;
            $config['smtp_pass'] = $clave;
            $config['smtp_port'] = 25;
            $config['charset'] = 'utf-8';
            $config['mailtype'] = 'html';
            $config['wordwrap'] = TRUE;

            $this->email->initialize($config);

            $nombre_remitente = "WebMaster-Zougzoug";
            
            
            $this->email->from($correo_remitente, $nombre_remitente);

            $this->email->to($destinatario);
            
            $titulo = $arr_mensaje["orden_compra"];
            $asunto = $arr_mensaje["orden_compra"]." #".$datosOrdenCompra["numero_orden_compra"]." zougzoug.net";
            //--Armo la variable mensaje
            

            $mensaje =  "<html>
                         <head>
                            <title>".$arr_mensaje["orden_compra"]." #".$datosOrdenCompra["numero_orden_compra"]."</title>
                         </head>
                         <body>
                            <p style='font-weight:bold'>".$arr_mensaje["su_cuenta"]."</p>
                            <p style='font-weight:bold'>Email: </p><p>".$web['correo']."</p>
                            <p style='font-weight:bold'>".$arr_mensaje["id_paypal_cliente_texto"]."</p><p>".$payerID."</p>
                            <p style='font-weight:bold'>".$arr_mensaje["id_pago_paypal_texto"]."</p><p>".$paymentID."</p>
                            <p style='font-weight:bold'>".$arr_mensaje["token_pago_texto"]."</p><p>".$paymentToken."</p>
                            <br><br>
                            ".$tablaProductosEmail."
                            <p>".$arr_mensaje["info_link"]."</p><h4><a href='".base_url()."' target='_blank'>zougzoug.net</a></h4>
                         </body>
                         </html>"; 
            //---
            $this->email->subject($asunto);
            $this->email->message($mensaje);
            //---
            #Envio el email
            $respuesta_email = $this->email->send();
            //var_dump($respuesta_email);
            //--Fin d eenvio de email
            //-----------------------------------------------
            //--Bloque Auditoria
            if ($this->session->userdata("web")) {
                $web = $_SESSION["web"];
            }
            $data_auditoria = array(
                                    "id_usuario"=>$web["id"],
                                    "modulo"=>'2',
                                    "accion"=>$accion,
                                    "ip"=>$this->Auditoria_model->get_client_ip(),
                                    "fecha_hora"=> date("Y-m-d H:i:00")
            );
            $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
            //$res_cambio_inv = $this->WebCheckOut_model->CambiarInvOrden($datosOrdenCompra["id_orden_compra"],$res_info_carrito,$id_carrito);
            //----------------------------------
        }else{
            $datosOrdenCompra = $this->WebCheckOut_model->consultarOrden($id_carrito);
        }
        
        
        $datos["numero_orden_compra"] = $datosOrdenCompra["numero_orden_compra"];
        $datos["correo"] = $web["correo"];
        $datos["id_pago_paypal"] = $paymentID;
        $datos["id_cliente_paypal"] = $payerID;
        $datos["token_pago_paypal"] = $paymentToken;
        //----
        $datos_menu["productos"] = $this->consultarCarritoUsuario($datos["idioma"]);
        $datos_menu["cuantos"] = count($datos_menu["productos"]);
        if($datos["idioma"] =="1"){
            $datos_menu["ir_cart"] ="carrito";
            $datos_menu["ir_orden"] ="orden_usuario";  
        }else{
            $datos_menu["ir_cart"] ="cart";
            $datos_menu["ir_orden"] ="order_us"; 
        }
       

        //----
        $this->load->view('web/header');
        $this->load->view('web/menu',$datos_menu);
        $this->load->view('web/checkout',$datos);
        $this->load->view('web/footer');
    }
    //---
    #Armar mensaje email
    public function ArmarMensajeCompra($idioma){
        $arr_mensaje = [];
            if($idioma=="1"){
                $arr_mensaje = array(
                                        "titulo_tabla_producto"=>"Producto",
                                        "cantidad_tabla_producto"=>"Cantidad",
                                        "monto_tabla_producto"=>"Monto",
                                        "monto_total"=>"Monto total",
                                        "monto_total_producto_tabla_producto"=>"Monto total por producto",
                                        "orden_compra"=>"Orden de compra",
                                        "su_cuenta"=>"Su cuenta",
                                        "id_paypal_cliente_texto"=>"Id Paypal Cliente:",
                                        "id_pago_paypal_texto"=>"Id Pago Paypal:",
                                        "token_pago_texto"=>"Token Pago:",
                                        "info_link"=>"Puede ver el historial de sus transacciones a traves de nuestra'página web",
                );
            }else{
                $arr_mensaje = array(
                                         "titulo_tabla_producto"=>"Product",
                                         "cantidad_tabla_producto"=>"Count",
                                         "monto_tabla_producto"=>"Mount",
                                          "monto_total"=>"Total Mount:",
                                         "monto_total_producto_tabla_producto"=>"Total mount for product",
                                         "orden_compra"=>"Purchase order",
                                         "su_cuenta"=>"You Account",
                                         "id_paypal_cliente_texto"=>"Id Paypal Client:",
                                         "id_pago_paypal_texto"=>"Id Pay Paypal:",
                                         "token_pago_texto"=>"Token:",
                                         "info_link"=>"You can see the history of your transactions through our website",
                );
            }
            return $arr_mensaje;
    }
    //---
    public function armarDescripcionProductoIdioma($id_producto){
            $detProducto = $this->WebCheckOut_model->consultarClonadoEmail($id_producto);
            $arrColores = $this->WebCheckOut_model->consultarColoresIngles($detProducto[0]->id_color);
            ///
            $tituloProducto = $detProducto[0]->producto." Size:".$detProducto[0]->talla." Color:".$arrColores[0]->descripcion_colores;
            return $tituloProducto;
    }
    //---
}    