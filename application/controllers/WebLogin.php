<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class WebLogin extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->database();
			$this->load->library('session');
            $this->load->model('Social_login_model');
            $this->load->library('google');
            $this->load->library('facebook');
            $this->load->model('WebInicio_model');
        }
		public function registrarPersonas(){
            $datos= json_decode(file_get_contents('php://input'), TRUE);      
            $_SESSION['url'] = str_replace("/zoug_zoug/", "", $_SESSION['url']);       
            $existe = $this->Social_login_model->existe($datos['correo']);
           if(!$existe){

            $set = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $code = substr(str_shuffle($set), 0, 12);
            //cambiar estatus del nuevo usuario a 2
			$data = array(
			  'nombre_apellido' => $datos['nombre'],
			  'correo' => trim(strtolower($datos['correo'])),
              'clave' => sha1($datos['clave']),
              'codigo' => $code,
              'tp_login' => '1',
              'estatus' => '2'
            );

            $respuesta = $this->Social_login_model->registro($data);
            $id = $this->Social_login_model->buscarID($code);
              //var_dump($data,$datos,$id['id']);die;
            $data1 = array(
                'id_usuario' => $id['id'],
                'id_identificador' => sha1($id['id']),              
                'estatus' => '2'
                );
            $respuesta = $this->Social_login_model->guardar_codigo($data1);
           
            if($datos["idioma"]==1){
                $link = "registro";
            }else{
                $link = "register";
            }
            $arr_mensaje = $this->armarMensaje($datos["idioma"]);
            $mensaje ="<html>
                            <head>
                                <title>".$arr_mensaje["titulo_verificacion"]."</title>
                            </head>
                            <body>
                                <h2>".$arr_mensaje["gracias_verificacion"]."</h2>
                                <p>".$arr_mensaje["tucuenta_verificacion"]."</p>
                                <p>Email: ".$datos['correo']."</p>
                                <p>Password: ".$datos['clave']."</p>
                                <p>".$arr_mensaje["mensaje_link"]."</p>
                                <h4><a href='".base_url().$link."/".sha1($id['id'])."/".$code."'>".$arr_mensaje["activar_mi_cuenta"]."</a></h4>
                            </body>
                        </html>";
          
            //-------------------------------------------------------------
            $usuario = "info@zougzoug.net";
            $clave="zougzoug.181931";
            $correo_remitente = $usuario;
            $destinatario = trim(strtolower($datos['correo']));
            
            $this->load->library('email');
            
            //$config['protocol'] = 'smtp';
            $config['smtp_host'] = 'localhost';
            $config['smtp_user'] = $usuario;
            $config['smtp_pass'] = $clave;
            $config['smtp_port'] = 25;
            $config['charset'] = 'utf-8';
            $config['mailtype'] = 'html';
            $config['wordwrap'] = TRUE;

            $this->email->initialize($config);

            $nombre_remitente = "WebMaster-Zougzoug";
            
            
            $this->email->from($correo_remitente, $nombre_remitente);

            $this->email->to($destinatario);
            
            $titulo = $arr_mensaje["verificacion_usuario"];
            $asunto = $arr_mensaje["verificacion_usuario"]." zougzoug.net";
            
            $this->email->subject($asunto);
            $this->email->message($mensaje);
            //---
            $respuesta_email = $this->email->send();
            if(!$respuesta_email){
                $mensajes["mensaje"] = "error_mail";
            }else{
                if($respuesta==true){
                    $mensajes["mensaje"] = "registro_procesado";
                }else{
                    $mensajes["mensaje"] = "no_registro";
                }
            }
            //---
			}
			if($existe){
    			$mensajes["mensaje"] = "existe";}
    			die(json_encode($mensajes));
    		}
        //---
         public function armarMensaje($id_idioma){
            $arr_mensaje = [];
            if($id_idioma=="1"){
                $arr_mensaje = array(
                                        "titulo_verificacion"=>"Código de verificación",
                                        "gracias_verificacion"=>"Gracias por registrarte.", 
                                        "tucuenta_verificacion"=>"Su cuenta:",
                                        "mensaje_link"=>"Por favor haga clic en el enlace de abajo para activar su cuenta.",
                                        "activar_mi_cuenta"=>"Activar mi cuenta",
                                        "verificacion_usuario"=>"Verificación de usuario:",             
                );
            }else{
                $arr_mensaje = array(
                                         "titulo_verificacion"=>"Verification Code",
                                         "gracias_verificacion"=>"Thanks for register.",
                                         "tucuenta_verificacion"=>"Your account:",
                                         "mensaje_link"=>"Please click the link below to activate your account.",
                                         "activar_mi_cuenta"=>"Activate my account
",
                                         "verificacion_usuario"=>"User verification:",            
                );
            }
            return $arr_mensaje;
        }
        //---    
		public function inicioSesion(){
            $_SESSION['url'] = str_replace("/zoug_zoug/", "", $_SESSION['url']);
            $datos= json_decode(file_get_contents('php://input'), TRUE); 
                $respuesta = $this->Social_login_model->inicioSesion(trim(strtolower($datos['correo'])),sha1($datos['clave']));
                if($respuesta){
                $mensajes["mensaje"] = "inicio_exitoso";
                $data = array(
                  'web' => array( 
                    'login' => true,
                    'id' => $respuesta[0]->id,
                    'nombre_persona' => substr($respuesta[0]->nombre_apellido,0,30),
                    'correo' => $respuesta[0]->correo,
                    'estatus' => $respuesta[0]->estatus,
                    'logueado' =>true
                   )
                );    
                $this->session->set_userdata($data);              
                }
                else{
                    $res1 = $this->Social_login_model->buscarCorreo(trim(strtolower($datos['correo'])));
                    $res2 = $this->Social_login_model->buscarInactivo(trim(strtolower($datos['correo'])),sha1($datos['clave']));
                    if($res1){
                        $mensajes["mensaje"] = "no_clave";
                    }else if($res2){
                        $mensajes["mensaje"] = "inactivo";
                    }else{
                        $mensajes["mensaje"] = "no_existe";
                    }
                }
                die(json_encode($mensajes));
        }
        public function olvide_clave(){
            $datos= json_decode(file_get_contents('php://input'), TRUE); 
            $correo=trim(strtolower($datos['correo']));
            $existe = $this->Social_login_model->buscarUser($correo);
           if($existe){
            //--------------------------------------------------------------------
            #Cambio realizado por Gianni Santucci
            #Si el correo a cambiar es de tipo 2, no debe permitir recuperar clave
            if($existe["tp_login"]==2){
                $mensajes["mensaje"] = "no_puede_recupe";
                die(json_encode($mensajes));
            }
            //--------------------------------------------------------------------

            $set = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $code = substr(str_shuffle($set), 0, 12);
			$data = array(
              'codigo' => $code,
              'estatus' => '1'
            );
            $respuesta = $this->Social_login_model->codigo_contraseña($data,sha1($existe["id"]));
            /*$this->load->library('email');
            $config = array(
                'protocol' => 'mail', // 'mail', 'sendmail', or 'smtp'
                'smtp_host' => 'smtp.example.com', 
                'smtp_port' => 465,
                'smtp_user' => 'oswaldorivas2011315@gmail.com',
                'smtp_pass' => 'raringan12',
                'smtp_crypto' => 'ssl', //can be 'ssl' or 'tls' for example
                'mailtype' => 'html', //plaintext 'text' mails or 'html'
                'smtp_timeout' => '4', //in seconds
                'charset' => 'iso-8859-1',
                'wordwrap' => TRUE
            );*/
            if($datos["idioma"]==1){
                $link = "recuperar_clave";
            }else{
                $link = "password_recovery";
            }
            $arr_mensaje_cv = $this->armarMensajeCodVerificacion($datos["idioma"]);
            $mensaje =  "<html>
                         <head>
                            <title>".$arr_mensaje_cv["titulo_codigo_verificacion"]."</title>
                         </head>
                         <body>
                            <h2>".$arr_mensaje_cv["cambio_clave"]."</h2>
                            <p>".$arr_mensaje_cv["su_cuenta"]."</p>
                            <p>Email: ".$datos['correo']."</p>
                            <p>".$arr_mensaje_cv["link_cambio_clave"]."</p>
                            <h4><a href='".base_url().$link."/".sha1($existe["id"])."/".$code."'>".$arr_mensaje_cv["activar_mi_cuenta"]."</a></h4>
                         </body>
                         </html>"; 

            //-------------------------------------------------------------
            $usuario = "info@zougzoug.net";
            $clave="zougzoug.181931";
            $correo_remitente = $usuario;
            $destinatario = trim(strtolower($datos['correo']));
            
            $this->load->library('email');
            
            //$config['protocol'] = 'smtp';
            $config['smtp_host'] = 'localhost';
            $config['smtp_user'] = $usuario;
            $config['smtp_pass'] = $clave;
            $config['smtp_port'] = 25;
            $config['charset'] = 'utf-8';
            $config['mailtype'] = 'html';
            $config['wordwrap'] = TRUE;

            $this->email->initialize($config);

            $nombre_remitente = "WebMaster-Zougzoug";
            
            
            $this->email->from($correo_remitente, $nombre_remitente);

            $this->email->to($destinatario);
            
            $titulo = $arr_mensaje_cv["titulo_verificacion_cv"];
            $asunto = $arr_mensaje_cv["titulo_verificacion_cv"]." zougzoug.net";
            
            $this->email->subject($asunto);
            $this->email->message($mensaje);
            //---
                $respuesta_email = $this->email->send();
                if(!$respuesta_email){
                    $mensajes["mensaje"] = "error_mail";
                }else{
                    if($respuesta==true){
                        $mensajes["mensaje"] = "registro_procesado";
                    }else{
                        $mensajes["mensaje"] = "no_registro";
                    }
                }
            //---
			}
			if(!$existe){
			$mensajes["mensaje"] = "no_existe";}
			die(json_encode($mensajes));
        }
        //----------------------------------------------------------
         public function armarMensajeCodVerificacion($id_idioma){
            $arr_mensaje = [];
            if($id_idioma=="1"){
                $arr_mensaje = array(
                                        "titulo_codigo_verificacion"=>"Cambio de contraseña",
                                        "cambio_clave"=>"Se ha solicitado un cambio de contraseña.",
                                        "su_cuenta"=>"Su cuenta:",
                                        "link_cambio_clave"=>"Por favor haga clic en el enlace de abajo para cambiar la contraseña.",
                                        "activar_mi_cuenta"=>"Activar mi cuenta",
                                        "titulo_verificacion_cv"=>"Cambio de contraseña:",
                );
            }else{
                $arr_mensaje = array(
                                         "titulo_codigo_verificacion"=>"Change password",
                                         "cambio_clave"=>"A password change has been requested.", 
                                         "su_cuenta"=>"Your account",
                                         "link_cambio_clave"=>"Please click the link below to change the password.",
                                          "activar_mi_cuenta"=>"Activate my account",
                                          "titulo_verificacion_cv"=>"Change password",

                );
            }
            return $arr_mensaje;
        }
        //--- 
        //----------------------------------------------------------
        public function confirm_account_en($identificador,$codigo){
            $data = [
                'url' => $_SERVER['REQUEST_URI'],
                'localhost' => 'http://localhost'
            ];
            $this->session->set_userdata($data);
            $res = $this->Social_login_model->identificar_usuario($identificador);
            $id = $res["id_usuario"];
            $datos["datos"] = $this->Social_login_model->identificar($id,$codigo);
            $datos["idioma"] = 2;
            //-------------------------------------------------------------------------
            $datos_menu["productos"] = $this->consultarCarritoUsuario($datos["idioma"]);
            $datos_menu["cuantos"] = count($datos_menu["productos"]);
            if($datos["idioma"] =="1"){
                $datos_menu["ir_cart"] ="carrito";
                $datos_menu["ir_orden"] ="orden_usuario";  
            }else{
                $datos_menu["ir_cart"] ="cart";
                $datos_menu["ir_orden"] ="order_us"; 
            }
            //---------------------------------------------------------------------------

            $this->load->view('web/header');
            $this->load->view('web/menu',$datos_menu);
            $this->load->view('web/confirm_account',$datos);
            $this->load->view('web/footer');

        }   
        public function confirm_account_es($identificador,$codigo){
            $data = [
                'url' => $_SERVER['REQUEST_URI'],
                'localhost' => 'http://localhost'
            ];
            $this->session->set_userdata($data);
            $res = $this->Social_login_model->identificar_usuario($identificador);
            $id = $res["id_usuario"];
            $datos["datos"] = $this->Social_login_model->identificar($id,$codigo);
            $datos["idioma"] = 1;
            //-------------------------------------------------------------------------
            $datos_menu["productos"] = $this->consultarCarritoUsuario($datos["idioma"]);
            $datos_menu["cuantos"] = count($datos_menu["productos"]);
            if($datos["idioma"] =="1"){
                $datos_menu["ir_cart"] ="carrito";
                $datos_menu["ir_orden"] ="orden_usuario";  
            }else{
                $datos_menu["ir_cart"] ="cart";
                $datos_menu["ir_orden"] ="order_us"; 
            }
            //---------------------------------------------------------------------------
            $this->load->view('web/header');
            $this->load->view('web/menu',$datos_menu);
            $this->load->view('web/confirm_account',$datos);
            $this->load->view('web/footer');
    }
    public function consultarCarritoUsuario($id_idioma){
        $listado = [];
        if ($this->session->userdata("web")) {
            $web = $_SESSION["web"]; 
            $id_suario = $web["id"];
            $datos = array(
                                "id_idioma"=>$id_idioma,
                                "id_usuario"=>$id_suario
            );
            $respuesta = $this->WebInicio_model->consultarCarrito($datos);
            foreach ($respuesta as $clave_carrito => $valor_carrito) {
                $registro_img = $this->WebInicio_model->consultarimg_sola("",$valor_carrito->id_producto,$valor_carrito->id_cantidad_producto);
                //var_dump($registro_img);die('');
                $arreglo_registro = $registro_img[0] ;
                //--Patch si idioma es 2 para que traiga los valores referentes al idioma
                if($id_idioma==2){
                    //--Titulo en ingles
                    $arreglo_titulo =  $this->WebInicio_model->consultar_ingles($arreglo_registro->id);
                    //--Color en ingles
                    $arreglo_color = $this->WebInicio_model->consultar_ingles_color($arreglo_registro->id_color);

                    $arreglo_registro->titulo = $arreglo_titulo[0]->titulo;
                    $arreglo_registro->color = $arreglo_color[0]->titulo_color;
                }
                //var_dump($registro_img);die('');
                $listado[] = $arreglo_registro;
            }
        }
        //---
        //var_dump($listado);die;
        return $listado;
        //---

    }
    public function activar_usuario(){
        $datos= json_decode(file_get_contents('php://input'), TRUE); 
        $user = $this->Social_login_model->identificar($datos["id"],$datos["codigo"]);
        if($user){
            $data = array(
                'id' => $datos["id"],
                'estatus' => '1'
                );
            $respuesta = $this->Social_login_model->activar_user($data);
            if($respuesta==true){
                $mensajes["mensaje"] = "registro_procesado";
            }else{
                $mensajes["mensaje"] = "no_registro";
            }

        }else if(!$user){
            $activado = $this->Social_login_model->verActivacion($datos["id"],$datos["codigo"]);
            if($activado==true){
                $mensajes["mensaje"] = "ya_activado";
            }else{
                $mensajes["mensaje"] = "error";
            }
        } 
        die(json_encode($mensajes));
    }   
    public function recuperar_cuenta_en($identificador,$codigo){
        $data = [
            'url' => $_SERVER['REQUEST_URI'],
            'localhost' => 'http://localhost'
        ];
        
        $this->session->set_userdata($data);
        $res = $this->Social_login_model->identificar_usuario($identificador);
        $datos["id"] = $res["id_usuario"];
        $datos["codigo"] = $codigo;
        $datos["idioma"] = 2;
        //-------------------------------------------------------------------------
        $datos_menu["productos"] = $this->consultarCarritoUsuario($datos["idioma"]);
        $datos_menu["cuantos"] = count($datos_menu["productos"]);
        if($datos["idioma"] =="1"){
            $datos_menu["ir_cart"] ="carrito";
            $datos_menu["ir_orden"] ="orden_usuario";  
        }else{
            $datos_menu["ir_cart"] ="cart";
            $datos_menu["ir_orden"] ="order_us"; 
        }
        //---------------------------------------------------------------------------
        $this->load->view('web/header');
        $this->load->view('web/menu',$datos_menu);
        $this->load->view('web/recovery_password',$datos);
        $this->load->view('web/footer');

    }   
    public function recuperar_cuenta_es($identificador,$codigo){
        $data = [
            'url' => $_SERVER['REQUEST_URI'],
            'localhost' => 'http://localhost'
        ];
        $this->session->set_userdata($data);
        $res = $this->Social_login_model->identificar_usuario($identificador);
        $datos["id"] = $res["id_usuario"];
        $datos["codigo"] = $codigo;
        $datos["idioma"] = 1;
        //-------------------------------------------------------------------------
        $datos_menu["productos"] = $this->consultarCarritoUsuario($datos["idioma"]);
        $datos_menu["cuantos"] = count($datos_menu["productos"]);
        if($datos["idioma"] =="1"){
            $datos_menu["ir_cart"] ="carrito";
            $datos_menu["ir_orden"] ="orden_usuario";  
        }else{
            $datos_menu["ir_cart"] ="cart";
            $datos_menu["ir_orden"] ="order_us"; 
        }
        //---------------------------------------------------------------------------

        $this->load->view('web/header');
        $this->load->view('web/menu',$datos_menu);
        $this->load->view('web/recovery_password',$datos);
        $this->load->view('web/footer');
    }
    public function verificar_cambio(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);  
        $user = $this->Social_login_model->buscar_peticion($datos["id"],$datos["codigo"]);
        if($user){
                $mensajes["mensaje"] = "aprobado";
            }else{
                $mensajes["mensaje"] = "no_aprobado";
            }
        die(json_encode($mensajes));
    }  
    
    public function cambiar_clave(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);     
        $existe = $this->Social_login_model->buscar_peticion($datos["id"],$datos["codigo"]);
        if($existe){
            $data1 = array(
                'id' => $datos["id"],
                'clave' => sha1($datos["clave"])
                );           
                $respuesta = $this->Social_login_model->cambio_clave($data1);
                if($respuesta==true){
                    $data2 = array(
                        'id' => $existe["id_tabla"],
                        'estatus' => '2'
                        );
                    $respuesta = $this->Social_login_model->desactivar_cambio($data2);
                }
            if($respuesta==true){
                $mensajes["mensaje"] = "cambio_logrado";
            }else{
                $mensajes["mensaje"] = "no_cambio";
            }

        }else if(!$existe){
                $mensajes["mensaje"] = "error";
        } 
        die(json_encode($mensajes));
    }  
    public function redirigir_cuenta_es($tp_login){
        $data = [
            'url' => $_SERVER['REQUEST_URI'],
            'localhost' => 'http://localhost'
        ];
        $this->session->set_userdata($data);
        $datos["idioma"] = 1;
        //-------------------------------------------------------------------------
        $datos_menu["productos"] = $this->consultarCarritoUsuario($datos["idioma"]);
        $datos_menu["cuantos"] = count($datos_menu["productos"]);
        if($datos["idioma"] =="1"){
            $datos_menu["ir_cart"] ="carrito";
            $datos_menu["ir_orden"] ="orden_usuario";  
        }else{
            $datos_menu["ir_cart"] ="cart";
            $datos_menu["ir_orden"] ="order_us"; 
        }
        //---------------------------------------------------------------------------
        $datos["tp_login"] =$tp_login;
        $this->load->view('web/header');
        $this->load->view('web/menu',$datos_menu);
        $this->load->view('web/redirigir_cuenta',$datos);
        $this->load->view('web/footer');
    }
    public function redirigir_cuenta_en($tp_login){
        $data = [
            'url' => $_SERVER['REQUEST_URI'],
            'localhost' => 'http://localhost'
        ];
        $this->session->set_userdata($data);
        $datos["idioma"] = 2;
        //-------------------------------------------------------------------------
        $datos_menu["productos"] = $this->consultarCarritoUsuario($datos["idioma"]);
        $datos_menu["cuantos"] = count($datos_menu["productos"]);
        if($datos["idioma"] =="1"){
            $datos_menu["ir_cart"] ="carrito";
            $datos_menu["ir_orden"] ="orden_usuario";  
        }else{
            $datos_menu["ir_cart"] ="cart";
            $datos_menu["ir_orden"] ="order_us"; 
        }
        //---------------------------------------------------------------------------
      $datos["tp_login"] =$tp_login;

        $this->load->view('web/header');
        $this->load->view('web/menu',$datos_menu);
        $this->load->view('web/redirigir_cuenta',$datos);
        $this->load->view('web/footer');
    }
}

?>