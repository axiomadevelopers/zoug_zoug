<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class WebOrdenUSuario extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('session');
      $this->load->model('WebOrdenUsuario_model');
      $this->load->model('WebCart_model');
      $this->load->model('WebInicio_model');
      $this->load->helper('meses');
      $this->load->library('google');
      $this->load->library('facebook');
    }
    /*
    *
    */
    public function consultarCarritoUsuario($id_idioma){
        $listado = [];
        if ($this->session->userdata("web")) {
            $web = $_SESSION["web"]; 
            $id_suario = $web["id"];
            $datos = array(
                                "id_idioma"=>$id_idioma,
                                "id_usuario"=>$id_suario
            );
            $respuesta = $this->WebInicio_model->consultarCarrito($datos);
            foreach ($respuesta as $clave_carrito => $valor_carrito) {
                $registro_img = $this->WebInicio_model->consultarimg_sola("",$valor_carrito->id_producto,$valor_carrito->id_cantidad_producto);
                //var_dump($registro_img);die('');
                $arreglo_registro = $registro_img[0] ;
                //--Patch si idioma es 2 para que traiga los valores referentes al idioma
                if($id_idioma==2){
                    //--Titulo en ingles
                    $arreglo_titulo =  $this->WebInicio_model->consultar_ingles($arreglo_registro->id);
                    //--Color en ingles
                    $arreglo_color = $this->WebInicio_model->consultar_ingles_color($arreglo_registro->id_color);

                    $arreglo_registro->titulo = $arreglo_titulo[0]->titulo;
                    $arreglo_registro->color =  ucfirst(strtolower($arreglo_color[0]->titulo_color));
                }
                //var_dump($registro_img);die('');
                $listado[] = $arreglo_registro;
            }
        }
        //---
        //var_dump($listado);die;
        return $listado;
        //---

    }
    /*
    *   ver orden
    */
    public function verOrdenUsuario($idioma=1){
        $data = [
            'url' => $_SERVER['REQUEST_URI'],
            'localhost' => 'http://localhost'
        ];
        $this->session->set_userdata($data);
        $datos["idioma"] = $idioma;
        if ($this->session->userdata("web")) {
            $web = $_SESSION["web"];
        }
        $datos["id_usuario"] = $web["id"];
        //-------------------------------------------------------------------------
        $datos_menu["productos"] = $this->consultarCarritoUsuario($datos["idioma"]);
        $datos_menu["cuantos"] = count($datos_menu["productos"]);
        if($datos["idioma"] =="1"){
            $datos_menu["ir_cart"] ="carrito";
            $datos_menu["ir_orden"] ="orden_usuario";  
        }else{
            $datos_menu["ir_cart"] ="cart";
            $datos_menu["ir_orden"] ="order_us"; 
        }
        //---------------------------------------------------------------------------
        //Aqui debo pasarle el id del cliente...
        //$datos = $this->WebProductos_model->consultarCarrito();
        $this->load->view('web/header');
        $this->load->view('web/menu',$datos_menu);
        $this->load->view('web/orderUsuario',$datos);
        $this->load->view('web/footer');
    }
    /*
    *
    */
    public function consultarProductosOrden(){
        $res = [];
        $listado = [];
        $acumulador= 0;
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        if ($this->session->userdata("web")) {
            $web = $_SESSION["web"];
        }
        $id_suario = $web["id"];
        //---
        #Consulto la orden
        $ordenEncabezado = $this->WebOrdenUsuario_model->consultarOrden($datos,$id_suario);
        //---
        $acum_monto = 0;
        if(count($ordenEncabezado)>0){
            foreach ($ordenEncabezado as $clave_enc => $valor_enc) {
              //---
              $valores = $valor_enc;
              //--
              $fecha = $valores->fecha;
              $vector_fecha = explode("-",$fecha);
              $super_fecha = $vector_fecha[2]."/".$vector_fecha[1]."/".$vector_fecha[0];
              $valores->fecha =$super_fecha;
              //--
              $respuesta = $this->WebOrdenUsuario_model->consultarCarrito($datos,$valor_enc->id);
              $valores->orden_detalle = $respuesta;
              $res_productos=[];
              foreach ($respuesta as $clave_producto => $valor_producto) {
                  $valores_productos = $valor_producto;
                  $cons_img = $this->WebCart_model->consultarimgdetalleProd($valor_producto->id_producto_real);
                  $acum_monto = $acum_monto+$valor_producto->monto_total;
                  $valor_producto->monto = number_format($valor_producto->monto,2);
                  $valor_producto->monto_total = number_format($valor_producto->monto_total,2);
                  //var_dump($valor_producto);die('xxx');
                //-------------------------------------------------------------
                $detalles = $this->WebCart_model->consultarDetalles
                ($valor_producto->id_cantidad_producto);
                $valor_producto->descripcion_color = ucfirst(strtolower($detalles[0]->descripcion_color));
                $valor_producto->descripcion_talla = ucfirst(strtolower($detalles[0]->descripcion_talla));

                if($datos['id_idioma']==2){
                    //--Color en ingles
                    $arreglo_color = $this->WebInicio_model->consultar_ingles_color($detalles[0]->id_color);
                    $valor_producto->descripcion_color = ucfirst(strtolower($arreglo_color[0]->titulo_color));
                }
                //-------------------------------------------------------------
                  $valores_productos->ruta = $cons_img[0]->ruta;
                  $res_productos[] = $valores_productos;
              }
              $valores->orden_detalle = $res_productos;
              $valores->orden_monto = number_format($acum_monto,2);
              $acum_monto = 0;
              $super_valor[] = $valores;
              //---
            }
            $listado = (object)$super_valor;
        }
        //var_dump($listado);die;
        //---
        /*$respuesta = $this->WebOrdenUsuario_model->consultarCarrito($datos,$id_suario);
        if(count($respuesta)>0){
          //---
          foreach ($respuesta as $clave => $valor) {
             $valores = $valor;
             $acumulador = $acumulador+$valores->monto_total;
             $valores->cantidad_objetos = count($respuesta);
             $valores->monto_oculto = $valores->monto;
             $valores->monto_total_oculto = $valores->monto_total;
             $valores->monto_individual= number_format($valores->monto,2);
             $valores->monto_total = number_format($valores->monto_total,2);
             //--Para consultar las imagenes
             //var_dump($valor->id);die();
             $cons_img = $this->WebCart_model->consultarimgdetalleProd($valor->id_producto_real);
              foreach ($cons_img as $key => $value) {
                  $valorImg = $value;
                  $res[]=$valorImg;
              }
             $valores->imagenes= $res;
             $res = [];
             $listado[] = $valores;
          }
          $listado[0]->monto_global_total_oculto = $acumulador;
          $listado[0]->monto_global_total = number_format($acumulador,2);
          $listado = (object)$respuesta;
          //----
        }*/
       
        die(json_encode($listado));
    }
}    