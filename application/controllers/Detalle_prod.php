<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Detalle_prod extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->database();
			$this->load->library('session');
			$this->load->model('Detalle_prod_model');
			$this->load->model('Auditoria_model');
			$cms = $_SESSION["cms"];
			if (!$cms["login"]) {
				redirect(base_url());
			}
		}

		public function index(){
			//--- Datos de usuario
	        $cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
	        //--
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard',$data);
	        $this->load->view('cpanel/menu',$data);
	        $this->load->view('modulos/productos/detalle_prod');
	        $this->load->view('cpanel/footer');
	    }
	    /*
	    *	Metodo que clona un prodcuto en ingles
	    */
	    public function clonar_detalleProd(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			//var_dump($datos);die;
			$id_producto_original = $datos["id"];
			#Modificacion gsantucci 19-06-2019
			$existe_titulo = $this->Detalle_prod_model->consultarExisteTitulo("",$datos["titulo"]);
			//$existe_codigo = $this->Detalle_prod_model->ExisteCodigo("",$datos["codigo"]);

			if($existe_titulo==0 ){
				//var_dump($existe_codigo,$existe_titulo);die;

				$data = array(
					'titulo' 			=> trim($datos['titulo']),
				 	'descripcion'		=> trim($datos['descripcion']),
					'id_idioma' 		=> $datos['id_idioma'],
					'id_categoria_prod' => $datos['categoria_prod'],
					'id_tipo_prod'		=> $datos['tipo_prod'],
					'id_marca'			=> $datos['marca'],
					/* 'id_color'			=> $datos['color'],
					'id_talla'			=> $datos['talla'], */
					'precio'			=> $datos['precio'],
					'slug' 				=> $this->generarSlug($datos["titulo"]),
					'codigo'			=> $datos['codigo'],

					'id_original_clonado'=>$id_producto_original,
					'clonado'=>true,
			 	);

				/* $cantidad  = $datos['cantidad']; */
				$id_imagen = $datos['id_imagen'];
				$respuesta = $this->Detalle_prod_model->clonarDetalleProd($data,$id_imagen,$id_producto_original);
				if($respuesta==true){
					$mensajes["mensaje"] = "registro_procesado";
					//----------------------------------------------------
					//--Bloque Auditoria 
					$accion = "Clonacion de producto padre-id: ".$id_producto_original; 
					$cms = $_SESSION["cms"];
			        $data_auditoria = array(
			                                "id_usuario"=>(integer)$cms["id"],
			                                "modulo"=>'1',
			                                "accion"=>$accion,
			                                "ip"=>$this->Auditoria_model->get_client_ip(),
			                                "fecha_hora"=> date("Y-m-d H:i:00")
			        );
			        $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
					//-----------------------------------------------------
				}else{
					$mensajes["mensaje"] = "no_registro";
				}
			}
			if($existe_titulo ){
				if($existe_titulo){
					$mensajes["mensaje"] = "titulo";
				}
				
		}
			die(json_encode($mensajes));
		}
		/*
		*
		*/
		public function insertar_detalleProd(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			//var_dump($datos);die;
			#Modificacion gsantucci 19-06-2019
			$existe_titulo = $this->Detalle_prod_model->consultarExisteTitulo("",$datos["titulo"]);
			$existe_codigo = $this->Detalle_prod_model->ExisteCodigo("",$datos["codigo"]);
			//var_dump($existe_codigo);die;

        	if($existe_titulo==0 and $existe_codigo==0){
				$data = array(
					'titulo' 			=> trim($datos['titulo']),
				 	'descripcion'		=> trim($datos['descripcion']),
					'id_idioma' 		=> $datos['id_idioma'],
					'id_categoria_prod' => $datos['categoria_prod'],
					'id_tipo_prod'		=> $datos['tipo_prod'],
					'id_marca'			=> $datos['marca'],
					/* 'id_color'			=> $datos['color'],
					'id_talla'			=> $datos['talla'], */
					'precio'			=> $datos['precio'],
					'codigo'			=> trim(mb_strtoupper($datos['codigo'])),
					'slug' 				=> $this->generarSlug($datos["titulo"]),
			 	);

				/* $cantidad  = $datos['cantidad']; */
				$id_imagen = $datos['id_imagen'];
				$respuesta = $this->Detalle_prod_model->guardardetalleProd($data,$id_imagen);
				if($respuesta==true){
					$mensajes["mensaje"] = "registro_procesado";
					//------------------------------------------------------------
					//--Bloque Auditoria 
					$id = $this->Auditoria_model->consultar_max_id("detalle_productos");
					$accion = "Registro de producto id:".$id.",titulo:".trim($datos['titulo']);  
					$cms = $_SESSION["cms"];         
			        $data_auditoria = array(
			                                "id_usuario"=>(integer)$cms["id"],
			                                "modulo"=>'1',
			                                "accion"=>$accion,
			                                "ip"=>$this->Auditoria_model->get_client_ip(),
			                                "fecha_hora"=> date("Y-m-d H:i:00")
			        );
			        $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
					//------------------------------------------------------------
				}else{
					$mensajes["mensaje"] = "no_registro";
				}
			}if($existe_titulo or $existe_codigo ){
				if($existe_titulo){
					$mensajes["mensaje"] = "titulo";
				}
				if($existe_codigo){

					$mensajes["mensaje"] = "codigo";
				}
				if($existe_titulo and $existe_codigo){
					$mensajes["mensaje"] = "ambos";
				}
		}
			die(json_encode($mensajes));
		}

		public function consultar_detProd(){
			//--- Datos de usuario
	        $cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
	        //--
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard',$data);
	        $this->load->view('cpanel/menu',$data);
	        $this->load->view('modulos/productos/consultar_detProd');
	        $this->load->view('cpanel/footer');
	    }

		public function consultarDetalleProdTodas(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
				 

	        $res = [];
	        $respuesta = $this->Detalle_prod_model->consultardetalleProd($datos);
	        foreach ($respuesta as $key => $value){
				$valor = $value;
				//---------------------------------------
				/* ($value->id_original_clonado==0) ? $idBuscarCantidad=$value->id: $idBuscarCantidad=$value->id_original_clonado;
				$value->cantidad = $this->Detalle_prod_model->consultarCantidad($idBuscarCantidad); */
				//---------------------------------------
				$cons_img = $this->Detalle_prod_model->consultarimgdetalleProd($value->id);//CONSULTA SECUNDARIA
				$x = 0;
				$imagen = "";
				$id_imagen = "";
				foreach ($cons_img as $campo2){
					if ($x==0) {
						$imagen.=$campo2->ruta;
						$id_imagen.=$campo2->id;
					}else{
						$imagen.="|".$campo2->ruta;
						$id_imagen.="|".$campo2->id;
					}
					$x++;
				}
				$valor->imagen[]= array('ruta' => $imagen , 'id_imagen'=> $id_imagen);
				$valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
	            $res[] = $valor;
	        }
			$listado = (object)$res;
			//var_dump($listado);die;
	        die(json_encode($listado));
	    }

		public function detalleProdVer(){
			//--- Datos de usuario
	        $cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
	        //--
			$datos["id"] = $this->input->post('id_detalle_prod');
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard',$data);
	        $this->load->view('cpanel/menu',$data);
	        $this->load->view('modulos/productos/detalle_prod', $datos);
	        $this->load->view('cpanel/footer');
	    }
	    public function detalleProdVerClonar(){
	    	//--- Datos de usuario
	        $cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
	        //--
			$datos["id"] = $this->input->post('id_detalle_prod');
			$datos["clonar"] = "1";
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard',$data);
	        $this->load->view('cpanel/menu',$data);
	        $this->load->view('modulos/productos/detalle_prod', $datos);
	        $this->load->view('cpanel/footer');
	    }
		public function modificar_detalleProd(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			//var_dump($datos);die;
			$existe1 = $this->Detalle_prod_model->consultarExiste($datos["id"]);
			$existe2 = $this->Detalle_prod_model->consultarExisteCodigo($datos["id"],$datos["codigo"],$datos['id_idioma']);

			if($existe1>0 and $existe2>0){
				//--Verifico si existe un registro con ese titulo diferente a ese id
				$existe_titulo = $this->Detalle_prod_model->consultarExisteTitulo($datos["id"],$datos["titulo"]);
        		if($existe_titulo==0){
        			if($datos['id_idioma']==1){
        				//-------------------------------
        				$data = array(
							'id' 				=>  $datos['id'],
							'titulo' 			=> trim($datos['titulo']),
						 	'descripcion' 		=> trim($datos['descripcion']),
							//'id_idioma' 		=> $datos['id_idioma'],
							'id_categoria_prod' => $datos['categoria_prod'],
							'id_tipo_prod'		=> $datos['tipo_prod'],
							'id_marca'			=> $datos['marca'],
							/* 'id_color'			=> $datos['color'],
							'id_talla'			=> $datos['talla'], */
							'precio'			=> $datos['precio'],
							'slug' 				=>$this->generarSlug($datos["titulo"]),
							'codigo'			=> $datos['codigo'],


					 	);
        				//-------------------------------
        			}else{
        				//-------------------------------
        				$data = array(
							'id' 				=>  $datos['id'],
							'titulo' 			=> trim($datos['titulo']),
						 	'descripcion' 		=> trim($datos['descripcion']),
							'slug' 				=>$this->generarSlug($datos["titulo"]),

					 	);
        				//-------------------------------
        			}
					
					/* $cantidad  = $datos['cantidad']; */
					$id_imagen = $datos['id_imagen'];

					$respuesta = $this->Detalle_prod_model->modificarDetalleProd($data,$id_imagen);

					if($respuesta==true){
						$mensajes["mensaje"] = "modificacion_procesada";
						//----------------------------------------------------
						//--Bloque Auditoria 
						$accion = "Actualizacion de producto id: ".$datos['id'];
						$cms = $_SESSION["cms"];      
				        $data_auditoria = array(
				                                "id_usuario"=>(integer)$cms["id"],
				                                "modulo"=>'1',
				                                "accion"=>$accion,
				                                "ip"=>$this->Auditoria_model->get_client_ip(),
				                                "fecha_hora"=> date("Y-m-d H:i:00")
				        );
				        $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
						//-----------------------------------------------------
					}else{
						$mensajes["mensaje"] = "no_registro";
						//print_r($mensajes);die;
					}
				}else{
					$mensajes["mensaje"] = "existe_titulo";
				}
			}
			if($existe2 ) {
				//var_dump($existe2);die;
				 $mensajes["mensaje"] = "existe";
			}
			die(json_encode($mensajes));
		}

		function consultar_talla(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
	        $respuesta = $this->Detalle_prod_model->consultarTallas($datos);
	        die(json_encode($respuesta));
		}

		public function consultar_color(){
	        $res = [];
	        $datos= json_decode(file_get_contents('php://input'), TRUE);
	        $respuesta = $this->Detalle_prod_model->consultarcolor($datos);
	        $a = 1;
	        foreach ($respuesta as $key => $value) {
	            $valor = $value;
	            $res[] = $valor;
	            $a++;
	        }
	        $listado = (object)$res;
	        die(json_encode($listado));
	    }

		public function modificarDetalleEstatus(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			//print_r($datos);die;
			$data = array(
			  'id' =>$datos['id'],
			  'estatus' => $datos['estatus'],
			);
			//--
			//Verificar carrito
			$existe_carrito = $this->Detalle_prod_model->verificarCarrito($datos['id']);
			if($existe_carrito==0){
				$respuesta = $this->Detalle_prod_model->modificarDetalleEstatus($data);
				if($respuesta==true){
					$mensajes["mensaje"] = "modificacion_procesada";
					//----------------------------------------------------
					//--Bloque Auditoria 
					switch ($data["estatus"]) {
						case '0':
							$accion="Inactivar producto: ".$datos['id'];
							break;
						case '1':
							$accion="Activar producto: ".$datos['id'];
							break;
						case '2':
							$accion="Eliminar producto: ".$datos['id'];
							break;
					}
					$cms = $_SESSION["cms"];
			        $data_auditoria = array(
			                                "id_usuario"=>(integer)$cms["id"],
			                                "modulo"=>'1',
			                                "accion"=>$accion,
			                                "ip"=>$this->Auditoria_model->get_client_ip(),
			                                "fecha_hora"=> date("Y-m-d H:i:00")
			        );
			        $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
					//-----------------------------------------------------
				}else{
					$mensajes["mensaje"] = "no_modifico";
				}
			}else{
				$mensajes["mensaje"] = "existe_carrito";
			}
			//--
			

			die(json_encode($mensajes));
		}

		public function generarSlug($cadena){
	        $titulo_min = strtolower($this->normaliza($cadena));
	        $slug_noticias = str_replace(" ","-",$titulo_min);
	        $slug_noticias = preg_replace("/[^a-zA-Z0-9_-]+/", "", $slug_noticias);
	        return $slug_noticias;
	    }

	    public function normaliza ($cadena){
	        $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
	        $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
	        //$cadena = utf8_decode($cadena);
	        //$cadena = strtr($cadena, utf8_decode($originales), $modificadas);
	        $cadena = strtr($cadena, $originales, $modificadas);
	        $cadena = strtolower($cadena);
	        //return utf8_encode($cadena);
	        return $cadena;
	    }
	    /*
	    *	consultarValoresOtroIdioma
	    */
	    public function consultarValoresOtroIdioma(){
	    	$datos= json_decode(file_get_contents('php://input'), TRUE);
	    	$categorias = $datos["categoria"];
	    	$tipo_producto = $datos["tipo_producto"];
	    	$marca = $datos["marca"];
	    	/* $color = $datos["color"];
	    	$talla = $datos["talla"]; */
	    	//----
	    	#Marca producto ///linea producto
	    	//$marca_en = $this->Detalle_prod_model->consultarVAloresOtroIdioma("categoria_producto",$marca);
	    	#marca 
	    	$marca_en = $this->Detalle_prod_model->consultarVAloresOtroIdioma("Marcas",$marca);
	    	#categoria producto ///genero
	    	$categoria_en = $this->Detalle_prod_model->consultarVAloresOtroIdioma("Categoría Productos",$categorias);
	    	#colores 
	    	/* $colores_en = $this->Detalle_prod_model->consultarVAloresOtroIdioma("Colores",$color); */
	    	#tipo_producto  
	    	$tipo_producto = $this->Detalle_prod_model->consultarVAloresOtroIdioma("Tipo Producto",$tipo_producto);
	    	$data = array(
	    					"marca"=> $marca_en["0"]->id_categoria_en,
	    					"categoria"=> $categoria_en["0"]->id_categoria_en,
	    					/* "colores"=>$colores_en["0"]->id_categoria_en, */
	    					"tipo_producto"=>$tipo_producto["0"]->id_categoria_en
	    	);
	    	//----
	    	die(json_encode($data));
	    }
	}

?>
