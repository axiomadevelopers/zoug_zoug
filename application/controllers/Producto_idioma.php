<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Producto_idioma extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->database();
			$this->load->library('session');
			$this->load->model('Producto_idioma_model');
			$cms = $_SESSION["cms"];
if (!$cms["login"]) {				redirect(base_url());
			}
		}

		public function index(){
			//--- Datos de usuario
        	$cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        	//--
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard');
	        $this->load->view('cpanel/menu');
	        $this->load->view('modulos/producto_idioma/producto_prueba');
	        $this->load->view('cpanel/footer');
	    }

		public function registrarColores(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			//print_r($datos);die;
			$existe = $this->Colores_model->existeColores($datos['id'],$datos['descripcion']);
			//var_dump($existe);die;
			if(!$existe){
			$data = array(
			  'descripcion' => trim(mb_strtoupper($datos['descripcion'])),
			  'id_idioma' => $datos['id_idioma'],
			  'estatus' => '1'
              );

				$respuesta = $this->Colores_model->guardarColores($data);
				if($respuesta==true){
					$mensajes["mensaje"] = "registro_procesado";
				}else{
					$mensajes["mensaje"] = "no_registro";
				}
			}
			if($existe){
			$mensajes["mensaje"] = "existe";}
			

			
			die(json_encode($mensajes));
		}

		public function consultar_colores(){
			//--- Datos de usuario
        	$cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        	//--
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard');
	        $this->load->view('cpanel/menu');
	        $this->load->view('modulos/colores/consultar_colores');
	        $this->load->view('cpanel/footer');
	    }

		public function consultarColoresTodos(){
		   //$res = [];

		   $datos= json_decode(file_get_contents('php://input'), TRUE);
		   $respuesta = $this->Colores_model->consultarColores($datos);
		   $a = 1;
		   foreach ($respuesta as $key => $value) {
			   $valor = $value;
			   //$valor->descripcion_sin_html = strip_tags($value->descripcion);
			   //$valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";

			   $res[] = $valor;
			   $a++;
		   }

		   $listado = (object)$res;

		   die(json_encode($listado));
	   }

	   public function coloresVer(){
		   $datos["id"] = $this->input->post('id_color');
		   //--- Datos de usuario
        	$cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        	//--
		   $this->load->view('cpanel/header');
		   $this->load->view('cpanel/dashBoard');
		   $this->load->view('cpanel/menu');
		   $this->load->view('modulos/colores/colores',$datos);
		   $this->load->view('cpanel/footer');
	   }

	   public function modificarColores(){
		   $datos = json_decode(file_get_contents('php://input'), TRUE);

		   //-Verifico si existe una noticia con ese titulo....
		   $existe = $this->Colores_model->consultarExiste($datos["id"]);
			//var_dump($existe);die;
			if(!$existe){
				$data = array(
				 'id' =>  $datos['id'],
				 'descripcion' => trim(mb_strtoupper($datos['descripcion'])),
				 'estatus' => '1'
			   );
			   //var_dump($data);die('');
			   $respuesta = $this->Colores_model->modificarColores($data);
			   if($respuesta==true){
				   $mensajes["mensaje"] = "modificacion_procesada";
			   }else{
				   $mensajes["mensaje"] = "no_registro";
			   }
		   }else{

				$mensajes["mensaje"] = "existe";
		   }
		   //--
		   die(json_encode($mensajes));
	   }

	   public function modificarColoresEstatus(){
		   $datos= json_decode(file_get_contents('php://input'), TRUE);
		   $data = array(
			 'id' =>$datos['id'],
			 'estatus' => $datos['estatus'],
		   );
		   $respuesta = $this->Colores_model->modificarColoresEstatus($data);

		   if($respuesta==true){
			   $mensajes["mensaje"] = "modificacion_procesada";
		   }else{
			   $mensajes["mensaje"] = "no_modifico";
		   }

		   die(json_encode($mensajes));
	   }

	   public function consultarmarca_idioma(){
		   //print_r("hola");
		   $res = [];
		   $datos= json_decode(file_get_contents('php://input'), TRUE);
		   $respuesta = $this->Marcas_model->consultarMarcas_idioma($datos);
		   //print_r($respuesta);die;
		   $a = 1;
		   foreach ($respuesta as $key => $value) {
			   $valor = $value;
			   $res[] = $valor;
			   $a++;
		   }
		   $listado = (object)$res;
		   die(json_encode($listado));
	   }
	   function consultar_color_espanol(){
		$datos= json_decode(file_get_contents('php://input'), TRUE);
		$respuesta = $this->Producto_idioma_model->consultarColorEspanol($datos);
		die(json_encode($respuesta));
	}

	}
?>

