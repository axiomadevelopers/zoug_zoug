<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Nosotros extends CI_Controller{

		function __construct(){
			parent::__construct();
			
			$this->load->library('session');
			$cms = $_SESSION["cms"];
			if (!$cms["login"]) {		        
				redirect(base_url());
		    }
		    $this->load->database();
			$this->load->model('Empresa_nosotros_model');
			$this->load->model('Idiomas_model');
			$this->load->model('Auditoria_model');
		}

		public function index(){
			//--
			//--- Datos de usuario
			$cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
			//--
			$this->load->view('cpanel/header');
			$this->load->view('cpanel/dashBoard',$data);
	        $this->load->view('cpanel/menu',$data);
			$this->load->view('modulos/empresa/nosotros');
			$this->load->view('cpanel/footer');
		}

		public function consultar_nosotros(){
			//--- Datos de usuario
			$cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
			//--
			$this->load->view('cpanel/header');
			$this->load->view('cpanel/dashBoard',$data);
	        $this->load->view('cpanel/menu',$data);
			$this->load->view('modulos/empresa/consultarNosotros');
			$this->load->view('cpanel/footer');
		}

		function consultar_idioma(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
	        $respuesta = $this->Idiomas_model->consultarIdiomas($datos);
	        die(json_encode($respuesta));
		}

		public function insertarNosotros(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			$data = array(
				'somos' => trim($datos['somos']),
				'mision' => trim($datos['mision']),
				'vision' => trim($datos['vision']),
				'id_imagen' => $datos['id_imagen'],
		        'id_idioma' => $datos['id_idioma'],
		        'estatus' => '1'
        	);
			//print_r($data);
			$respuesta = $this->Empresa_nosotros_model->guardar_nosotros($data);
			if($respuesta==true){
				$mensajes["mensaje"] = "registro_procesado";
				//------------------------------------------------------------
					//--Bloque Auditoria 
					if($datos['id_idioma']== 1){
						$idioma = "Español";
					}if($datos['id_idioma']== 2){
						$idioma = "Inglés";
					}
					$id = $this->Auditoria_model->consultar_max_id("empresa_nosotros");
					$accion = "Registro de empresa nosotros id:".$id.",en el idioma:".trim($idioma); 
					$cms = $_SESSION["cms"];           
			        $data_auditoria = array(
			                                "id_usuario"=>(integer)$cms["id"],
			                                "modulo"=>'1',
			                                "accion"=>$accion,
			                                "ip"=>$this->Auditoria_model->get_client_ip(),
			                                "fecha_hora"=> date("Y-m-d H:i:00")
					);
			        $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
				//------------------------------------------------------------
	        }else{
	            $mensajes["mensaje"] = "no_registro";
	        }
	        die(json_encode($mensajes));
		}

		public function modificar_nosotros(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			$data = array(
				'id' => $datos['id'],
				'somos' => trim($datos['somos']),
				'mision' => trim($datos['mision']),
				'vision' => trim($datos['vision']),
				'id_imagen' => $datos['id_imagen'],
		        'id_idioma' => $datos['id_idioma'],
		        'estatus' => '1'
        	);
			//print_r($data);die;
			$existe = $this->Empresa_nosotros_model->existe_nosotros($data['id']);
			//print_r ($existe);die;
			if ($existe == false) {
				$mensajes["mensaje"] = "no_existe";
			}else{
				$respuesta = $this->Empresa_nosotros_model->modificar_nosotros($data);
				//print_r ($respuesta);die;
				if($respuesta!=false){
					$mensajes["mensaje"] = "registro_procesado";
									     //----------------------------------------------------
						//--Bloque Auditoria 
						$accion = "Actualizacion de empresa nosotros id: ".$datos['id']; 
						$cms = $_SESSION["cms"];           
				        $data_auditoria = array(
				                                "id_usuario"=>(integer)$cms["id"],
				                                "modulo"=>'1',
				                                "accion"=>$accion,
				                                "ip"=>$this->Auditoria_model->get_client_ip(),
				                                "fecha_hora"=> date("Y-m-d H:i:00")
				        );
				        $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
					//-----------------------------------------------------
		        }else{
		            $mensajes["mensaje"] = "no_registro";
		        }
		        die(json_encode($mensajes));
			}
		}

		public function consultarNosotrosTodos(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			$respuesta = $this->Empresa_nosotros_model->consultarNosotros($datos);
			foreach ($respuesta as $key => $value) {
	            $valor[] = array(
								"id" => $value->id,
								"id_idioma" => $value->id_idioma,
								"somos" => $value->somos,
								"mision" => $value->mision,
								"vision" => $value->vision,
								"id_imagen" => $value->id_imagen,
								"estatus" => $value->estatus,
								"descripcion_idioma" => $value->descripcion_idioma,
								"ruta" => $value->ruta,
								"somos1" => substr(strip_tags($value->somos),0,150)."...",
								"mision1" => substr(strip_tags($value->mision),0,150)."...",
								"vision1" => substr(strip_tags($value->vision),0,150)."..."
				);
	        }
	        $listado = (object)$valor;
	        die(json_encode($listado));
		}

		public function nosotrosVer(){
			//--- Datos de usuario
			$cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
			//--
			$datos["id"] = $this->input->post('id_nosotros');
			$this->load->view('cpanel/header');
			$this->load->view('cpanel/dashBoard',$data);
	        $this->load->view('cpanel/menu',$data);
			$this->load->view('modulos/empresa/nosotros', $datos);
			$this->load->view('cpanel/footer');
		}

		public function modificarnosotrosEstatus(){
	        $datos= json_decode(file_get_contents('php://input'), TRUE);
	        $data = array(
	          'id' =>$datos['id'],
	          'estatus' => $datos['estatus'],
	        );
	        $respuesta = $this->Empresa_nosotros_model->modificarestatus($data);

	        if($respuesta==true){
				$mensajes["mensaje"] = "modificacion_procesada";
					//----------------------------------------------------
					//--Bloque Auditoria 
					switch ($data["estatus"]) {
						case '0':
							$accion="Inactivar empresa nosotros id: ".$datos['id'];
							break;
						case '1':
							$accion="Activar empresa nosotros id: ".$datos['id'];
							break;
						case '2':
							$accion="Eliminar empresa nosotros id: ".$datos['id'];
							break;
					}
					$cms = $_SESSION["cms"]; 
			        $data_auditoria = array(
			                                "id_usuario"=>(integer)$cms["id"],
			                                "modulo"=>'1',
			                                "accion"=>$accion,
			                                "ip"=>$this->Auditoria_model->get_client_ip(),
			                                "fecha_hora"=> date("Y-m-d H:i:00")
			        );
			        $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
					//-----------------------------------------------------
	        }else{
	            $mensajes["mensaje"] = "no_modifico";
	        }

	        die(json_encode($mensajes));
	    }
	}
?>
