<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class WebNoticias extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('session');
      $this->load->model('WebNoticias_model');
      $this->load->model('WebInicio_model');
      $this->load->helper('meses');
      $this->load->library('google');
      $this->load->library('facebook');
    }

    public function index($idioma=1){

        
        $data = [
            'url' => $_SERVER['REQUEST_URI'],
            'localhost' => 'http://localhost'
        ];
        $this->session->set_userdata($data);


        $datos["idioma"] = $idioma;
        //-------------------------------------------------------
        $datos_menu["productos"] = $this->consultarCarritoUsuario($datos["idioma"]);
        $datos_menu["cuantos"] = count($datos_menu["productos"]);
        if($datos["idioma"] =="1"){
            $datos_menu["ir_cart"] ="carrito";
            $datos_menu["ir_orden"] ="orden_usuario";  
        }else{
            $datos_menu["ir_cart"] ="cart";
            $datos_menu["ir_orden"] ="order_us"; 
        }
        //-------------------------------------------------------
        $this->load->view('web/header');
        $this->load->view('web/menu',$datos_menu);
        $this->load->view('web/noticias',$datos);
        $this->load->view('web/footer');
    }
    /*
    *
    */
    public function consultarCarritoUsuario($id_idioma){
        $listado = [];
        if ($this->session->userdata("web")) {
            $web = $_SESSION["web"]; 
            $id_suario = $web["id"];
            $datos = array(
                                "id_idioma"=>$id_idioma,
                                "id_usuario"=>$id_suario
            );
            $respuesta = $this->WebInicio_model->consultarCarrito($datos);
            foreach ($respuesta as $clave_carrito => $valor_carrito) {
                $registro_img = $this->WebInicio_model->consultarimg_sola("",$valor_carrito->id_producto,$valor_carrito->id_cantidad_producto);
                //var_dump($registro_img);die('');
                $arreglo_registro = $registro_img[0] ;
                //--Patch si idioma es 2 para que traiga los valores referentes al idioma
                if($id_idioma==2){
                    //--Titulo en ingles
                    $arreglo_titulo =  $this->WebInicio_model->consultar_ingles($arreglo_registro->id);
                    //--Color en ingles
                    $arreglo_color = $this->WebInicio_model->consultar_ingles_color($arreglo_registro->id_color);

                    $arreglo_registro->titulo = $arreglo_titulo[0]->titulo;
                    $arreglo_registro->color = $arreglo_color[0]->titulo_color;
                }
                //var_dump($registro_img);die('');
                $listado[] = $arreglo_registro;
            }
        }
        //---
        //var_dump($listado);die;
        return $listado;
        //---

    }
    /*
    *
    */
    public function consultarNoticias(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebNoticias_model->consultarNoticias($datos);
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            $fecha = $value->fecha;
            $vector_fecha = explode("-",$fecha);
            $valor->dias = $vector_fecha[2];
            $valor->mes = strtoupper(meses($vector_fecha[1]));
            $valor->fecha = $vector_fecha[2]."/".$vector_fecha[1]."/".$vector_fecha[0];
            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
            /***/
            if(($value->usuario=="administrador")&&($datos["id_idioma"]=="2")){
                $value->usuario = "administrator";
            }
            /***/
            $res[] = $valor;
        }
        $listado = (object)$res;
        die(json_encode($listado));
    }
    /*
    *   Metodo que carga las noticias del card inferior
    */
    public function consultarSubNoticias(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebNoticias_model->consultarSubNoticias($datos);
        if(count($respuesta)>0){
            foreach ($respuesta as $key => $value) {
                $valor = $value;
                $fecha = $value->fecha;
                $vector_fecha = explode("-",$fecha);
                $valor->dias = $vector_fecha[2];
                $valor->mes = strtoupper(meses($vector_fecha[1]));
                $valor->fecha = $vector_fecha[2]."/".$vector_fecha[1]."/".$vector_fecha[0];
                $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
                /***/
                if(($value->usuario=="administrador")&&($datos["id_idioma"]=="2")){
                    $value->usuario = "administrator";
                }
                /***/
                $res[] = $valor;
            }
            $listado = (object)$res;
            die(json_encode($listado));
        }else{
            die('null');
        }
        
    }
     /*
    * Para ver el detalle de la noticia
    */
    public function verNoticiaDetalle($idioma=1,$slug_noticia){
        $datos["idioma"] = $idioma;
        $datos["slug_noticia"] = $slug_noticia;
        //-------------------------------------------------------
        $datos_menu["productos"] = $this->consultarCarritoUsuario($datos["idioma"]);
        $datos_menu["cuantos"] = count($datos_menu["productos"]);
        if($datos["idioma"] =="1"){
            $datos_menu["ir_cart"] ="carrito";
            $datos_menu["ir_orden"] ="orden";  
        }else{
            $datos_menu["ir_cart"] ="cart";
            $datos_menu["ir_orden"] ="order"; 
        }
        //-------------------------------------------------------
        $this->load->view('web/header');
        $this->load->view('web/menu',$datos_menu);
        $this->load->view('web/noticiasDetalles',$datos);
        $this->load->view('web/footer');
    }
    /*
    *   Consulta el slug de la noticia
    */
    public function consultarNoticiasSlug(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebNoticias_model->consultarNoticiasSlug($datos);
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            $fecha = $value->fecha;
            $vector_fecha = explode("-",$fecha);
            $valor->dias = $vector_fecha[2];
            $valor->mes = strtoupper(meses($vector_fecha[1]));
            $valor->fecha = $vector_fecha[2]."/".$vector_fecha[1]."/".$vector_fecha[0];
            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
            /***/
            if(($value->usuario=="administrador")&&($datos["id_idioma"]=="2")){
                $value->usuario = "administrator";
            }
            /***/
            $res[] = $valor;
        }
        $listado = (object)$res;
        die(json_encode($listado));
    }
}
