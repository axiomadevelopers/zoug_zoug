<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class ConsultaCarrito extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->database();
			$this->load->library('session');
			$this->load->model('CarritoCompra_model');
			$cms = $_SESSION["cms"];
			if (!$cms["login"]) {	
				        redirect(base_url());
		    }
			//---Valido que solo el admin pueda ingresar
		    if ($cms["id"]!="1") {
		    	$url_error = base_url()."cms/no_tiene_permisos";
		        redirect($url_error);
		    }
		    //---
		}

		public function index(){
			//--- Datos de usuario
	        $cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);

	        //--
			$this->load->view('cpanel/header');
			$this->load->view('cpanel/dashBoard',$data);
	        $this->load->view('cpanel/menu',$data);
			$this->load->view('modulos/consulta_carrito/consulta_carrito');
			$this->load->view('cpanel/footer');
		}
		/*
		*	Metodo para obtener los datos de los contactos consultados
		*/
		public function consultar_carrito_compra(){
			$respuesta = $this->CarritoCompra_model->consultar_carrito_compra();
			foreach ($respuesta as $key => $value) {
				$detal=[];
				$detalles = $this->CarritoCompra_model->consultar_detalles($value->id);
				foreach ($detalles as $individual => $detalles_carrito) {
					$ruta = $this->CarritoCompra_model->consultar_img_prod($detalles_carrito->id_producto);
					$color = $this->CarritoCompra_model->consultar_color($detalles_carrito->id_color);
					$talla = $this->CarritoCompra_model->consultar_talla($detalles_carrito->id_talla);
					//var_dump($color[0]->descripcion);die;

					$vector_fecha = explode("-", $detalles_carrito->fecha);
					$detalles_carrito->fecha = $vector_fecha[2]."/".$vector_fecha[1]."/".$vector_fecha[0];
					
					$detal[] = array(
						"id_producto" => $detalles_carrito->id_producto,
						"cantidad" => $detalles_carrito->cantidad,
						"monto" => "$".number_format((float)$detalles_carrito->monto, 2, '.', ''),
						"monto_total" => "$".number_format((float)$detalles_carrito->monto_total, 2, '.', ''),
						"producto" => $detalles_carrito->producto,
						"id_carrito" => $detalles_carrito->id_carrito,
						"para_sumar" => $detalles_carrito->monto_total,
						"ruta" => $ruta,
						"color" => $color[0]->descripcion,
						"talla" => $talla[0]->descripcion,
					);
				}			
				$conjunto = (object)$detal;
				$total=0;
				foreach ($conjunto as $sumatoria => $contador) {
						$total = $total + $contador['para_sumar'];
					}
				if($value->estatus==2){
					$estatus = "Pasó a orden";
				}else{
					$estatus = "Compra pendiente";
				}
	            $valor[] = array(
								"numero_carrito" => $value->id,
								"usuario" => $value->usuario,
								"fecha" => $detalles_carrito->fecha,
								"total" => "$".number_format((float)$total, 2, '.', ''),
								"detalles" => $conjunto,
								"estatus" => $estatus,


								
				);
	        }
			$listado = (object)$valor;
			//var_dump($listado);die;
	        die(json_encode($listado));
		}
	}	