<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class WebInicio extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('session');
      $this->load->model('Login_model');
      $this->load->model('WebInicio_model');
      $this->load->helper('meses');
      $this->load->library('google');
      $this->load->library('facebook');

    }
    /*
    *   Pantalla de inicio para servidor de produccíon bloquear esto para local
    */
    public function validar_acceso(){
        //------------------------------------------
        if ($this->session->userdata("activo")){
            $this->session->sess_destroy();
        } 
        $data = [
            'url' => $_SERVER['REQUEST_URI'],
            'localhost' => 'http://localhost'
        ];
        $this->session->set_userdata($data);
        $datos["idioma"] = 1;
        $this->load->view('web/header');
        $this->load->view('web/validar_acceso',$datos);
        $this->load->view('web/footer');
        //------------------------------------------ 
    }

    /*
    *   Pantalla de inicio para servidor de produccíon bloquear esto para local
    */
    public function commin_soon(){
        //------------------------------------------
        if ($this->session->userdata("activo")){
            $this->session->sess_destroy();
        } 
        $data = [
            'url' => $_SERVER['REQUEST_URI'],
            'localhost' => 'http://localhost'
        ];
        $this->session->set_userdata($data);
        $datos["idioma"] = 1;
        $this->load->view('web/header');
        $this->load->view('web/coming_soon',$datos);
        $this->load->view('web/footer');
        //------------------------------------------ 
    }
    public function index($idioma=1){
        if ($this->session->userdata("activo")){
            $this->session->sess_destroy();
        } 
        $data = [
            'url' => $_SERVER['REQUEST_URI'],
            'localhost' => 'http://localhost'
        ];
        $this->session->set_userdata($data);
        $datos["idioma"] = $idioma;
        $datos["slider"] = $this->consultarSliderPhp($idioma);
        $datos_menu["productos"] = $this->consultarCarritoUsuario($datos["idioma"]);
        $datos_menu["cuantos"] = count($datos_menu["productos"]);
        if($datos["idioma"] =="1"){
            $datos_menu["ir_cart"] ="carrito";
            $datos_menu["ir_orden"] ="orden_usuario";  
        }else{
            $datos_menu["ir_cart"] ="cart";
            $datos_menu["ir_orden"] ="order_us"; 
        }
        $this->load->view('web/header');
        $this->load->view('web/menu',$datos_menu);
        $this->load->view('web/home',$datos);
        $this->load->view('web/footer');
    }
    
    public function consultarCarritoUsuario($id_idioma){
        $listado = [];
        if ($this->session->userdata("web")) {
            $web = $_SESSION["web"]; 
            $id_suario = $web["id"];
            $datos = array(
                                "id_idioma"=>$id_idioma,
                                "id_usuario"=>$id_suario
            );
            $respuesta = $this->WebInicio_model->consultarCarrito($datos);
            foreach ($respuesta as $clave_carrito => $valor_carrito) {
                $registro_img = $this->WebInicio_model->consultarimg_sola("",$valor_carrito->id_producto,$valor_carrito->id_cantidad_producto);
                $arreglo_registro = $registro_img[0] ;
                //--Patch si idioma es 2 para que traiga los valores referentes al idioma
                if($id_idioma==2){
                    //--Titulo en ingles
                    $arreglo_titulo =  $this->WebInicio_model->consultar_ingles($arreglo_registro->id);
                    //--Color en ingles
                    $arreglo_color = $this->WebInicio_model->consultar_ingles_color($arreglo_registro->id_color);

                    $arreglo_registro->titulo = $arreglo_titulo[0]->titulo;
                    $arreglo_registro->color = $arreglo_color[0]->titulo_color;
                }
                //var_dump($registro_img);die('');
                $listado[] = $arreglo_registro;
            }
        }/*else{
            echo "No ha iniciado sesion!";
        }*/
        //---
        //var_dump($listado);die;
        return $listado;
        //---

    }

    public function consultarSliderPhp($id_idioma=1){
        $res = [];
        $datos["id_idioma"] = $id_idioma;
        $respuesta = $this->WebInicio_model->consultarSlider($datos);
        //var_dump($respuesta);die;
        $numero = 1;
        $numero2 = 0;
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            $calculo_par = $numero%2;

            if($numero=='1'){
                $par='derecha';
            }else{
                ($calculo_par=='0')?$par='derecha':$par="izquierda";
            }
            //$valor->direccion = $value->direccion;
            $valor->url = strtolower($valor->url);
            $valor->par = $par;
            $valor->num = $numero2;
            $numero++;
            $numero2++;
            //$valor->descripcion_sin_html = strip_tags($value->descripcion);
            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
            $res[] = $valor;
        }
        $listado = (object)$res;
        return $listado;
    }

    public function consultarSlider(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebInicio_model->consultarSlider($datos);
        $numero = 1;
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            $calculo_par = $numero%2;

            if($numero=='1'){
                $par='derecha';
            }else{
                ($calculo_par=='0')?$par='derecha':$par="izquierda";
            }

            $valor->par = $par;
            $valor->num = $numero;
            $numero++;

            //$valor->descripcion_sin_html = strip_tags($value->descripcion);
            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
            $res[] = $valor;
        }
        $listado = (object)$res;
        die(json_encode($listado));
    }

    public function consultarNosotros(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebInicio_model->consultarNosotros($datos);
        foreach ($respuesta as $key => $value) {
              $valor[] = array(
                "id" => $value->id,
                "id_idioma" => $value->id_idioma,
                "somos" => $value->somos,
                "mision" => $value->mision,
                "vision" => $value->vision,
                "id_imagen" => $value->id_imagen,
                "estatus" => $value->estatus,
                "descripcion_idioma" => $value->descripcion_idioma,
                "ruta" => $value->ruta,
                "somos1" => strip_tags($value->somos),
                "mision1" => strip_tags($value->mision),
                "vision1" => strip_tags($value->vision)
              );
        }
        $listado = (object)$valor;
        die(json_encode($listado));
    }

    public function registrarContactos(){
        $datos = json_decode(file_get_contents('php://input'), TRUE);
       // var_dump($datos);die('');
        $data = array(
            "nombres" => $datos["nombres"],
            "telefono" => $datos["telefono"],
            "email" => $datos["email"],
            "mensaje" => $datos["mensaje"],
            "id_idioma" => $datos["id_idioma"]
        );
        //$respuesta = $this->WebInicio_model->guardarContactos($data);
        $respuesta=true;
        if($respuesta==true){
            //--
            $correo_respuesta = $this->enviarEmail($data);
            if($correo_respuesta){
                $mensajes["mensaje"] = "registro_procesado";
            }else{
                $mensajes["mensaje"] = "error_correo";
            }
            //--
            //
        }else{
            $mensajes["mensaje"] = "no_registro";
        }
        die(json_encode($mensajes));
    }

    public function enviarEmail($data){
        //---------------------------------
        $usuario = "info@zougzoug.net";
        $clave="zougzoug.181931";
        $correo_remitente = $usuario;
        $destinatario = "gianni.d.santucci@gmail.com";
        
        $this->load->library('email');
        
        //$config['protocol'] = 'smtp';
        $config['smtp_host'] = 'localhost';
        $config['smtp_user'] = $usuario;
        $config['smtp_pass'] = $clave;
        $config['smtp_port'] = 25;
        $config['charset'] = 'utf-8';
        $config['mailtype'] = 'html';
        $config['wordwrap'] = TRUE;

        $this->email->initialize($config);

        $nombre_remitente = "WebMaster-Zougzoug";
        
        
        $this->email->from($correo_remitente, $nombre_remitente);

        $this->email->to($destinatario);
        
        $arr_mensaje = $this->armarMensaje($data);
        $titulo =  $arr_mensaje["titulo"];
        $encabezado = $arr_mensaje["encabezado"];
        $asunto = $arr_mensaje["asunto"];
        $mensaje =  $this->crear_cuadro($titulo,$encabezado,$data,$arr_mensaje);

        
        $this->email->subject($asunto);
        $this->email->message($mensaje);

        return $this->email->send();
        //---------------------------------
    }
    //----
    public function armarMensaje($data){
        $arr_mensaje = [];
        if($data["id_idioma"]=="1"){
            $arr_mensaje = array(
                                    "titulo"=>"Datos de clientes:",
                                    "encabezado"=>$data['nombres']." realizó su registro a través de zougzoug.net:<br>",
                                    "asunto"=>"Mensaje desde formulario contacto zougzoug.net",
                                    "mensaje_cliente_correo"=>"MENSAJE CLIENTE:",
                                    "cliente_correo"=>"Cliente:",
                                    "telefono_correo"=>"Teléfono:",
                                    "email_correo"=>"Email:",
                                    "mensaje_correo"=>"Mensaje",
                                    "pie_pagina"=>"Ingrese en el manejador de contenido para verificar estos datos:",
            );
        }else{
            $arr_mensaje = array(
                                    "titulo"=>"Customer data:",
                                    "encabezado"=>$data['nombres']." made its registration through zougzoug.net:<br>",
                                    "asunto"=>"Message from contact form zougzoug.net",
                                    "mensaje_cliente_correo"=>"CLIENT MESSAGE:",
                                    "cliente_correo"=>"Client:",
                                    "telefono_correo"=>"Telephone:",
                                    "email_correo"=>"Email:",
                                    "mensaje_correo"=>"Message:",
                                    "pie_pagina"=>"Enter the content handler to verify this data:",
            );
        }
        return $arr_mensaje;
    }
    //----
    #Para emails:
    public function crear_cuadro($titulo,$encabezado,$arreglo_datos,$arr_mensaje){

        $estructura_mensaje ="<div stle='margin-top:10px;'>
                                <div style='padding:1%;width:95%;height:auto;background-color:#fff;color:#000;font-weight:bold;font-size:14pt;'>
                                    <style='width:3%'>".$arr_mensaje["mensaje_cliente_correo"]."</div>
                                    <div style='padding:1%;''>
                                        <div style='padding1%;width:97%;height:auto;background-color:#fff;color:#000;font-weight:bold;font-size:12pt;'>".$encabezado."</div><br>
                                        <div style='margin-top:5px;;padding1%;width:95%;height:auto;background-color:#fff;color:#000;font-size:12;'>
                                            <div><label style='font-weight:bold;'>".$arr_mensaje["cliente_correo"]." : </label>".$arreglo_datos["nombres"]."</div>
                                            <div>
                                                <p><label style='font-weight:bold;'> ".$arr_mensaje["telefono_correo"]." </label>".$arreglo_datos["telefono"]."</p>
                                                <p><label style='font-weight:bold;'> ".$arr_mensaje["email_correo"]." </label>".$arreglo_datos["email"]."</p>
                                                <p style='text-align:justify;'><label style='font-weight:bold;'> ".$arr_mensaje["mensaje_correo"]." </label>".$arreglo_datos["mensaje"]."</p>
                                            </div>
                                        <div style='clear:both'></div>  
                                    </div>
                                    <div style=';width:95%;height:auto;background-color:#fff;color:#000;font-size:10pt;'>".$arr_mensaje["pie_pagina"]." zougzoug.net/cms </div>
                            </div>.";                   
        //$estructura_mensaje ="Esto es una prueba de correo".$arreglo_datos["nombres_contacto"]." ".$arreglo_datos["apellidos_contacto"];
        return $estructura_mensaje;                 
    }
    //----
    public function consultarRedes(){
        $datos = json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebInicio_model->consultarRedes($datos);
        $listado = (object)$respuesta;
        die(json_encode($listado));
    }

    public function consultarFooter(){
        $datos = json_decode(file_get_contents('php://input'), TRUE);
        $lineas = [];
        $respuesta = $this->WebInicio_model->consultarFooter($datos);
        //$respuesta["lineas"] = $this->WebInicio_model->consultarLinea($datos);
        $super_lineas = $this->WebInicio_model->consultarLinea($datos);

        foreach ($super_lineas as $clave => $valor) {
           $valor->titulo = strtolower($valor->titulo);
           $lineas[]=$valor;
        }
        $respuesta["lineas"] = $lineas;
        $listado = (object)$respuesta;
        die(json_encode($listado));
    }

    public function consultarproductos(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $res = [];
        $respuesta = $this->WebInicio_model->consultarproductos($datos);
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            $img = $this->WebInicio_model->consultarimg($value->id);
            $ruta_imagen = "";
            $precio = "";
            $titulo = "";
                foreach ($img as $campod) {
                    $valord = $campod;
                    $id_producto = $valord->id_det_prod;
                    $capturar_id = $valord->imagen;
                    $separar_id = explode(",", $capturar_id);
                    $id_final = $separar_id[0];
                    $una_img = $this->WebInicio_model->consultarimg_sola($id_final,$id_producto,'');
                    foreach ($una_img as $value2) {
                        $ruta_imagen = $value2->ruta;
                        $precio =  $value2->precio;
                        $titulo =  $value2->titulo;
                        $slug = $value2->slug;
                    }
                }
            $valor->ruta = $ruta_imagen;
            $valor->precio = $precio;
            $valor->titulo = $titulo;
            $valor->slug = $slug;
            $res[] = $valor;
        }
        $listado = (object)$res;
        die(json_encode($listado));
    }

    public function consultarNoticiasFiltros(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $datos["id_idioma"]  =  (integer)$datos["id_idioma"];
        $respuesta = $this->WebInicio_model->consultarNoticiasFiltros($datos);
      
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            $fecha = $value->fecha;
            $vector_fecha = explode("-",$fecha);
            $valor->dias = $vector_fecha[2];
            $valor->mes = strtoupper(meses($vector_fecha[1]));
            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
            /***/
            if(($value->usuario=="administrador")&&($datos["id_idioma"]=="2")){
                $value->usuario = "administrator";
            }
            /***/
            $res[] = $valor;
        }
        $listado = (object)$res;
        die(json_encode($listado));
    }

    /*
    *   consultarMetaTag
    */
    public function consultarMetaTag(){
        $datos = json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = array("descripcion"=>"","palabras_claves"=>"");
        $arr_descripcion = $this->WebInicio_model->consultarDescripcion();
        $arr_palabras_claves = $this->WebInicio_model->consultarPalabrasClaves();
        
        if(isset($arr_descripcion[0]->descripcion))
             $respuesta["descripcion"]= $arr_descripcion[0]->descripcion;
        
        if(isset($arr_descripcion[0]->palabras_claves)) 
            $respuesta["palabras_claves"]= $arr_palabras_claves[0]->palabras_claves;
        
        //$respuesta["lineas"] = $this->WebInicio_model->consultarLinea($datos);
        $listado = (object)$respuesta;
        die(json_encode($listado));
    }
}
