<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class WebOrden extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('session');
      $this->load->model('WebOrden_model');
      $this->load->model('WebInicio_model');
      $this->load->helper('meses');
      $this->load->library('google');
      $this->load->library('facebook');
    }
    /*
    *
    */
    public function consultarCarritoUsuario($id_idioma){
        $listado = [];
        if ($this->session->userdata("web")) {
            $web = $_SESSION["web"]; 
            $id_suario = $web["id"];
            $datos = array(
                                "id_idioma"=>$id_idioma,
                                "id_usuario"=>$id_suario
            );
            $respuesta = $this->WebInicio_model->consultarCarrito($datos);
            foreach ($respuesta as $clave_carrito => $valor_carrito) {
                $registro_img = $this->WebInicio_model->consultarimg_sola("",$valor_carrito->id_producto,$valor_carrito->id_cantidad_producto);
                //var_dump($registro_img);die('');
                $arreglo_registro = $registro_img[0] ;
                //--Patch si idioma es 2 para que traiga los valores referentes al idioma
                if($id_idioma==2){
                    //--Titulo en ingles
                    $arreglo_titulo =  $this->WebInicio_model->consultar_ingles($arreglo_registro->id);
                    //--Color en ingles
                    $arreglo_color = $this->WebInicio_model->consultar_ingles_color($arreglo_registro->id_color);

                    $arreglo_registro->titulo = $arreglo_titulo[0]->titulo;
                    $arreglo_registro->color = $arreglo_color[0]->titulo_color;
                }
                //var_dump($registro_img);die('');
                $listado[] = $arreglo_registro;
            }
        }
        //---
        //var_dump($listado);die;
        return $listado;
        //---

    }
    /*
    *   ver orden
    */
    public function verOrden($idioma=1){
        $data = [
            'url' => $_SERVER['REQUEST_URI'],
            'localhost' => 'http://localhost'
        ];
        $this->session->set_userdata($data);
        
        $datos["idioma"] = $idioma;
        //-------------------------------------------------------------------------
        $datos_menu["productos"] = $this->consultarCarritoUsuario($datos["idioma"]);
        $datos_menu["cuantos"] = count($datos_menu["productos"]);
        if($datos["idioma"] =="1"){
            $datos_menu["ir_cart"] ="carrito";
            $datos_menu["ir_orden"] ="orden_usuario";  
        }else{
            $datos_menu["ir_cart"] ="cart";
            $datos_menu["ir_orden"] ="order_us"; 
        }
        //---------------------------------------------------------------------------
        //Aqui debo pasarle el id del cliente...
        //$datos = $this->WebProductos_model->consultarCarrito();
        $this->load->view('web/header');
        $this->load->view('web/menu',$datos_menu);
        $this->load->view('web/order',$datos);
        $this->load->view('web/footer');
    }
    /*
    *
    */
    public function ipn(){
        $req = 'cmd=_notify-validate';
        foreach ($_POST as $key => $value) {
            $value = urlencode(stripslashes($value));
            $req .= "&$key=$value";
        }

        //header para el sistema de paypal
        $header = "POST /cgi-bin/webscr HTTP/1.0\r\n";
        $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
        //header para el correo
        $headers = 'From: webmaster@ejemplo.com' . "\r\n" .
        'Reply-To: webmaster@ejemplo.com' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
        //Si estamos usando el testeo de paypal:
        $fp = fsockopen ('ssl://www.sandbox.paypal.com', 443, $errno, $errstr, 30);
        //En caso de querer usar PayPal oficialmente:
        //$fp = fsockopen (‘ssl://www.paypal.com’, 443, $errno, $errstr, 30);
        if (!$fp) {
            // ERROR DE HTTP
            echo "no se ha aiberto el socket<br/>";
        }else{ 
            echo "si se ha abierto el socket<br/>";
            fputs ($fp, $header . $req);
            while (!feof($fp)) {
                $res = fgets ($fp, 1024);
                if (strcmp ($res, "VERIFIED") == 0) {//Almacenamos todos los valores recibidos por $_POST.
                    foreach($_POST as $key => $value){
                        $recibido.= $key." = ". $value."\r\n";
                    }
                }
            }
            fclose ($fp);
        }            
    }
    /***/
}    