<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AdministracionCantidad extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('session');
      $this->load->model('AdministraCantidad_model');
      $this->load->model('Auditoria_model');
      $this->load->model('Categorias_model');
      
      /*$cms = $_SESSION["cms"];
$cms = $_SESSION["cms"];
if (!$cms["login"]) {        redirect(base_url());
      }*/

    }

    public function index(){
        //--- Datos de usuario
        $cms = $_SESSION["cms"];
        $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        //--
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard',$data);
        $this->load->view('cpanel/menu',$data);
        $this->load->view('modulos/administracion_cantidad/administracion_cantidad');
        $this->load->view('cpanel/footer');
    }

		

    
    public function consultaProductos(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->AdministraCantidad_model->consultarProducos($datos);
	
	        foreach ($respuesta as $key => $value) {
				$valor = $value;
				//$valor->descripcion_sin_html = strip_tags($value->descripcion);
				$valor->titulo = substr(strip_tags($value->titulo),0,150);
				$res[] = $valor;
			}
	$listado = (object)$res;
		//var_dump($listado);die;

	die(json_encode($listado));
    }
    public function consultaExistente(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
       // var_dump($datos);die;

        $respuesta = $this->AdministraCantidad_model->consultaExistente($datos);
        if(!$respuesta){
            $listado=0;
        }else{
            $listado=$respuesta[0]->cantidad+0;
        }

	    
	
		//var_dump($listadoie;

	die(json_encode($listado));
    }

    
    public function registrarCantidad(){
        
        $datos= json_decode(file_get_contents('php://input'), TRUE);
            $existe = $this->AdministraCantidad_model->existeProducto($datos['id_producto'],
            $datos['id_color'],$datos['id_talla']);

			if($existe){
                $n_cantidad = $existe[0]->cantidad + $datos['cantidad'];
                if($n_cantidad < 0){
                    $respuesta1= false;    
                }
                $data = array(
                    'id' => $existe[0]->id,
                    'id_det_prod' => $existe[0]->id_det_prod,
                    'cantidad' => $n_cantidad,
                    'id_color' => $existe[0]->id_color,
                    'id_talla' => $existe[0]->id_talla,
                );
                if($n_cantidad >=0){
                    $respuesta1 = $this->AdministraCantidad_model->cambiarCantidad($data);
                    $respuesta1 = $this->AdministraCantidad_model->cambiarCantidadCarrito($data);
                    $respuesta1 = $this->AdministraCantidad_model->cambiarCantidadOrdenes($data);
                }
                if($respuesta1==true){
                    $mensajes["mensaje"] = "cambio_cantidad";
                }else{
                    $mensajes["mensaje"] = "menor_a_cero";
                }
            }
			if(!$existe){
                $data = array(
                    'id' => "",
                    'id_det_prod' => $datos['id_producto'],
                    'cantidad' => $datos['cantidad'],
                    'id_color' => $datos['id_color'],
                    'id_talla' => $datos['id_talla'],
                    'estatus' => 1
                );
                $respuesta2 = $this->AdministraCantidad_model->RegistroCantidad($data);
                $trae_id = $this->AdministraCantidad_model->TraeID($data);
                $data1 = array(
                    'id' => $trae_id[0]->id,
                    'id_det_prod' => $datos['id_producto'],
                    'cantidad' => $datos['cantidad'],
                    'id_color' => $datos['id_color'],
                    'id_talla' => $datos['id_talla'],
                    'estatus' => 1
                );
                $respuesta2 = $this->AdministraCantidad_model->RegistroCantidadCarrito($data1);
                $respuesta2 = $this->AdministraCantidad_model->RegistroCantidadOrdenes($data1);

                if($respuesta2==true){
                    $mensajes["mensaje"] = "registro_cantidad";
                    //-----------------------------------------------------
                    //Bloque de auditoria:
                    $cms = $_SESSION["cms"];  
                    $accion = "Registro cantidad producto id: ".$trae_id[0]->id;
                    $data_auditoria = array(
                                            "id_usuario"=>(integer)$cms["id"],
                                            "modulo"=>'1',
                                            "accion"=>$accion,
                                            "ip"=>$this->Auditoria_model->get_client_ip(),
                                            "fecha_hora"=> date("Y-m-d H:i:00")
                    );
                    $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
                    //-----------------------------------------------------

                }else{
                    $mensajes["mensaje"] = "no_registro";
                }			
    
            }	

			die(json_encode($mensajes));
    }

    public function consultar_cantidades(){
        //--- Datos de usuario
        $cms = $_SESSION["cms"];
        $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        //--
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard',$data);
        $this->load->view('cpanel/menu',$data);
        $this->load->view('modulos/administracion_cantidad/cantidad_consulta');
        $this->load->view('cpanel/footer');
      }

      public function consultaExistenteTodos(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->AdministraCantidad_model->consultaExistenteTodos($datos);
    //var_dump($respuesta);die;

        foreach ($respuesta as $key => $value) {

            $valor = $value;
            $valor->titulo_prod = trim(mb_strtoupper($value->titulo_prod));
            $valor->color_prod = trim(mb_strtoupper($value->color_prod));
            $valor->talla_prod = trim(mb_strtoupper($value->talla_prod));
            $res[] = $valor;
        }

	$listado = (object)$res;

	die(json_encode($listado));
    }

    public function modificarEstatus(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $data = array(
          'id' =>$datos['id'],
          'estatus' => $datos['estatus'],
        );
        //var_dump($datos);die;
        $respuesta = $this->AdministraCantidad_model->modificarEstatusCantidad($data);
        $respuesta = $this->AdministraCantidad_model->modificarEstatusCarrito($data);
        $respuesta = $this->AdministraCantidad_model->modificarEstatusOrden($data);

        if($respuesta==true){
            $mensajes["mensaje"] = "modificacion_procesada";
            //----------------------------------------------------
            //--Bloque Auditoria 
            switch ($data["estatus"]) {
                case '0':
                    $accion="Inactivar cantidad producto: ".$datos['id'];
                    break;
                case '1':
                    $accion="Activar cantidad producto: ".$datos['id'];
                    break;
                case '2':
                    $accion="Eliminar cantidad producto: ".$datos['id'];
                    break;
            }
            $cms = $_SESSION["cms"];

            $data_auditoria = array(
                                    "id_usuario"=>(integer)$cms["id"],
                                    "modulo"=>'1',
                                    "accion"=>$accion,
                                    "ip"=>$this->Auditoria_model->get_client_ip(),
                                    "fecha_hora"=> date("Y-m-d H:i:00")
            );
            $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
            //-----------------------------------------------------
        }else{
            $mensajes["mensaje"] = "no_modifico";
        }

        die(json_encode($mensajes));
    }
    
}
