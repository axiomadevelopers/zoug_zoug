<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class WebCart extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('session');
      $this->load->model('WebCart_model');
      $this->load->model('WebProductos_model');
      $this->load->model('Auditoria_model');
      $this->load->model('WebInicio_model');
      $this->load->helper('meses');
      $this->load->library('google');
      $this->load->library('facebook');
    }
    /*
    *
    */
    public function consultarCarritoUsuario($id_idioma){
        $listado = [];
        if ($this->session->userdata("web")) {
            $web = $_SESSION["web"]; 
            $id_suario = $web["id"];
            //var_dump($id_usuario);die('');
            $datos = array(
                                "id_idioma"=>$id_idioma,
                                "id_usuario"=>$id_suario
            );
            $respuesta = $this->WebInicio_model->consultarCarrito($datos);
            foreach ($respuesta as $clave_carrito => $valor_carrito) {
                $registro_img = $this->WebInicio_model->consultarimg_sola("",$valor_carrito->id_producto,$valor_carrito->id_cantidad_producto);
                //var_dump($respuesta);die;
                $arreglo_registro = $registro_img[0] ;
                //--Patch si idioma es 2 para que traiga los valores referentes al idioma
                if($id_idioma==2){
                    //--Titulo en ingles
                    $arreglo_titulo =  $this->WebInicio_model->consultar_ingles($arreglo_registro->id);
                    //--Color en ingles
                    $arreglo_color = $this->WebInicio_model->consultar_ingles_color($arreglo_registro->id_color);

                    $arreglo_registro->titulo = $arreglo_titulo[0]->titulo;
                    $arreglo_registro->color = $arreglo_color[0]->titulo_color;
                }
                //var_dump($registro_img);die('');
                $listado[] = $arreglo_registro;
            }
        }
        //---
        //var_dump($listado);die;
        return $listado;
        //---

    }
    /*
    *
    */
    public function verCarrito($idioma=1){

        $data = [
            'url' => $_SERVER['REQUEST_URI'],
            'localhost' => 'http://localhost'
        ];
        $this->session->set_userdata($data);
        
        $datos["idioma"] = $idioma;
        //Aqui debo pasarle el id del cliente...
        //$datos = $this->WebProductos_model->consultarCarrito();
        //-------------------------------------------------------
        $datos_menu["productos"] = $this->consultarCarritoUsuario($datos["idioma"]);
        $datos_menu["cuantos"] = count($datos_menu["productos"]);
        if($datos["idioma"] =="1"){
            $datos_menu["ir_cart"] ="carrito";
            $datos_menu["ir_orden"] ="orden_usuario";  
        }else{
            $datos_menu["ir_cart"] ="cart";
            $datos_menu["ir_orden"] ="order_us"; 
        }
        //-------------------------------------------------------
        //var_dump($datos_menu);die;
        $this->load->view('web/header');
        $this->load->view('web/menu',$datos_menu);
        $this->load->view('web/cart',$datos);
        $this->load->view('web/footer');
    }

    public function consultarCarrito($value=''){
        $res = [];
        $listado = [];
        $acumulador= 0;
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        if ($this->session->userdata("web")) {
            $web = $_SESSION["web"];
        }
        $datos["id_usuario"] = (integer)$web["id"];
        //--
        if($datos["id_usuario"]){
            //-----------------------------------------
            $respuesta = $this->WebCart_model->consultarCarrito($datos);
            if(count($respuesta)>0){
                //---
                foreach ($respuesta as $clave => $valor) {
                   $valores = $valor;
                   //var_dump($valores->id_cantidad_producto);die;

                   $acumulador = $acumulador+$valores->monto_total;
                   $valores->cantidad_objetos = count($respuesta);
                   $valores->monto_oculto = $valores->monto;
                   $valores->monto_total_oculto = $valores->monto_total;
                   $valores->monto_individual= number_format($valores->monto,2);
                   $valores->monto_total = number_format($valores->monto_total,2);

                   $detalles = $this->WebCart_model->consultarDetalles
                   ($valores->id_cantidad_producto);
                    $valores->descripcion_color = ucfirst(strtolower($detalles[0]->descripcion_color));
                    $valores->descripcion_talla = strtoupper($detalles[0]->descripcion_talla);
                    
                    if($datos['id_idioma']==2){
                        //--Color en ingles
                        $arreglo_color = $this->WebInicio_model->consultar_ingles_color($detalles[0]->id_color);
                        $valores->descripcion_color = ucfirst(strtolower($arreglo_color[0]->titulo_color));
                    }

                   //--Para consultar las imagenes
                   //var_dump($valor->id);die();
                   $cons_img = $this->WebCart_model->consultarimgdetalleProd($valor->id);
                    foreach ($cons_img as $key => $value) {
                        $valorImg = $value;
                        $res[]=$valorImg;
                    }
                   $valores->imagenes= $res;
                   $res = [];
                   $listado[] = $valores;
                }
                $listado[0]->monto_global_total_oculto = $acumulador;
                $listado[0]->monto_global_total = number_format($acumulador,2);
                //--
                #Variables de configuracion  de paypal...
                $listado[0]->PayPalENV = PayPalENV;
                $listado[0]->ProPayPal = ProPayPal;
                $listado[0]->PayPalClientId = PayPalClientId;
                $listado[0]->productPrice = $acumulador;
                $listado[0]->currency = "USD";
                $listado[0]->PayPalBaseUrl = PayPalBaseUrl;
                $listado[0]->productId = $listado[0]->id;
                //--
                $listado = (object)$respuesta;
                //----
            }
            //----------------------------------------
        }
        //--
        //var_dump($listado);die;
       
        die(json_encode($listado));
    }

    public function eliminar_producto(){
        $mensajes = array("mensaje"=>"");
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $data = array(
                        'estatus' =>  '2',
                    ); 
        $id_cantidad = $this->WebCart_model->TraeCantidadProducto($datos["id"]);
        $nueva_cantidad = $id_cantidad[0]->cantidad + $id_cantidad[0]->cantidad_total;
        $array = array(
            'cantidad' =>  $nueva_cantidad,
        );
        

        $res_eliminar = $this->WebCart_model->eliminarProducto($datos["id"],$data);
        $arreglar_cantidad = $this->WebCart_model->ArreglarCantidad($array,$id_cantidad[0]->id_cantidad_producto);

        if($res_eliminar){
            //--Verifico: SI no hay mas productos asociados, que elimine el carrito
            //-Paso 1: consulto el id del carrito padre
            $carrito_padre = $this->WebCart_model->consultarProductoPadre($datos["id"]);
            $id_carrito_padre = $carrito_padre[0]->id_carrito;
            //-Paso 2: consulto si el carrito tiene varios productos activos
            $cuantos_productos_carrito_detalle = $this->WebCart_model->cuantosProductosCarritoDetalle($id_carrito_padre);
            //-Paso 3: si no tiene productos asociados elimino el carrito padre
            if($cuantos_productos_carrito_detalle==0){
                $res_eliminar_productos = $this->WebCart_model->eliminarCarritoPadre($id_carrito_padre,$data);

                $arreglar_cantidad = $this->WebCart_model->ArreglarCantidad($array,$id_cantidad[0]->id_cantidad_producto);
            }
            $mensajes["mensaje"] = "modificacion_procesada";
            //----------------------------------------------------------------------
            //--Bloque Auditoria
            $datos_producto_detalle = $this->WebProductos_model->consultar_producto_auditoria("",$id_cantidad[0]->id_cantidad_producto);
            
            $accion="Elimino producto ".$datos_producto_detalle["0"]->descripcion_producto." de talla:".$datos_producto_detalle["0"]->descripcion_talla." , color: ".$datos_producto_detalle["0"]->descripcion_color." del carrito con id:".$id_carrito_padre;
            if ($this->session->userdata("web")) {
                $web = $_SESSION["web"];
            }
            $data_auditoria = array(
                                    "id_usuario"=>$web["id"],
                                    "modulo"=>'2',
                                    "accion"=>$accion,
                                    "ip"=>$this->Auditoria_model->get_client_ip(),
                                    "fecha_hora"=> date("Y-m-d H:i:00")
            );
            $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
            //------------------------------------------------------------------------
        }else{
             $mensajes["mensaje"] = "error";
        }
        die(json_encode($mensajes));
    }

    public function modificar_cantidad_carrito_productos(){
        
        $mensajes = array("mensaje"=>"");
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $id = $datos["id"];
        $cantidad = $datos["cantidad"];
        $monto_total = $datos["monto_total"];

        $c = 0;
        //--------------------------------------
        foreach ($id as $clave => $valor) {
            
            //--Obtengo el id del producto
            $rs_carrito = $this->WebCart_model->consultarDatosCarrito($valor);

            $id_producto = $rs_carrito[0]->id_producto;
            $id_cantidad_producto = $rs_carrito[0]->id_cantidad_producto;
            $cantidad_tabla = $rs_carrito[0]->cantidad_tabla;
            //var_dump($id_cantidad_producto);die;
            //Verifico que no exceda la cantidad
            $rs_cantidad = $this->WebCart_model->cantidad_productos($id_cantidad_producto,$cantidad[$c]);

            if($cantidad[$c]>$rs_cantidad[0]->cantidad){
                $mensajes["mensaje"] = "excede_cantidad";
                die(json_encode($mensajes));
            }

            //armo datos a modificar en carrito...
            $data = array(
                            'cantidad' =>  $cantidad[$c],
                            'monto_total'=> $monto_total[$c]
                        ); 
            $res_modificar_cantidad = $this->WebCart_model->modificarCantidad($valor,$data);
            
            //Proceso para modificar en inventario carrito...

            //Verifico si estamos quitando o agregando al carrito
            if($cantidad[$c] < $rs_carrito[0]->cantidad){
                $nueva_cantidad = $rs_carrito[0]->cantidad - $cantidad[$c];
               // var_dump($rs_carrito[0]->cantidad,$cantidad[$c],$nueva_cantidad);die;

                $operador = "resta";
            }if($cantidad[$c] > $rs_carrito[0]->cantidad){
                $nueva_cantidad = $cantidad[$c] - $rs_carrito[0]->cantidad;
                $operador = "sumar";
            }if($cantidad[$c] == $rs_carrito[0]->cantidad) {
                $cantidad[$c] = 0;
                $operador = "omitir";
            }

            //Saco cantidad nueva.
            if($operador == "sumar"){
                $cantidad_final = $cantidad_tabla - $nueva_cantidad;
                //var_dump($cantidad_final,$operador,$cantidad_tabla,$nueva_cantidad);die;
            }if ($operador == "resta"){
                $cantidad_final = $cantidad_tabla + $nueva_cantidad;
                //var_dump($cantidad_final,$operador,$cantidad_tabla,$nueva_cantidad);die;
            } if($operador == "omitir"){
                $cantidad_final = $cantidad_tabla;
                //var_dump($cantidad_final,$operador,$cantidad_tabla,$nueva_cantidad);die;

            }

            $data = array(
                'cantidad' =>  $cantidad_final
            ); 
             //-----------------------------------------------------------------
            #Consulto los datos de titulo de producto, talla y color
            $datos_producto_detalle = $this->WebProductos_model->consultar_producto_auditoria($id_producto,$id_cantidad_producto);
            
            $accion = "Se modifico la cantidad del producto: ".$datos_producto_detalle["0"]->descripcion_producto." Talla:".$datos_producto_detalle["0"]->descripcion_talla." Color:".$datos_producto_detalle["0"]->descripcion_color." cantidad ingresada:".$cantidad[$c]." cantidad anterior:".$rs_carrito[0]->cantidad." cantidad en tabla: ".$cantidad_tabla." cantidad nueva calculo: ".$nueva_cantidad." cantidad total: ".$cantidad_final;
            //-----------------------------------------------------------------

            $res_modificar_tabla_cantidad = $this->WebCart_model->modificarTablaCantidad($id_cantidad_producto,$data); 
             //--Bloque Auditoria            
             if ($this->session->userdata("web")) {
                $web = $_SESSION["web"];
            }
            $data_auditoria = array(
                                    "id_usuario"=>$web["id"],
                                    "modulo"=>'2',
                                    "accion"=>$accion,
                                    "ip"=>$this->Auditoria_model->get_client_ip(),
                                    "fecha_hora"=> date("Y-m-d H:i:00")
            );
            $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
            //------------------------------------------------------------------------

            $c++;      
        }
        //---------------------------------------
        //Fin del for each
        if($c>0){
            $mensajes["mensaje"] = "modificacion_procesada";
           
        }else{
            $mensajes["mensaje"] = "error";
        }
        die(json_encode($mensajes));
    }
}    