<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class OrdenCompra extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->database();
			$this->load->library('session');
			$this->load->model('OrdenCompra_model');
			$cms = $_SESSION["cms"];
			if (!$cms["login"]) {		        
			redirect(base_url());
		    }
		    //---Valido que solo el admin pueda ingresar
		    if ($cms["id"]!="1") {
		    	$url_error = base_url()."cms/no_tiene_permisos";
		        redirect($url_error);
		    }
		    //---
		}

		public function index(){
			//--- Datos de usuario
	        $cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
	        //--
			$this->load->view('cpanel/header');
			$this->load->view('cpanel/dashBoard',$data);
	        $this->load->view('cpanel/menu',$data);
			$this->load->view('modulos/orden_compra/orden_compra');
			$this->load->view('cpanel/footer');
		}
		/*
		*	Metodo para obtener los datos de los contactos consultados
		*/
		public function consultar_orden_compra(){
			$respuesta = $this->OrdenCompra_model->consultar_orden_compra();
			$number = 0;
			foreach ($respuesta as $key => $value) {
				$detal=[];
				$detalles = $this->OrdenCompra_model->consultar_detalles($value->id);
				foreach ($detalles as $individual => $detalles_orden) {

					$ruta = $this->OrdenCompra_model->consultar_img_prod($detalles_orden->id_producto);
					$color = $this->OrdenCompra_model->consultar_color($detalles_orden->id_color);
					$talla = $this->OrdenCompra_model->consultar_talla($detalles_orden->id_talla);

					$detal[] = array(
						"id_producto" => $detalles_orden->id_producto,
						"cantidad" => $detalles_orden->cantidad,
						"monto" => "$".number_format((float)$detalles_orden->monto, 2, '.', ''),
						"monto_total" => "$".number_format((float)$detalles_orden->monto_total, 2, '.', ''),
						"producto" => $detalles_orden->producto,
						"id_orden" => $detalles_orden->id_orden,
						"para_sumar" => $detalles_orden->monto_total,
						"ruta" => $ruta,
						"color" => $color[0]->descripcion,
						"talla" => $talla[0]->descripcion,
					);

				}

				$conjunto = (object)$detal;
				$total=0;
				foreach ($conjunto as $sumatoria => $contador) {
						$total = $total + $contador['para_sumar'];
					}
					//var_dump($total);die;

				$vector_fecha = explode("-", $value->fecha);
				$value->fecha = $vector_fecha[2]."/".$vector_fecha[1]."/".$vector_fecha[0];
				
	            $valor[] = array(
								"numero_orden" => $value->numero_orden_compra,
								"fecha" => $value->fecha,
								"payment_id" => $value->payment_id,
								"payer_id" => $value->payer_id,
								"payment_token" => $value->payment_token,
								"usuario" => $value->usuario,
								"detalles" => $conjunto,
								"monto_total" => "$".number_format((float)$total, 2, '.', ''),
						
				);

			}
			$listado = (object)$valor;
	        die(json_encode($listado));
		}
	}	