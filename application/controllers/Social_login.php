<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Social_login extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('session');
      $this->load->model('Social_login_model');
      $this->load->library('facebook');
      $this->load->library('google');
    }
    public function google(){
        //---
        #Limpiando las url
        $_SESSION['url'] = str_replace("/zoug_zoug/", "", $_SESSION['url']);
        //---
        $google_data=$this->google->validate();
		$datas =[
				'nombre_apellido'=>$google_data['name'],
                'correo'=>$google_data['email'],
                'tp_login'=> '2',
                'estatus'=> '1'

                ];
        $recordset = $this->Social_login_model->existe($google_data['email']);
        if($recordset > 0 ){
            $recordset1 = $this->Social_login_model->existe_email($google_data['email']);
            /*var_dump($recordset1["tp_login"]);
            var_dump($google_data['email']);
            die();*/
             if($recordset1["tp_login"]==1 || $recordset1["tp_login"]==3){
                 if($_SESSION['url']=="inicio" || $_SESSION['url']=="nosotros"
                 || $_SESSION['url']=="productos"|| $_SESSION['url']=="noticias"
                 || $_SESSION['url']=="carrito"|| $_SESSION['url']=="orden"
                 || $_SESSION['url']=="contactanos"|| $_SESSION['url']=="politicas_privacidad"
                 || $_SESSION['url']=="terminos" || $_SESSION['url']==""
                 || $_SESSION['url']=="register"  || $_SESSION['url']=="recuperar_clave"
                 || $_SESSION['url']=="redirigir_cuenta"){
                    redirect(base_url()."redirigir_cuenta"."/".$recordset1["tp_login"]);
                 }else {
                    redirect(base_url()."redirect_account"."/".$recordset1["tp_login"]);
                 }
            }
            #Consulto al usuario
            $record_usuario = $this->Social_login_model->login_social($google_data['name'],$google_data['email']);
            //
            $mensajes["mensajes"] = "inicio_exitoso";


            $data1 = array(
              'web' => array( 
                'login' => true,
                'id' => $record_usuario[0]->id,
                'nombre_persona' => substr($record_usuario[0]->nombre_apellido,0,30),
                'correo' => $record_usuario[0]->correo,
                'estatus' => $record_usuario[0]->estatus,
                'logueado' =>true
              )
            );

            $this->session->set_userdata($data1);
            redirect(base_url().$_SESSION['url']);
          
            }
        else{

            /* $recordset = $this->Social_login_model->existe_email($google_data['email']);
            if($recordset["codigo"]){
                $data1 = array(
                    'tp_login' => 1
                );
                $this->session->set_userdata($data1);
                redirect(base_url().$_SESSION['url']);
            } */

            $record_usuario = $this->Social_login_model->registro($datas);
            //var_dump($record_usuario);die;
            $record_usuario = $this->Social_login_model->login_social($google_data['name'],$google_data['email']);
            $mensajes["mensajes"] = "inicio_exitoso";
            $data1 = array(
              'web' => array( 
                'login' => true,
                'id' => $record_usuario[0]->id,
                'nombre_persona' => substr($record_usuario[0]->nombre_apellido,0,30),
                'correo' => $record_usuario[0]->correo,
                'estatus' => $record_usuario[0]->estatus,
                'logueado' =>true
              )
            );
            $this->session->set_userdata($data1);
            redirect(base_url().$_SESSION['url']);
        }
    }
    public function facebook(){
        $userData1 = array();
		if($this->facebook->is_authenticated()){
			$userProfile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,gender,locale,picture');
        }
        $nombre= $userProfile['first_name']." ".$userProfile['last_name'];
		$datas =[
				'nombre_apellido'=>$nombre,
                'correo'=>$userProfile['email'],
                'tp_login'=> '3',
                'estatus'=> '1'
                ];

        $recordset = $this->Social_login_model->existe($userProfile['email']);
        if($recordset > 0){
            $recordset1 = $this->Social_login_model->existe_email($userProfile['email']);
            if($recordset1["tp_login"]==1 || $recordset1["tp_login"]==2){
                if($_SESSION['url']=="/zoug_zoug/inicio" || $_SESSION['url']=="/zoug_zoug/nosotros"
                || $_SESSION['url']=="/zoug_zoug/productos"|| $_SESSION['url']=="/zoug_zoug/noticias"
                || $_SESSION['url']=="/zoug_zoug/carrito"|| $_SESSION['url']=="/zoug_zoug/orden"
                || $_SESSION['url']=="/zoug_zoug/contactanos"|| $_SESSION['url']=="/zoug_zoug/politicas_privacidad"
                || $_SESSION['url']=="/zoug_zoug/terminos" || $_SESSION['url']=="/zoug_zoug/"
                || $_SESSION['url']=="/zoug_zoug/register"  || $_SESSION['url']=="/zoug_zoug/recuperar_clave"
                || $_SESSION['url']=="/zoug_zoug/redirigir_cuenta"){
                   redirect($_SESSION['localhost']."/zoug_zoug"."/"."redirigir_cuenta"."/".$recordset1["tp_login"]);
                }else {
                   redirect($_SESSION['localhost']."/zoug_zoug"."/"."redirect_account"."/".$recordset1["tp_login"]);
                }
           }
            #Consulto al usuario
            $record_usuario = $this->Social_login_model->login_social($nombre,$userProfile['email']);
            //
            $mensajes["mensajes"] = "inicio_exitoso";
            $data1 = array(
              'web' => array( 
                'logueado' => true,
                'id' => $record_usuario[0]->id,
                'nombre_persona' => substr($record_usuario[0]->nombre_apellido,0,30),
                'correo' => $record_usuario[0]->correo,
                'estatus' => $record_usuario[0]->estatus
              )
            );
            $this->session->set_userdata($data1);
            redirect($_SESSION['localhost'].$_SESSION['url']);
            }
        else{

            $record_usuario = $this->Social_login_model->registro($datas);
            $record_usuario = $this->Social_login_model->login_social($nombre,$userProfile['email']);
            $mensajes["mensajes"] = "inicio_exitoso";
            $data1 = array(
             'web' => array( 
                'logueado' => true,
                'id' => $record_usuario[0]->id,
                'nombre_persona' => substr($record_usuario[0]->nombre_apellido,0,30),
                'correo' => $record_usuario[0]->correo,
                'estatus' => $record_usuario[0]->estatus
             )
            );
            $this->session->set_userdata($data1);
            redirect($_SESSION['localhost'].$_SESSION['url']);
        }
    }
    public function logout()
        {
            $this->session->unset_userdata("web");
            #Limpiando las url
            $_SESSION['url'] = str_replace("/zoug_zoug/", "", $_SESSION['url']);
            //---
            redirect(base_url().$_SESSION['url']);

        }
    
}  