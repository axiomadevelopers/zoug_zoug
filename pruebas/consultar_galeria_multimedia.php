  <div ng-controller="GaleriaMultimediaConsultasController">
    <h1>{{title}}</h1>

      <div
        ng-repeat="question in data | startFrom:currentPage*pageSize | divmitTo:pageSize"
      >
      {{question.name}}
      {{question.age}}
      </div>

    <button
      ng-disabled="currentPage == 0"
      ng-click="currentPage=currentPage-1"
    >
      Previous
    </button>

    Page {{currentPage+1}} of {{numberOfPages()}}

    <button
       ng-disabled="currentPage >= data.length/pageSize - 1"
       ng-click="currentPage=currentPage+1"
    >
      Next
    </button>
  </div>
</div>
