angular.module("ZougZougApp")
	.controller("confirmAccountController", function($scope,$http,$sce,$compile,$location,funcionaFactory,serviciosFactory,multIdioma,carritoFactory){
		//Cuerpo declaraciones
		$scope.productos = {}
		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#idioma").val();
		$scope.id = $("#idUser").html();
		$scope.codigo = $("#codigo").html();
		//console.log($scope.codigo);
		$scope.currentTab1 = '';

		$scope.correoUsuario = $("#correoUsuario").html();

		$scope.nombre_apellido = $("#nombre_apellido").html();
		$(".menu_web").removeClass("active")

		$(".primera").removeClass("side-nav")

		$( "#target" ).click(function() {
		  alert( "Handler for  called." );
		});
		//console.log($scope.correoUsuario);
		//console.log($scope.idioma);

		$scope.definir_url_home = function(){
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
		}
		//----
		$scope.backpageCart = function(){
			var idioma = $scope.idioma
			if(idioma=="1")
				var url_carrito = $scope.base_url+"carrito";
			else
				var url_carrito = $scope.base_url+"cart";
			$("#formOrderProducto").attr("action",url_carrito)
			$("#formOrderProducto").attr("method","post");
			$("#formOrderProducto").submit()
		}
		$scope.activarUsuario = function () {
			$http.post($scope.base_url + "/WebLogin/activar_usuario", {
				'id': $scope.id,
				'codigo': $scope.codigo,
			}).success(function (data, estatus, headers, config) {
				$scope.mensajes = data;
				//console.log(data);
				if ($scope.mensajes.mensaje == "registro_procesado") {
					$scope.currentTab1 ='exito';
				} else if ($scope.mensajes.mensaje == "ya_activado") {
					$scope.currentTab1 ='ya_activado';
				} else if ($scope.mensajes.mensaje == 'error') {
						$scope.currentTab1 ='error';
				}
			}).error(function (data, estatus) {
				console.log(data);
			});ya_activado
		}
		//-----
		$scope.activarUsuario();
		$scope.definir_url_home();
		//----
	});
