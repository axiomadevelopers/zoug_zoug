angular.module("ZougZougApp")
	.controller("nosotrosController", function($scope,$sce,$http,$compile,$location,funcionaFactory,nosotrosFactory,clientesFactory,multIdioma){
		//--
		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#id_idioma").val();
		$(".menu_web").removeClass("active")
		$("#menu2").addClass("active")
		//------------------------------------------------------------
		/*
		*	Bloque obligatorio para todo controlador
		*/
		/*
		*	Define segun el valor del idioma los titulos
		*/
		$scope.definir_url_home = function(){
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
			//console.log($scope.btn);
		}
		/*
		*
		*/
		//-----------------------------------------------------------
		//---
		$scope.consultar_nosotros = function(){

			$scope.definir_url_home();

			nosotrosFactory.asignar_valores($scope.idioma,$scope.base_url)
			nosotrosFactory.cargar_nosotros(function(data){
				$scope.nosotros=data[0];
				//console.log($scope.nosotros);
			});
		}
		//---
		$scope.consultar_nosotros();
	}); 
