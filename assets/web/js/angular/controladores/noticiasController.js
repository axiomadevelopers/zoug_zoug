angular.module("ZougZougApp")
	.controller("noticiasController", function($scope,$http,$compile,$location,funcionaFactory,footerFactory,noticiaFactory,multIdioma,productosHomeFactory){

		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#id_idioma").val();
		//console.log($scope.idioma);
		$(".menu_web").removeClass("active")
		$("#menu4").addClass("active")
		//------------------------------------------------------------
		/*
		*	Bloque obligatorio para todo controlador
		*/
		/*
		*	Define segun el valor del idioma los titulos
		*/
		$scope.definir_url_home = function(){
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
			//console.log($scope.btn);
		}
		/*
		*
		*/
		$scope.noticias = {
								'id':'',
								'id_idioma':'',
								'titulo':'',
								'descripcion':'',
								'estatus':'',
								'id_imagen':''
		}
		$scope.sub_noticias = []
		//-----------------------------------------------------------
		//---
		$scope.consultar_noticias = function(){
			//alert("aqui");
			$scope.definir_url_home();

			noticiaFactory.asignar_valores($scope.idioma,$scope.base_url,"","","")
			noticiaFactory.cargar_noticia(function(data){
				$scope.noticia=data[0];
				//console.log($scope.noticia);
				$scope.limit = 3
				$scope.start =  0
				$scope.consultar_subnoticias($scope.noticia.id);
			});
		}
		//---
		$scope.consultar_subnoticias = function(id){

			noticiaFactory.asignar_valores($scope.idioma,$scope.base_url,id,$scope.limit,$scope.start)
			noticiaFactory.cargar_sub_noticias(function(data){
				/*$scope.sub_noticias=data;
				console.log($scope.sub_noticias);*/
				for (i in data) {
					$scope.sub_noticias.push(data[i]);
				}
				$scope.start += 1;
				//console.log($scope.sub_noticias)
			});
		}
		//---
		$scope.cargar_mas_subnoticias = function(){
			//console.log($scope.noticia.id);
			noticiaFactory.asignar_valores($scope.idioma,$scope.base_url,$scope.noticia.id,$scope.limit,$scope.start)
			noticiaFactory.cargar_sub_noticias(function(data){
				//----------------------------------------
				if (data === null || data == "null" || data=="") {
					if($scope.idioma==1)
						mensaje_alerta("#campo_mensaje_noticias","Todas las noticias fueron cargadas!","alert-warning");
					else
						mensaje_alerta("#campo_mensaje_noticias","All news was charged!","alert-warning");

				} else {
					for (i in data) {
						$scope.sub_noticias.push(data[i]);
					}
					$scope.start += 1;
				}
				//----------------------------------------
			});
		}
		//---
		$scope.productosHomeFactory = function(){
			//console.log("si llega");
			vacio = ""
			var offset = 0;
			var limit = 2;
    		productosHomeFactory.asignar_valores($scope.idioma,'',limit,offset,$scope.base_url);
			productosHomeFactory.cargar_productos(function(data){
				$scope.productos = data;
				//console.log($scope.productos[0]);
				$scope.super_producto_noticias1 = $scope.productos[0]
				$scope.super_producto_noticias2 = $scope.productos[1]
				/*
				$scope.productos_footer1.titulo = $scope.productos[0].titulo
				$scope.productos_footer2.titulo = $scope.productos[1].titulo
				$scope.productos_footer3.titulo = $scope.productos[2].titulo
				$scope.productos_footer1.slug = $scope.productos[0].slug
				$scope.productos_footer2.slug = $scope.productos[1].slug
				$scope.productos_footer3.slug = $scope.productos[2].slug*/
			});
		}
		//---
		$scope.definir_url_home();
		$scope.consultar_noticias();
		$scope.productosHomeFactory();
		//
	});
