angular.module("ZougZougApp")
	.controller("productosController", function($scope,$http,$sce,$compile,$location,funcionaFactory,nosotrosFactory,cargar,serviciosFactory,multIdioma,productos_todos_HomeFactory,mas_productosFactory,lineaFactory,generoFactory,colorFactory,tallaFactory,cargar_titulo_producto,cargar_img_linea,cargar_img_genero,cargar_img_color,cargar_img_talla,cargar_img_precio, cargar_val2, cargarfil2, cargarfil3, cargarfil4,cargarfil5,cargarfil6,cargarfil7,cargarfil8,cargarfil9,cargarfil10,
												cargar_val3,cargar_val4,cargar_val5,cargar_val6,cargar_val7,cargar_val8,cargar_val9,cargar_val10,cargar_val111,cargar_val112,cargar_val113,cargar_val114,cargar_val115)
	{
		//Cuerpo declaraciones
		$scope.productos = {}
		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#id_idioma").val();
		$scope.start = 0;
		$(".menu_web").removeClass("active")
		$("#menu3").addClass("active")


		$(".primera").removeClass("side-nav")
		$("#menu3").addClass("active")

		$( "#target" ).click(function() {
		  alert( "Handler for  called." );
		});

		$scope.consultar_inicial = true
		
		if(screen.width==760){
			$scope.consultar_inicial = false;
		}

		//alert($scope.consultar_inicial)
		
		//------------------------------------------------------------
		/*
		*	Bloque obligatorio para todo controlador
		*/
		/*
		*	Define segun el valor del idioma los titulos
		*/

		if ($scope.idioma == 1) {
			$scope.titulo_genero = 'Filtro por Género'
		}else {
			$scope.titulo_genero = 'Filter by Gender'
		}

		if ($scope.idioma == 1) {
			$scope.titulo_color = 'Filtro por Color'
		}else {
			$scope.titulo_color = 'Filter by Color'
		}

		if ($scope.idioma == 1) {
			$scope.titulo_talla = 'Filtro por Talla'
		}else {
			$scope.titulo_talla = 'Filter by Size'
		}

		if ($scope.idioma == 1) {
			$scope.titulo_linea = 'Filtro por Línea'
		}else {
			$scope.titulo_linea = 'Line filter'
		}

		if ($scope.idioma == 1) {
			$scope.titulo_precio = 'Filtro por Precio'
		}else {
			$scope.titulo_precio = 'Filter by Price'
		}

		if ($scope.idioma == 1) {
			$scope.nombre_rango = 'Rango de Precio'
		}else {
			$scope.nombre_rango = 'Price range'
		}

		$scope.definir_url_home = function(){
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
		}

		$scope.sub_productos = []

		$scope.productos = {
								'id':'',
								'id_idioma':'',
								'titulo':'',
								'precio':'',
								'estatus':'',
								'id_imagen':''
		}

		$scope.consultar_productos = function(){
    		productos_todos_HomeFactory.asignar_valores($scope.idioma,$scope.base_url);
			productos_todos_HomeFactory.cargar_productos(function(data){
				$scope.productos = data;
				//console.log($scope.productos);
					for (i in data) {
						$scope.sub_productos.push(data[i]);
					}
					$scope.start =  0
			});
		}

		$scope.cargar_mas_productos = function(){
			$scope.prueba = $scope.productos
			var ids = Object.values($scope.prueba);
			$scope.id_d = ids.length - 1
			$scope.id = $scope.productos[$scope.id_d]['id']
			$scope.id_imagen = $scope.productos[$scope.id_d]['id_imagen']

			$scope.limit = 3
			$scope.start = $scope.start+5;
			mas_productosFactory.asignar_valores($scope.idioma,$scope.base_url,$scope.id,$scope.id_imagen,$scope.limit,$scope.start)
			mas_productosFactory.cargar_mas_prod(function(data){
				$scope.productos = data;
				if (data === null || data === "null" || data === "") {
					$("#cargar_mas").prop('disabled', true);
					if($scope.idioma==1)
						mensaje_alerta("#campo_mensaje_productos","Todas los productos disponibles fueron cargadas!","alert-warning");
					else
						mensaje_alerta("#campo_mensaje_productos","All available products were loaded!","alert-warning");
				} else {
					for (i in data) {
						$scope.sub_productos.push(data[i]);
					}
					$scope.start += 1;
				}
			});
		}

		$scope.cargar_linea = function(){
			lineaFactory.asignar_valores($scope.base_url,$scope.idioma)
			lineaFactory.cargar_linea(function(data){
				$scope.linea=data;
			});
		}

		$scope.cargar_genero = function(){
			generoFactory.asignar_valores($scope.base_url,$scope.idioma)
			generoFactory.cargar_generos(function(data){
				$scope.genero=data;
			});
		}

		$scope.cargar_color = function(){
			colorFactory.asignar_valores($scope.base_url,$scope.idioma)
			colorFactory.cargar_color(function(data){
				$scope.color=data;
			});
		}

		$scope.cargar_talla = function(){
			tallaFactory.asignar_valores($scope.base_url,$scope.idioma)
			tallaFactory.cargar_talla(function(data){
				$scope.talla=data;
			});
		}

		$scope.sub_productos_filtro =[];

		$scope.linea_filtrar = '';
		$scope.genero_filtrar = '';
		$scope.precio_filtrar_1 = '';
		$scope.precio_filtrar_2 = '';
		$scope.color_filtrar = '';
		$scope.talla_filtrar = '';
		
		$scope.limpiar_var_filtros = function(){
			$scope.linea_filtrar = ""
			$scope.genero_filtrar =  ""
			$scope.precio_filtrar_1 = ""
			$scope.precio_filtrar_2 = ""
			$scope.color_filtrar = ""
			$scope.talla_filtrar = ""
		}

		$scope.filtro_titulo = function(){
			
			if($scope.titulo){
				$(".activo_productos").removeClass("activo_productos")
				//--
				$scope.limpiar_var_filtros()
				//--
				$scope.titulo_filtrar = $scope.titulo;
				$scope.filtros();
				$scope.esconder();
			}else{
				$scope.titulo_filtrar = "";
				location.reload();
			}
			
			
		}

		$scope.filtro_linea = function(id){
			$scope.titulo ="";
			$scope.lineas = id;
			$scope.linea_filtrar = $scope.lineas;
			$scope.filtros();
		}

		$scope.filtro_genero = function(id){
			$scope.titulo ="";
			$scope.generos = id;
			$scope.genero_filtrar = $scope.generos;
			$scope.filtros();
		}

		$scope.filtro_precio = function(){
			$scope.titulo ="";
			$scope.rango_1 = $("#price-amount-1").val();
			$scope.rango_2 = $("#price-amount-2").val();

			$scope.precio_filtrar_1 = $scope.rango_1;
			$scope.precio_filtrar_2 = $scope.rango_2;
			$scope.filtros();
		}

		$scope.filtro_color = function(id){
			$scope.titulo ="";
			$scope.colores = id;
			$scope.color_filtrar = $scope.colores;
			$scope.filtros();
		}
		$scope.filtro_talla = function(id){
			$scope.titulo ="";
			$scope.tallas = id;
			$scope.talla_filtrar = $scope.tallas;
			$scope.filtros();
		}

		$scope.filtros = function(){
			//X TITULO
			if($scope.titulo_filtrar != ''){
				cargar_titulo_producto.asignar_valores($scope.base_url,$scope.idioma,$scope.titulo_filtrar)
				cargar_titulo_producto.cargar_filtro_titulo(function(data){
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						//$scope.limit = 1
						$scope.start =  0
					}
				});
			}
			//X FILTRO
			if ($scope.linea_filtrar != '') {
				cargar_img_linea.asignar_valores($scope.base_url,$scope.idioma,$scope.linea_filtrar)
				cargar_img_linea.cargar_img_linea(function(data){
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						//$scope.limit = 1
						$scope.start =  0
					}
				});
			}
			//X GENERO
			if ($scope.genero_filtrar != '') {
				cargar_img_genero.asignar_valores($scope.base_url,$scope.idioma,$scope.genero_filtrar)
				cargar_img_genero.cargar_img_genero(function(data){
					$scope.sub_productos_filtro =[];
					//console.log($scope.sub_productos_filtro)
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para este Género!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Genre!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//PRECIO
			if (($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')) {
					cargar_img_precio.asignar_valores($scope.base_url,$scope.idioma,$scope.precio_filtrar_1,$scope.precio_filtrar_2)
					cargar_img_precio.cargar_img_precio(function(data){
						$scope.prueba = data;
						$scope.sub_productos_filtro =[];

						if (data === null || data == "null" || data=="") {
							if($scope.idioma==1)
								mensaje_alerta("#campo_mensaje","No hay productos disponible para este Rango de Precio!","alert-warning");
							else
								mensaje_alerta("#campo_mensaje","There are no products available for this Price Range!","alert-warning");
						}else{
							for (i in data) {
								$scope.sub_productos_filtro.push(data[i]);
							}
							$scope.start =  0
						}
					});
			}
			//X COLOR
			if ($scope.color_filtrar != '') {
				cargar_img_color.asignar_valores($scope.base_url,$scope.idioma,$scope.color_filtrar)
				cargar_img_color.cargar_img_color(function(data){
					$scope.sub_productos_filtro =[];

					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para este Color!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Color!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//X TALLA
			if ($scope.talla_filtrar != '') {
				cargar_img_talla.asignar_valores($scope.base_url,$scope.idioma,$scope.talla_filtrar)
				cargar_img_talla.cargar_img_talla(function(data){
					$scope.prueba = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Talla!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Size!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//X LINEA Y GENERO
			if (($scope.linea_filtrar != '')&&($scope.genero_filtrar != '')) {
				cargar.asignar_valores($scope.idioma,$scope.linea_filtrar, $scope.genero_filtrar)
				cargar.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//XLIENA Y PRECIO
			if (($scope.linea_filtrar != '')&&($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')) {
				cargarfil2.asignar_valores($scope.idioma,$scope.linea_filtrar, $scope.precio_filtrar_1, $scope.precio_filtrar_2)
				cargarfil2.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//Xlinea Y color
			if (($scope.linea_filtrar != '')&&($scope.color_filtrar != '')) {
				cargarfil4.asignar_valores($scope.idioma,$scope.linea_filtrar, $scope.color_filtrar)
				cargarfil4.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//Xlinea Y talla
			if (($scope.linea_filtrar != '')&&($scope.talla_filtrar != '')) {
				cargarfil5.asignar_valores($scope.idioma,$scope.linea_filtrar, $scope.talla_filtrar)
				cargarfil5.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//X GENERO Y PRECIO
			if (($scope.genero_filtrar != '')&&($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')) {
				cargarfil3.asignar_valores($scope.idioma,$scope.genero_filtrar, $scope.precio_filtrar_1, $scope.precio_filtrar_2)
				cargarfil3.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta  Filtro!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Filtro!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//X GENERO Y COLOR
			if (($scope.genero_filtrar != '')&&($scope.color_filtrar != '')) {
				cargarfil6.asignar_valores($scope.idioma,$scope.genero_filtrar, $scope.color_filtrar)
				cargarfil6.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta  Filtro!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Filtro!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//X GENERO Y TALLA
			if (($scope.genero_filtrar != '')&&($scope.talla_filtrar != '')) {
				cargarfil7.asignar_valores($scope.idioma,$scope.genero_filtrar, $scope.talla_filtrar)
				cargarfil7.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta  Filtro!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Filtro!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//X PRECIO Y COLOR
			if ((($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')&&$scope.color_filtrar != '')) {
				cargarfil8.asignar_valores($scope.idioma,$scope.color_filtrar, $scope.precio_filtrar_1, $scope.precio_filtrar_2)
				cargarfil8.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta  Filtro!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Filtro!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//X PRECIO Y TALLA
			if ((($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')&&$scope.talla_filtrar != '')) {
				cargarfil9.asignar_valores($scope.idioma,$scope.talla_filtrar, $scope.precio_filtrar_1, $scope.precio_filtrar_2)
				cargarfil9.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta  Filtro!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Filtro!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//X COLOR Y TALLA
			if (($scope.color_filtrar != '')&&($scope.talla_filtrar != '')) {
				cargarfil10.asignar_valores($scope.idioma,$scope.color_filtrar, $scope.talla_filtrar)
				cargarfil10.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta  Filtro!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Filtro!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//XLINEA, GENERO Y PRECIO
			if (($scope.linea_filtrar != '')&&($scope.genero_filtrar != '') &&($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')){
				cargar_val2.asignar_valores($scope.idioma,$scope.linea_filtrar,$scope.genero_filtrar, $scope.precio_filtrar_1, $scope.precio_filtrar_2)
				cargar_val2.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//XLINEA, GENERO Y COLOR
			if (($scope.linea_filtrar!='')&&($scope.genero_filtrar!='') &&($scope.color_filtrar!='')){
				cargar_val3.asignar_valores($scope.idioma,$scope.linea_filtrar,$scope.genero_filtrar, $scope.color_filtrar)
				cargar_val3.valores(function(data){
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}

			//XLINEA, GENERO Y TALLA
			if (($scope.linea_filtrar!='')&&($scope.genero_filtrar!='') &&($scope.talla_filtrar!='')){
				cargar_val4.asignar_valores($scope.idioma,$scope.linea_filtrar,$scope.genero_filtrar, $scope.talla_filtrar)
				cargar_val4.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}

			//XLINEA, PRECIO Y COLOR
			if (($scope.linea_filtrar != '')&&($scope.color_filtrar != '') &&($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')){
				cargar_val5.asignar_valores($scope.idioma,$scope.linea_filtrar,$scope.color_filtrar, $scope.precio_filtrar_1, $scope.precio_filtrar_2)
				cargar_val5.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}

			//XLINEA, PRECIO Y TALLA
			if (($scope.linea_filtrar != '')&&($scope.talla_filtrar != '') &&($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')){
				cargar_val6.asignar_valores($scope.idioma,$scope.linea_filtrar,$scope.talla_filtrar, $scope.precio_filtrar_1, $scope.precio_filtrar_2)
				cargar_val6.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}

			//XLINEA, COLOR Y TALLA
			if (($scope.linea_filtrar != '')&&($scope.color_filtrar != '') &&($scope.talla_filtrar != '')){
				cargar_val7.asignar_valores($scope.idioma,$scope.linea_filtrar,$scope.color_filtrar, $scope.talla_filtrar)
				cargar_val7.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}

			//GENERO, PRECIO Y COLOR
			if (($scope.genero_filtrar != '')&&($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')&&($scope.color_filtrar != '') ){
				cargar_val8.asignar_valores($scope.idioma,$scope.genero_filtrar,$scope.precio_filtrar_1, $scope.precio_filtrar_2,$scope.color_filtrar)
				cargar_val8.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}

			//GENERO, PRECIO Y TALLA
			if (($scope.genero_filtrar != '')&&($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')&&($scope.talla_filtrar != '') ){
				cargar_val9.asignar_valores($scope.idioma,$scope.genero_filtrar,$scope.precio_filtrar_1, $scope.precio_filtrar_2,$scope.talla_filtrar)
				cargar_val9.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}

			//PRECIO, COLOR y TALLA
			if (($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')&&($scope.color_filtrar != '')&&($scope.talla_filtrar != '')){
				cargar_val10.asignar_valores($scope.idioma,$scope.precio_filtrar_1, $scope.precio_filtrar_2,$scope.color_filtrar,$scope.talla_filtrar)
				cargar_val10.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}

			//LINEA, GENERO y PRECIO Y COLOR
			if (($scope.linea_filtrar != '')&&($scope.genero_filtrar != '')&&($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')&&($scope.color_filtrar != '')){
				cargar_val111.asignar_valores($scope.idioma,$scope.linea_filtrar,$scope.genero_filtrar,$scope.precio_filtrar_1, $scope.precio_filtrar_2,$scope.color_filtrar)
				cargar_val111.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}

			//LINEA, GENERO y PRECIO Y TALLA
			if (($scope.linea_filtrar != '')&&($scope.genero_filtrar != '')&&($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')&&($scope.talla_filtrar != '')){
				cargar_val112.asignar_valores($scope.idioma,$scope.linea_filtrar,$scope.genero_filtrar,$scope.precio_filtrar_1, $scope.precio_filtrar_2,$scope.talla_filtrar)
				cargar_val112.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}

			//LINEA, PRECIO, TALLA Y COLOR
			if (($scope.linea_filtrar != '')&&($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')&&($scope.talla_filtrar != '')&&($scope.color_filtrar != '')){
				cargar_val113.asignar_valores($scope.idioma,$scope.linea_filtrar,$scope.precio_filtrar_1, $scope.precio_filtrar_2,$scope.talla_filtrar,$scope.color_filtrar)
				cargar_val113.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}

			//PRECIO, TALLA, COLOR y GENERO
			if (($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')&&($scope.talla_filtrar != '')&&($scope.color_filtrar != '')&&($scope.genero_filtrar != '')){
				cargar_val114.asignar_valores($scope.idioma,$scope.precio_filtrar_1, $scope.precio_filtrar_2,$scope.talla_filtrar,$scope.color_filtrar,$scope.genero_filtrar)
				cargar_val114.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}

			//LINEA,GENERO, PRECIO, COLOR Y TALLA
			if (($scope.linea_filtrar != '')&&($scope.genero_filtrar != '')&&($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')&&($scope.color_filtrar != '')&&($scope.talla_filtrar != '')){
				cargar_val115.asignar_valores($scope.idioma,$scope.linea_filtrar,$scope.genero_filtrar,$scope.precio_filtrar_1, $scope.precio_filtrar_2,$scope.color_filtrar,$scope.talla_filtrar)
				cargar_val115.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
		}

		$scope.esconder = function(){
			$scope.variable = true;
			$scope.variable2 = false;
		}

		$scope.no_esconder = function(){
			$scope.variable = false;
			$scope.variable2 = true;
		}

		$scope.ver_categoriaprod = function(valor){
			var id = $("#ver"+valor).attr("data");
			$scope.id_categoria = id;
			$("#id_categoria").val($scope.id_categoria)
		}
		
		$scope.cargar_linea();
		$scope.cargar_genero();
		$scope.cargar_color();
		$scope.cargar_talla();

		$scope.consultar_productos();
		$scope.definir_url_home();
	});
