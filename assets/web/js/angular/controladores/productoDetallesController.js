angular.module("ZougZougApp")
	.controller("productoDetallesController", function($scope,$http,$sce,$compile,$location,funcionaFactory,serviciosFactory,multIdioma,productoFactory,productosHomeFactory,tallaProdFactory,colorProdFactory,cantidadPFactory){
		//Cuerpo declaraciones
		
		$scope.productos = {}
		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#id_idioma").val();
		$scope.id_prod = "";
		$(".menu_web").removeClass("active")
		$("#menu3").addClass("active")

		$(".primera").removeClass("side-nav")
		$("#menu3").addClass("active")

		$( "#target" ).click(function() {
		  alert( "Handler for  called." );
		});

		if($scope.idioma == 1){
			$scope.titulo_1 = 'Detalle del Producto'
		}else{
			$scope.titulo_1 = 'Product Detail'
		}

		$scope.definir_url_home = function(){
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
		}

		
		//-----------------------------------------------------------
		$scope.consultar_producto_slug = function(){
			$scope.definir_url_home();
			$scope.slug = $("#slug_producto").text();
			productoFactory.asignar_valores($scope.idioma,$scope.base_url,$scope.slug)
			productoFactory.cargar_producto_slug(function(data){
				$scope.producto=data[0];
				//console.log($scope.producto);
				$scope.consultar_subproducto($scope.producto.id,3,0);

				//--Formato en minusculas$scope.id_prod
				$scope.producto.categoria = mayus_primera_letra($scope.producto.categoria)
				$scope.id_prod = $scope.producto.id
				$scope.producto.tipo_producto = mayus_primera_letra($scope.producto.tipo_producto)
				
				$scope.consultar_mas_producto($scope.producto.id);
				$scope.consultar_talla($scope.producto.id);

			});
		}
			//-----------------------------------------------------------
		$scope.consultar_talla = function(id){
			$scope.id_prod = id;
			//console.log($scope.id_prod);
			tallaProdFactory.asignar_valores("",$scope.id_prod,$scope.base_url)
			tallaProdFactory.cargar_talla(function(data){
			$scope.tallas=data;				
			//console.log($scope.tallas);
			})
		} 
		//--/-----------------------------------------------------------
		$scope.ColoresExistentes = function(){
			
			$("#sin_preloader").val("1")
			
			if($scope.talla.id_talla!=""){
			colorProdFactory.asignar_valores($scope.talla.id_talla,$scope.id_prod,$scope.base_url)
			colorProdFactory.cargar_color(function(data){
			$scope.colores=data;				
			//console.log($scope.colores);
			})}
		}
		$scope.descripcion_color = "Colores";
		//-------------------------------------------------------------
		$scope.CantidadExistentes = function(){

			$("#sin_preloader").val("1")

			if($scope.color.descripcion_color!=$scope.descripcion_color){
				$scope.descripcion_color = "Colores";
				cantidadPFactory.asignar_valores($scope.color.id_color,$scope.talla.id_talla,$scope.id_prod,$scope.base_url)
			cantidadPFactory.cargar_cantidad(function(data){
			$scope.cantidad=data;				
			//console.log($scope.descripcion_color);
			$scope.asignarCantidad()

			}) }

		}
		
		//--/-----------------------------------------------------------
		$scope.consultar_subproducto = function(id,limit, start){
				productoFactory.asignar_valores($scope.idioma,$scope.base_url,id,limit,start)
				productoFactory.cargar_sub_producto(function(data){
				$scope.sub_producto=data;
			});
		  }
		
		//-----------------------------------------------------------
		$(document).on("click",".imagen_selector",function(){
			div_imagen = $(this).attr("dat");
		    //--Limpio el intervalo...
		    //clearInterval(intervalo);
		    //--Inicio un nuevo intervalo---
	    	$('#slider div:first-child').hide();
	    	imagen = $(this).attr("src")
		    $(div_imagen).fadeIn().prependTo('#slider');
		    //$(div_imagen).zoom();
		    //$scope.generar_slider();     						
		});
		//-----------------------------------------------------------
		$scope.generar_slider=function(){
			//$('#slider div:gt(1)').hide();
		    //intervalo = setInterval(function(){
		    $('#slider div:first-child').fadeOut("slow")
	        							.next('div').fadeIn("slow")
	         							.end().appendTo('#slider');
		   // }, 6000);	
		}
		//-----------------------------------------------------------
		$scope.agregarCarrito = function(){
			
			var id_productos = $("#id_producto").html()
			var cantidad = $("#cantidadProducto").val()
			var monto_individual = parseFloat($scope.producto.precio_oculto)
			var monto_total = parseFloat(monto_individual*cantidad)
			var idioma = $scope.idioma
			var id_producto_clonado = $scope.producto.id_original_clonado;
			//console.log($scope.producto)
			/*if(idioma==1)
				mensaje_alerta("#campo_mensaje_productos","Disculpe, la opción de compra aun no se encuentra activa","alert-info");
			else
				mensaje_alerta("#campo_mensaje_productos","Excuse me, the purchase option is not yet active","alert-info");
			*/
			//----------------------------------------
			if($scope.validar_form(id_productos,cantidad)==true){
				$http.post($scope.base_url+"WebProductos/agregarCarrito",
				{
					
					'id'	  : id_productos,
					'cantidad': cantidad,
					'monto_individual': monto_individual,
					'monto_total': monto_total,
					'id_idioma': idioma,
					'id_producto_clonado':id_producto_clonado,
					'cantidad_existencia':$scope.producto.cantidad,
					'id_talla':$scope.talla.id_talla,
					'id_color':$scope.color.id_color,
					'cantidad_total':$scope.cantidad.cantidad,
					
				}).success(function(data, estatus, headers, config){
					$scope.mensajes  = data;
					//console.log($scope.mensajes)
					if($scope.mensajes.mensaje == "registro_procesado"){
						if(idioma==1)
							mensaje_alerta("#campo_mensaje_productos","Se ha agregado el producto a su carrito de compras","alert-info");
						else
							mensaje_alerta("#campo_mensaje_productos","The product was add to your cart","alert-info");
						setTimeout(function(){
							if(idioma=="1")
								var url_carrito = $scope.base_url+"carrito";
							else
								var url_carrito = $scope.base_url+"cart";
							$("#formProducto").attr("action",url_carrito)
							$("#formProducto").attr("method","post");
							$("#formProducto").submit()
						},2000)
					}else if($scope.mensajes.mensaje=="excede_cantidad"){
						if(idioma==1)
							mensaje_alerta("#campo_mensaje_productos","El producto no puede ser agregado al carrito porque excede la cantidad en inventario","alert-warning");
						else
							mensaje_alerta("#campo_mensaje_productos","The product cannot be added to the cart because it exceeds the amount in inventory","alert-warning");

					}
					else{
						if(idioma==1)
							mensaje_alerta("#campo_mensaje_productos","Ocurrío un error inesperado","alert-warning");
						else
							mensaje_alerta("#campo_mensaje_productos","An unexpected error occurred","alert-warning");
					}
				}).error(function(data,estatus){
					console.log(data);
				});
			}
			//----------------------------------------
		}
		//-----------------------------------------------------------
		$scope.validar_form = function(id_productos,cantidad){

			if(id_productos==""){
				if($scope.idioma==1)
					mensaje_alerta("#campo_mensaje_productos","Debe haber seleccionado un producto!","alert-warning");
				else
					mensaje_alerta("#campo_mensaje_productos","You must have selected a producto!","alert-warning");
				return false;
			} else if(cantidad=="0"){
				if($scope.idioma==1)
					mensaje_alerta("#campo_mensaje_productos","Debe haber seleccionado cantidad!","alert-warning");
				else
					mensaje_alerta("#campo_mensaje_productos","You must have selected quantity!","alert-warning");
				return false;
			}else{
				return true;
			}
		}
		//-----------------------------------------------------------
		$scope.asignarCantidad = function(){
			//console.log($scope.cantidad.cantidad);
			var cantidad = $scope.cantidad.cantidad
			for(i=1;i<=cantidad;i++){
				option = '<option>'+i+'</option>'
				$("#cantidadProducto").append(option);
			}
		}
		$scope.VaciaCantidad = function(){
			//var cantidad = $scope.cantidad.cantidad
			//$("#cantidadProducto").append(option);
			$('#cantidadProducto' ).children().remove();
			option = "<option>"+0+"</option>";
			$("#cantidadProducto").append(option);
		}


		$scope.VaciaColor = function(){
			//console.log($scope.descripcion_talla,$scope.talla.descripcion_talla);

			if($scope.descripcion_talla != $scope.talla.descripcion_talla){
				$scope.talla.descripcion_talla = $scope.talla.descripcion_talla;

				$scope.colores = "";
				$scope.color = "";
				$scope.ColoresExistentes();
			 }
		}
		
		//-----------------------------------------------------------
		$scope.consultar_mas_producto = function(id){
			var start = 0;
			var limit = 3;
			var id =
			productoFactory.asignar_valores($scope.idioma,$scope.base_url,id,limit,start)
			productoFactory.cargar_mas_producto(function(data){
				$scope.mas_productos=data;
				//console.log($scope.mas_productos)
			});
		}		
		//-----------------------------------------------------------
		$scope.consultar_producto_slug();

		$scope.definir_url_home();

		
	});
//------------------------------------------------	
/*function bandera(){
	alert("aqui!");
}*/

function super_hover(){
	$('.img-slider-ppal').zoom({ on:'mouseover' });				
}
//super_hover()
//-----------------------------------------------