angular.module("ZougZougApp")
	.controller("MainController", function($scope,$sce,$http,$compile,$location,funcionaFactory,contactosFactory,direccionFactory,portafoliosFactory,blogFactory,serviciosFactory,clientesFactory,noticiaHomeFactory,sliderFactory,nosotrosFactory,multIdioma,productosHomeFactory){
		//Cuerpo declaraciones
		$scope.contactos = {
								'id':'',
								'nombres':'',
								'telefono':'',
								'email':'',
								'mensaje':''
		}
		
		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#id_idioma").val();
		$(".menu_web").removeClass("active")
		$("#menu1").addClass("active")
		//------------------------------------------------------------
		/*
		*	Bloque obligatorio para todo controlador
		*/
		/*
		*	Define segun el valor del idioma los titulos
		*/
		$scope.definir_url_home = function(){
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
			//console.log($scope.btn);
		}
		/*
		*
		*/
		//-----------------------------------------------------------
		//Validando el idioma
		//Multi lenguaje para home

		///Se debe descomentar....
		/*if(($location.path()=="/home")||($location.path()=="/")){
			$scope.idioma=2
		}else{
			$scope.idioma=1
		}*/

		$scope.arreglo_id = []

		$scope.marcas_rutas = []
		//--
		$scope.cargar_home = function(){
			//1- Defino las url de la pantalla}
			$scope.definir_url_home();
			//2-consulto la info del home
			//$scope.consultar_slider();
			$scope.consultar_nosotros();
			//3-Hago el llamado de el resto de los metodos..
			$scope.consultar_productos();
			$scope.consultar_subnoticias();
			//carga_inicio();

		}
		/////////////////////
		$scope.consultar_productos = function(){
			//console.log("si llega");
			vacio = ""
			var offset = 0;
			var limit = 2;
    		productosHomeFactory.asignar_valores($scope.idioma,'',limit,offset,$scope.base_url);
			productosHomeFactory.cargar_productos(function(data){
				$scope.productos = data;
				//console.log($scope.productos[0].titulo);
				/*
				$scope.productos_footer1.titulo = $scope.productos[0].titulo
				$scope.productos_footer2.titulo = $scope.productos[1].titulo
				$scope.productos_footer3.titulo = $scope.productos[2].titulo
				$scope.productos_footer1.slug = $scope.productos[0].slug
				$scope.productos_footer2.slug = $scope.productos[1].slug
				$scope.productos_footer3.slug = $scope.productos[2].slug*/
			});
		}

		////////////////////////
		$scope.consultar_subnoticias = function(){
			vacio = ""
			var offset = 0;
			var limit = 2;
    		noticiaHomeFactory.asignar_valores($scope.idioma,'',limit,offset,$scope.base_url);
			noticiaHomeFactory.cargar_noticias_filtro(function(data){
				$scope.sub_noticias = data;
				//console.log($scope.sub_noticias);
			});
		}
		//--
		$scope.consultar_slider = function(){
			sliderFactory.asignar_valores($scope.idioma,$scope.base_url)
			sliderFactory.cargar_slider(function(data){
				$scope.slider=data;
				//console.log($scope.slider)
			})
		}
		$scope.armar_slider = function(){
			var slider = ""
			/*$.each($scope.slider, function( index, value ) {
			  slider+= '<li data-transition="slidehorizontal" data-slotamount="5" data-masterspeed="700"  data-title="Slide '+value.num+'">'
			   slider+='<div class="tp-caption rs-caption-1 sft start" style="left:0px;"'
               slider+=       '  data-hoffset="0"'
               slider+=       '  data-x="0px"'
               slider+=       '  data-y="0"'
               slider+=       '  data-speed="800"'
               slider+=       '  data-start="1500"'
               slider+=       '  data-easing="Back.easeInOut"'
               slider+=       '  data-endspeed="300">'
               slider+=       '  <img src="'+$scope.base_url+value.ruta+'" alt="slider-image" style="width: auto; height: auto;left:0px;">'
               slider+=       '</div>'
               slider+=       '<div class="tp-caption rs-caption-2 sft"'
               slider+=       '  data-hoffset="0"'
               slider+=       '  data-y="100"'
               slider+=       '  data-x="600"'
		         slider+=        '  data-speed="800"'
		         slider+=        '  data-start="2000"'
		         slider+=        '  data-easing="Back.easeInOut"'
		         slider+=        '  data-endspeed="300">'
		         slider+=        '  <span style="color:#fff;font-weight: bold;text-shadow: 2px 2px #000;">'+value.titulo+'</span>'
		         slider+=        '</div>'
		         slider+=        '<div class="tp-caption rs-caption-3 sft"'
		         slider+=        '  data-hoffset="0"'
		         slider+=        ' data-y="100"'
		         slider+=        '  data-x="600"'
		         slider+=        '  data-speed="1000"'
		         slider+=            '  data-start="3000"'
		         slider+=            '  data-easing="Power4.easeOut"'
		         slider+=            '  data-endspeed="300"'
		         slider+=            '  data-endeasing="Power1.easeIn"'
		         slider+=            '  data-captionhidden="off" style="width: 90vh;">'
		         slider+=            '  <span style="color:#fff;font-weight: bold;text-shadow: 2px 2px #000;" ng-bind-html="'+value.descripcion+'"></span>'
		         slider+=            '</div>'
		         slider+=            '<div class="tp-caption rs-caption-4 sft"'
		         slider+=            '  data-hoffset="0"'
		         slider+=            '  data-y="310"'
		         slider+=            '  data-x="600"'
		         slider+=            '  data-speed="800"'
		         slider+=            '  data-start="3500"'
		         slider+=            '  data-easing="Power4.easeOut"'
		         slider+=            '  data-endspeed="300"'
		         slider+=            '  data-endeasing="Power1.easeIn"'
		         slider+=            '  data-captionhidden="off">'
		         slider+=            '  <span class="page-scroll"><a href="'+$scope.base_url+value.url+'" class="btn primary-btn" style="border: solid #b9ca46;margin-top:20px;">'+value.boton
		         slider+=            '<i class="fa fa-chevron-right"></i></a></span>'
		         slider+=            '</div>'
		         slider+=        '</div>'
		         slider+=    '</li>';
			});}*/
			//$("#ul_slider").html(slider);
			//console.log(slider);
		}
		//--
		$scope.consultar_nosotros = function(){
			nosotrosFactory.asignar_valores($scope.idioma,$scope.base_url)
			nosotrosFactory.cargar_nosotros(function(data){
				$scope.nosotros=data[0];
				//console.log($scope.nosotros);
			});
		}
		//--
		$scope.validar_guardar = function(){
			if($scope.contactos.nombres==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar sus nombres", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter their names", "alert-danger");

				return false;
			}else
			if($("#contactos_email").val()==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar una dirección de correo v&aacute;lida", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter valid email direction", "alert-danger");
				return false;
			}
			else
			if($("#contactos_telefono").val()==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar un número de teléfono válido", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter valid telephone number", "alert-danger");
				return false;
			}
			else if($scope.contactos.mensaje==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar un mensaje", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter a message", "alert-danger");
				return false;
			}else
				return true;
		}
		//--
		//--Registrar contactos
		$scope.registrar_contactos = function(){
			if($scope.validar_guardar()==true){
				//uploader_reg("#campo_mensaje_clientes","#nombres_clientes,#email_clientes,#telefono_clientes,#mensaje_clientes");
				$http.post($scope.base_url+"/WebInicio/registrarContactos",{
					'nombres': $scope.contactos.nombres,
					'email': $scope.contactos.email,
					'telefono': $scope.contactos.telefono,
					'mensaje': $scope.contactos.mensaje,
					'id_idioma':$scope.idioma
				}).success(function(data, estatus, headers, config){
					$scope.mensajes  = data;
					if($scope.mensajes.mensaje == "registro_procesado"){
						if($scope.idioma == "1")
							mensaje_alerta("#campo_mensaje_clientes","Gracias por tu tiempo, tu mensaje ha sido enviado!","alert-success");
						else
							mensaje_alerta("#campo_mensaje_clientes","Thank you for your time, your message has been sent!","alert-success");
						$scope.limpiar_cajas();
					}else if($scope.mensajes.mensaje =="existe"){
						if($scope.idioma == "1")
							mensaje_alerta("#campo_mensaje_clientes","Ya fue registrado un contacto con esa dirección de email","alert-success");
						else
							mensaje_alerta("#campo_mensaje_clientes","A contact has already been registered with that email address","alert-success");
					}else if($scope.mensajes.mensaje =="error_correo"){
						mensaje_alerta("#campo_mensaje_clientes","Error durante el envío de email","alert-success");
					}
					else{
						mensaje_alerta("#campo_mensaje_clientes","Ocurrió un error inesperado","alert-danger");

					}
					//desbloquear_pantalla("#campo_mensaje_clientes","#nombres_clientes,#email_clientes,#telefono_clientes,#mensaje_clientes")
				}).error(function(data,estatus){
					showErrorMessage("Ocurrió un error inesperado:"+data);
				})
			}
		}

		//--Limpiar cajas
		$scope.limpiar_cajas = function(){
			$scope.contactos = {
									"id":"",
									"nombres":"",
									"email":"",
									"telefono":"",
									"mensaje":""
			}
		}
		//--
		//--Limpiar cajas
		$scope.limpiar_cajas = function(){
			$scope.contactos = {
									"id":"",
									"nombres":"",
									"email":"",
									"telefono":"",
									"mensaje":""
			}
		}
		//--
		$scope.recorrer_direcciones_home = function(){
			//console.log($scope.direccion_f);
			google.maps.event.addDomListener(window, 'load', initialize_standar($scope.direccion_f[0].latitud,$scope.direccion_f[0].longitud,'0'));
		}
		//--
		$scope.sanitizeMe = function(text) {
		    return $sce.trustAsHtml(text)
		}
		//---Bloque de llamados
		$scope.cargar_home()
		//--------------------------------------
	});
