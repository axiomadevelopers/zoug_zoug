angular.module("ZougZougApp")
	.controller("inicioController", function ($scope, $http, $window, $compile, $location, direccionFactory, footerFactory, upload, multIdioma) {
		//Bloque de metodos
		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#id_idioma").val();
		$scope.tp_login = $("#tp_login").val();
		///---
		$scope.currentTab = 'login_email';
		$scope.registro = {
			'nombre': '',
			'correo': '',
			'clave': '',
			'r_clave': ''
		}
		$scope.vaciarDatos = function () {
			$scope.registro = {
				'nombre': '',
				'correo': '',
				'clave': '',
				'r_clave': ''
			}
			//En español
			if($scope.idioma==1){
				if($scope.currentTab=="registro_email"){
				$scope.menu.titulo_inicio_sesion="Crear Cuenta"
				}else if($scope.currentTab=="login_email"){
					$scope.menu.titulo_inicio_sesion="Inicio de Sesión"
				}else if($scope.currentTab=="olvide_clave"){
					$scope.menu.titulo_inicio_sesion="Cambiar Contraseña"
				}
				//En ingles
			}else if($scope.idioma==2){
				if($scope.currentTab=="registro_email"){
				$scope.menu.titulo_inicio_sesion="Create Account"
				}else if($scope.currentTab=="login_email"){
					$scope.menu.titulo_inicio_sesion="Sign In"
				}else if($scope.currentTab=="olvide_clave"){
					$scope.menu.titulo_inicio_sesion="Change Password"
				}
			}
			
		}


		//console.log($scope.tp_login);
		if($scope.tp_login==1){
			$("#aviso").modal('show');
		}

		$scope.inicio = function (id, nombre_apellido, correo, estatus) {
			$scope.inicio = {
				'id': id,
				'nombre_apellido': nombre_apellido,
				'correo': correo,
				'estatus': estatus,

			}
			$scope.login = true;


			/* $scope.inicio.nombre_apellido = nombre_apellido;
			$scope.inicio.id_correo = id_correo;  */

			//console.log($scope.login);
		};
		$scope.registrarUsuario = function () {
			if ($scope.validar_registro() == true) {
				//Para guardar
				$scope.insertar_personas();
			}
		}
		$scope.validar_registro = function () {
			aviso_codigo
			var exr = /^\w+[a-z_0-9\-\.]+@\w+[0-9a-z\-\.]+\.[a-z]{2,4}$/i;
			if ($scope.registro.nombre == "") {
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_registro", "Debe ingresar el nombre completo", "alert-warning");
				}
				else{
					mensaje_alerta("#campo_mensaje_registro", "You must enter the full name", "alert-warning");
				}

				return false;
			}
			if ($scope.registro.correo == "") {
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_registro", "Debe ingresar un correo electronico", "alert-warning");
				}
				else{
					mensaje_alerta("#campo_mensaje_registro", "You must enter an email", "alert-warning");
				}
				return false;
			} else if (!(exr.test($scope.registro.correo))) {
				$scope.registro.correo = "";
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_registro", "Debe ingresar una dirección de correo v&aacute;lida: xxxxxxxxxxxxxx@host.com", "alert-warning");
				}
				else{
					mensaje_alerta("#campo_mensaje_registro", "You must enter a valid email address: xxxxxxxxxxxxxx@host.com", "alert-warning");
				}
			} else if ($scope.registro.clave == "") {
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_registro", "Debe ingresar una contraseña", "alert-warning");
				}
				else{
					mensaje_alerta("#campo_mensaje_registro", "You must enter a password", "alert-warning");
				}
				return false;
			} else if ($scope.registro.r_clave == "") {
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_registro", "Debe repetir la contraseña", "alert-warning");
				}
				else{
					mensaje_alerta("#campo_mensaje_registro", "You must repeat the password", "alert-warning");
				}
				return false;
			} else if ($scope.registro.r_clave != $scope.registro.clave) {
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_registro", "Las contraseñas no coinciden", "alert-warning");
				}
				else{
					mensaje_alerta("#campo_mensaje_registro", "Passwords do not match", "alert-warning");
				}
				return false;
			}else if(document.getElementById('clave').value.length < 8){
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_registro", "Debe ingresar una contraseña con 8 caracteres", "alert-warning");
				}
				else{
					mensaje_alerta("#campo_mensaje_registro", "You must enter a password with 8 characters", "alert-warning");
				}
			} else {
				return true;
			}
		}
		/////////// Registro de usuarios
		$scope.insertar_personas = function () {
			//console.log($scope.registro);

			$http.post($scope.base_url + "/WebLogin/registrarPersonas", {
				'nombre': $scope.registro.nombre,
				'correo': $scope.registro.correo,
				'clave': $scope.registro.clave,
				'idioma': $scope.idioma,
				
			}).success(function (data, estatus, headers, config) {
				$scope.mensajes = data;
				if ($scope.mensajes.mensaje == "registro_procesado") {
					if($scope.idioma==1){
						mensaje_alerta("#campo_mensaje_registro", "El registro fue realizado de manera exitosa", "alert-info");
					}else{
						mensaje_alerta("#campo_mensaje_registro", "El registro fue realizado de manera exitosa", "alert-info");	
					}
					$scope.currentTab = 'aviso_codigo';

				} else if ($scope.mensajes.mensaje == "existe") {

					if($scope.idioma==1){
						mensaje_alerta("#campo_mensaje_registro", "Correo no disponible", "alert-warning");
					}else{
						mensaje_alerta("#campo_mensaje_registro", "Mail not available", "alert-warning");	
					}
				} else {
					if($scope.idioma==1){
						mensaje_alerta("#campo_mensaje_registro", "Ocurrío un error inesperado", "alert-warning");
					}else{
						mensaje_alerta("#campo_mensaje_registro", "An unexpected error would occur", "alert-warning");	
					}
				}
				//$mensajes["mensaje"] = "no_registro";

			}).error(function (data, estatus) {
				console.log(data);
			});
		}
		$scope.iniciarUsuario = function () {
			if ($scope.validar_inicio() == true) {
				//Para guardar
				//console.log($scope.registro);
				$scope.ingresarUsuario();

			}
		}
		$scope.validar_inicio = function () {
			var exr = /^\w+[a-z_0-9\-\.]+@\w+[0-9a-z\-\.]+\.[a-z]{2,4}$/i;
			if ($scope.registro.correo == "") {
				if($scope.idioma==1){
						mensaje_alerta("#campo_mensaje_inicio", "Debe ingresar un correo electronico", "alert-warning");
					}
					else{
						mensaje_alerta("#campo_mensaje_inicio", "You must enter an email", "alert-warning");
					}
					return false;
				}
				else if (!(exr.test($scope.registro.correo))) {
				$scope.registro.correo = "";
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_inicio", "Debe ingresar una dirección de correo v&aacute;lida: xxxxxxxxxxxxxx@host.com", "alert-warning");
				}else{
					mensaje_alerta("#campo_mensaje_inicio", "You must enter a valid email address: xxxxxxxxxxxxxx@host.com", "alert-warning");
				}
			
			}else if ($scope.registro.clave == "") {
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_inicio", "Debe ingresar una contraseña", "alert-warning");
				}
				else{
					mensaje_alerta("#campo_mensaje_inicio", "You must enter a password", "alert-warning");
				}
				return false;
			} else {
				return true;
			}
		}
		$scope.ingresarUsuario = function () {
			$http.post($scope.base_url + "/WebLogin/inicioSesion", {
				'correo': $scope.registro.correo,
				'clave': $scope.registro.clave,
			}).success(function (data, estatus, headers, config) {
				$scope.mensajes = data;
				//console.log(data);
				if ($scope.mensajes.mensaje == "inicio_exitoso") {
					$scope.ir_dashboard();
				} else if ($scope.mensajes.mensaje == "no_existe") {
					if($scope.idioma==1){
						mensaje_alerta("#campo_mensaje_inicio", "No se encuentra una cuenta asociada a este correo", "alert-warning");
					}
					else{
						mensaje_alerta("#campo_mensaje_inicio", "Cannot find an account associated with this email", "alert-warning");
					}
				} else if ($scope.mensajes.mensaje == "no_clave") {
					if($scope.idioma==1){
						mensaje_alerta("#campo_mensaje_inicio", "Contraseña incorrecta", "alert-warning");
					}
					else{
						mensaje_alerta("#campo_mensaje_inicio", "Incorrect password", "alert-warning");
					}
				} else if ($scope.mensajes.mensaje == "inactivo") {
					if($scope.idioma==1){
						mensaje_alerta("#campo_mensaje_inicio", "Usuario inactivo, por favor revisar su correo para realizar la activación o contáctenos", "alert-warning");
					}
					else{
						mensaje_alerta("#campo_mensaje_inicio", "Inactive user, please check your email to activate or contact us", "alert-warning");
					}
				} else {
					if($scope.idioma==1){
						mensaje_alerta("#campo_mensaje_inicio", "Ocurrío un error inesperado", "alert-warning");
					}else{
						mensaje_alerta("#campo_mensaje_inicio", "An unexpected error would occur", "alert-warning");	
					}
				}
				$mensajes["mensaje"] = "no_registro";
			}).error(function (data, estatus) {
				console.log(data);
			});
		}
		$scope.ir_dashboard = function () {
			window.location.reload(false);

		}
		$scope.cambiarClave = function () {
			if ($scope.validar_correo() == true) {
				//Para guardar
				$scope.olvide_clave();

			}
		}
		$scope.validar_correo = function () {
			var exr = /^\w+[a-z_0-9\-\.]+@\w+[0-9a-z\-\.]+\.[a-z]{2,4}$/i;

			if ($scope.registro.correo == "") {
				if($scope.idioma==1){
				mensaje_alerta("#campo_mensaje_clave", "Debe ingresar un correo electronico", "alert-warning");
			}
			else{
				mensaje_alerta("#campo_mensaje_clave", "You must enter an email", "alert-warning");
			}
				return false;
			} else if (!(exr.test($scope.registro.correo))) {
				$scope.registro.correo = "";
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_clave", "Debe ingresar una dirección de correo v&aacute;lida: xxxxxxxxxxxxxx@host.com", "alert-warning");
				}
				else{
					mensaje_alerta("#campo_mensaje_clave", "You must enter a valid email address: xxxxxxxxxxxxxx@host.com", "alert-warning");
				}
			} else {
				return true;
			}
		}
		$scope.olvide_clave = function () {
			$http.post($scope.base_url + "/WebLogin/olvide_clave", {
				'correo': $scope.registro.correo,
				'idioma': $scope.idioma,


			}).success(function (data, estatus, headers, config) {
				$scope.mensajes = data;
				//console.log(data);
				//----------------------------------------------------
				if( $scope.mensajes.mensaje == "no_puede_recupe"){
					if($scope.idioma==1){
						mensaje_alerta("#campo_mensaje_clave", "No puede recuperar contraseña de un usuario no registrado por correo", "alert-warning");
					}else{
						mensaje_alerta("#campo_mensaje_clave", "You cannot recover the password of an unregistered user by mail", "alert-warning");	
					}
				}else if ($scope.mensajes.mensaje == "registro_procesado") {
				//----------------------------------------------------
				
					$scope.currentTab = 'aviso_clave';


				} else if ($scope.mensajes.mensaje == "no_existe") {
					if($scope.idioma==1){
						mensaje_alerta("#campo_mensaje_clave", "No se encuentra una cuenta asociada a este correo", "alert-warning");
					}else{
						mensaje_alerta("#campo_mensaje_clave", "Cannot find an account associated with this email", "alert-warning");	
					}
				
				} else {
					if($scope.idioma==1){
						mensaje_alerta("#campo_mensaje_clave", "Ocurrío un error inesperado", "alert-warning");
					}else{
						mensaje_alerta("#campo_mensaje_clave", "An unexpected error would occur", "alert-warning");	
					}
				}
				$mensajes["mensaje"] = "no_registro";
			}).error(function (data, estatus) {
				console.log(data);
			});
		}
		
			/* $("#aviso").modal('show'); */

		$scope.consultar_direcciones_todas = function () {
			offset = 0
			limit = 2
			direccionFactory.asignar_valores(offset, limit, "");
			direccionFactory.cargar_direcciones_todas(function (data) {
				$scope.direccion_es = data["es"];
				$scope.direccion_en = data["en"];
				//$scope.direccion_f = data["es"];
				//console.log($scope.direccion_f);
				$scope.ruta = $location.path()
				ruta = $scope.ruta
				vec_ruta = ruta.split("/")
				if (($scope.ruta == '/') || ($scope.ruta == '/inicio') || ($scope.ruta == '/nosotros') || ($scope.ruta == '/contactanos') || ($scope.ruta == '/productos') || ($scope.ruta == '/portafolio') || ($scope.ruta == '/noticias') || ($scope.ruta == '/contactanos') || (vec_ruta[1] == 'portafolio') || ((vec_ruta[1] == "noticias") && (vec_ruta[2] == "es"))) {
					$scope.direccion_f = data["es"];
				}
				if (($scope.ruta == '/home') || ($scope.ruta == '/about') || ($scope.ruta == '/products') || ($scope.ruta == '/portfolio') || ($scope.ruta == '/contact_us') || ($scope.ruta == '/news') || (vec_ruta[1] == 'portfolio') || ((vec_ruta[1] == "news") && (vec_ruta[2] == "en"))) {
					$scope.direccion_f = data["en"];
				}
				//console.log($scope.direccion_f);
			});
		}
		//----
		$scope.consultar_redes = function () {
			footerFactory.asignar_valores($scope.idioma, $scope.base_url);
			footerFactory.cargar_redes(function (data) {
				$scope.redes = data;
				//console.log($scope.redes);
			});
		}
		$scope.consultar_footer = function () {
			footerFactory.asignar_valores($scope.idioma, $scope.base_url);
			footerFactory.cargar_footer(function (data) {
				$scope.footer = data[0];
				$scope.lineas = data["lineas"]
				//console.log($scope.lineas);
			});
		}

		$scope.consultar_meta = function () {
			footerFactory.cargar_meta_tag(function (data) {
				$scope.meta_descripcion = data.descripcion;
				$scope.meta_palabras_claves = data.palabras_claves;
				$("meta[name='keywords']").attr('content', $scope.meta_palabras_claves);
				$("meta[name='description']").attr('content', $scope.meta_descripcion);
				//console.log(data);
			});
		}

		//-------------------------------------------------------------
		//--Coloca la bandera segun la url: Segun lo que tenga la url se colocan los titulos del menu
		$scope.definir_url = function () {
			if ($scope.idioma == '1') {
				res = multIdioma.cargar_inicio_espain($scope.base_url)
			} else {
				res = multIdioma.cargar_inicio_uk($scope.base_url)
			}
			$scope.menu = res.titulo
			$scope.btn = res.btn
			$scope.url = res.url
			//console.log($scope.menu)
		}
		//----------------------------------------
		$scope.cambio_idioma = function () {
			res = multIdioma.cambiar_idioma($scope.base_url)
			//$scope.menu = res.titulo
			$scope.btn = res.btn
			$scope.url = res.url
			$scope.idioma_inicio = res.idioma_inicio
			$scope.cambiar_page_idioma()

			//Cuando agregue las direcciones
			/*if($scope.idioma_inicio=="1"){
				$scope.direccion_f = $scope.direccion_es;
			}else{
				$scope.direccion_f = $scope.direccion_en;
			}*/
		}
		//--Para cambiar el path

		//--Para cambiar los titulos y el path
		$scope.cambiar_page_idioma = function () {
			$scope.ruta = window.location.href
			var cadena = window.location.href
			$scope.ruta = cadena.replace($scope.base_url, "/")
			switch ($scope.ruta) {

				case '/':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url;
					else
						window.location.href = $scope.base_url + "home";
					break;
				case '/inicio':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url;
					else
						window.location.href = $scope.base_url + "home";
					break;

				case '/home':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url;
					else
						window.location.href = $scope.base_url + "home";
					break;

				case '/nosotros':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "nosotros";
					else
						window.location.href = $scope.base_url + "about";
					break;

				case '/about':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "nosotros";
					else
						window.location.href = $scope.base_url + "about";
					break;

				case '/productos':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "productos";
					else
						window.location.href = $scope.base_url + "products";
					break;

				case '/products':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "productos";
					else
						window.location.href = $scope.base_url + "products";
					break;

				case '/portafolio':
					if ($scope.idioma_inicio == 1)
						$location.path("/portafolio");
					else
						$location.path("/portfolio");
					break;

				case '/portfolio':
					if ($scope.idioma_inicio == 1)
						$location.path("/portafolio");
					else
						$location.path("/portfolio");
					break;

				case '/noticias':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "noticias";
					else
						window.location.href = $scope.base_url + "news";
					break;

				case '/news':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "noticias";
					else
						window.location.href = $scope.base_url + "news";
					break;

				case '/contactanos':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "contactanos";
					else
						window.location.href = $scope.base_url + "contact_us";
					break;

				case '/contact_us':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "contactanos";
					else
						window.location.href = $scope.base_url + "contact_us";
					break;

				case '/carrito':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "carrito";
					else
						window.location.href = $scope.base_url + "cart";
					break;

				case '/cart':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "carrito";
					else
						window.location.href = $scope.base_url + "cart";
					break;
				case '/orden':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "orden";
					else
						window.location.href = $scope.base_url + "order";
					break;
				case '/order':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "orden";
					else
						window.location.href = $scope.base_url + "order";
					break;

				case '/orden_usuario':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "orden_usuario";
					else
						window.location.href = $scope.base_url + "order_us";
					break;
				case '/order_us':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "orden_usuario";
					else
						window.location.href = $scope.base_url + "order_us";
					break;
				case '/politicas_privacidad':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "politicas_privacidad";
					else
						window.location.href = $scope.base_url + "privacy_policy";
					break;
				case '/privacy_policy':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "politicas_privacidad";
					else
						window.location.href = $scope.base_url + "privacy_policy";
					break;
				case '/terminos':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "terminos";
					else
						window.location.href = $scope.base_url + "terms";
					break;
				case '/terms':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "terminos";
					else
						window.location.href = $scope.base_url + "terms";
					break;
				default:
					ruta = $scope.ruta;
					vec_ruta = ruta.split("/")
					//--
					//Si es inversiones
					//---------------------------------------------
					if (vec_ruta[1] == "products")
						window.location.href = $scope.base_url + "productos";
					else if (vec_ruta[1] == "productos")
						window.location.href = $scope.base_url + "products";
					//---------------------------------------------
					if (vec_ruta[1] == "noticias") {
						window.location.href = $scope.base_url + "news";
					} else
					if (vec_ruta[1] == "news") {
						window.location.href = $scope.base_url + "noticias";
					}
					//-------------------------------------------------
					if (vec_ruta[1] == "register") {
						resto_url = $scope.base_url + "registro"
						a = 0
						vec_ruta.forEach(function (element) {
							a++;
							if (a > 2) {
								resto_url += "/" + element
							}
						});
						window.location.href = resto_url;
						//window.location.href=$scope.base_url+"procesar_compra/"+vec_ruta[2];
					}
					//------------------------------------------------
					else if (vec_ruta[1] == "registro") {
						resto_url = $scope.base_url + "register"
						b = 0
						vec_ruta.forEach(function (element) {
							b++;
							if (b > 2) {
								resto_url += "/" + element
							}
						});
						window.location.href = resto_url;
					}
					//------------------------------------------------
					if (vec_ruta[1] == "password_recovery") {
						resto_url = $scope.base_url + "recuperar_clave"
						a = 0
						vec_ruta.forEach(function (element) {
							a++;
							if (a > 2) {
								resto_url += "/" + element
							}
						});
						window.location.href = resto_url;
						//window.location.href=$scope.base_url+"procesar_compra/"+vec_ruta[2];
					}
					//------------------------------------------------
					else if (vec_ruta[1] == "recuperar_clave") {
						resto_url = $scope.base_url + "password_recovery"
						b = 0
						vec_ruta.forEach(function (element) {
							b++;
							if (b > 2) {
								resto_url += "/" + element
							}
						});
						window.location.href = resto_url;
					}
					//------------------------------------------------
					if (vec_ruta[1] == "checkout_step") {
						resto_url = $scope.base_url + "procesar_compra"
						a = 0
						vec_ruta.forEach(function (element) {
							a++;
							if (a > 2) {
								resto_url += "/" + element
							}
						});
						window.location.href = resto_url;
						//window.location.href=$scope.base_url+"procesar_compra/"+vec_ruta[2];
					}
					//------------------------------------------------
					else if (vec_ruta[1] == "procesar_compra") {
						resto_url = $scope.base_url + "checkout_step"
						b = 0
						vec_ruta.forEach(function (element) {
							b++;
							if (b > 2) {
								resto_url += "/" + element
							}
						});
						window.location.href = resto_url;
					}
					break;
					//-----------------------------------------------
			}
			//---
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
			//--
		}
		//---------------------------------------------------------------------------------
		//--Para cambiar los titulos y el path
		/*$scope.cambiar_page_idioma_inicio = function(){
			$scope.ruta = $location.path()
			//alert($scope.ruta)
			//$scope.titulos_home = multIdioma.cambiar_idioma_home()
			switch($scope.ruta){
				case '/':
					$("#idioma").removeClass("esta_ingles").addClass("esta_espaniol");
					$scope.idioma_inicio = 1
					break;
				case '/inicio':
					$("#idioma").removeClass("esta_ingles").addClass("esta_espaniol");
					$scope.idioma_inicio = 1
					break;
				case '/home':
					$("#idioma").removeClass("esta_espaniol").addClass("esta_ingles");
					$scope.idioma_inicio = 2
					break;
				case '/nosotros':
					$("#idioma").removeClass("esta_ingles").addClass("esta_espaniol");
					$scope.idioma_inicio = 1
					break;
				case '/about':
					$("#idioma").removeClass("esta_espaniol").addClass("esta_ingles");
					$scope.idioma_inicio = 2
					break;
				case '/contactanos':
					$("#idioma").removeClass("esta_ingles").addClass("esta_espaniol");
					$scope.idioma_inicio = 1
					break;
				case '/contact_us':
					$("#idioma").removeClass("esta_espaniol").addClass("esta_ingles");
					$scope.idioma_inicio = 2
					break;
				case '/blog/es/':
					$("#idioma").removeClass("esta_ingles").addClass("esta_espaniol");
					$scope.idioma_inicio = 1
					//alert("es")
					break;
				case '/blog/en/':
					$("#idioma").removeClass("esta_espaniol").addClass("esta_ingles");
					$scope.idioma_inicio = 2
					//alert("en")
					break;
			}
			multIdioma.definir_idioma($scope.base_url);
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
		}*/
		//-------------------------------------------------------------
		//Bloque de llamados
		//$scope.consultar_direcciones();

		$scope.definir_url();
		$scope.consultar_footer();
		$scope.consultar_redes();
		$scope.consultar_meta();
		limpiarHash();
	});