angular.module("ZougZougApp")
	.controller("recoveryPasswordController", function($scope,$http,$sce,$compile,$location,funcionaFactory,serviciosFactory,multIdioma,carritoFactory){
		//Cuerpo declaraciones
		$scope.productos = {}
		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#id_idioma").val();
		$scope.clave = "";
		$scope.clave_r = "";
		$scope.id = $("#idUser").html();
		$scope.codigo = $("#codigo").html();
		//console.log($scope.codigo);
		$scope.currentTab1 = '';

		$scope.correoUsuario = $("#correoUsuario").html();

		$scope.nombre_apellido = $("#nombre_apellido").html();
		$(".menu_web").removeClass("active")

		$(".primera").removeClass("side-nav")

		$( "#target" ).click(function() {
		  alert( "Handler for  called." );
		});
		//console.log($scope.correoUsuario);
		//console.log($scope.idioma);

		$scope.definir_url_home = function(){
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
		}
		//----
		$scope.backpageCart = function(){
			var idioma = $scope.idioma
			if(idioma=="1")
				var url_carrito = $scope.base_url+"carrito";
			else
				var url_carrito = $scope.base_url+"cart";
			$("#formOrderProducto").attr("action",url_carrito)
			$("#formOrderProducto").attr("method","post");
			$("#formOrderProducto").submit()
		}

		$scope.verificarClave = function(){
			if($scope.validar_form()==true){
					$scope.cambiarClave();
			}
		}					
		$scope.validar_form = function () {
			
			if ($scope.clave == "") {
				//console.log($scope.clave);
				
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_cambio_clave", "Debe ingresar una contraseña", "alert-warning");
				}
				else{
					mensaje_alerta("#campo_mensaje_cambio_clave", "You must enter a password", "alert-warning");
				}
				return false;
			} else if ($scope.clave_r == "") {
				/*console.log($scope.clave_r);
				console.log($scope.clave);*/
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_cambio_clave", "Debe repetir la contraseña", "alert-warning");
				}
				else{
					mensaje_alerta("#campo_mensaje_cambio_clave", "You must repeat the password", "alert-warning");
				}
				return false;
			} else if ($scope.clave != $scope.clave_r) {
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_cambio_clave", "Las contraseñas no coinciden", "alert-warning");
				}
				else{
					mensaje_alerta("#campo_mensaje_cambio_clave", "Passwords do not match", "alert-warning");
				}
				return false;
			} else if(document.getElementById('clave_cambio').value.length < 8){
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_cambio_clave", "Debe ingresar una contraseña con 8 caracteres", "alert-warning");
				}
				else{
					mensaje_alerta("#campo_mensaje_cambio_clave", "You must enter a password with 8 characters", "alert-warning");
				}
			}else {
				return true;
			}
		}
		$scope.cambio_clave = function () {
			$http.post($scope.base_url + "/WebLogin/verificar_cambio", {
				'id': $scope.id,
				'codigo': $scope.codigo,
			}).success(function (data, estatus, headers, config) {
				$scope.mensajes = data;
				//console.log(data);
				if ($scope.mensajes.mensaje == "aprobado") {
					$scope.currentTab1 ='aprobado';
				} else if ($scope.mensajes.mensaje == 'no_aprobado') {
						$scope.currentTab1 ='error';
				}
			}).error(function (data, estatus) {
				console.log(data);
			});ya_activado
		}
		$scope.cambiarClave = function () {
			$http.post($scope.base_url + "/WebLogin/cambiar_clave", {
				'clave': $scope.clave,
				'clave_r': $scope.clave_r,
				'id': $scope.id,
				'codigo': $scope.codigo,
			}).success(function (data, estatus, headers, config) {
				$scope.mensajes = data;
				//console.log(data);
				if ($scope.mensajes.mensaje == "cambio_logrado") {
					$scope.currentTab1 ='cambio_logrado';
				} else if ($scope.mensajes.mensaje == 'no_aprobado') {
						$scope.currentTab1 ='error';
				}
			}).error(function (data, estatus) {
				console.log(data);
			});ya_activado
		}
		//-----
		$scope.cambio_clave();
		$scope.definir_url_home();
		//----
	});
