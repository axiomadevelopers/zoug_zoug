angular.module("ZougZougApp")
	.controller("inicioController", function ($scope, $http, $window, $compile, $location, direccionFactory, footerFactory, upload, multIdioma) {
		//Bloque de metodos
		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#id_idioma").val();
		$scope.tp_login = $("#tp_login").val();
		///---
		$scope.currentTab = 'login_email';
		$scope.registro = {
			'nombre': '',
			'correo': '',
			'clave': '',
			'r_clave': ''
		}
		$scope.vaciarDatos = function () {
			$scope.registro = {
				'nombre': '',
				'correo': '',
				'clave': '',
				'r_clave': ''
			}
			//En español
			if($scope.idioma==1){
				if($scope.currentTab=="registro_email"){
				$scope.menu.titulo_inicio_sesion="Crear Cuenta"
				}else if($scope.currentTab=="login_email"){
					$scope.menu.titulo_inicio_sesion="Inicio de Sesión"
				}else if($scope.currentTab=="olvide_clave"){
					$scope.menu.titulo_inicio_sesion="Cambiar Contraseña"
				}
				//En ingles
			}else if($scope.idioma==2){
				if($scope.currentTab=="registro_email"){
				$scope.menu.titulo_inicio_sesion="Create Account"
				}else if($scope.currentTab=="login_email"){
					$scope.menu.titulo_inicio_sesion="Sign In"
				}else if($scope.currentTab=="olvide_clave"){
					$scope.menu.titulo_inicio_sesion="Change Password"
				}
			}
			
		}


		//console.log($scope.tp_login);
		if($scope.tp_login==1){
			$("#aviso").modal('show');
		}

		$scope.inicio = function (id, nombre_apellido, correo, estatus) {
			$scope.inicio = {
				'id': id,
				'nombre_apellido': nombre_apellido,
				'correo': correo,
				'estatus': estatus,

			}
			$scope.login = true;


			/* $scope.inicio.nombre_apellido = nombre_apellido;
			$scope.inicio.id_correo = id_correo;  */

			//console.log($scope.login);
		};
		$scope.registrarUsuario = function () {
			if ($scope.validar_registro() == true) {
				//Para guardar
				$scope.insertar_personas();
			}
		}
		$scope.validar_registro = function () {
			aviso_codigo
			var exr = /^\w+[a-z_0-9\-\.]+@\w+[0-9a-z\-\.]+\.[a-z]{2,4}$/i;
			if ($scope.registro.nombre == "") {
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_registro", "Debe ingresar el nombre completo", "alert-warning");
				}
				else{
					mensaje_alerta("#campo_mensaje_registro", "You must enter the full name", "alert-warning");
				}

				return false;
			}
			if ($scope.registro.correo == "") {
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_registro", "Debe ingresar un correo electronico", "alert-warning");
				}
				else{
					mensaje_alerta("#campo_mensaje_registro", "You must enter an email", "alert-warning");
				}
				return false;
			} else if (!(exr.test($scope.registro.correo))) {
				$scope.registro.correo = "";
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_registro", "Debe ingresar una dirección de correo v&aacute;lida: xxxxxxxxxxxxxx@host.com", "alert-warning");
				}
				else{
					mensaje_alerta("#campo_mensaje_registro", "You must enter a valid email address: xxxxxxxxxxxxxx@host.com", "alert-warning");
				}
			} else if ($scope.registro.clave == "") {
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_registro", "Debe ingresar una contraseña", "alert-warning");
				}
				else{
					mensaje_alerta("#campo_mensaje_registro", "You must enter a password", "alert-warning");
				}
				return false;
			} else if ($scope.registro.r_clave == "") {
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_registro", "Debe repetir la contraseña", "alert-warning");
				}
				else{
					mensaje_alerta("#campo_mensaje_registro", "You must repeat the password", "alert-warning");
				}
				return false;
			} else if ($scope.registro.r_clave != $scope.registro.clave) {
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_registro", "Las contraseñas no coinciden", "alert-warning");
				}
				else{
					mensaje_alerta("#campo_mensaje_registro", "Passwords do not match", "alert-warning");
				}
				return false;
			}else if(document.getElementById('clave').value.length < 8){
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_registro", "Debe ingresar una contraseña con 8 caracteres", "alert-warning");
				}
				else{
					mensaje_alerta("#campo_mensaje_registro", "You must enter a password with 8 characters", "alert-warning");
				}
			} else {
				return true;
			}
		}
		/////////// Registro de usuarios
		$scope.insertar_personas = function () {
			//console.log($scope.registro);

			$http.post($scope.base_url + "/WebLogin/registrarPersonas", {
				'nombre': $scope.registro.nombre,
				'correo': $scope.registro.correo,
				'clave': $scope.registro.clave,
				'idioma': $scope.idioma,
				
			}).success(function (data, estatus, headers, config) {
				$scope.mensajes = data;
				if ($scope.mensajes.mensaje == "registro_procesado") {
					if($scope.idioma==1){
						mensaje_alerta("#campo_mensaje_registro", "El registro fue realizado de manera exitosa", "alert-info");
					}else{
						mensaje_alerta("#campo_mensaje_registro", "El registro fue realizado de manera exitosa", "alert-info");	
					}
					$scope.currentTab = 'aviso_codigo';

				} else if ($scope.mensajes.mensaje == "existe") {

					if($scope.idioma==1){
						mensaje_alerta("#campo_mensaje_registro", "Correo no disponible", "alert-warning");
					}else{
						mensaje_alerta("#campo_mensaje_registro", "Mail not available", "alert-warning");	
					}
				} else {
					if($scope.idioma==1){
						mensaje_alerta("#campo_mensaje_registro", "Ocurrío un error inesperado", "alert-warning");
					}else{
						mensaje_alerta("#campo_mensaje_registro", "An unexpected error would occur", "alert-warning");	
					}
				}
				//$mensajes["mensaje"] = "no_registro";

			}).error(function (data, estatus) {
				console.log(data);
			});
		}
		$scope.iniciarUsuario = function () {
			if ($scope.validar_inicio() == true) {
				//Para guardar
				//console.log($scope.registro);
				$scope.ingresarUsuario();

			}
		}
		$scope.validar_inicio = function () {
			var exr = /^\w+[a-z_0-9\-\.]+@\w+[0-9a-z\-\.]+\.[a-z]{2,4}$/i;
			if ($scope.registro.correo == "") {
				if($scope.idioma==1){
						mensaje_alerta("#campo_mensaje_inicio", "Debe ingresar un correo electronico", "alert-warning");
					}
					else{
						mensaje_alerta("#campo_mensaje_inicio", "You must enter an email", "alert-warning");
					}
					return false;
				}
				else if (!(exr.test($scope.registro.correo))) {
				$scope.registro.correo = "";
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_inicio", "Debe ingresar una dirección de correo v&aacute;lida: xxxxxxxxxxxxxx@host.com", "alert-warning");
				}else{
					mensaje_alerta("#campo_mensaje_inicio", "You must enter a valid email address: xxxxxxxxxxxxxx@host.com", "alert-warning");
				}
			
			}else if ($scope.registro.clave == "") {
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_inicio", "Debe ingresar una contraseña", "alert-warning");
				}
				else{
					mensaje_alerta("#campo_mensaje_inicio", "You must enter a password", "alert-warning");
				}
				return false;
			} else {
				return true;
			}
		}
		$scope.ingresarUsuario = function () {
			$http.post($scope.base_url + "/WebLogin/inicioSesion", {
				'correo': $scope.registro.correo,
				'clave': $scope.registro.clave,
			}).success(function (data, estatus, headers, config) {
				$scope.mensajes = data;
				//console.log(data);
				if ($scope.mensajes.mensaje == "inicio_exitoso") {
					$scope.ir_dashboard();
				} else if ($scope.mensajes.mensaje == "no_existe") {
					if($scope.idioma==1){
						mensaje_alerta("#campo_mensaje_inicio", "No se encuentra una cuenta asociada a este correo", "alert-warning");
					}
					else{
						mensaje_alerta("#campo_mensaje_inicio", "Cannot find an account associated with this email", "alert-warning");
					}
				} else if ($scope.mensajes.mensaje == "no_clave") {
					if($scope.idioma==1){
						mensaje_alerta("#campo_mensaje_inicio", "Contraseña incorrecta", "alert-warning");
					}
					else{
						mensaje_alerta("#campo_mensaje_inicio", "Incorrect password", "alert-warning");
					}
				} else if ($scope.mensajes.mensaje == "inactivo") {
					if($scope.idioma==1){
						mensaje_alerta("#campo_mensaje_inicio", "Usuario inactivo, por favor revisar su correo para realizar la activación o contáctenos", "alert-warning");
					}
					else{
						mensaje_alerta("#campo_mensaje_inicio", "Inactive user, please check your email to activate or contact us", "alert-warning");
					}
				} else {
					if($scope.idioma==1){
						mensaje_alerta("#campo_mensaje_inicio", "Ocurrío un error inesperado", "alert-warning");
					}else{
						mensaje_alerta("#campo_mensaje_inicio", "An unexpected error would occur", "alert-warning");	
					}
				}
				$mensajes["mensaje"] = "no_registro";
			}).error(function (data, estatus) {
				console.log(data);
			});
		}
		$scope.ir_dashboard = function () {
			window.location.reload(false);

		}
		$scope.cambiarClave = function () {
			if ($scope.validar_correo() == true) {
				//Para guardar
				$scope.olvide_clave();

			}
		}
		$scope.validar_correo = function () {
			var exr = /^\w+[a-z_0-9\-\.]+@\w+[0-9a-z\-\.]+\.[a-z]{2,4}$/i;

			if ($scope.registro.correo == "") {
				if($scope.idioma==1){
				mensaje_alerta("#campo_mensaje_clave", "Debe ingresar un correo electronico", "alert-warning");
			}
			else{
				mensaje_alerta("#campo_mensaje_clave", "You must enter an email", "alert-warning");
			}
				return false;
			} else if (!(exr.test($scope.registro.correo))) {
				$scope.registro.correo = "";
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_clave", "Debe ingresar una dirección de correo v&aacute;lida: xxxxxxxxxxxxxx@host.com", "alert-warning");
				}
				else{
					mensaje_alerta("#campo_mensaje_clave", "You must enter a valid email address: xxxxxxxxxxxxxx@host.com", "alert-warning");
				}
			} else {
				return true;
			}
		}
		$scope.olvide_clave = function () {
			$http.post($scope.base_url + "/WebLogin/olvide_clave", {
				'correo': $scope.registro.correo,
				'idioma': $scope.idioma,


			}).success(function (data, estatus, headers, config) {
				$scope.mensajes = data;
				//console.log(data);
				//----------------------------------------------------
				if( $scope.mensajes.mensaje == "no_puede_recupe"){
					if($scope.idioma==1){
						mensaje_alerta("#campo_mensaje_clave", "No puede recuperar contraseña de un usuario no registrado por correo", "alert-warning");
					}else{
						mensaje_alerta("#campo_mensaje_clave", "You cannot recover the password of an unregistered user by mail", "alert-warning");	
					}
				}else if ($scope.mensajes.mensaje == "registro_procesado") {
				//----------------------------------------------------
				
					$scope.currentTab = 'aviso_clave';


				} else if ($scope.mensajes.mensaje == "no_existe") {
					if($scope.idioma==1){
						mensaje_alerta("#campo_mensaje_clave", "No se encuentra una cuenta asociada a este correo", "alert-warning");
					}else{
						mensaje_alerta("#campo_mensaje_clave", "Cannot find an account associated with this email", "alert-warning");	
					}
				
				} else {
					if($scope.idioma==1){
						mensaje_alerta("#campo_mensaje_clave", "Ocurrío un error inesperado", "alert-warning");
					}else{
						mensaje_alerta("#campo_mensaje_clave", "An unexpected error would occur", "alert-warning");	
					}
				}
				$mensajes["mensaje"] = "no_registro";
			}).error(function (data, estatus) {
				console.log(data);
			});
		}
		
			/* $("#aviso").modal('show'); */

		$scope.consultar_direcciones_todas = function () {
			offset = 0
			limit = 2
			direccionFactory.asignar_valores(offset, limit, "");
			direccionFactory.cargar_direcciones_todas(function (data) {
				$scope.direccion_es = data["es"];
				$scope.direccion_en = data["en"];
				//$scope.direccion_f = data["es"];
				//console.log($scope.direccion_f);
				$scope.ruta = $location.path()
				ruta = $scope.ruta
				vec_ruta = ruta.split("/")
				if (($scope.ruta == '/') || ($scope.ruta == '/inicio') || ($scope.ruta == '/nosotros') || ($scope.ruta == '/contactanos') || ($scope.ruta == '/productos') || ($scope.ruta == '/portafolio') || ($scope.ruta == '/noticias') || ($scope.ruta == '/contactanos') || (vec_ruta[1] == 'portafolio') || ((vec_ruta[1] == "noticias") && (vec_ruta[2] == "es"))) {
					$scope.direccion_f = data["es"];
				}
				if (($scope.ruta == '/home') || ($scope.ruta == '/about') || ($scope.ruta == '/products') || ($scope.ruta == '/portfolio') || ($scope.ruta == '/contact_us') || ($scope.ruta == '/news') || (vec_ruta[1] == 'portfolio') || ((vec_ruta[1] == "news") && (vec_ruta[2] == "en"))) {
					$scope.direccion_f = data["en"];
				}
				//console.log($scope.direccion_f);
			});
		}
		//----
		$scope.consultar_redes = function () {
			footerFactory.asignar_valores($scope.idioma, $scope.base_url);
			footerFactory.cargar_redes(function (data) {
				$scope.redes = data;
				//console.log($scope.redes);
			});
		}
		$scope.consultar_footer = function () {
			footerFactory.asignar_valores($scope.idioma, $scope.base_url);
			footerFactory.cargar_footer(function (data) {
				$scope.footer = data[0];
				$scope.lineas = data["lineas"]
				//console.log($scope.lineas);
			});
		}

		$scope.consultar_meta = function () {
			footerFactory.cargar_meta_tag(function (data) {
				$scope.meta_descripcion = data.descripcion;
				$scope.meta_palabras_claves = data.palabras_claves;
				$("meta[name='keywords']").attr('content', $scope.meta_palabras_claves);
				$("meta[name='description']").attr('content', $scope.meta_descripcion);
				//console.log(data);
			});
		}

		//-------------------------------------------------------------
		//--Coloca la bandera segun la url: Segun lo que tenga la url se colocan los titulos del menu
		$scope.definir_url = function () {
			if ($scope.idioma == '1') {
				res = multIdioma.cargar_inicio_espain($scope.base_url)
			} else {
				res = multIdioma.cargar_inicio_uk($scope.base_url)
			}
			$scope.menu = res.titulo
			$scope.btn = res.btn
			$scope.url = res.url
			//console.log($scope.menu)
		}
		//----------------------------------------
		$scope.cambio_idioma = function () {
			res = multIdioma.cambiar_idioma($scope.base_url)
			//$scope.menu = res.titulo
			$scope.btn = res.btn
			$scope.url = res.url
			$scope.idioma_inicio = res.idioma_inicio
			$scope.cambiar_page_idioma()

			//Cuando agregue las direcciones
			/*if($scope.idioma_inicio=="1"){
				$scope.direccion_f = $scope.direccion_es;
			}else{
				$scope.direccion_f = $scope.direccion_en;
			}*/
		}
		//--Para cambiar el path

		//--Para cambiar los titulos y el path
		$scope.cambiar_page_idioma = function () {
			$scope.ruta = window.location.href
			var cadena = window.location.href
			$scope.ruta = cadena.replace($scope.base_url, "/")
			switch ($scope.ruta) {

				case '/':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url;
					else
						window.location.href = $scope.base_url + "home";
					break;
				case '/inicio':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url;
					else
						window.location.href = $scope.base_url + "home";
					break;

				case '/home':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url;
					else
						window.location.href = $scope.base_url + "home";
					break;

				case '/nosotros':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "nosotros";
					else
						window.location.href = $scope.base_url + "about";
					break;

				case '/about':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "nosotros";
					else
						window.location.href = $scope.base_url + "about";
					break;

				case '/productos':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "productos";
					else
						window.location.href = $scope.base_url + "products";
					break;

				case '/products':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "productos";
					else
						window.location.href = $scope.base_url + "products";
					break;

				case '/portafolio':
					if ($scope.idioma_inicio == 1)
						$location.path("/portafolio");
					else
						$location.path("/portfolio");
					break;

				case '/portfolio':
					if ($scope.idioma_inicio == 1)
						$location.path("/portafolio");
					else
						$location.path("/portfolio");
					break;

				case '/noticias':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "noticias";
					else
						window.location.href = $scope.base_url + "news";
					break;

				case '/news':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "noticias";
					else
						window.location.href = $scope.base_url + "news";
					break;

				case '/contactanos':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "contactanos";
					else
						window.location.href = $scope.base_url + "contact_us";
					break;

				case '/contact_us':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "contactanos";
					else
						window.location.href = $scope.base_url + "contact_us";
					break;

				case '/carrito':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "carrito";
					else
						window.location.href = $scope.base_url + "cart";
					break;

				case '/cart':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "carrito";
					else
						window.location.href = $scope.base_url + "cart";
					break;
				case '/orden':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "orden";
					else
						window.location.href = $scope.base_url + "order";
					break;
				case '/order':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "orden";
					else
						window.location.href = $scope.base_url + "order";
					break;

				case '/orden_usuario':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "orden_usuario";
					else
						window.location.href = $scope.base_url + "order_us";
					break;
				case '/order_us':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "orden_usuario";
					else
						window.location.href = $scope.base_url + "order_us";
					break;
				case '/politicas_privacidad':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "politicas_privacidad";
					else
						window.location.href = $scope.base_url + "privacy_policy";
					break;
				case '/privacy_policy':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "politicas_privacidad";
					else
						window.location.href = $scope.base_url + "privacy_policy";
					break;
				case '/terminos':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "terminos";
					else
						window.location.href = $scope.base_url + "terms";
					break;
				case '/terms':
					if ($scope.idioma_inicio == 1)
						window.location.href = $scope.base_url + "terminos";
					else
						window.location.href = $scope.base_url + "terms";
					break;
				default:
					ruta = $scope.ruta;
					vec_ruta = ruta.split("/")
					//--
					//Si es inversiones
					//---------------------------------------------
					if (vec_ruta[1] == "products")
						window.location.href = $scope.base_url + "productos";
					else if (vec_ruta[1] == "productos")
						window.location.href = $scope.base_url + "products";
					//---------------------------------------------
					if (vec_ruta[1] == "noticias") {
						window.location.href = $scope.base_url + "news";
					} else
					if (vec_ruta[1] == "news") {
						window.location.href = $scope.base_url + "noticias";
					}
					//-------------------------------------------------
					if (vec_ruta[1] == "register") {
						resto_url = $scope.base_url + "registro"
						a = 0
						vec_ruta.forEach(function (element) {
							a++;
							if (a > 2) {
								resto_url += "/" + element
							}
						});
						window.location.href = resto_url;
						//window.location.href=$scope.base_url+"procesar_compra/"+vec_ruta[2];
					}
					//------------------------------------------------
					else if (vec_ruta[1] == "registro") {
						resto_url = $scope.base_url + "register"
						b = 0
						vec_ruta.forEach(function (element) {
							b++;
							if (b > 2) {
								resto_url += "/" + element
							}
						});
						window.location.href = resto_url;
					}
					//------------------------------------------------
					if (vec_ruta[1] == "password_recovery") {
						resto_url = $scope.base_url + "recuperar_clave"
						a = 0
						vec_ruta.forEach(function (element) {
							a++;
							if (a > 2) {
								resto_url += "/" + element
							}
						});
						window.location.href = resto_url;
						//window.location.href=$scope.base_url+"procesar_compra/"+vec_ruta[2];
					}
					//------------------------------------------------
					else if (vec_ruta[1] == "recuperar_clave") {
						resto_url = $scope.base_url + "password_recovery"
						b = 0
						vec_ruta.forEach(function (element) {
							b++;
							if (b > 2) {
								resto_url += "/" + element
							}
						});
						window.location.href = resto_url;
					}
					//------------------------------------------------
					if (vec_ruta[1] == "checkout_step") {
						resto_url = $scope.base_url + "procesar_compra"
						a = 0
						vec_ruta.forEach(function (element) {
							a++;
							if (a > 2) {
								resto_url += "/" + element
							}
						});
						window.location.href = resto_url;
						//window.location.href=$scope.base_url+"procesar_compra/"+vec_ruta[2];
					}
					//------------------------------------------------
					else if (vec_ruta[1] == "procesar_compra") {
						resto_url = $scope.base_url + "checkout_step"
						b = 0
						vec_ruta.forEach(function (element) {
							b++;
							if (b > 2) {
								resto_url += "/" + element
							}
						});
						window.location.href = resto_url;
					}
					break;
					//-----------------------------------------------
			}
			//---
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
			//--
		}

		$scope.definir_url();
		$scope.consultar_footer();
		$scope.consultar_redes();
		$scope.consultar_meta();
		limpiarHash();
	})
	.controller("MainController", function($scope,$sce,$http,$compile,$location,funcionaFactory,contactosFactory,direccionFactory,portafoliosFactory,blogFactory,serviciosFactory,clientesFactory,noticiaHomeFactory,sliderFactory,nosotrosFactory,multIdioma,productosHomeFactory){
		//Cuerpo declaraciones
		$scope.contactos = {
								'id':'',
								'nombres':'',
								'telefono':'',
								'email':'',
								'mensaje':''
		}
		
		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#id_idioma").val();
		$(".menu_web").removeClass("active")
		$("#menu1").addClass("active")
		//------------------------------------------------------------
		/*
		*	Bloque obligatorio para todo controlador
		*/
		/*
		*	Define segun el valor del idioma los titulos
		*/
		$scope.definir_url_home = function(){
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
			//console.log($scope.btn);
		}
		/*
		*
		*/
		//-----------------------------------------------------------
		//Validando el idioma
		//Multi lenguaje para home

		///Se debe descomentar....
		/*if(($location.path()=="/home")||($location.path()=="/")){
			$scope.idioma=2
		}else{
			$scope.idioma=1
		}*/

		$scope.arreglo_id = []

		$scope.marcas_rutas = []
		//--
		$scope.cargar_home = function(){
			//1- Defino las url de la pantalla}
			$scope.definir_url_home();
			//2-consulto la info del home
			//$scope.consultar_slider();
			$scope.consultar_nosotros();
			//3-Hago el llamado de el resto de los metodos..
			$scope.consultar_productos();
			$scope.consultar_subnoticias();
			//carga_inicio();

		}
		/////////////////////
		$scope.consultar_productos = function(){
			//console.log("si llega");
			vacio = ""
			var offset = 0;
			var limit = 2;
    		productosHomeFactory.asignar_valores($scope.idioma,'',limit,offset,$scope.base_url);
			productosHomeFactory.cargar_productos(function(data){
				$scope.productos = data;
				//console.log($scope.productos[0].titulo);
				/*
				$scope.productos_footer1.titulo = $scope.productos[0].titulo
				$scope.productos_footer2.titulo = $scope.productos[1].titulo
				$scope.productos_footer3.titulo = $scope.productos[2].titulo
				$scope.productos_footer1.slug = $scope.productos[0].slug
				$scope.productos_footer2.slug = $scope.productos[1].slug
				$scope.productos_footer3.slug = $scope.productos[2].slug*/
			});
		}

		////////////////////////
		$scope.consultar_subnoticias = function(){
			vacio = ""
			var offset = 0;
			var limit = 2;
    		noticiaHomeFactory.asignar_valores($scope.idioma,'',limit,offset,$scope.base_url);
			noticiaHomeFactory.cargar_noticias_filtro(function(data){
				$scope.sub_noticias = data;
				//console.log($scope.sub_noticias);
			});
		}
		//--
		$scope.consultar_slider = function(){
			sliderFactory.asignar_valores($scope.idioma,$scope.base_url)
			sliderFactory.cargar_slider(function(data){
				$scope.slider=data;
				//console.log($scope.slider)
			})
		}
		$scope.armar_slider = function(){
			var slider = ""
			/*$.each($scope.slider, function( index, value ) {
			  slider+= '<li data-transition="slidehorizontal" data-slotamount="5" data-masterspeed="700"  data-title="Slide '+value.num+'">'
			   slider+='<div class="tp-caption rs-caption-1 sft start" style="left:0px;"'
               slider+=       '  data-hoffset="0"'
               slider+=       '  data-x="0px"'
               slider+=       '  data-y="0"'
               slider+=       '  data-speed="800"'
               slider+=       '  data-start="1500"'
               slider+=       '  data-easing="Back.easeInOut"'
               slider+=       '  data-endspeed="300">'
               slider+=       '  <img src="'+$scope.base_url+value.ruta+'" alt="slider-image" style="width: auto; height: auto;left:0px;">'
               slider+=       '</div>'
               slider+=       '<div class="tp-caption rs-caption-2 sft"'
               slider+=       '  data-hoffset="0"'
               slider+=       '  data-y="100"'
               slider+=       '  data-x="600"'
		         slider+=        '  data-speed="800"'
		         slider+=        '  data-start="2000"'
		         slider+=        '  data-easing="Back.easeInOut"'
		         slider+=        '  data-endspeed="300">'
		         slider+=        '  <span style="color:#fff;font-weight: bold;text-shadow: 2px 2px #000;">'+value.titulo+'</span>'
		         slider+=        '</div>'
		         slider+=        '<div class="tp-caption rs-caption-3 sft"'
		         slider+=        '  data-hoffset="0"'
		         slider+=        ' data-y="100"'
		         slider+=        '  data-x="600"'
		         slider+=        '  data-speed="1000"'
		         slider+=            '  data-start="3000"'
		         slider+=            '  data-easing="Power4.easeOut"'
		         slider+=            '  data-endspeed="300"'
		         slider+=            '  data-endeasing="Power1.easeIn"'
		         slider+=            '  data-captionhidden="off" style="width: 90vh;">'
		         slider+=            '  <span style="color:#fff;font-weight: bold;text-shadow: 2px 2px #000;" ng-bind-html="'+value.descripcion+'"></span>'
		         slider+=            '</div>'
		         slider+=            '<div class="tp-caption rs-caption-4 sft"'
		         slider+=            '  data-hoffset="0"'
		         slider+=            '  data-y="310"'
		         slider+=            '  data-x="600"'
		         slider+=            '  data-speed="800"'
		         slider+=            '  data-start="3500"'
		         slider+=            '  data-easing="Power4.easeOut"'
		         slider+=            '  data-endspeed="300"'
		         slider+=            '  data-endeasing="Power1.easeIn"'
		         slider+=            '  data-captionhidden="off">'
		         slider+=            '  <span class="page-scroll"><a href="'+$scope.base_url+value.url+'" class="btn primary-btn" style="border: solid #b9ca46;margin-top:20px;">'+value.boton
		         slider+=            '<i class="fa fa-chevron-right"></i></a></span>'
		         slider+=            '</div>'
		         slider+=        '</div>'
		         slider+=    '</li>';
			});}*/
			//$("#ul_slider").html(slider);
			//console.log(slider);
		}
		//--
		$scope.consultar_nosotros = function(){
			nosotrosFactory.asignar_valores($scope.idioma,$scope.base_url)
			nosotrosFactory.cargar_nosotros(function(data){
				$scope.nosotros=data[0];
				//console.log($scope.nosotros);
			});
		}
		//--
		$scope.validar_guardar = function(){
			if($scope.contactos.nombres==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar sus nombres", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter their names", "alert-danger");

				return false;
			}else
			if($("#contactos_email").val()==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar una dirección de correo v&aacute;lida", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter valid email direction", "alert-danger");
				return false;
			}
			else
			if($("#contactos_telefono").val()==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar un número de teléfono válido", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter valid telephone number", "alert-danger");
				return false;
			}
			else if($scope.contactos.mensaje==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar un mensaje", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter a message", "alert-danger");
				return false;
			}else
				return true;
		}
		//--
		//--Registrar contactos
		$scope.registrar_contactos = function(){
			if($scope.validar_guardar()==true){
				//uploader_reg("#campo_mensaje_clientes","#nombres_clientes,#email_clientes,#telefono_clientes,#mensaje_clientes");
				$http.post($scope.base_url+"/WebInicio/registrarContactos",{
					'nombres': $scope.contactos.nombres,
					'email': $scope.contactos.email,
					'telefono': $scope.contactos.telefono,
					'mensaje': $scope.contactos.mensaje,
					'id_idioma':$scope.idioma,

				}).success(function(data, estatus, headers, config){
					$scope.mensajes  = data;
					if($scope.mensajes.mensaje == "registro_procesado"){
						if($scope.idioma == "1")
							mensaje_alerta("#campo_mensaje_clientes","Gracias por tu tiempo, tu mensaje ha sido enviado!","alert-success");
						else
							mensaje_alerta("#campo_mensaje_clientes","Thank you for your time, your message has been sent!","alert-success");
						$scope.limpiar_cajas();
					}else if($scope.mensajes.mensaje =="existe"){
						if($scope.idioma == "1")
							mensaje_alerta("#campo_mensaje_clientes","Ya fue registrado un contacto con esa dirección de email","alert-success");
						else
							mensaje_alerta("#campo_mensaje_clientes","A contact has already been registered with that email address","alert-success");
					}
					else{
						mensaje_alerta("#campo_mensaje_clientes","Ocurrió un error inesperado","alert-danger");

					}
					//desbloquear_pantalla("#campo_mensaje_clientes","#nombres_clientes,#email_clientes,#telefono_clientes,#mensaje_clientes")
				}).error(function(data,estatus){
					showErrorMessage("Ocurrió un error inesperado:"+data);
				})
			}
		}

		//--Limpiar cajas
		$scope.limpiar_cajas = function(){
			$scope.contactos = {
									"id":"",
									"nombres":"",
									"email":"",
									"telefono":"",
									"mensaje":""
			}
		}
		//--
		//--Limpiar cajas
		$scope.limpiar_cajas = function(){
			$scope.contactos = {
									"id":"",
									"nombres":"",
									"email":"",
									"telefono":"",
									"mensaje":""
			}
		}
		//--
		$scope.recorrer_direcciones_home = function(){
			//console.log($scope.direccion_f);
			google.maps.event.addDomListener(window, 'load', initialize_standar($scope.direccion_f[0].latitud,$scope.direccion_f[0].longitud,'0'));
		}
		//--
		$scope.sanitizeMe = function(text) {
		    return $sce.trustAsHtml(text)
		}
		//---Bloque de llamados
		$scope.cargar_home()
		//--------------------------------------
	})
	.controller("nosotrosController", function($scope,$sce,$http,$compile,$location,funcionaFactory,nosotrosFactory,clientesFactory,multIdioma){
		//--
		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#id_idioma").val();
		$(".menu_web").removeClass("active")
		$("#menu2").addClass("active")
		//------------------------------------------------------------
		/*
		*	Bloque obligatorio para todo controlador
		*/
		/*
		*	Define segun el valor del idioma los titulos
		*/
		$scope.definir_url_home = function(){
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
			//console.log($scope.btn);
		}
		/*
		*
		*/
		//-----------------------------------------------------------
		//---
		$scope.consultar_nosotros = function(){

			$scope.definir_url_home();

			nosotrosFactory.asignar_valores($scope.idioma,$scope.base_url)
			nosotrosFactory.cargar_nosotros(function(data){
				$scope.nosotros=data[0];
				//console.log($scope.nosotros);
			});
		}
		//---
		$scope.consultar_nosotros();
	})
	.controller("noticiasController", function($scope,$http,$compile,$location,funcionaFactory,footerFactory,noticiaFactory,multIdioma,productosHomeFactory){

		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#id_idioma").val();
		//console.log($scope.idioma);
		$(".menu_web").removeClass("active")
		$("#menu4").addClass("active")
		//------------------------------------------------------------
		/*
		*	Bloque obligatorio para todo controlador
		*/
		/*
		*	Define segun el valor del idioma los titulos
		*/
		$scope.definir_url_home = function(){
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
			//console.log($scope.btn);
		}
		/*
		*
		*/
		$scope.noticias = {
								'id':'',
								'id_idioma':'',
								'titulo':'',
								'descripcion':'',
								'estatus':'',
								'id_imagen':''
		}
		$scope.sub_noticias = []
		//-----------------------------------------------------------
		//---
		$scope.consultar_noticias = function(){
			//alert("aqui");
			$scope.definir_url_home();

			noticiaFactory.asignar_valores($scope.idioma,$scope.base_url,"","","")
			noticiaFactory.cargar_noticia(function(data){
				$scope.noticia=data[0];
				//console.log($scope.noticia);
				$scope.limit = 3
				$scope.start =  0
				$scope.consultar_subnoticias($scope.noticia.id);
			});
		}
		//---
		$scope.consultar_subnoticias = function(id){

			noticiaFactory.asignar_valores($scope.idioma,$scope.base_url,id,$scope.limit,$scope.start)
			noticiaFactory.cargar_sub_noticias(function(data){
				/*$scope.sub_noticias=data;
				console.log($scope.sub_noticias);*/
				for (i in data) {
					$scope.sub_noticias.push(data[i]);
				}
				$scope.start += 1;
				//console.log($scope.sub_noticias)
			});
		}
		//---
		$scope.cargar_mas_subnoticias = function(){
			//console.log($scope.noticia.id);
			noticiaFactory.asignar_valores($scope.idioma,$scope.base_url,$scope.noticia.id,$scope.limit,$scope.start)
			noticiaFactory.cargar_sub_noticias(function(data){
				//----------------------------------------
				if (data === null || data == "null" || data=="") {
					if($scope.idioma==1)
						mensaje_alerta("#campo_mensaje_noticias","Todas las noticias fueron cargadas!","alert-warning");
					else
						mensaje_alerta("#campo_mensaje_noticias","All news was charged!","alert-warning");

				} else {
					for (i in data) {
						$scope.sub_noticias.push(data[i]);
					}
					$scope.start += 1;
				}
				//----------------------------------------
			});
		}
		//---
		$scope.productosHomeFactory = function(){
			//console.log("si llega");
			vacio = ""
			var offset = 0;
			var limit = 2;
    		productosHomeFactory.asignar_valores($scope.idioma,'',limit,offset,$scope.base_url);
			productosHomeFactory.cargar_productos(function(data){
				$scope.productos = data;
				//console.log($scope.productos[0]);
				$scope.super_producto_noticias1 = $scope.productos[0]
				$scope.super_producto_noticias2 = $scope.productos[1]
				/*
				$scope.productos_footer1.titulo = $scope.productos[0].titulo
				$scope.productos_footer2.titulo = $scope.productos[1].titulo
				$scope.productos_footer3.titulo = $scope.productos[2].titulo
				$scope.productos_footer1.slug = $scope.productos[0].slug
				$scope.productos_footer2.slug = $scope.productos[1].slug
				$scope.productos_footer3.slug = $scope.productos[2].slug*/
			});
		}
		//---
		$scope.definir_url_home();
		$scope.consultar_noticias();
		$scope.productosHomeFactory();
		//
	})
	.controller("noticiasDetallesController", function($scope,$http,$location,serverDataMensajes,noticiaFactory,multIdioma){
		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#id_idioma").val();
		//console.log($scope.idioma);
		$(".menu_web").removeClass("active")
		$("#menu4").addClass("active")
		//------------------------------------------------------------
		$scope.definir_url_home = function(){
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
			//console.log($scope.btn);
		}
		//-----------------------------------------------------------
		$scope.consultar_noticias_slug = function(){
			$scope.definir_url_home();
			$scope.slug = $("#slug_noticia").text();
			noticiaFactory.asignar_valores($scope.idioma,$scope.base_url,$scope.slug)
			noticiaFactory.cargar_noticia_slug(function(data){
				$scope.noticia=data[0];
				//console.log($scope.noticia);
				$scope.consultar_subnoticias($scope.noticia.id,3,0);
			});
		}
		//-----------------------------------------------------------
		$scope.consultar_subnoticias = function(id,limit, start){
			noticiaFactory.asignar_valores($scope.idioma,$scope.base_url,id,limit,start)
			noticiaFactory.cargar_sub_noticias(function(data){
				$scope.sub_noticias=data;
				//console.log($scope.sub_noticias);
			});
		}
		//-----------------------------------------------------------
		$scope.definir_url_home();
		$scope.consultar_noticias_slug();
		//-----------------------------------------------------------
	})
	.controller("carritoController", function($scope,$http,$sce,$compile,$location,funcionaFactory,serviciosFactory,multIdioma,carritoFactory){
		//Cuerpo declaraciones
		$scope.productos = {}
		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#id_idioma").val();

		$(".menu_web").removeClass("active")
		$("#menu3").addClass("active")

		$(".primera").removeClass("side-nav")
		$("#menu3").addClass("active")

		$( "#target" ).click(function() {
		  alert( "Handler for  called." );
		});

		if($scope.idioma == 1){
			$scope.titulo_1 = 'Detalle del Producto'
		}else{
			$scope.titulo_1 = 'Product Detail'
		}

		$scope.definir_url_home = function(){
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
		}
		//-----------------------------------------------------------
		$scope.consultar_info_carrito = function(){
			carritoFactory.asignar_valores($scope.idioma,$scope.base_url,'')
			carritoFactory.cargar_producto_carrito(function(data){
				$scope.productos=data;
				if($scope.productos==""){
					if($scope.idioma==1)
						mensaje_alerta("#campo_mensaje_productos","No hay productos agregados a su carrito de compra","alert-warning");
					else
						mensaje_alerta("#campo_mensaje_productos","Don't have products in your cart","alert-warning");
				}else{
					$scope.cuantos_productos = $scope.productos[0].cantidad_objetos
				}
				//console.log($scope.productos)
			});
		}

		$scope.eliminarProducto = function(event){
			var caja = event.currentTarget.id;
			var id = $("#"+caja).attr("data");
			//------------------------------------------
			$http.post($scope.base_url+"/WebCart/eliminar_producto",
			{
				'id' : id,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					//--Mensaje
					if($scope.idioma==1){
						mensaje_alerta("#campo_mensaje_productos","El producto sera retirado de su carrito de compras","alert-info");
						setTimeout(function(){
							window.location.href=$scope.base_url+"carrito";
						},2000);
					}
					else{
						mensaje_alerta("#campo_mensaje_productos","The product will be removed from your shopping cart","alert-info");
						setTimeout(function(){
							window.location.href=$scope.base_url+"cart";
						},2000);
					}
					//--
					//$scope.consultar_info_carrito();
				}else{
					//---
					//Mensaje de no realizado
					if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje_productos","Ocurrío un error inesperado","alert-warning");
						else
							mensaje_alerta("#campo_mensaje_productos","An unexpected error occurred","alert-warning");
 
					//---
				}
			}).error(function(data,estatus){
				console.log(data);
			});
			//------------------------------------------
		}
		//--
		
		//--
		$scope.update_cart = function(){
			$scope.id_producto_carrito = []
			$scope.cantidad = []
			$scope.monto_total = []
			var noCantidad = true
			$(".tabla_producto").each(function(index,element){
        	    numero = $(element).attr("data")
        	    $scope.id_producto_carrito.push($("#eliminar_"+numero).attr("data"));
        	    $scope.cantidad.push($("#cantidad_"+numero).val());
        	    $monto_total = (parseFloat($("#monto_oculto_"+numero).html()))*(parseInt($("#cantidad_"+numero).val()))
        	    $scope.monto_total.push($monto_total);
        	    //--
        	    //Verifico su alguno no tiene cantidad
        	    if(($("#cantidad_"+numero).val()=="")||($("#cantidad_"+numero).val()==0)){
        	    	noCantidad= false;
        	    }
        	    //--
        	});
        	//---
        	//console.log($scope.monto_total)
        	if(noCantidad){
        		//---
	        	$http.post($scope.base_url+"/WebCart/modificar_cantidad_carrito_productos",
				{
					'id' : $scope.id_producto_carrito,
					'cantidad': $scope.cantidad,
					'monto_total': $scope.monto_total
				}).success(function(data, estatus, headers, config){
					$scope.mensajes  = data;
					if($scope.mensajes.mensaje == "modificacion_procesada"){
						//--Mensaje
						if($scope.idioma==1){
							mensaje_alerta("#campo_mensaje_productos","Se ha actualizado su carrito","alert-info");
						}else{
							
							mensaje_alerta("#campo_mensaje_productos","The purchase was processed successfully","alert-info");
						}					
						//
					}else if($scope.mensajes.mensaje == "excede_cantidad"){
						//Mensaje de no realizado
						if($scope.idioma==1)
								mensaje_alerta("#campo_mensaje_productos","No puede exceder la cantidad en existencia del producto","alert-warning");
							else
								mensaje_alerta("#campo_mensaje_productos","It cannot exceed the quantity in stock of the product","alert-warning");
	 
						//---
					}
					else{
						//---
						//Mensaje de no realizado
						if($scope.idioma==1)
								mensaje_alerta("#campo_mensaje_productos","Ocurrío un error inesperado","alert-warning");
							else
								mensaje_alerta("#campo_mensaje_productos","An unexpected error occurred","alert-warning");
	 
						//---
					}
				}).error(function(data,estatus){
					console.log(data);
				});
        		//---
        	}else{
        		//Mensaje de no realizado
				if($scope.idioma==1)
						mensaje_alerta("#campo_mensaje_productos","El campo cantidad no debe estar en blanco","alert-warning");
					else
						mensaje_alerta("#campo_mensaje_productos","The quantity field must not be blank","alert-warning");

				//---
        	}
    	    
    	    //---
		}
		
		$scope.consultar_info_carrito();
		$scope.definir_url_home();
		//----
		//#Cuerpo de funciones js
		$scope.calcularMontoTotal = function(numero){
			cantidad = parseInt($("#cantidad_"+numero).val())
			monto_ind = parseFloat($("#monto_oculto_"+numero).html());
			total = cantidad * monto_ind
			$("#monto_total_"+numero).html(number_format(total,2))
			$("#monto_total_oculto_"+numero).html(total)
			$scope.recorrerSumaTotal()
		}
		//----
		$scope.recorrerSumaTotal = function(){
			acum = 0;
			$(".tabla_producto").each(function(index,element){
        	    numero = $(element).attr("data")
        	    acum+=parseFloat($("#monto_total_oculto_"+numero).html())

        	});
        	//---
        	$("#subtotalMonto").html(number_format(acum,2))
    	    $("#totalMonto").html(number_format(acum,2))
    	    $("#monto_total_oculto").html(acum)
        	//---
		}
		//---
		$scope.irOrden = function(){
			if($scope.idioma=="1")
				var url_carrito = $scope.base_url+"orden";
			else
				var url_carrito = $scope.base_url+"order";
			$("#formCarrito").attr("action",url_carrito)
			$("#formCarrito").attr("method","post");
			$("#formCarrito").submit()
		}
		//---
	})
	.controller("orderController", function($scope,$http,$sce,$compile,$location,funcionaFactory,serviciosFactory,multIdioma,carritoFactory){
		//Cuerpo declaraciones
		$scope.productos = {}
		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#id_idioma").val();

		$(".menu_web").removeClass("active")
		$("#menu3").addClass("active")

		$(".primera").removeClass("side-nav")
		$("#menu3").addClass("active")

		$( "#target" ).click(function() {
		  alert( "Handler for  called." );
		});

		//-------------------------------------------------------
		$correo_paypal = "nlmdiaz@gmail.com"
		$scope.concepto = "Ropa de cocina"
		$scope.url_ipn = $scope.base_url+"WebOrden/ipn"
		//http://jose-aguilar.com/scripts/php/pago-con-paypal/ipn.php
		if($scope.idioma=="1"){
			$scope.titulo_1 = 'Detalle del Producto'
		}else{
			$scope.titulo_1 = 'Product Detail'
		}
		//-------------------------------------------------------
		$scope.definir_url_home = function(){
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
		}
		//-----------------------------------------------------------
		$scope.consultar_info_carrito = function(){
			carritoFactory.asignar_valores($scope.idioma,$scope.base_url,'')
			carritoFactory.cargar_producto_carrito(function(data){
				$scope.productos=data;
				//console.log($scope.productos)
				$scope.cuantos_productos = object_size($scope.productos)
				
				if($scope.cuantos_productos==0){
					if($scope.idioma==1)
						mensaje_alerta("#campo_mensaje_productos","Debe iniciar sesion e ir al carrito de compras","alert-warning");
					else
						mensaje_alerta("#campo_mensaje_productos","You must login and go to the shopping cart","alert-warning");

				}
				$scope.id_carrito=$scope.productos[0].id_carrito;
				$scope.productos[0].monto_global_total_oculto = number_format($scope.productos[0].monto_global_total,2)
				$scope.monto_global_total = $scope.productos[0].monto_global_total
				if($scope.idioma=="1"){
					$scope.url_carrito = $scope.base_url+"procesar_compra/"+$scope.id_carrito;
					$scope.url_shopping = $scope.base_url+"orden"
				}else{
					$scope.url_carrito = $scope.base_url+"checkout_step/"+$scope.id_carrito;
					$scope.url_shopping = $scope.base_url+"order"
				}
				//--
				$scope.configurarPaypal()
				//--
			});
		}

		$scope.eliminarProducto = function(event){
			var caja = event.currentTarget.id;
			var id = $("#"+caja).attr("data");
			//------------------------------------------
			$http.post($scope.base_url+"/WebProductos/eliminar_producto",
			{
				'id' : id,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					//--Mensaje
					if($scope.idioma==1)
						mensaje_alerta("#campo_mensaje_productos","Se ha retirado el producto de su carrito de compras","alert-info");
					else
						mensaje_alerta("#campo_mensaje_productos","The purchase was processed successfully","alert-info");
					//--
					$scope.consultar_info_carrito();
				}else{
					//---
					//Mensaje de no realizado
					if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje_productos","Ocurrío un error inesperado","alert-warning");
						else
							mensaje_alerta("#campo_mensaje_productos","An unexpected error occurred","alert-warning");
 
					//---
				}
			}).error(function(data,estatus){
				console.log(data);
			});
			//------------------------------------------
		}
		//--
		
		//--
		$scope.update_cart = function(){
			$scope.id_producto_carrito = []
			$scope.cantidad = []
			$scope.monto_total = []
			$(".tabla_producto").each(function(index,element){
        	    numero = $(element).attr("data")
        	    $scope.id_producto_carrito.push($("#eliminar_"+numero).attr("data"));
        	    $scope.cantidad.push($("#cantidad_"+numero).val());
        	    $monto_total = (parseFloat($("#monto_oculto_"+numero).html()))*(parseInt($("#cantidad_"+numero).val()))
        	    $scope.monto_total.push($monto_total);
        	});
        	//---
        	//console.log($scope.monto_total)
    	    $http.post($scope.base_url+"/WebProductos/modificar_cantidad_carrito_productos",
			{
				'id' : $scope.id_producto_carrito,
				'cantidad': $scope.cantidad,
				'monto_total': $scope.monto_total
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					//--Mensaje
					if($scope.idioma==1)
						mensaje_alerta("#campo_mensaje_productos","Se ha actualizado su carrito","alert-info");
					else
						mensaje_alerta("#campo_mensaje_productos","The purchase was processed successfully","alert-info");
					//--
					$scope.consultar_info_carrito();
				}else{
					//---
					//Mensaje de no realizado
					if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje_productos","Ocurrío un error inesperado","alert-warning");
						else
							mensaje_alerta("#campo_mensaje_productos","An unexpected error occurred","alert-warning");
 
					//---
				}
			}).error(function(data,estatus){
				console.log(data);
			});
    	    //---
		}
		
		
		//----
		//#Cuerpo de funciones js
		$scope.calcularMontoTotal = function(numero){
			cantidad = parseInt($("#cantidad_"+numero).val())
			monto_ind = parseFloat($("#monto_oculto_0").html());
			total = cantidad * monto_ind
			$("#monto_total_"+numero).html(number_format(total,2))
			$("#monto_total_oculto_"+numero).html(total)
			$scope.recorrerSumaTotal()
		}
		//----
		$scope.recorrerSumaTotal = function(){
			acum = 0;
			$(".tabla_producto").each(function(index,element){
        	    numero = $(element).attr("data")
        	    acum+=parseFloat($("#monto_total_oculto_"+numero).html())
        	    //alert(numero)
        	});
        	//---
        	$("#subtotalMonto").html(number_format(acum,2))
    	    $("#totalMonto").html(number_format(acum,2))
    	    $("#monto_total_oculto").html(acum)
        	//---
		}
		//----
		$scope.backpageCart = function(){
			var idioma = $scope.idioma
			if(idioma=="1")
				var url_carrito = $scope.base_url+"carrito";
			else
				var url_carrito = $scope.base_url+"cart";
			$("#formOrderProducto").attr("action",url_carrito)
			$("#formOrderProducto").attr("method","post");
			$("#formOrderProducto").submit()
		}
		//-----
		$scope.FrompageCheckout = function(){
			$("#realizarPago").submit();
			/*alert($scope.url_carrito)
			$("#formOrderProducto").attr("action",$scope.url_carrito)
			$("#formOrderProducto").attr("method","post");
			$("#formOrderProducto").submit()*/
		}
		//-----
		$scope.definir_url_home();
		$scope.consultar_info_carrito();
		$scope.recorrerSumaTotal()
		//----------------------------------------------------------------------
		$scope.configurarPaypal = function(){
			//------------------------------------
			paypal.Button.render({
				env: $scope.productos[0].PayPalENV,
				style: {
					label: 'paypal',
				},
				client: {
					//production: '<?php echo PayPalClientId; ?>'
					sandbox: $scope.productos[0].PayPalClientId
				},
				payment: function (data, actions) {
					return actions.payment.create({
					  transactions: [{
						amount: {
						  total: $scope.productos[0].productPrice,
						  currency: $scope.productos[0].currency
						}
					  }]
					});
				},
				onAuthorize: function (data, actions) {
				return actions.payment.execute()
				  .then(function () {
				  	$scope.super_url = $scope.url_carrito+"/"+data.paymentID+"/"+data.payerID+"/"+data.paymentToken+"/"+$scope.productos[0].productId
					//alert($scope.super_url)
					window.location = $scope.super_url;
				  });
				}
			}, '#paypal-button');	
			//----------------------------------------
		}
	})
	.controller("orderUsuarioController", function($scope,$http,$sce,$compile,$location,funcionaFactory,serviciosFactory,multIdioma,ordenUsuarioFactory){
		//Cuerpo declaraciones
		$scope.productos = {}
		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#id_idioma").val();

		$(".menu_web").removeClass("active")
		$("#menu3").addClass("active")

		$(".primera").removeClass("side-nav")
		$("#menu3").addClass("active")

		$( "#target" ).click(function() {
		  alert( "Handler for  called." );
		});

		if($scope.idioma == 1){
			$scope.titulo_1 = 'Detalle del Producto'
		}else{
			$scope.titulo_1 = 'Product Detail'
		}

		$scope.definir_url_home = function(){
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
		}
		//-----------------------------------------------------------
		$scope.consultar_info_carrito = function(){
			ordenUsuarioFactory.asignar_valores($scope.idioma,$scope.base_url,'')
			ordenUsuarioFactory.cargar_producto_orden(function(data){
				
				///Datos del comprador
				iniciarSesion = $(".iniciar-sesion").html();
				$scope.id_usuario = $("#idUsuarioorden").html();
				vectSesion = iniciarSesion.split("|");
				$scope.usuarioOrden = vectSesion[0]
				$scope.emailOrden = vectSesion[1]
				$("#email-us").html($(".span_correo").text())
				if($scope.id_usuario==""){
					if($scope.idioma==1)
						mensaje_alerta("#campo_mensaje_productos","No hay ordenes de compra","alert-warning");
					else
						mensaje_alerta("#campo_mensaje_productos","Don't have orders","alert-warning");
				}else{
					$scope.ordenes=data;
					//console.log($scope.ordenes)
					$scope.id_carrito=$scope.ordenes.id_carrito;
				}
			});
		}

		$scope.eliminarProducto = function(event){
			var caja = event.currentTarget.id;
			var id = $("#"+caja).attr("data");
			//------------------------------------------
			$http.post($scope.base_url+"/WebProductos/eliminar_producto",
			{
				'id' : id,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					//--Mensaje
					if($scope.idioma==1)
						mensaje_alerta("#campo_mensaje_productos","Se ha retirado el producto de su carrito de compras","alert-info");
					else
						mensaje_alerta("#campo_mensaje_productos","The purchase was processed successfully","alert-info");
					//--
					$scope.consultar_info_carrito();
				}else{
					//---
					//Mensaje de no realizado
					if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje_productos","Ocurrío un error inesperado","alert-warning");
						else
							mensaje_alerta("#campo_mensaje_productos","An unexpected error occurred","alert-warning");
 
					//---
				}
			}).error(function(data,estatus){
				console.log(data);
			});
			//------------------------------------------
		}
		//--
		
		//----
		//#Cuerpo de funciones js
		$scope.calcularMontoTotal = function(numero){
			cantidad = parseInt($("#cantidad_"+numero).val())
			monto_ind = parseFloat($("#monto_oculto_0").html());
			total = cantidad * monto_ind
			$("#monto_total_"+numero).html(number_format(total,2))
			$("#monto_total_oculto_"+numero).html(total)
			$scope.recorrerSumaTotal()
		}
		//----
		$scope.recorrerSumaTotal = function(){
			acum = 0;
			$(".tabla_producto").each(function(index,element){
        	    numero = $(element).attr("data")
        	    acum+=parseFloat($("#monto_total_oculto_"+numero).html())
        	    //alert(numero)
        	});
        	//---
        	$("#subtotalMonto").html(number_format(acum,2))
    	    $("#totalMonto").html(number_format(acum,2))
    	    $("#monto_total_oculto").html(acum)
        	//---
		}
		//-----
		$scope.definir_url_home();
		$scope.consultar_info_carrito();
		$scope.recorrerSumaTotal()
		//----
	})
	.controller("checkoutController", function($scope,$http,$sce,$compile,$location,funcionaFactory,serviciosFactory,multIdioma,carritoFactory){
		//Cuerpo declaraciones
		$scope.productos = {}
		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#id_idioma").val();
		$scope.numero_orden = $("#idOrdenCompra").html();
		$scope.correoUsuario = $("#correoUsuario").html();
		$scope.id_pago_paypal = $("#payment_id_oculto").html();
		$scope.id_cliente_paypal = $("#payer_id_oculto").html();
		$(".menu_web").removeClass("active")
		$("#menu3").addClass("active")
		$(".primera").removeClass("side-nav")
		$("#menu3").addClass("active")

		$( "#target" ).click(function() {
		  alert( "Handler for  called." );
		});

		$scope.definir_url_home = function(){
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
		}
		//----
		$scope.backpageCart = function(){
			var idioma = $scope.idioma
			if(idioma=="1")
				var url_carrito = $scope.base_url+"carrito";
			else
				var url_carrito = $scope.base_url+"cart";
			$("#formOrderProducto").attr("action",url_carrito)
			$("#formOrderProducto").attr("method","post");
			$("#formOrderProducto").submit()
		}
		//-----
		$scope.definir_url_home();
		//----
	})
	.controller("confirmAccountController", function($scope,$http,$sce,$compile,$location,funcionaFactory,serviciosFactory,multIdioma,carritoFactory){
		//Cuerpo declaraciones
		$scope.productos = {}
		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#idioma").val();
		$scope.id = $("#idUser").html();
		$scope.codigo = $("#codigo").html();
		console.log($scope.codigo);
		$scope.currentTab1 = '';

		$scope.correoUsuario = $("#correoUsuario").html();

		$scope.nombre_apellido = $("#nombre_apellido").html();
		$(".menu_web").removeClass("active")

		$(".primera").removeClass("side-nav")

		$( "#target" ).click(function() {
		  alert( "Handler for  called." );
		});
		//console.log($scope.correoUsuario);
		//console.log($scope.idioma);

		$scope.definir_url_home = function(){
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
		}
		//----
		$scope.backpageCart = function(){
			var idioma = $scope.idioma
			if(idioma=="1")
				var url_carrito = $scope.base_url+"carrito";
			else
				var url_carrito = $scope.base_url+"cart";
			$("#formOrderProducto").attr("action",url_carrito)
			$("#formOrderProducto").attr("method","post");
			$("#formOrderProducto").submit()
		}
		$scope.activarUsuario = function () {
			$http.post($scope.base_url + "/WebLogin/activar_usuario", {
				'id': $scope.id,
				'codigo': $scope.codigo,
			}).success(function (data, estatus, headers, config) {
				$scope.mensajes = data;
				//console.log(data);
				if ($scope.mensajes.mensaje == "registro_procesado") {
					$scope.currentTab1 ='exito';
				} else if ($scope.mensajes.mensaje == "ya_activado") {
					$scope.currentTab1 ='ya_activado';
				} else if ($scope.mensajes.mensaje == 'error') {
						$scope.currentTab1 ='error';
				}
			}).error(function (data, estatus) {
				console.log(data);
			});ya_activado
		}
		//-----
		$scope.activarUsuario();
		$scope.definir_url_home();
		//----
	})
	.controller("contactanosController", function($scope,$http,$compile,$location,footerFactory,contactosFactory,multIdioma){
		//Cuerpo declaraciones
		$scope.contactos = {
								'id':'',
								'nombres':'',
								'telefono':'',
								'email':'',
								'mensaje':''
		}
		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#id_idioma").val();
		$(".menu_web").removeClass("active")
		$("#menu5").addClass("active")
		//------------------------------------------------------------
		/*
		*	Bloque obligatorio para todo controlador
		*/
		/*
		*	Define segun el valor del idioma los titulos
		*/
		$scope.definir_url_home = function(){
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
			//console.log($scope.btn);
		}
		//--
		$scope.validar_guardar = function(){
			if($scope.contactos.nombres==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar sus nombres", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter their names", "alert-danger");

				return false;
			}else
			if($("#contactos_email").val()==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar una dirección de correo v&aacute;lida", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter valid email direction", "alert-danger");
				return false;
			}
			else
			if($("#contactos_telefono").val()==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar un número de teléfono válido", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter valid telephone number", "alert-danger");
				return false;
			}
			else if($scope.contactos.mensaje==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar un mensaje", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter a message", "alert-danger");
				return false;
			}else
				return true;
		}
		//--
		//--Registrar contactos
		$scope.registrar_contactos = function(){
			if($scope.validar_guardar()==true){
				//uploader_reg("#campo_mensaje_clientes","#nombres_clientes,#email_clientes,#telefono_clientes,#mensaje_clientes");
				$http.post($scope.base_url+"/WebInicio/registrarContactos",{
					'nombres': $scope.contactos.nombres,
					'email': $scope.contactos.email,
					'telefono': $scope.contactos.telefono,
					'mensaje': $scope.contactos.mensaje,
					'id_idioma':$scope.idioma,
				}).success(function(data, estatus, headers, config){
					$scope.mensajes  = data;
					if($scope.mensajes.mensaje == "registro_procesado"){
						if($scope.idioma == "1")
							mensaje_alerta("#campo_mensaje_clientes","Gracias por tu tiempo, tu mensaje ha sido enviado!","alert-success");
						else
							mensaje_alerta("#campo_mensaje_clientes","Thank you for your time, your message has been sent!","alert-success");
						$scope.limpiar_cajas();
					}else if($scope.mensajes.mensaje =="existe"){
						if($scope.idioma == "1")
							mensaje_alerta("#campo_mensaje_clientes","Ya fue registrado un contacto con esa dirección de email","alert-success");
						else
							mensaje_alerta("#campo_mensaje_clientes","A contact has already been registered with that email address","alert-success");
					}
					else{
						mensaje_alerta("#campo_mensaje_clientes","Ocurrió un error inesperado","alert-danger");

					}
					//desbloquear_pantalla("#campo_mensaje_clientes","#nombres_clientes,#email_clientes,#telefono_clientes,#mensaje_clientes")
				}).error(function(data,estatus){
					showErrorMessage("Ocurrió un error inesperado:"+data);
				})
			}
		}
		
		//--Limpiar cajas
		$scope.limpiar_cajas = function(){
			$scope.contactos = {
									"id":"",
									"nombres":"",
									"email":"",
									"telefono":"",
									"mensaje":""
			}
		}
		//--
		/*
		*
		*/
		$scope.definir_url_home();
	
	})
	.controller("privacyController", function($scope,$sce,$http,$compile,$location,funcionaFactory,nosotrosFactory,clientesFactory){
		//---------------------------------------	
		if($location.path()=="/privacy_policy"){
			$scope.idioma=2 
		}else{
			$scope.idioma=1
		}
		
		//--
		$scope.sanitizeMe = function(text) {
		    return $sce.trustAsHtml(text)
		}
		//--
		//Bloque de metodos	
		//---
		carga_inicio()
		
		//---------------------------------------
	})
	.controller("productosController", function($scope,$http,$sce,$compile,$location,funcionaFactory,nosotrosFactory,cargar,serviciosFactory,multIdioma,productos_todos_HomeFactory,mas_productosFactory,lineaFactory,generoFactory,colorFactory,tallaFactory,cargar_titulo_producto,cargar_img_linea,cargar_img_genero,cargar_img_color,cargar_img_talla,cargar_img_precio, cargar_val2, cargarfil2, cargarfil3, cargarfil4,cargarfil5,cargarfil6,cargarfil7,cargarfil8,cargarfil9,cargarfil10,
												cargar_val3,cargar_val4,cargar_val5,cargar_val6,cargar_val7,cargar_val8,cargar_val9,cargar_val10,cargar_val111,cargar_val112,cargar_val113,cargar_val114,cargar_val115)
	{
		//Cuerpo declaraciones
		$scope.productos = {}
		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#id_idioma").val();
		$scope.start = 0;
		$(".menu_web").removeClass("active")
		$("#menu3").addClass("active")


		$(".primera").removeClass("side-nav")
		$("#menu3").addClass("active")

		$( "#target" ).click(function() {
		  alert( "Handler for  called." );
		});

		$scope.consultar_inicial = true
		
		if(screen.width==760){
			$scope.consultar_inicial = false;
		}

		//alert($scope.consultar_inicial)
		
		//------------------------------------------------------------
		/*
		*	Bloque obligatorio para todo controlador
		*/
		/*
		*	Define segun el valor del idioma los titulos
		*/

		if ($scope.idioma == 1) {
			$scope.titulo_genero = 'Filtro por Género'
		}else {
			$scope.titulo_genero = 'Filter by Gender'
		}

		if ($scope.idioma == 1) {
			$scope.titulo_color = 'Filtro por Color'
		}else {
			$scope.titulo_color = 'Filter by Color'
		}

		if ($scope.idioma == 1) {
			$scope.titulo_talla = 'Filtro por Talla'
		}else {
			$scope.titulo_talla = 'Filter by Size'
		}

		if ($scope.idioma == 1) {
			$scope.titulo_linea = 'Filtro por Línea'
		}else {
			$scope.titulo_linea = 'Line filter'
		}

		if ($scope.idioma == 1) {
			$scope.titulo_precio = 'Filtro por Precio'
		}else {
			$scope.titulo_precio = 'Filter by Price'
		}

		if ($scope.idioma == 1) {
			$scope.nombre_rango = 'Rango de Precio'
		}else {
			$scope.nombre_rango = 'Price range'
		}

		$scope.definir_url_home = function(){
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
		}

		$scope.sub_productos = []

		$scope.productos = {
								'id':'',
								'id_idioma':'',
								'titulo':'',
								'precio':'',
								'estatus':'',
								'id_imagen':''
		}

		$scope.consultar_productos = function(){
    		productos_todos_HomeFactory.asignar_valores($scope.idioma,$scope.base_url);
			productos_todos_HomeFactory.cargar_productos(function(data){
				$scope.productos = data;
				//console.log($scope.productos);
					for (i in data) {
						$scope.sub_productos.push(data[i]);
					}
					$scope.start =  0
			});
		}

		$scope.cargar_mas_productos = function(){
			$scope.prueba = $scope.productos
			var ids = Object.values($scope.prueba);
			$scope.id_d = ids.length - 1
			$scope.id = $scope.productos[$scope.id_d]['id']
			$scope.id_imagen = $scope.productos[$scope.id_d]['id_imagen']

			$scope.limit = 3
			$scope.start = $scope.start+5;
			mas_productosFactory.asignar_valores($scope.idioma,$scope.base_url,$scope.id,$scope.id_imagen,$scope.limit,$scope.start)
			mas_productosFactory.cargar_mas_prod(function(data){
				$scope.productos = data;
				if (data === null || data === "null" || data === "") {
					$("#cargar_mas").prop('disabled', true);
					if($scope.idioma==1)
						mensaje_alerta("#campo_mensaje_productos","Todas los productos disponibles fueron cargadas!","alert-warning");
					else
						mensaje_alerta("#campo_mensaje_productos","All available products were loaded!","alert-warning");
				} else {
					for (i in data) {
						$scope.sub_productos.push(data[i]);
					}
					$scope.start += 1;
				}
			});
		}

		$scope.cargar_linea = function(){
			lineaFactory.asignar_valores($scope.base_url,$scope.idioma)
			lineaFactory.cargar_linea(function(data){
				$scope.linea=data;
			});
		}

		$scope.cargar_genero = function(){
			generoFactory.asignar_valores($scope.base_url,$scope.idioma)
			generoFactory.cargar_generos(function(data){
				$scope.genero=data;
			});
		}

		$scope.cargar_color = function(){
			colorFactory.asignar_valores($scope.base_url,$scope.idioma)
			colorFactory.cargar_color(function(data){
				$scope.color=data;
			});
		}

		$scope.cargar_talla = function(){
			tallaFactory.asignar_valores($scope.base_url,$scope.idioma)
			tallaFactory.cargar_talla(function(data){
				$scope.talla=data;
			});
		}

		$scope.sub_productos_filtro =[];

		$scope.linea_filtrar = '';
		$scope.genero_filtrar = '';
		$scope.precio_filtrar_1 = '';
		$scope.precio_filtrar_2 = '';
		$scope.color_filtrar = '';
		$scope.talla_filtrar = '';
		
		$scope.limpiar_var_filtros = function(){
			$scope.linea_filtrar = ""
			$scope.genero_filtrar =  ""
			$scope.precio_filtrar_1 = ""
			$scope.precio_filtrar_2 = ""
			$scope.color_filtrar = ""
			$scope.talla_filtrar = ""
		}

		$scope.filtro_titulo = function(){
			
			if($scope.titulo){
				$(".activo_productos").removeClass("activo_productos")
				//--
				$scope.limpiar_var_filtros()
				//--
				$scope.titulo_filtrar = $scope.titulo;
				$scope.filtros();
				$scope.esconder();
			}else{
				$scope.titulo_filtrar = "";
				location.reload();
			}
			
			
		}

		$scope.filtro_linea = function(id){
			$scope.titulo ="";
			$scope.lineas = id;
			$scope.linea_filtrar = $scope.lineas;
			$scope.filtros();
		}

		$scope.filtro_genero = function(id){
			$scope.titulo ="";
			$scope.generos = id;
			$scope.genero_filtrar = $scope.generos;
			$scope.filtros();
		}

		$scope.filtro_precio = function(){
			$scope.titulo ="";
			$scope.rango_1 = $("#price-amount-1").val();
			$scope.rango_2 = $("#price-amount-2").val();

			$scope.precio_filtrar_1 = $scope.rango_1;
			$scope.precio_filtrar_2 = $scope.rango_2;
			$scope.filtros();
		}

		$scope.filtro_color = function(id){
			$scope.titulo ="";
			$scope.colores = id;
			$scope.color_filtrar = $scope.colores;
			$scope.filtros();
		}
		$scope.filtro_talla = function(id){
			$scope.titulo ="";
			$scope.tallas = id;
			$scope.talla_filtrar = $scope.tallas;
			$scope.filtros();
		}

		$scope.filtros = function(){
			//X TITULO
			if($scope.titulo_filtrar != ''){
				cargar_titulo_producto.asignar_valores($scope.base_url,$scope.idioma,$scope.titulo_filtrar)
				cargar_titulo_producto.cargar_filtro_titulo(function(data){
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						//$scope.limit = 1
						$scope.start =  0
					}
				});
			}
			//X FILTRO
			if ($scope.linea_filtrar != '') {
				cargar_img_linea.asignar_valores($scope.base_url,$scope.idioma,$scope.linea_filtrar)
				cargar_img_linea.cargar_img_linea(function(data){
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						//$scope.limit = 1
						$scope.start =  0
					}
				});
			}
			//X GENERO
			if ($scope.genero_filtrar != '') {
				cargar_img_genero.asignar_valores($scope.base_url,$scope.idioma,$scope.genero_filtrar)
				cargar_img_genero.cargar_img_genero(function(data){
					$scope.sub_productos_filtro =[];
					//console.log($scope.sub_productos_filtro)
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para este Género!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Genre!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//PRECIO
			if (($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')) {
					cargar_img_precio.asignar_valores($scope.base_url,$scope.idioma,$scope.precio_filtrar_1,$scope.precio_filtrar_2)
					cargar_img_precio.cargar_img_precio(function(data){
						$scope.prueba = data;
						$scope.sub_productos_filtro =[];

						if (data === null || data == "null" || data=="") {
							if($scope.idioma==1)
								mensaje_alerta("#campo_mensaje","No hay productos disponible para este Rango de Precio!","alert-warning");
							else
								mensaje_alerta("#campo_mensaje","There are no products available for this Price Range!","alert-warning");
						}else{
							for (i in data) {
								$scope.sub_productos_filtro.push(data[i]);
							}
							$scope.start =  0
						}
					});
			}
			//X COLOR
			if ($scope.color_filtrar != '') {
				cargar_img_color.asignar_valores($scope.base_url,$scope.idioma,$scope.color_filtrar)
				cargar_img_color.cargar_img_color(function(data){
					$scope.sub_productos_filtro =[];

					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para este Color!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Color!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//X TALLA
			if ($scope.talla_filtrar != '') {
				cargar_img_talla.asignar_valores($scope.base_url,$scope.idioma,$scope.talla_filtrar)
				cargar_img_talla.cargar_img_talla(function(data){
					$scope.prueba = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Talla!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Size!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//X LINEA Y GENERO
			if (($scope.linea_filtrar != '')&&($scope.genero_filtrar != '')) {
				cargar.asignar_valores($scope.idioma,$scope.linea_filtrar, $scope.genero_filtrar)
				cargar.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//XLIENA Y PRECIO
			if (($scope.linea_filtrar != '')&&($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')) {
				cargarfil2.asignar_valores($scope.idioma,$scope.linea_filtrar, $scope.precio_filtrar_1, $scope.precio_filtrar_2)
				cargarfil2.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//Xlinea Y color
			if (($scope.linea_filtrar != '')&&($scope.color_filtrar != '')) {
				cargarfil4.asignar_valores($scope.idioma,$scope.linea_filtrar, $scope.color_filtrar)
				cargarfil4.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//Xlinea Y talla
			if (($scope.linea_filtrar != '')&&($scope.talla_filtrar != '')) {
				cargarfil5.asignar_valores($scope.idioma,$scope.linea_filtrar, $scope.talla_filtrar)
				cargarfil5.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//X GENERO Y PRECIO
			if (($scope.genero_filtrar != '')&&($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')) {
				cargarfil3.asignar_valores($scope.idioma,$scope.genero_filtrar, $scope.precio_filtrar_1, $scope.precio_filtrar_2)
				cargarfil3.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta  Filtro!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Filtro!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//X GENERO Y COLOR
			if (($scope.genero_filtrar != '')&&($scope.color_filtrar != '')) {
				cargarfil6.asignar_valores($scope.idioma,$scope.genero_filtrar, $scope.color_filtrar)
				cargarfil6.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta  Filtro!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Filtro!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//X GENERO Y TALLA
			if (($scope.genero_filtrar != '')&&($scope.talla_filtrar != '')) {
				cargarfil7.asignar_valores($scope.idioma,$scope.genero_filtrar, $scope.talla_filtrar)
				cargarfil7.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta  Filtro!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Filtro!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//X PRECIO Y COLOR
			if ((($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')&&$scope.color_filtrar != '')) {
				cargarfil8.asignar_valores($scope.idioma,$scope.color_filtrar, $scope.precio_filtrar_1, $scope.precio_filtrar_2)
				cargarfil8.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta  Filtro!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Filtro!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//X PRECIO Y TALLA
			if ((($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')&&$scope.talla_filtrar != '')) {
				cargarfil9.asignar_valores($scope.idioma,$scope.talla_filtrar, $scope.precio_filtrar_1, $scope.precio_filtrar_2)
				cargarfil9.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta  Filtro!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Filtro!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//X COLOR Y TALLA
			if (($scope.color_filtrar != '')&&($scope.talla_filtrar != '')) {
				cargarfil10.asignar_valores($scope.idioma,$scope.color_filtrar, $scope.talla_filtrar)
				cargarfil10.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta  Filtro!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Filtro!","alert-warning");

					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//XLINEA, GENERO Y PRECIO
			if (($scope.linea_filtrar != '')&&($scope.genero_filtrar != '') &&($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')){
				cargar_val2.asignar_valores($scope.idioma,$scope.linea_filtrar,$scope.genero_filtrar, $scope.precio_filtrar_1, $scope.precio_filtrar_2)
				cargar_val2.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
			//XLINEA, GENERO Y COLOR
			if (($scope.linea_filtrar!='')&&($scope.genero_filtrar!='') &&($scope.color_filtrar!='')){
				cargar_val3.asignar_valores($scope.idioma,$scope.linea_filtrar,$scope.genero_filtrar, $scope.color_filtrar)
				cargar_val3.valores(function(data){
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}

			//XLINEA, GENERO Y TALLA
			if (($scope.linea_filtrar!='')&&($scope.genero_filtrar!='') &&($scope.talla_filtrar!='')){
				cargar_val4.asignar_valores($scope.idioma,$scope.linea_filtrar,$scope.genero_filtrar, $scope.talla_filtrar)
				cargar_val4.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}

			//XLINEA, PRECIO Y COLOR
			if (($scope.linea_filtrar != '')&&($scope.color_filtrar != '') &&($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')){
				cargar_val5.asignar_valores($scope.idioma,$scope.linea_filtrar,$scope.color_filtrar, $scope.precio_filtrar_1, $scope.precio_filtrar_2)
				cargar_val5.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}

			//XLINEA, PRECIO Y TALLA
			if (($scope.linea_filtrar != '')&&($scope.talla_filtrar != '') &&($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')){
				cargar_val6.asignar_valores($scope.idioma,$scope.linea_filtrar,$scope.talla_filtrar, $scope.precio_filtrar_1, $scope.precio_filtrar_2)
				cargar_val6.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}

			//XLINEA, COLOR Y TALLA
			if (($scope.linea_filtrar != '')&&($scope.color_filtrar != '') &&($scope.talla_filtrar != '')){
				cargar_val7.asignar_valores($scope.idioma,$scope.linea_filtrar,$scope.color_filtrar, $scope.talla_filtrar)
				cargar_val7.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}

			//GENERO, PRECIO Y COLOR
			if (($scope.genero_filtrar != '')&&($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')&&($scope.color_filtrar != '') ){
				cargar_val8.asignar_valores($scope.idioma,$scope.genero_filtrar,$scope.precio_filtrar_1, $scope.precio_filtrar_2,$scope.color_filtrar)
				cargar_val8.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}

			//GENERO, PRECIO Y TALLA
			if (($scope.genero_filtrar != '')&&($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')&&($scope.talla_filtrar != '') ){
				cargar_val9.asignar_valores($scope.idioma,$scope.genero_filtrar,$scope.precio_filtrar_1, $scope.precio_filtrar_2,$scope.talla_filtrar)
				cargar_val9.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}

			//PRECIO, COLOR y TALLA
			if (($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')&&($scope.color_filtrar != '')&&($scope.talla_filtrar != '')){
				cargar_val10.asignar_valores($scope.idioma,$scope.precio_filtrar_1, $scope.precio_filtrar_2,$scope.color_filtrar,$scope.talla_filtrar)
				cargar_val10.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}

			//LINEA, GENERO y PRECIO Y COLOR
			if (($scope.linea_filtrar != '')&&($scope.genero_filtrar != '')&&($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')&&($scope.color_filtrar != '')){
				cargar_val111.asignar_valores($scope.idioma,$scope.linea_filtrar,$scope.genero_filtrar,$scope.precio_filtrar_1, $scope.precio_filtrar_2,$scope.color_filtrar)
				cargar_val111.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}

			//LINEA, GENERO y PRECIO Y TALLA
			if (($scope.linea_filtrar != '')&&($scope.genero_filtrar != '')&&($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')&&($scope.talla_filtrar != '')){
				cargar_val112.asignar_valores($scope.idioma,$scope.linea_filtrar,$scope.genero_filtrar,$scope.precio_filtrar_1, $scope.precio_filtrar_2,$scope.talla_filtrar)
				cargar_val112.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}

			//LINEA, PRECIO, TALLA Y COLOR
			if (($scope.linea_filtrar != '')&&($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')&&($scope.talla_filtrar != '')&&($scope.color_filtrar != '')){
				cargar_val113.asignar_valores($scope.idioma,$scope.linea_filtrar,$scope.precio_filtrar_1, $scope.precio_filtrar_2,$scope.talla_filtrar,$scope.color_filtrar)
				cargar_val113.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}

			//PRECIO, TALLA, COLOR y GENERO
			if (($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')&&($scope.talla_filtrar != '')&&($scope.color_filtrar != '')&&($scope.genero_filtrar != '')){
				cargar_val114.asignar_valores($scope.idioma,$scope.precio_filtrar_1, $scope.precio_filtrar_2,$scope.talla_filtrar,$scope.color_filtrar,$scope.genero_filtrar)
				cargar_val114.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}

			//LINEA,GENERO, PRECIO, COLOR Y TALLA
			if (($scope.linea_filtrar != '')&&($scope.genero_filtrar != '')&&($scope.precio_filtrar_1 != '')&&($scope.precio_filtrar_2 != '')&&($scope.color_filtrar != '')&&($scope.talla_filtrar != '')){
				cargar_val115.asignar_valores($scope.idioma,$scope.linea_filtrar,$scope.genero_filtrar,$scope.precio_filtrar_1, $scope.precio_filtrar_2,$scope.color_filtrar,$scope.talla_filtrar)
				cargar_val115.valores(function(data){
					$scope.yeah = data;
					$scope.sub_productos_filtro =[];
					if (data === null || data == "null" || data=="") {
						if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje","No hay productos disponible para esta Línea!","alert-warning");
						else
							mensaje_alerta("#campo_mensaje","There are no products available for this Line!","alert-warning");
					} else {
						for (i in data) {
							$scope.sub_productos_filtro.push(data[i]);
						}
						$scope.start =  0
					}
				});
			}
		}

		$scope.esconder = function(){
			$scope.variable = true;
			$scope.variable2 = false;
		}

		$scope.no_esconder = function(){
			$scope.variable = false;
			$scope.variable2 = true;
		}

		$scope.ver_categoriaprod = function(valor){
			var id = $("#ver"+valor).attr("data");
			$scope.id_categoria = id;
			$("#id_categoria").val($scope.id_categoria)
		}
		
		$scope.cargar_linea();
		$scope.cargar_genero();
		$scope.cargar_color();
		$scope.cargar_talla();

		$scope.consultar_productos();
		$scope.definir_url_home();
	})
	.controller("recoveryPasswordController", function($scope,$http,$sce,$compile,$location,funcionaFactory,serviciosFactory,multIdioma,carritoFactory){
		//Cuerpo declaraciones
		$scope.productos = {}
		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#id_idioma").val();
		$scope.clave = "";
		$scope.clave_r = "";
		$scope.id = $("#idUser").html();
		$scope.codigo = $("#codigo").html();
		//console.log($scope.codigo);
		$scope.currentTab1 = '';

		$scope.correoUsuario = $("#correoUsuario").html();

		$scope.nombre_apellido = $("#nombre_apellido").html();
		$(".menu_web").removeClass("active")

		$(".primera").removeClass("side-nav")

		$( "#target" ).click(function() {
		  alert( "Handler for  called." );
		});
		//console.log($scope.correoUsuario);
		//console.log($scope.idioma);

		$scope.definir_url_home = function(){
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
		}
		//----
		$scope.backpageCart = function(){
			var idioma = $scope.idioma
			if(idioma=="1")
				var url_carrito = $scope.base_url+"carrito";
			else
				var url_carrito = $scope.base_url+"cart";
			$("#formOrderProducto").attr("action",url_carrito)
			$("#formOrderProducto").attr("method","post");
			$("#formOrderProducto").submit()
		}

		$scope.verificarClave = function(){
			if($scope.validar_form()==true){
					$scope.cambiarClave();
			}
		}					
		$scope.validar_form = function () {
			
			if ($scope.clave == "") {
				console.log($scope.clave);
				
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_cambio_clave", "Debe ingresar una contraseña", "alert-warning");
				}
				else{
					mensaje_alerta("#campo_mensaje_cambio_clave", "You must enter a password", "alert-warning");
				}
				return false;
			} else if ($scope.clave_r == "") {
				/*console.log($scope.clave_r);
				console.log($scope.clave);*/
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_cambio_clave", "Debe repetir la contraseña", "alert-warning");
				}
				else{
					mensaje_alerta("#campo_mensaje_cambio_clave", "You must repeat the password", "alert-warning");
				}
				return false;
			} else if ($scope.clave != $scope.clave_r) {
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_cambio_clave", "Las contraseñas no coinciden", "alert-warning");
				}
				else{
					mensaje_alerta("#campo_mensaje_cambio_clave", "Passwords do not match", "alert-warning");
				}
				return false;
			} else if(document.getElementById('clave_cambio').value.length < 8){
				if($scope.idioma==1){
					mensaje_alerta("#campo_mensaje_cambio_clave", "Debe ingresar una contraseña con 8 caracteres", "alert-warning");
				}
				else{
					mensaje_alerta("#campo_mensaje_cambio_clave", "You must enter a password with 8 characters", "alert-warning");
				}
			}else {
				return true;
			}
		}
		$scope.cambio_clave = function () {
			$http.post($scope.base_url + "/WebLogin/verificar_cambio", {
				'id': $scope.id,
				'codigo': $scope.codigo,
			}).success(function (data, estatus, headers, config) {
				$scope.mensajes = data;
				//console.log(data);
				if ($scope.mensajes.mensaje == "aprobado") {
					$scope.currentTab1 ='aprobado';
				} else if ($scope.mensajes.mensaje == 'no_aprobado') {
						$scope.currentTab1 ='error';
				}
			}).error(function (data, estatus) {
				console.log(data);
			});ya_activado
		}
		$scope.cambiarClave = function () {
			$http.post($scope.base_url + "/WebLogin/cambiar_clave", {
				'clave': $scope.clave,
				'clave_r': $scope.clave_r,
				'id': $scope.id,
				'codigo': $scope.codigo,
			}).success(function (data, estatus, headers, config) {
				$scope.mensajes = data;
				//console.log(data);
				if ($scope.mensajes.mensaje == "cambio_logrado") {
					$scope.currentTab1 ='cambio_logrado';
				} else if ($scope.mensajes.mensaje == 'no_aprobado') {
						$scope.currentTab1 ='error';
				}
			}).error(function (data, estatus) {
				console.log(data);
			});ya_activado
		}
		//-----
		$scope.cambio_clave();
		$scope.definir_url_home();
		//----
	})
	.controller("redirectAccountController", function($scope,$http,$sce,$compile,$location,funcionaFactory,serviciosFactory,multIdioma,carritoFactory){
		//Cuerpo declaraciones
		$scope.productos = {}
		$scope.tp_login = "";
		$scope.base_url = $("#base_url").val();
		$scope.tp_login = $("#tp_login").html();
		//console.log($scope.tp_login);
		$(".menu_web").removeClass("active")

		$(".primera").removeClass("side-nav")

		$( "#target" ).click(function() {
		  alert( "Handler for  called." );
		});
		/*console.log($scope.correoUsuario);
		console.log($scope.idioma);*/

		$scope.definir_mensaje = function(){
			if($scope.tp_login==1){
				$scope.currentab ="email"	
			}else if($scope.tp_login==2){
				$scope.currentab ="google"	
			}else if($scope.tp_login==3){
				$scope.currentab ="facebook"	
			}else{
				$scope.currentab ="error"	
			}
		}
		$scope.definir_url_home = function(){
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
		}
		//----
		$scope.backpageCart = function(){
			var idioma = $scope.idioma
			if(idioma=="1")
				var url_carrito = $scope.base_url+"carrito";
			else
				var url_carrito = $scope.base_url+"cart";
			$("#formOrderProducto").attr("action",url_carrito)
			$("#formOrderProducto").attr("method","post");
			$("#formOrderProducto").submit()
		}
		
		$scope.definir_mensaje();

		$scope.definir_url_home();
		//----
	})
	.controller("productoDetallesController", function($scope,$http,$sce,$compile,$location,funcionaFactory,serviciosFactory,multIdioma,productoFactory,productosHomeFactory,tallaProdFactory,colorProdFactory,cantidadPFactory){
		//Cuerpo declaraciones
		
		$scope.productos = {}
		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#id_idioma").val();
		$scope.id_prod = "";
		$(".menu_web").removeClass("active")
		$("#menu3").addClass("active")

		$(".primera").removeClass("side-nav")
		$("#menu3").addClass("active")

		$( "#target" ).click(function() {
		  alert( "Handler for  called." );
		});

		if($scope.idioma == 1){
			$scope.titulo_1 = 'Detalle del Producto'
		}else{
			$scope.titulo_1 = 'Product Detail'
		}

		$scope.definir_url_home = function(){
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
		}

		
		//-----------------------------------------------------------
		$scope.consultar_producto_slug = function(){
			$scope.definir_url_home();
			$scope.slug = $("#slug_producto").text();
			productoFactory.asignar_valores($scope.idioma,$scope.base_url,$scope.slug)
			productoFactory.cargar_producto_slug(function(data){
				$scope.producto=data[0];
				//console.log($scope.producto);
				$scope.consultar_subproducto($scope.producto.id,3,0);

				//--Formato en minusculas$scope.id_prod
				$scope.producto.categoria = mayus_primera_letra($scope.producto.categoria)
				$scope.id_prod = $scope.producto.id
				$scope.producto.tipo_producto = mayus_primera_letra($scope.producto.tipo_producto)
				
				$scope.consultar_mas_producto($scope.producto.id);
				$scope.consultar_talla($scope.producto.id);

			});
		}
			//-----------------------------------------------------------
		$scope.consultar_talla = function(id){
			$scope.id_prod = id;
			//console.log($scope.id_prod);
			tallaProdFactory.asignar_valores("",$scope.id_prod,$scope.base_url)
			tallaProdFactory.cargar_talla(function(data){
			$scope.tallas=data;				
			//console.log($scope.tallas);
			})
		} 
		//--/-----------------------------------------------------------
		$scope.ColoresExistentes = function(){
			
			$("#sin_preloader").val("1")
			
			if($scope.talla.id_talla!=""){
			colorProdFactory.asignar_valores($scope.talla.id_talla,$scope.id_prod,$scope.base_url)
			colorProdFactory.cargar_color(function(data){
			$scope.colores=data;				
			//console.log($scope.colores);
			})}
		}
		$scope.descripcion_color = "Colores";
		//-------------------------------------------------------------
		$scope.CantidadExistentes = function(){

			$("#sin_preloader").val("1")

			if($scope.color.descripcion_color!=$scope.descripcion_color){
				$scope.descripcion_color = "Colores";
				cantidadPFactory.asignar_valores($scope.color.id_color,$scope.talla.id_talla,$scope.id_prod,$scope.base_url)
			cantidadPFactory.cargar_cantidad(function(data){
			$scope.cantidad=data;				
			//console.log($scope.descripcion_color);
			$scope.asignarCantidad()

			}) }

		}
		
		//--/-----------------------------------------------------------
		$scope.consultar_subproducto = function(id,limit, start){
				productoFactory.asignar_valores($scope.idioma,$scope.base_url,id,limit,start)
				productoFactory.cargar_sub_producto(function(data){
				$scope.sub_producto=data;
			});
		  }
		
		//-----------------------------------------------------------
		$(document).on("click",".imagen_selector",function(){
			div_imagen = $(this).attr("dat");
		    //--Limpio el intervalo...
		    //clearInterval(intervalo);
		    //--Inicio un nuevo intervalo---
	    	$('#slider div:first-child').hide();
	    	imagen = $(this).attr("src")
		    $(div_imagen).fadeIn().prependTo('#slider');
		    //$(div_imagen).zoom();
		    //$scope.generar_slider();     						
		});
		//-----------------------------------------------------------
		$scope.generar_slider=function(){
			//$('#slider div:gt(1)').hide();
		    //intervalo = setInterval(function(){
		    $('#slider div:first-child').fadeOut("slow")
	        							.next('div').fadeIn("slow")
	         							.end().appendTo('#slider');
		   // }, 6000);	
		}
		//-----------------------------------------------------------
		$scope.agregarCarrito = function(){
			
			var id_productos = $("#id_producto").html()
			var cantidad = $("#cantidadProducto").val()
			var monto_individual = parseFloat($scope.producto.precio_oculto)
			var monto_total = parseFloat(monto_individual*cantidad)
			var idioma = $scope.idioma
			var id_producto_clonado = $scope.producto.id_original_clonado;
			//console.log($scope.producto)
			/*if(idioma==1)
				mensaje_alerta("#campo_mensaje_productos","Disculpe, la opción de compra aun no se encuentra activa","alert-info");
			else
				mensaje_alerta("#campo_mensaje_productos","Excuse me, the purchase option is not yet active","alert-info");
			*/
			//----------------------------------------
			if($scope.validar_form(id_productos,cantidad)==true){
				$http.post($scope.base_url+"WebProductos/agregarCarrito",
				{
					
					'id'	  : id_productos,
					'cantidad': cantidad,
					'monto_individual': monto_individual,
					'monto_total': monto_total,
					'id_idioma': idioma,
					'id_producto_clonado':id_producto_clonado,
					'cantidad_existencia':$scope.producto.cantidad,
					'id_talla':$scope.talla.id_talla,
					'id_color':$scope.color.id_color,
					'cantidad_total':$scope.cantidad.cantidad,
					
				}).success(function(data, estatus, headers, config){
					$scope.mensajes  = data;
					//console.log($scope.mensajes)
					if($scope.mensajes.mensaje == "registro_procesado"){
						if(idioma==1)
							mensaje_alerta("#campo_mensaje_productos","Se ha agregado el producto a su carrito de compras","alert-info");
						else
							mensaje_alerta("#campo_mensaje_productos","The product was add to your cart","alert-info");
						setTimeout(function(){
							if(idioma=="1")
								var url_carrito = $scope.base_url+"carrito";
							else
								var url_carrito = $scope.base_url+"cart";
							$("#formProducto").attr("action",url_carrito)
							$("#formProducto").attr("method","post");
							$("#formProducto").submit()
						},2000)
					}else if($scope.mensajes.mensaje=="excede_cantidad"){
						if(idioma==1)
							mensaje_alerta("#campo_mensaje_productos","El producto no puede ser agregado al carrito porque excede la cantidad en inventario","alert-warning");
						else
							mensaje_alerta("#campo_mensaje_productos","The product cannot be added to the cart because it exceeds the amount in inventory","alert-warning");

					}
					else{
						if(idioma==1)
							mensaje_alerta("#campo_mensaje_productos","Ocurrío un error inesperado","alert-warning");
						else
							mensaje_alerta("#campo_mensaje_productos","An unexpected error occurred","alert-warning");
					}
				}).error(function(data,estatus){
					console.log(data);
				});
			}
			//----------------------------------------
		}
		//-----------------------------------------------------------
		$scope.validar_form = function(id_productos,cantidad){

			if(id_productos==""){
				if($scope.idioma==1)
					mensaje_alerta("#campo_mensaje_productos","Debe haber seleccionado un producto!","alert-warning");
				else
					mensaje_alerta("#campo_mensaje_productos","You must have selected a producto!","alert-warning");
				return false;
			} else if(cantidad=="0"){
				if($scope.idioma==1)
					mensaje_alerta("#campo_mensaje_productos","Debe haber seleccionado cantidad!","alert-warning");
				else
					mensaje_alerta("#campo_mensaje_productos","You must have selected quantity!","alert-warning");
				return false;
			}else{
				return true;
			}
		}
		//-----------------------------------------------------------
		$scope.asignarCantidad = function(){
			//console.log($scope.cantidad.cantidad);
			var cantidad = $scope.cantidad.cantidad
			for(i=1;i<=cantidad;i++){
				option = '<option>'+i+'</option>'
				$("#cantidadProducto").append(option);
			}
		}
		$scope.VaciaCantidad = function(){
			//var cantidad = $scope.cantidad.cantidad
			//$("#cantidadProducto").append(option);
			$('#cantidadProducto' ).children().remove();
			option = "<option>"+0+"</option>";
			$("#cantidadProducto").append(option);
		}


		$scope.VaciaColor = function(){
			//console.log($scope.descripcion_talla,$scope.talla.descripcion_talla);

			if($scope.descripcion_talla != $scope.talla.descripcion_talla){
				$scope.talla.descripcion_talla = $scope.talla.descripcion_talla;

				$scope.colores = "";
				$scope.color = "";
				$scope.ColoresExistentes();
			 }
		}
		
		//-----------------------------------------------------------
		$scope.consultar_mas_producto = function(id){
			var start = 0;
			var limit = 3;
			var id =
			productoFactory.asignar_valores($scope.idioma,$scope.base_url,id,limit,start)
			productoFactory.cargar_mas_producto(function(data){
				$scope.mas_productos=data;
				//console.log($scope.mas_productos)
			});
		}		
		//-----------------------------------------------------------
		$scope.consultar_producto_slug();

		$scope.definir_url_home();

		
	});
	function super_hover(){
		$('.img-slider-ppal').zoom({ on:'mouseover' });				
	}