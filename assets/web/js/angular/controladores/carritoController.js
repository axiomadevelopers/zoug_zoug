angular.module("ZougZougApp")
	.controller("carritoController", function($scope,$http,$sce,$compile,$location,funcionaFactory,serviciosFactory,multIdioma,carritoFactory){
		//Cuerpo declaraciones
		$scope.productos = {}
		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#id_idioma").val();

		$(".menu_web").removeClass("active")
		$("#menu3").addClass("active")

		$(".primera").removeClass("side-nav")
		$("#menu3").addClass("active")

		$( "#target" ).click(function() {
		  alert( "Handler for  called." );
		});

		if($scope.idioma == 1){
			$scope.titulo_1 = 'Detalle del Producto'
		}else{
			$scope.titulo_1 = 'Product Detail'
		}

		$scope.definir_url_home = function(){
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
		}
		//-----------------------------------------------------------
		$scope.consultar_info_carrito = function(){
			carritoFactory.asignar_valores($scope.idioma,$scope.base_url,'')
			carritoFactory.cargar_producto_carrito(function(data){
				$scope.productos=data;
				if($scope.productos==""){
					if($scope.idioma==1)
						mensaje_alerta("#campo_mensaje_productos","No hay productos agregados a su carrito de compra","alert-warning");
					else
						mensaje_alerta("#campo_mensaje_productos","Don't have products in your cart","alert-warning");
				}else{
					$scope.cuantos_productos = $scope.productos[0].cantidad_objetos
				}
				//console.log($scope.productos)
			});
		}

		$scope.eliminarProducto = function(event){
			var caja = event.currentTarget.id;
			var id = $("#"+caja).attr("data");
			//------------------------------------------
			$http.post($scope.base_url+"/WebCart/eliminar_producto",
			{
				'id' : id,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					//--Mensaje
					if($scope.idioma==1){
						mensaje_alerta("#campo_mensaje_productos","El producto sera retirado de su carrito de compras","alert-info");
						setTimeout(function(){
							window.location.href=$scope.base_url+"carrito";
						},2000);
					}
					else{
						mensaje_alerta("#campo_mensaje_productos","The product will be removed from your shopping cart","alert-info");
						setTimeout(function(){
							window.location.href=$scope.base_url+"cart";
						},2000);
					}
					//--
					//$scope.consultar_info_carrito();
				}else{
					//---
					//Mensaje de no realizado
					if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje_productos","Ocurrío un error inesperado","alert-warning");
						else
							mensaje_alerta("#campo_mensaje_productos","An unexpected error occurred","alert-warning");
 
					//---
				}
			}).error(function(data,estatus){
				console.log(data);
			});
			//------------------------------------------
		}
		//--
		
		//--
		$scope.update_cart = function(){
			$scope.id_producto_carrito = []
			$scope.cantidad = []
			$scope.monto_total = []
			var noCantidad = true
			$(".tabla_producto").each(function(index,element){
        	    numero = $(element).attr("data")
        	    $scope.id_producto_carrito.push($("#eliminar_"+numero).attr("data"));
        	    $scope.cantidad.push($("#cantidad_"+numero).val());
        	    $monto_total = (parseFloat($("#monto_oculto_"+numero).html()))*(parseInt($("#cantidad_"+numero).val()))
        	    $scope.monto_total.push($monto_total);
        	    //--
        	    //Verifico su alguno no tiene cantidad
        	    if(($("#cantidad_"+numero).val()=="")||($("#cantidad_"+numero).val()==0)){
        	    	noCantidad= false;
        	    }
        	    //--
        	});
        	//---
        	//console.log($scope.monto_total)
        	if(noCantidad){
        		//---
	        	$http.post($scope.base_url+"/WebCart/modificar_cantidad_carrito_productos",
				{
					'id' : $scope.id_producto_carrito,
					'cantidad': $scope.cantidad,
					'monto_total': $scope.monto_total
				}).success(function(data, estatus, headers, config){
					$scope.mensajes  = data;
					if($scope.mensajes.mensaje == "modificacion_procesada"){
						//--Mensaje
						if($scope.idioma==1){
							mensaje_alerta("#campo_mensaje_productos","Se ha actualizado su carrito","alert-info");
						}else{
							
							mensaje_alerta("#campo_mensaje_productos","The purchase was processed successfully","alert-info");
						}					
						//
					}else if($scope.mensajes.mensaje == "excede_cantidad"){
						//Mensaje de no realizado
						if($scope.idioma==1)
								mensaje_alerta("#campo_mensaje_productos","No puede exceder la cantidad en existencia del producto","alert-warning");
							else
								mensaje_alerta("#campo_mensaje_productos","It cannot exceed the quantity in stock of the product","alert-warning");
	 
						//---
					}
					else{
						//---
						//Mensaje de no realizado
						if($scope.idioma==1)
								mensaje_alerta("#campo_mensaje_productos","Ocurrío un error inesperado","alert-warning");
							else
								mensaje_alerta("#campo_mensaje_productos","An unexpected error occurred","alert-warning");
	 
						//---
					}
				}).error(function(data,estatus){
					console.log(data);
				});
        		//---
        	}else{
        		//Mensaje de no realizado
				if($scope.idioma==1)
						mensaje_alerta("#campo_mensaje_productos","El campo cantidad no debe estar en blanco","alert-warning");
					else
						mensaje_alerta("#campo_mensaje_productos","The quantity field must not be blank","alert-warning");

				//---
        	}
    	    
    	    //---
		}
		
		$scope.consultar_info_carrito();
		$scope.definir_url_home();
		//----
		//#Cuerpo de funciones js
		$scope.calcularMontoTotal = function(numero){
			cantidad = parseInt($("#cantidad_"+numero).val())
			monto_ind = parseFloat($("#monto_oculto_"+numero).html());
			total = cantidad * monto_ind
			$("#monto_total_"+numero).html(number_format(total,2))
			$("#monto_total_oculto_"+numero).html(total)
			$scope.recorrerSumaTotal()
		}
		//----
		$scope.recorrerSumaTotal = function(){
			acum = 0;
			$(".tabla_producto").each(function(index,element){
        	    numero = $(element).attr("data")
        	    acum+=parseFloat($("#monto_total_oculto_"+numero).html())

        	});
        	//---
        	$("#subtotalMonto").html(number_format(acum,2))
    	    $("#totalMonto").html(number_format(acum,2))
    	    $("#monto_total_oculto").html(acum)
        	//---
		}
		//---
		$scope.irOrden = function(){
			if($scope.idioma=="1")
				var url_carrito = $scope.base_url+"orden";
			else
				var url_carrito = $scope.base_url+"order";
			$("#formCarrito").attr("action",url_carrito)
			$("#formCarrito").attr("method","post");
			$("#formCarrito").submit()
		}
		//---
	});
