angular.module("ZougZougApp")
	.controller("orderController", function($scope,$http,$sce,$compile,$location,funcionaFactory,serviciosFactory,multIdioma,carritoFactory){
		//Cuerpo declaraciones
		$scope.productos = {}
		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#id_idioma").val();

		$(".menu_web").removeClass("active")
		$("#menu3").addClass("active")

		$(".primera").removeClass("side-nav")
		$("#menu3").addClass("active")

		$( "#target" ).click(function() {
		  alert( "Handler for  called." );
		});

		//-------------------------------------------------------
		$correo_paypal = "nlmdiaz@gmail.com"
		$scope.concepto = "Ropa de cocina"
		$scope.url_ipn = $scope.base_url+"WebOrden/ipn"
		//http://jose-aguilar.com/scripts/php/pago-con-paypal/ipn.php
		if($scope.idioma=="1"){
			$scope.titulo_1 = 'Detalle del Producto'
		}else{
			$scope.titulo_1 = 'Product Detail'
		}
		//-------------------------------------------------------
		$scope.definir_url_home = function(){
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
		}
		//-----------------------------------------------------------
		$scope.consultar_info_carrito = function(){
			carritoFactory.asignar_valores($scope.idioma,$scope.base_url,'')
			carritoFactory.cargar_producto_carrito(function(data){
				$scope.productos=data;
				//console.log($scope.productos)
				$scope.cuantos_productos = object_size($scope.productos)
				
				if($scope.cuantos_productos==0){
					if($scope.idioma==1)
						mensaje_alerta("#campo_mensaje_productos","Debe iniciar sesion e ir al carrito de compras","alert-warning");
					else
						mensaje_alerta("#campo_mensaje_productos","You must login and go to the shopping cart","alert-warning");

				}
				$scope.id_carrito=$scope.productos[0].id_carrito;
				$scope.productos[0].monto_global_total_oculto = number_format($scope.productos[0].monto_global_total,2)
				$scope.monto_global_total = $scope.productos[0].monto_global_total
				if($scope.idioma=="1"){
					$scope.url_carrito = $scope.base_url+"procesar_compra/"+$scope.id_carrito;
					$scope.url_shopping = $scope.base_url+"orden"
				}else{
					$scope.url_carrito = $scope.base_url+"checkout_step/"+$scope.id_carrito;
					$scope.url_shopping = $scope.base_url+"order"
				}
				//--
				$scope.configurarPaypal()
				//--
			});
		}

		$scope.eliminarProducto = function(event){
			var caja = event.currentTarget.id;
			var id = $("#"+caja).attr("data");
			//------------------------------------------
			$http.post($scope.base_url+"/WebProductos/eliminar_producto",
			{
				'id' : id,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					//--Mensaje
					if($scope.idioma==1)
						mensaje_alerta("#campo_mensaje_productos","Se ha retirado el producto de su carrito de compras","alert-info");
					else
						mensaje_alerta("#campo_mensaje_productos","The purchase was processed successfully","alert-info");
					//--
					$scope.consultar_info_carrito();
				}else{
					//---
					//Mensaje de no realizado
					if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje_productos","Ocurrío un error inesperado","alert-warning");
						else
							mensaje_alerta("#campo_mensaje_productos","An unexpected error occurred","alert-warning");
 
					//---
				}
			}).error(function(data,estatus){
				console.log(data);
			});
			//------------------------------------------
		}
		//--
		
		//--
		$scope.update_cart = function(){
			$scope.id_producto_carrito = []
			$scope.cantidad = []
			$scope.monto_total = []
			$(".tabla_producto").each(function(index,element){
        	    numero = $(element).attr("data")
        	    $scope.id_producto_carrito.push($("#eliminar_"+numero).attr("data"));
        	    $scope.cantidad.push($("#cantidad_"+numero).val());
        	    $monto_total = (parseFloat($("#monto_oculto_"+numero).html()))*(parseInt($("#cantidad_"+numero).val()))
        	    $scope.monto_total.push($monto_total);
        	});
        	//---
        	//console.log($scope.monto_total)
    	    $http.post($scope.base_url+"/WebProductos/modificar_cantidad_carrito_productos",
			{
				'id' : $scope.id_producto_carrito,
				'cantidad': $scope.cantidad,
				'monto_total': $scope.monto_total
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					//--Mensaje
					if($scope.idioma==1)
						mensaje_alerta("#campo_mensaje_productos","Se ha actualizado su carrito","alert-info");
					else
						mensaje_alerta("#campo_mensaje_productos","The purchase was processed successfully","alert-info");
					//--
					$scope.consultar_info_carrito();
				}else{
					//---
					//Mensaje de no realizado
					if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje_productos","Ocurrío un error inesperado","alert-warning");
						else
							mensaje_alerta("#campo_mensaje_productos","An unexpected error occurred","alert-warning");
 
					//---
				}
			}).error(function(data,estatus){
				console.log(data);
			});
    	    //---
		}
		
		
		//----
		//#Cuerpo de funciones js
		$scope.calcularMontoTotal = function(numero){
			cantidad = parseInt($("#cantidad_"+numero).val())
			monto_ind = parseFloat($("#monto_oculto_0").html());
			total = cantidad * monto_ind
			$("#monto_total_"+numero).html(number_format(total,2))
			$("#monto_total_oculto_"+numero).html(total)
			$scope.recorrerSumaTotal()
		}
		//----
		$scope.recorrerSumaTotal = function(){
			acum = 0;
			$(".tabla_producto").each(function(index,element){
        	    numero = $(element).attr("data")
        	    acum+=parseFloat($("#monto_total_oculto_"+numero).html())
        	    //alert(numero)
        	});
        	//---
        	$("#subtotalMonto").html(number_format(acum,2))
    	    $("#totalMonto").html(number_format(acum,2))
    	    $("#monto_total_oculto").html(acum)
        	//---
		}
		//----
		$scope.backpageCart = function(){
			var idioma = $scope.idioma
			if(idioma=="1")
				var url_carrito = $scope.base_url+"carrito";
			else
				var url_carrito = $scope.base_url+"cart";
			$("#formOrderProducto").attr("action",url_carrito)
			$("#formOrderProducto").attr("method","post");
			$("#formOrderProducto").submit()
		}
		//-----
		$scope.FrompageCheckout = function(){
			$("#realizarPago").submit();
			/*alert($scope.url_carrito)
			$("#formOrderProducto").attr("action",$scope.url_carrito)
			$("#formOrderProducto").attr("method","post");
			$("#formOrderProducto").submit()*/
		}
		//-----
		$scope.definir_url_home();
		$scope.consultar_info_carrito();
		$scope.recorrerSumaTotal()
		//----------------------------------------------------------------------
		$scope.configurarPaypal = function(){
			//------------------------------------
			paypal.Button.render({
				env: $scope.productos[0].PayPalENV,
				style: {
					label: 'paypal',
				},
				client: {
					//production: '<?php echo PayPalClientId; ?>'
					sandbox: $scope.productos[0].PayPalClientId
				},
				payment: function (data, actions) {
					return actions.payment.create({
					  transactions: [{
						amount: {
						  total: $scope.productos[0].productPrice,
						  currency: $scope.productos[0].currency
						}
					  }]
					});
				},
				onAuthorize: function (data, actions) {
				return actions.payment.execute()
				  .then(function () {
				  	$scope.super_url = $scope.url_carrito+"/"+data.paymentID+"/"+data.payerID+"/"+data.paymentToken+"/"+$scope.productos[0].productId
					//alert($scope.super_url)
					window.location = $scope.super_url;
				  });
				}
			}, '#paypal-button');	
			//----------------------------------------
		}

		//----------------------------------------------------------------------

	});
