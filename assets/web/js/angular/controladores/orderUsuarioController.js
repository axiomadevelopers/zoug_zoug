angular.module("ZougZougApp")
	.controller("orderUsuarioController", function($scope,$http,$sce,$compile,$location,funcionaFactory,serviciosFactory,multIdioma,ordenUsuarioFactory){
		//Cuerpo declaraciones
		$scope.productos = {}
		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#id_idioma").val();

		$(".menu_web").removeClass("active")
		$("#menu3").addClass("active")

		$(".primera").removeClass("side-nav")
		$("#menu3").addClass("active")

		$( "#target" ).click(function() {
		  alert( "Handler for  called." );
		});

		if($scope.idioma == 1){
			$scope.titulo_1 = 'Detalle del Producto'
		}else{
			$scope.titulo_1 = 'Product Detail'
		}

		$scope.definir_url_home = function(){
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
		}
		//-----------------------------------------------------------
		$scope.consultar_info_carrito = function(){
			ordenUsuarioFactory.asignar_valores($scope.idioma,$scope.base_url,'')
			ordenUsuarioFactory.cargar_producto_orden(function(data){
				
				///Datos del comprador
				iniciarSesion = $(".iniciar-sesion").html();
				$scope.id_usuario = $("#idUsuarioorden").html();
				vectSesion = iniciarSesion.split("|");
				$scope.usuarioOrden = vectSesion[0]
				$scope.emailOrden = vectSesion[1]
				$("#email-us").html($(".span_correo").text())
				if($scope.id_usuario==""){
					if($scope.idioma==1)
						mensaje_alerta("#campo_mensaje_productos","No hay ordenes de compra","alert-warning");
					else
						mensaje_alerta("#campo_mensaje_productos","Don't have orders","alert-warning");
				}else{
					$scope.ordenes=data;
					//console.log($scope.ordenes)
					$scope.id_carrito=$scope.ordenes.id_carrito;
				}
			});
		}

		$scope.eliminarProducto = function(event){
			var caja = event.currentTarget.id;
			var id = $("#"+caja).attr("data");
			//------------------------------------------
			$http.post($scope.base_url+"/WebProductos/eliminar_producto",
			{
				'id' : id,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					//--Mensaje
					if($scope.idioma==1)
						mensaje_alerta("#campo_mensaje_productos","Se ha retirado el producto de su carrito de compras","alert-info");
					else
						mensaje_alerta("#campo_mensaje_productos","The purchase was processed successfully","alert-info");
					//--
					$scope.consultar_info_carrito();
				}else{
					//---
					//Mensaje de no realizado
					if($scope.idioma==1)
							mensaje_alerta("#campo_mensaje_productos","Ocurrío un error inesperado","alert-warning");
						else
							mensaje_alerta("#campo_mensaje_productos","An unexpected error occurred","alert-warning");
 
					//---
				}
			}).error(function(data,estatus){
				console.log(data);
			});
			//------------------------------------------
		}
		//--
		
		//----
		//#Cuerpo de funciones js
		$scope.calcularMontoTotal = function(numero){
			cantidad = parseInt($("#cantidad_"+numero).val())
			monto_ind = parseFloat($("#monto_oculto_0").html());
			total = cantidad * monto_ind
			$("#monto_total_"+numero).html(number_format(total,2))
			$("#monto_total_oculto_"+numero).html(total)
			$scope.recorrerSumaTotal()
		}
		//----
		$scope.recorrerSumaTotal = function(){
			acum = 0;
			$(".tabla_producto").each(function(index,element){
        	    numero = $(element).attr("data")
        	    acum+=parseFloat($("#monto_total_oculto_"+numero).html())
        	    //alert(numero)
        	});
        	//---
        	$("#subtotalMonto").html(number_format(acum,2))
    	    $("#totalMonto").html(number_format(acum,2))
    	    $("#monto_total_oculto").html(acum)
        	//---
		}
		//-----
		$scope.definir_url_home();
		$scope.consultar_info_carrito();
		$scope.recorrerSumaTotal()
		//----
	});
