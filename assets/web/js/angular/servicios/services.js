angular.module("ZougZougApp")
//---------------------------------------------------------------------------------------
//--Bloque de servicios

//--Bloque de factorias

.factory("paginacionFactory",['$http', function($http){
	//--
	return{

	}
	//--
}])
.factory("nosotrosFactory",['$http',function($http){
	return{
	//-------------------
		asignar_valores : function (valor_idioma,url){
			if(valor_idioma!="")
				id_idioma = valor_idioma;
			else
				id_idioma = "";
			//---
			if(url!="")
				base_url = url;
			else
				base_url = "";
			//---
		},
		cargar_nosotros: function(callback){
			$http.post(base_url+"WebInicio/consultarNosotros", { id_idioma:id_idioma}).success(callback);
		}
	//-------------------
	}

}])

.factory("noticiaFactory",['$http',function($http){
	return{
	//-------------------
		asignar_valores : function (valor_idioma,url,id,limiter,starter){
			if(valor_idioma!="")
				id_idioma = valor_idioma;
			else
				id_idioma = "";
			//---

			if(url!="")
				base_url = url;
			else
				base_url = "";

			if(id!="")
				id_noticias = id;
			else
				id_noticias = id;

			if(limiter!="")
				limit = limiter;
			else
				limit = "";

			if(starter!="")
				start = starter;
			else
				start = "";
			//---
		},
		cargar_noticia: function(callback){
			$http.post(base_url+"WebNoticias/consultarNoticias", { id_idioma:id_idioma}).success(callback);
		},
		cargar_sub_noticias : function(callback){
			$http.post(base_url+"WebNoticias/consultarSubNoticias", { id_idioma:id_idioma,id_noticias:id_noticias,limit:limit,start:start}).success(callback);
		},
		cargar_noticia_slug: function(callback){
			$http.post(base_url+"WebNoticias/consultarNoticiasSlug", { id_idioma:id_idioma,id_noticias:id_noticias}).success(callback);
		}
	//-------------------
	}
}])

.factory("noticiaHomeFactory",['$http',function($http){
	return{
	//-------------------
		asignar_valores : function (valor_idioma,url){
			if(valor_idioma!="")
				id_idioma = valor_idioma;
			else
				id_idioma = "";
			//---
			if(url!="")
				base_url = url;
			else
				base_url = "";
			//---
		},
		cargar_noticias_filtro: function(callback){
			$http.post(base_url+"WebInicio/consultarNoticiasFiltros", { id_idioma:id_idioma}).success(callback);
		}
	//-------------------
	}

}])

.factory("productosHomeFactory",['$http',function($http){
	return{
	//-------------------
		asignar_valores : function (valor_idioma,url){
			if(valor_idioma!="")
				id_idioma = valor_idioma;
			else
				id_idioma = "";
			//---
			if(url!="")
				base_url = url;
			else
				base_url = "";
			//---
		},
		cargar_productos: function(callback){
			$http.post(base_url+"WebInicio/consultarproductos", { id_idioma:id_idioma}).success(callback);
		}

	//-------------------
	}
}])

.factory("productoFactory",['$http',function($http){
	return{
	//-------------------
		asignar_valores : function (valor_idioma,url,id,limiter,starter){
			if(valor_idioma!="")
				id_idioma = valor_idioma;
			else
				id_idioma = "";

			if(url!="")
				base_url = url;
			else
				base_url = "";

			if(id!="")
				id_producto = id;
			else
				id_producto = id;

			if(limiter!="")
				limit = limiter;
			else
				limit = "";

			if(starter!="")
				start = starter;
			else
				start = "";
		},

		cargar_producto_slug: function(callback){
			$http.post(base_url+"WebProductos/consultarProducto", { id_idioma:id_idioma,id_producto:id_producto}).success(callback);
		},
		cargar_sub_producto : function(callback){
			$http.post(base_url+"WebProductos/consultarSubProductos", { id_idioma:id_idioma,id_producto:id_producto,limit:limit,start:start}).success(callback);
		},
		cargar_mas_producto : function(callback){
			$http.post(base_url+"WebProductos/consultarMasProductos", { id_idioma:id_idioma,id_producto:id_producto,limit:limit,start:start}).success(callback);
		},
	//-------------------
	}
}])
.factory("ordenUsuarioFactory",['$http',function($http){
	return{
	//-------------------
		asignar_valores : function (valor_idioma,url,id){
			if(valor_idioma!="")
				id_idioma = valor_idioma;
			else
				id_idioma = "";

			if(url!="")
				base_url = url;
			else
				base_url = "";

			if(id!="")
				id_usuario = id;
			else
				id_usuario = id;
		},
		cargar_producto_orden: function(callback){
			$http.post(base_url+"WebOrdenUsuario/consultarProductosOrden", { id_idioma:id_idioma,id_usuario:id_usuario}).success(callback);
		},
	//-------------------
	}
}])
.factory("carritoFactory",['$http',function($http){
	return{
	//-------------------
		asignar_valores : function (valor_idioma,url,id){
			if(valor_idioma!="")
				id_idioma = valor_idioma;
			else
				id_idioma = "";

			if(url!="")
				base_url = url;
			else
				base_url = "";

			if(id!="")
				id_usuario = id;
			else
				id_usuario = id;
		},
		cargar_producto_carrito: function(callback){
			$http.post(base_url+"WebCart/consultarCarrito", { id_idioma:id_idioma,id_usuario:id_usuario}).success(callback);
		},
	//-------------------
	}
}])

.factory("productos_todos_HomeFactory",['$http',function($http){
	return{
	//-------------------
		asignar_valores : function (valor_idioma,url){
			if(valor_idioma!="")
				id_idioma = valor_idioma;
			else
				id_idioma = "";
			//---
			if(url!="")
				base_url = url;
			else
				base_url = "";
			//---
		},
		cargar_productos: function(callback){
			$http.post(base_url+"WebProductos/consultarproductos_todos", { id_idioma:id_idioma}).success(callback);
		}
	//-------------------
	}
}])
.factory("mas_productosFactory",['$http',function($http){
	return{
	//-------------------
		asignar_valores : function (valor_idioma,url,id,id_imagen,limiter,starter){
			if(valor_idioma!="")
				id_idioma = valor_idioma;
			else
				id_idioma = "";
			//---

			if(url!="")
				base_url = url;
			else
				base_url = "";

			if(id!="")
				id_producto = id;
			else
				id_producto = id;

			if(id_imagen!="")
				id_imag = id_imagen;
			else
				id_imag = id_imagen;

			if(limiter!="")
				limit = limiter;
			else
				limit = "";

			if(starter!="")
				start = starter;
			else
				start = "";
			//---
		},
		cargar_mas_prod : function(callback){
			$http.post(base_url+"WebProductos/consultarmas_productos", { id_idioma:id_idioma,id_producto:id_producto,id_imag:id_imag,limit:limit,start:start}).success(callback);
		},
	//-------------------
	}

}])

.factory("lineaFactory",['$http', function($http){
	return{
			asignar_valores : function (url,idioma){
				//console.log(base_url);
				if(url!="")
					base_url = url;
				else
					base_url = "";
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
			},
			cargar_linea : function(callback){
				$http.post(base_url+"WebProductos/consultar_linea", {id_idioma:id_idioma,}).success(callback);
			}
	}
}])

.factory("generoFactory",['$http', function($http){
	return{
			asignar_valores : function (url,idioma){
				//console.log(base_url);
				if(url!="")
					base_url = url;
				else
					base_url = "";
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
			},
			cargar_generos : function(callback){
				$http.post(base_url+"WebProductos/consultar_generos", {id_idioma:id_idioma,}).success(callback);
			}
	}
}])

.factory("colorFactory",['$http', function($http){
	return{
			asignar_valores : function (url,idioma){
				//console.log(base_url);
				if(url!="")
					base_url = url;
				else
					base_url = "";
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
			},
			cargar_color : function(callback){
				$http.post(base_url+"WebProductos/consultar_color", {id_idioma:id_idioma,}).success(callback);
			}
	}
}])

.factory("tallaFactory",['$http', function($http){
	return{
			asignar_valores : function (url,idioma){
				//console.log(base_url);
				if(url!="")
					base_url = url;
				else
					base_url = "";
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
			},
			cargar_talla : function(callback){
				$http.post(base_url+"WebProductos/consultar_talla", {id_idioma:id_idioma,}).success(callback);
			}
	}
}])

.factory("cargar_titulo_producto",['$http', function($http){
	return{
			asignar_valores : function (url,idioma,titulo){
				//console.log(base_url);
				if(url!="")
					base_url = url;
				else
					base_url = "";
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(titulo!="")
					titulo_producto = titulo;
				else
					titulo_producto = "";
			},
			cargar_filtro_titulo : function(callback){
				$http.post(base_url+"WebProductos/consultar_producto_titulo", {id_idioma:id_idioma,titulo_producto:titulo_producto}).success(callback);
			}
	}
}])

.factory("cargar_img_linea",['$http', function($http){
	return{
			asignar_valores : function (url,idioma,linea){
				//console.log(base_url);
				if(url!="")
					base_url = url;
				else
					base_url = "";
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(linea!="")
					id_linea = linea;
				else
					id_linea = "";
			},
			cargar_img_linea : function(callback){
				$http.post(base_url+"WebProductos/consultar_img_linea", {id_idioma:id_idioma,id_linea:id_linea}).success(callback);
			}
	}
}])

.factory("cargar_img_genero",['$http', function($http){
	return{
			asignar_valores : function (url,idioma,genero){
				//console.log(base_url);
				if(url!="")
					base_url = url;
				else
					base_url = "";
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(genero!="")
					id_genero = genero;
				else
					id_genero = "";
			},
			cargar_img_genero : function(callback){
				$http.post(base_url+"WebProductos/consultar_img_genero", {id_idioma:id_idioma,id_genero:id_genero}).success(callback);
			}
	}
}])

.factory("cargar_img_color",['$http', function($http){
	return{
			asignar_valores : function (url,idioma,color){
				//console.log(base_url);
				if(url!="")
					base_url = url;
				else
					base_url = "";
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(color!="")
					id_color = color;
				else
					id_color = "";
			},
			cargar_img_color : function(callback){
				$http.post(base_url+"WebProductos/consultar_img_color", {id_idioma:id_idioma,id_color:id_color}).success(callback);
			}
	}
}])

.factory("cargar_img_talla",['$http', function($http){
	return{
			asignar_valores : function (url,idioma,talla){
				//console.log(base_url);
				if(url!="")
					base_url = url;
				else
					base_url = "";
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(talla!="")
					id_talla = talla;
				else
					id_talla = "";
			},
			cargar_img_talla : function(callback){
				$http.post(base_url+"WebProductos/consultar_img_talla", {id_idioma:id_idioma,id_talla:id_talla}).success(callback);
			}
	}
}])


.factory("cargar_img_precio",['$http', function($http){
	return{
			asignar_valores : function (url,idioma,precio_uno,precio_dos){
				//console.log(base_url);
				if(url!="")
					base_url = url;
				else
					base_url = "";
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(precio_uno!="")
					id_precio_uno = precio_uno;
				else
					id_precio_uno = "";

					if(precio_dos!="")
						id_precio_dos = precio_dos;
					else
						id_precio_dos = "";
			},
			cargar_img_precio : function(callback){
				$http.post(base_url+"WebProductos/consultar_img_precio", {id_idioma:id_idioma,id_precio_uno:id_precio_uno,id_precio_dos:id_precio_dos}).success(callback);
			}
	}
}])
//XLINEA Y GENERO
.factory("cargar",['$http', function($http){
	return{
			asignar_valores : function (idioma,id_linea, id_genero){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id_linea!="")
					id_buscar_linea = id_linea;
				else
					id_buscar_linea = "";
				if(id_genero!="")
					id_buscar_genero = id_genero;
				else
					id_buscar_genero = "";
			},
			valores : function(callback){
				$http.post(base_url+"WebProductos/consultar", {id_idioma:id_idioma,id_buscar_linea:id_buscar_linea, id_buscar_genero:id_buscar_genero}).success(callback);
			}
	}
}])
//x linea y precio
.factory("cargarfil2",['$http', function($http){
	return{
			asignar_valores : function (idioma,id_linea,precio1, precio2){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id_linea!="")
					id_buscar_linea = id_linea;
				else
					id_buscar_linea = "";
				if(precio1!="")
					id_buscar_precio1 = precio1;
				else
					id_buscar_precio1 = "";
				if(precio2!="")
					id_buscar_precio2 = precio2;
				else
					id_buscar_precio2 = "";
			},
			valores : function(callback){
				$http.post(base_url+"WebProductos/consultarfiltro2", {id_idioma:id_idioma,id_buscar_linea:id_buscar_linea,id_buscar_precio1:id_buscar_precio1,id_buscar_precio2:id_buscar_precio2}).success(callback);
			}
	}
}])
//XLINEA Y COLOR
.factory("cargarfil4",['$http', function($http){
	return{
			asignar_valores : function (idioma,id_linea, id_color){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id_linea!="")
					id_buscar_linea = id_linea;
				else
					id_buscar_linea = "";
				if(id_color!="")
					id_buscar_color = id_color;
				else
					id_buscar_color = "";
			},
			valores : function(callback){
				$http.post(base_url+"WebProductos/consultarfiltro4", {id_idioma:id_idioma,id_buscar_linea:id_buscar_linea, id_buscar_color:id_buscar_color}).success(callback);
			}
	}
}])
//XLINEA Y talla
.factory("cargarfil5",['$http', function($http){
	return{
			asignar_valores : function (idioma,id_linea, id_talla){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id_linea!="")
					id_buscar_linea = id_linea;
				else
					id_buscar_linea = "";
				if(id_talla!="")
					id_buscar_talla = id_talla;
				else
					id_buscar_talla = "";
			},
			valores : function(callback){
				$http.post(base_url+"WebProductos/consultarfiltro5", {id_idioma:id_idioma,id_buscar_linea:id_buscar_linea, id_buscar_talla:id_buscar_talla}).success(callback);
			}
	}
}])
//x GENERO y PRECIO
.factory("cargarfil3",['$http', function($http){
	return{
			asignar_valores : function (idioma,id_genero,precio1, precio2){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id_genero!="")
					id_buscar_genero = id_genero;
				else
					id_buscar_genero = "";
				if(precio1!="")
					id_buscar_precio1 = precio1;
				else
					id_buscar_precio1 = "";
				if(precio2!="")
					id_buscar_precio2 = precio2;
				else
					id_buscar_precio2 = "";
			},
			valores : function(callback){
				$http.post(base_url+"WebProductos/consultarfiltro3", {id_idioma:id_idioma,id_buscar_genero:id_buscar_genero,id_buscar_precio1:id_buscar_precio1,id_buscar_precio2:id_buscar_precio2}).success(callback);
			}
	}
}])

//GENERO Y COLOR
.factory("cargarfil6",['$http', function($http){
	return{
			asignar_valores : function (idioma,id_generp, id_color){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id_genero!="")
					id_buscar_genero = id_genero;
				else
					id_buscar_genero = "";
				if(id_color!="")
					id_buscar_color = id_color;
				else
					id_buscar_color = "";
			},
			valores : function(callback){
				$http.post(base_url+"WebProductos/consultarfiltro6", {id_idioma:id_idioma,id_buscar_genero:id_buscar_genero, id_buscar_color:id_buscar_color}).success(callback);
			}
	}
}])
//GENERO Y TALLA
.factory("cargarfil7",['$http', function($http){
	return{
			asignar_valores : function (idioma,id_genero, id_talla){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id_genero!="")
					id_buscar_genero = id_genero;
				else
					id_buscar_genero = "";
				if(id_talla!="")
					id_buscar_talla = id_talla;
				else
					id_buscar_talla = "";
			},
			valores : function(callback){
				$http.post(base_url+"WebProductos/consultarfiltro7", {id_idioma:id_idioma,id_buscar_genero:id_buscar_genero, id_buscar_talla:id_buscar_talla}).success(callback);
			}
	}
}])
//x COLOR y PRECIO
.factory("cargarfil8",['$http', function($http){
	return{
			asignar_valores : function (idioma,id_color,precio1, precio2){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id_color!="")
					id_buscar_color = id_color;
				else
					id_buscar_color = "";
				if(precio1!="")
					id_buscar_precio1 = precio1;
				else
					id_buscar_precio1 = "";
				if(precio2!="")
					id_buscar_precio2 = precio2;
				else
					id_buscar_precio2 = "";
			},
			valores : function(callback){
				$http.post(base_url+"WebProductos/consultarfiltro8", {id_idioma:id_idioma,id_buscar_color:id_buscar_color,id_buscar_precio1:id_buscar_precio1,id_buscar_precio2:id_buscar_precio2}).success(callback);
			}
	}
}])
//x TALLA y PRECIO
.factory("cargarfil9",['$http', function($http){
	return{
			asignar_valores : function (idioma,id_talla,precio1, precio2){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id_talla!="")
					id_buscar_talla = id_talla;
				else
					id_buscar_talla = "";
				if(precio1!="")
					id_buscar_precio1 = precio1;
				else
					id_buscar_precio1 = "";
				if(precio2!="")
					id_buscar_precio2 = precio2;
				else
					id_buscar_precio2 = "";
			},
			valores : function(callback){
				$http.post(base_url+"WebProductos/consultarfiltro9", {id_idioma:id_idioma,id_buscar_talla:id_buscar_talla,id_buscar_precio1:id_buscar_precio1,id_buscar_precio2:id_buscar_precio2}).success(callback);
			}
	}
}])
//COLOR Y TALLA
.factory("cargarfil10",['$http', function($http){
	return{
			asignar_valores : function (idioma,id_color, id_talla){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id_color!="")
					id_buscar_color = id_color;
				else
					id_buscar_color = "";
				if(id_color!="")
					id_buscar_talla = id_talla;
				else
					id_buscar_talla = "";
			},
			valores : function(callback){
				$http.post(base_url+"WebProductos/consultarfiltro10", {id_idioma:id_idioma,id_buscar_color:id_buscar_color, id_buscar_talla:id_buscar_talla}).success(callback);
			}
	}
}])
//LINEA, GENERO Y PRECIO
.factory("cargar_val2",['$http', function($http){
	return{
			asignar_valores : function (idioma,id_linea,id_genero, precio1, precio2){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id_linea!="")
					id_buscar_linea = id_linea;
				else
					id_buscar_linea = "";
				if(id_genero!="")
					id_buscar_genero = id_genero;
				else
					id_buscar_genero = "";
				if(precio1!="")
					id_buscar_precio1 = precio1;
				else
					id_buscar_precio1 = "";
				if(precio2!="")
					id_buscar_precio2 = precio2;
				else
					id_buscar_precio2 = "";
			},
			valores : function(callback){
				$http.post(base_url+"WebProductos/consultar_val2", {id_idioma:id_idioma,id_buscar_linea:id_buscar_linea,id_buscar_genero:id_buscar_genero, id_buscar_precio1:id_buscar_precio1,id_buscar_precio2:id_buscar_precio2}).success(callback);
			}
	}
}])
//LINEA, GENERO Y COLOR
.factory("cargar_val3",['$http', function($http){
	return{
			asignar_valores : function (idioma,id_linea,id_genero,id_color){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id_linea!="")
					id_buscar_linea = id_linea;
				else
					id_buscar_linea = "";
				if(id_genero!="")
					id_buscar_genero = id_genero;
				else
					id_buscar_genero = "";
				if(id_color!="")
					id_buscar_color = id_color;
				else
					id_buscar_color = "";
			},
			valores : function(callback){
				$http.post(base_url+"WebProductos/consultar_val3", {id_idioma:id_idioma,id_buscar_linea:id_buscar_linea,id_buscar_genero:id_buscar_genero, id_buscar_color:id_buscar_color,}).success(callback);
			}
	}
}])
//LINEA, GENERO Y TALLA
.factory("cargar_val4",['$http', function($http){
	return{
			asignar_valores : function (idioma,id_linea,id_genero,id_talla){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id_linea!="")
					id_buscar_linea = id_linea;
				else
					id_buscar_linea = "";
				if(id_genero!="")
					id_buscar_genero = id_genero;
				else
					id_buscar_genero = "";
				if(id_talla!="")
					id_buscar_talla = id_talla;
				else
					id_buscar_talla = "";
			},
			valores : function(callback){
				$http.post(base_url+"WebProductos/consultar_val4", {id_idioma:id_idioma,id_buscar_linea:id_buscar_linea,id_buscar_genero:id_buscar_genero, id_buscar_talla:id_buscar_talla,}).success(callback);
			}
	}
}])
//LINEA, PRECIO Y COLOR
.factory("cargar_val5",['$http', function($http){
	return{
			asignar_valores : function (idioma,id_linea,id_color, precio1, precio2){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id_linea!="")
					id_buscar_linea = id_linea;
				else
					id_buscar_linea = "";
				if(id_color!="")
					id_buscar_color = id_color;
				else
					id_buscar_color = "";
				if(precio1!="")
					id_buscar_precio1 = precio1;
				else
					id_buscar_precio1 = "";
				if(precio2!="")
					id_buscar_precio2 = precio2;
				else
					id_buscar_precio2 = "";
			},
			valores : function(callback){
				$http.post(base_url+"WebProductos/consultar_val5", {id_idioma:id_idioma,id_buscar_linea:id_buscar_linea,id_buscar_color:id_buscar_color, id_buscar_precio1:id_buscar_precio1,id_buscar_precio2:id_buscar_precio2}).success(callback);
			}
	}
}])
//LINEA, PRECIO Y TALLA
.factory("cargar_val6",['$http', function($http){
	return{
			asignar_valores : function (idioma,id_linea,id_talla, precio1, precio2){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id_linea!="")
					id_buscar_linea = id_linea;
				else
					id_buscar_linea = "";
				if(id_talla!="")
					id_buscar_talla = id_talla;
				else
					id_buscar_talla = "";
				if(precio1!="")
					id_buscar_precio1 = precio1;
				else
					id_buscar_precio1 = "";
				if(precio2!="")
					id_buscar_precio2 = precio2;
				else
					id_buscar_precio2 = "";
			},
			valores : function(callback){
				$http.post(base_url+"WebProductos/consultar_val6", {id_idioma:id_idioma,id_buscar_linea:id_buscar_linea,id_buscar_talla:id_buscar_talla, id_buscar_precio1:id_buscar_precio1,id_buscar_precio2:id_buscar_precio2}).success(callback);
			}
	}
}])
//LINEA, COLOR Y TALLA
.factory("cargar_val7",['$http', function($http){
	return{
			asignar_valores : function (idioma,id_linea,id_color,id_talla){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id_linea!="")
					id_buscar_linea = id_linea;
				else
					id_buscar_linea = "";
				if(id_color!="")
					id_buscar_color = id_color;
				else
					id_buscar_color = "";
				if(id_talla!="")
					id_buscar_talla = id_talla;
				else
					id_buscar_talla = "";
			},
			valores : function(callback){
				$http.post(base_url+"WebProductos/consultar_val7", {id_idioma:id_idioma,id_buscar_linea:id_buscar_linea,id_buscar_color:id_buscar_color,id_buscar_talla:id_buscar_talla}).success(callback);
			}
	}
}])
//GENERO, PRECIO Y COLOR
.factory("cargar_val8",['$http', function($http){
	return{
			asignar_valores : function (idioma,id_genero,precio1,precio2,id_color){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id_genero!="")
					id_buscar_genero = id_genero;
				else
					id_buscar_genero = "";
				if(precio1!="")
					id_buscar_precio1 = precio1;
				else
					id_buscar_precio1 = "";
				if(precio2!="")
					id_buscar_precio2 = precio2;
				else
					id_buscar_precio2 = "";
				if(id_color!="")
					id_buscar_color = id_color;
				else
					id_buscar_color = "";
			},
			valores : function(callback){
				$http.post(base_url+"WebProductos/consultar_val8", {id_idioma:id_idioma,id_buscar_genero:id_buscar_genero,id_buscar_precio1:id_buscar_precio1,id_buscar_precio2:id_buscar_precio2,id_buscar_color:id_buscar_color}).success(callback);
			}
	}
}])
//GENERO, PRECIO Y TALLA
.factory("cargar_val9",['$http', function($http){
	return{
			asignar_valores : function (idioma,id_genero,precio1,precio2,id_talla){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id_genero!="")
					id_buscar_genero = id_genero;
				else
					id_buscar_genero = "";
				if(precio1!="")
					id_buscar_precio1 = precio1;
				else
					id_buscar_precio1 = "";
				if(precio2!="")
					id_buscar_precio2 = precio2;
				else
					id_buscar_precio2 = "";
				if(id_talla!="")
					id_buscar_talla = id_talla;
				else
					id_buscar_talla = "";
			},
			valores : function(callback){
				$http.post(base_url+"WebProductos/consultar_val9", {id_idioma:id_idioma,id_buscar_genero:id_buscar_genero,id_buscar_precio1:id_buscar_precio1,id_buscar_precio2:id_buscar_precio2,id_buscar_talla:id_buscar_talla}).success(callback);
			}
	}
}])
//PRECIO, COLOR y TALLA
.factory("cargar_val10",['$http', function($http){
	return{
			asignar_valores : function (idioma,precio1,precio2,id_color,id_talla){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(precio1!="")
					id_buscar_precio1 = precio1;
				else
					id_buscar_precio1 = "";
				if(precio2!="")
					id_buscar_precio2 = precio2;
				else
					id_buscar_precio2 = "";
				if(id_color!="")
					id_buscar_color = id_color;
				else
					id_buscar_color = "";
				if(id_talla!="")
					id_buscar_talla = id_talla;
				else
					id_buscar_talla = "";
			},
			valores : function(callback){
				$http.post(base_url+"WebProductos/consultar_val10", {id_idioma:id_idioma,id_buscar_precio1:id_buscar_precio1,id_buscar_precio2:id_buscar_precio2,id_buscar_color:id_buscar_color,id_buscar_talla:id_buscar_talla}).success(callback);
			}
	}
}])
//LINEA, GENERO y PRECIO Y COLOR
.factory("cargar_val111",['$http', function($http){
	return{
			asignar_valores : function (idioma,id_linea,id_genero,precio1,precio2,id_color){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id_linea!="")
					id_buscar_linea = id_linea;
				else
					id_buscar_linea = "";
				if(id_genero!="")
					id_buscar_genero = id_genero;
				else
					id_buscar_genero = "";
				if(precio1!="")
					id_buscar_precio1 = precio1;
				else
					id_buscar_precio1 = "";
				if(precio2!="")
					id_buscar_precio2 = precio2;
				else
					id_buscar_precio2 = "";
				if(id_color!="")
					id_buscar_color = id_color;
				else
					id_buscar_color = "";
			},
			valores : function(callback){
				$http.post(base_url+"WebProductos/consultar_val11", {id_idioma:id_idioma,id_buscar_linea:id_buscar_linea,id_buscar_genero:id_buscar_genero,id_buscar_precio1:id_buscar_precio1,id_buscar_precio2:id_buscar_precio2,id_buscar_color:id_buscar_color}).success(callback);
			}
	}
}])
	//LINEA, GENERO y PRECIO Y TALLA
.factory("cargar_val112",['$http', function($http){
	return{
			asignar_valores : function (idioma,id_linea,id_genero,precio1,precio2,id_talla){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id_linea!="")
					id_buscar_linea = id_linea;
				else
					id_buscar_linea = "";
				if(id_genero!="")
					id_buscar_genero = id_genero;
				else
					id_buscar_genero = "";
				if(precio1!="")
					id_buscar_precio1 = precio1;
				else
					id_buscar_precio1 = "";
				if(precio2!="")
					id_buscar_precio2 = precio2;
				else
					id_buscar_precio2 = "";
				if(id_talla!="")
					id_buscar_color = id_talla;
				else
					id_buscar_talla = "";
			},
			valores : function(callback){
				$http.post(base_url+"WebProductos/consultar_val12", {id_idioma:id_idioma,id_buscar_linea:id_buscar_linea,id_buscar_genero:id_buscar_genero,id_buscar_precio1:id_buscar_precio1,id_buscar_precio2:id_buscar_precio2,id_buscar_talla:id_buscar_talla}).success(callback);
			}
	}
}])
//LINEA, PRECIO, TALLA Y COLOR
.factory("cargar_val113",['$http', function($http){
return{
		asignar_valores : function (idioma,id_linea,precio1,precio2,id_talla, id_color){
			if(idioma!="")
				id_idioma = idioma;
			else
				id_idioma = "";
			if(id_linea!="")
				id_buscar_linea = id_linea;
			else
				id_buscar_linea = "";
			if(precio1!="")
				id_buscar_precio1 = precio1;
			else
				id_buscar_precio1 = "";
			if(precio2!="")
				id_buscar_precio2 = precio2;
			else
				id_buscar_precio2 = "";
			if(id_talla!="")
				id_buscar_color = id_talla;
			else
				id_buscar_talla = "";
			if(id_color!="")
				id_buscar_color = id_color;
			else
				id_buscar_color = "";
		},
		valores : function(callback){
			$http.post(base_url+"WebProductos/consultar_val13", {id_idioma:id_idioma,id_buscar_linea:id_buscar_linea,id_buscar_precio1:id_buscar_precio1,id_buscar_precio2:id_buscar_precio2,id_buscar_talla:id_buscar_talla,id_buscar_color:id_buscar_color}).success(callback);
		}
}
}])
//PRECIO, TALLA, COLOR y GENERO
.factory("cargar_val114",['$http', function($http){
return{
		asignar_valores : function (idioma,precio1,precio2,id_talla,id_color,id_genero){
			if(idioma!="")
				id_idioma = idioma;
			else
				id_idioma = "";
			if(precio1!="")
				id_buscar_precio1 = precio1;
			else
				id_buscar_precio1 = "";
			if(precio2!="")
				id_buscar_precio2 = precio2;
			else
				id_buscar_precio2 = "";
			if(id_talla!="")
				id_buscar_talla = id_talla;
			else
				id_buscar_talla = "";
			if(id_color!="")
				id_buscar_color = id_color;
			else
				id_buscar_color = "";
			if(id_genero!="")
				id_buscar_genero= id_genero;
			else
				id_buscar_genero = "";
		},
		valores : function(callback){
			$http.post(base_url+"WebProductos/consultar_val14", {id_idioma:id_idioma,id_buscar_precio1:id_buscar_precio1,id_buscar_precio2:id_buscar_precio2,id_buscar_talla:id_buscar_talla,id_buscar_color:id_buscar_color,id_buscar_genero:id_buscar_genero}).success(callback);
		}
}
}])
//LINEA,GENERO, PRECIO, COLOR Y TALLA
.factory("cargar_val115",['$http', function($http){
return{
		asignar_valores : function (idioma,id_linea, id_genero, precio1,precio2,id_color,id_talla){
			if(idioma!="")
				id_idioma = idioma;
			else
				id_idioma = "";

			if(id_linea!="")
				id_buscar_linea = id_linea;
			else
				id_buscar_linea = "";

			if(id_genero!="")
				id_buscar_genero = id_genero;
			else
				id_buscar_genero = "";

			if(precio1!="")
				id_buscar_precio1 = precio1;
			else
				id_buscar_precio1 = "";

			if(precio2!="")
				id_buscar_precio2 = precio2;
			else
				id_buscar_precio2 = "";

			if(id_color!="")
				id_buscar_color = id_color;
			else
				id_buscar_color = "";

			if(id_talla!="")
				id_buscar_talla= id_talla;
			else
				id_buscar_talla = "";
		},
		valores : function(callback){
			$http.post(base_url+"WebProductos/consultar_val15", {id_idioma:id_idioma,id_buscar_linea:id_buscar_linea,id_buscar_genero:id_buscar_genero,id_buscar_precio1:id_buscar_precio1,id_buscar_precio2:id_buscar_precio2,id_buscar_color:id_buscar_color,id_buscar_talla:id_buscar_talla}).success(callback);
		}
}
}])
.factory("funcionaFactory",['$http',function($http){
	return{
	//-------------------
		asignar_valores : function (valor_idioma){
			if(valor_idioma!="")
				idioma = valor_idioma;
			else
				idioma = "";
		},
		cargar_funciona: function(callback){
			$http.post("./controladores/nosotrosController.php", {accion:'consultar_funciona','idioma':idioma}).success(callback);
		}
	//-------------------
	}

}])
.factory("clientesFactory",['$http',function($http){
	return{
	//-------------------
		asignar_valores : function (valor_idioma){
			if(valor_idioma!="")
				id_idioma = valor_idioma;
			else
				id_idioma = "";
		},
		cargar_clientes: function(callback){
			$http.post("./controladores/nosotrosController.php", {accion:'consultar_clientes','id_idioma':id_idioma}).success(callback);
		}
	//-------------------
	}

}])
//
.factory("serviciosFactory",['$http', function($http){
	return{
		asignar_valores : function (id_idioma){
			if(id_idioma!="")
				idioma = id_idioma;
			else
				idioma = "";
		},

		cargar_servicios : function(callback){
			$http.post("./controladores/servicioController.php", {accion:'consultar_servicios',idioma:idioma}).success(callback);
		}
	}
}])
//
.factory("noticiasFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,limit_0,offset_0,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				//---
				if(id!="")
					id_noticias = id;
				else
					id_noticias = "";
				//---
				if(limit_0!="")
					limit = limit_0;
				else
					limit = "";
				//---
				if(offset_0!="")
					offset = offset_0;
				else
					offset = "";
				//---
				if(url!="")
					base_url = url;
				else
					base_url = "";
				//---
			},
			cargar_noticias_filtro : function(callback){
				//alert(base_url+"WebNoticias/consultarNoticiasFiltro");
				$http.post(base_url+"WebNoticias/consultarNoticiasFiltro", { id_idioma:id_idioma,id_noticias:id_noticias,limit:limit,offset:offset}).success(callback);
			}
	}
}])
.factory("sliderFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				//---
				if(url!="")
					base_url = url;
				else
					base_url = "";
				//---
			},
			cargar_slider : function(callback){
				//alert(base_url+"WebNoticias/consultarNoticiasFiltro");
				$http.post(base_url+"WebInicio/consultarSlider", { id_idioma:id_idioma}).success(callback);
			}
	}
}])
.factory("portafoliosFactory",['$http', function($http){
	return{
		asignar_idioma : function (idioma){
			if(idioma!="")
				id_idioma = idioma;
			else
				id_idioma = "";
		},
		asignar_valores_detalle : function(idioma,slug){
			if(idioma!="")
				id_idioma = idioma;
			else
				id_idioma = "";
			//--
			if(slug!="")
				super_slug = slug;
			else
				super_slug = "";
		},
		asignar_valores_otros : function (idioma,id_otros){
			if(idioma!="")
				id_idioma = idioma;
			else
				id_idioma = "";
			//--
			if(id_otros!="")
				id = id_otros;
			else
				id = "";
		},
		cargar_portafolios : function(callback){

			$http.post("./controladores/portafolioController.php", {accion:'consultar_portafolio',id_idioma:id_idioma}).success(callback);
		},

		cargar_detalle_portafolio : function (callback){
			$http.post("./controladores/portafolioController.php", {accion:'consultar_delalle_portafolio',id_idioma:id_idioma,slug:super_slug}).success(callback);
		},

		cargar_otros_portafolio : function (callback){
			$http.post("./controladores/portafolioController.php", {accion:'consultar_otros_portafolio',id_idioma:id_idioma,id:id}).success(callback);
		}
		//--
	}
}])
.factory("negociosFiltrosFactory",['$http', function($http){
	return{
		asignar_valores : function (valor_offset,valor_limit){
			if(valor_offset!=" ")
				offset = valor_offset;
			else
				offset = "";

			if(valor_limit!="")
				limit = valor_limit;
			else
				limit = "";
		},

		cargar_negocios : function(callback){
			$http.post("./controladores/detallesNegociosController.php", {accion:'consultar_negocios_filtros',limit:limit,offset:offset}).success(callback);
		}
	}
}])
.factory("tiposNegociosFactory",['$http', function($http){
	return{

		cargar_tipos_negocios : function(callback){
			$http.post("./controladores/detallesNegociosController.php", {accion:'consultar_tipos_negocios'}).success(callback);
		}
	}
}])
.factory("noticiasInicioFactory",['$http', function($http){
	return{

		cargar_noticias_inicio : function(callback){
			$http.post("./controladores/noticiasController.php", {accion:'consultar_noticias_inicio'}).success(callback);
		}
	}
}])
.factory("noticiasPrincipalFactory",['$http', function($http){
	return{
		asignar_valores : function (valor_offset,valor_limit,idioma){
			if(valor_offset!="")
				offset= valor_offset;
			else
				offset = "";

			if(valor_limit!="")
				limit= valor_limit;
			else
				limit = "";

			if(idioma!="")
				id_idioma= idioma;
			else
				id_idioma = "";
		},

		cargar_noticias_principal : function(callback){
			$http.post("./controladores/noticiasController.php", {accion:'consultar_noticias_principal',offset:offset,limit:limit, id_idioma:id_idioma}).success(callback);
		}
	}
}])
.factory("noticiasLimiteFactory",['$http', function($http){
	return{
		asignar_valores : function (valor_offset,valor_limit,idioma){
			if(valor_offset!="")
				offset= valor_offset;
			else
				offset = "";
			if(valor_limit!="")
				limit= valor_limit;
			else
				limit = "";
			if(idioma!="")
				id_idioma= idioma;
			else
				id_idioma = "";
		},

		cargar_noticias_limite : function(callback){
			$http.post("./controladores/noticiasController.php", {accion:'consultar_noticias_limite',offset:offset,limit:limit,id_idioma:id_idioma}).success(callback);
		}
	}
}])
.factory("videosFactory",['$http', function($http){
	return{
		asignar_valores : function (valor_x){
			if(valor_x!="")
				x= valor_x;
			else
				x = "";
		},
		cargar_videos : function(callback){
			$http.post("./controladores/videosController.php", {accion:'consultar_videos',x:x}).success(callback);
		}
	}
}])
.factory("videosPrincipalFactory",['$http', function($http){
	return{
		asignar_valores : function (valor_offset,valor_limit){
			if(valor_offset!="")
				offset= valor_offset;
			else
				offset = "";
			if(valor_limit!="")
				limit= valor_limit;
			else
				limit = "";
		},

		cargar_videos_limite : function(callback){
			$http.post("./controladores/videosController.php", {accion:'consultar_videos_principal',offset:offset,limit:limit}).success(callback);
		}
	}
}])
.factory("videosLimiteFactory",['$http', function($http){
	return{
		asignar_valores : function (valor_offset,valor_limit, valor_video_link){
			if(valor_offset!="")
				offset= valor_offset;
			else
				offset = "";

			if(valor_limit!="")
				limit= valor_limit;
			else
				limit = "";

			if(valor_video_link!="")
				video_link= valor_video_link;
			else
				video_link = "";
		},

		cargar_videos_limite : function(callback){
			$http.post("./controladores/videosController.php", {accion:'consultar_videos_limite',offset:offset,limit:limit, video_link:video_link}).success(callback);
		}
	}
}])
.factory("videosSubFactory",['$http', function($http){
	return{
		asignar_valores : function (valor_offset,valor_limit,valor_video_link){
			if(valor_offset!="")
				offset= valor_offset;
			else
				offset = "";
			if(valor_limit!="")
				limit= valor_limit;
			else
				limit = "";
			if(valor_video_link!="")
				video_link= valor_video_link;
			else
				video_link = "";
		},

		cargar_videos_limite : function(callback){
			$http.post("./controladores/videosController.php", {accion:'consultar_videos_sub',offset:offset,limit:limit,video_link:video_link}).success(callback);
		}
	}
}])
.factory("imagenFactory",['$http', function($http){
	return{
		asignar_valores : function (valor_offset,valor_limit){
			if(valor_offset!="")
				offset= valor_offset;
			else
				offset = "";
			if(valor_limit!="")
				limit= valor_limit;
			else
				limit = "";
		},

		cargar_imagenes : function(callback){
			$http.post("./controladores/imagenesController.php", {accion:'consultar_imagenes',offset:offset,limit:limit}).success(callback);
		}
	}
}])
.factory("imagenesLimiteFactory",['$http', function($http){
	return{
		asignar_valores : function (valor_offset,valor_limit){
			if(valor_offset!="")
				offset= valor_offset;
			else
				offset = "";
			if(valor_limit!="")
				limit= valor_limit;
			else
				limit = "";
		},

		cargar_imagenes_limite : function(callback){
			$http.post("./controladores/imagenesController.php", {accion:'consultar_imagenes_limite',offset:offset,limit:limit}).success(callback);
		}
	}
}])
.factory("noticiasPreviewFactory",['$http', function($http){
	return{
		asignar_valores_preview : function (valor_title_id){
			if(valor_title_id!="")
				title_id = valor_title_id;
			else
				title_id = "";
		},
		cargar_noticias_preview : function(callback){
			$http.post("./controladores/noticiasController.php", {accion:'consultar_noticias_preview',title_id:title_id}).success(callback);
		}
	}
}])
.factory("direccionFactory",['$http', function($http){
	return{
		asignar_valores : function (valor_offset,valor_limit,idioma){
			if(valor_offset!="")
				offset= valor_offset;
			else
				offset = "";
			if(valor_limit!="")
				limit= valor_limit;
			else
				limit = "";

			if(idioma!="")
				id_idioma= idioma;
			else
				id_idioma = "";
		},
		cargar_direcciones : function(callback){
			$http.post("./controladores/direccionController.php", {accion:'consultar_direccion_limite',offset:offset,limit:limit,id_idioma:id_idioma}).success(callback);
		},
		cargar_direcciones_todas : function(callback){
			$http.post("./controladores/direccionController.php", {accion:'consultar_direccion_todas',offset:offset,limit:limit}).success(callback);
		}
	}
}])
.factory("contactosFactory",['$http', function($http){
	return{
		asignar_valores : function (idioma){

			if(idioma!="")
				id_idioma= idioma;
			else
				id_idioma = "";

		},

		cargar_preguntas : function(callback){
			$http.post("./controladores/contactosController.php", {accion:'consultar_preguntas',id_idioma:id_idioma}).success(callback);
		}
	}
}])
.factory("footerFactory",['$http', function($http){
	return{
		asignar_valores : function (valor_idioma,url){
			if(valor_idioma!="")
				id_idioma = valor_idioma;
			else
				id_idioma = "";
			//---
			if(url!="")
				base_url = url;
			else
				base_url = "";
			//---
		},
		cargar_redes : function(callback){
			$http.post(base_url+"WebInicio/consultarRedes", {id_idioma:id_idioma}).success(callback);
		},
		cargar_footer:function(callback){
			$http.post(base_url+"WebInicio/consultarFooter", {id_idioma:id_idioma}).success(callback);
		},
		cargar_meta_tag:function(callback){
			$http.post(base_url+"WebInicio/consultarMetaTag", {id_idioma:id_idioma}).success(callback);
		},
	}
}])
.factory("correosFactory",['$http', function($http){
	return{
		cargar_redes : function(callback){
			$http.post("./controladores/correosController.php", {accion:'consultar_correos'}).success(callback);
		}
	}
}])
.factory('facebookService', function($q) {
    return {
        getMyLastName: function() {
            var deferred = $q.defer();
            FB.api('/me', {
                fields: 'last_name'
            }, function(response) {
                if (!response || response.error) {
                    deferred.reject('Error occured');
                } else {
                    deferred.resolve(response);
                }
            });
            return deferred.promise;
        }
    }
})
.factory("metaFactory",['$http',function($http){
	return{
		cargar_palabras_claves : function(callback){
			$http.post("./controladores/homeController.php", {accion:'consultar_palabras_claves'}).success(callback);
		}
	}
}])
.factory("blogFactory",['$http', function($http){
	return{
		asignar_limites : function (valor_offset,valor_limit,idioma,categoria,tags,valor_fecha){

			if(valor_offset!="")
				offset= valor_offset;
			else
				offset = "";

			if(valor_limit!="")
				limit= valor_limit;
			else
				limit = "";

			if(idioma!="")
				id_idioma= idioma;
			else
				id_idioma = "";

			if(categoria!="")
				id_categoria =	categoria;
			else
				id_categoria = "";

			if(tags!="")
				id_tags = tags;
			else
				id_tags = "";

			if(valor_fecha!="")
				fecha = valor_fecha;
			else
				fecha = valor_fecha;
		},
		asignar_valores_detalles : function(title_id,idioma){
			if(title_id!="")
				slug = title_id;
			else
				slug = "";
			if(idioma!="")
				id_idioma = idioma;
			else
				id_idioma = "";
		},
		cargar_blog : function(callback){
			$http.post("./controladores/blogController.php", {accion:'consultar_blog_limite',offset:offset,limit:limit,id_idioma:id_idioma,id_categoria:id_categoria,id_tags:id_tags,fecha:fecha}).success(callback);
		},
		cargar_fechas : function(callback){
			$http.post("./controladores/blogController.php", {accion:'consultar_fechas',id_idioma:id_idioma}).success(callback);
		},
		cargar_blog_render : function(callback){
			$http.post("./controladores/blogController.php", {accion:'consultar_blog_render',offset:offset,limit:limit,id_idioma:id_idioma,id_categoria:id_categoria,id_tags:id_tags,fecha:fecha}).success(callback);
		},
		cargar_blog_detalles : function(callback){
			$http.post("./controladores/blogController.php", {accion:'consultar_blog_detalle',slug:slug,id_idioma:id_idioma}).success(callback);
		}
	}
}])
.factory("categoriasBlogFactory",['$http', function($http){
	return{
			asignar_idioma : function (idioma){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
			},
			cargar_categorias_blog : function(callback){
				$http.post("./controladores/categorias_blogController.php", { accion:'consultar_categorias_blog',id_idioma:id_idioma}).success(callback);
			}
	}
}])
.factory("tagsFactory",['$http', function($http){
	return{
		asignar_idioma : function (idioma){
			if(idioma!="")
				id_idioma = idioma;
			else
				id_idioma = "";
		},
		cargar_tags : function(callback){
			$http.post("./controladores/tagsController.php", {accion:'consultar_tags2',id_idioma:id_idioma}).success(callback);
		}
	}
}])
//--Servicio para compartir datos de mensajes iniciales
.service('serverDataMensajes',[function(){
	var puente = [];
	this.puenteData = function(arreglo){
		puente = arreglo;
		return puente;
	}
	this.obtener_arreglo = function(){
		return puente;
	}
	this.limpiar_arreglo_servicio = function (){
		puente = [];
		return puente;
	}
}])
.service('multIdioma',["$http", "$q", function ($http, $q,$location){
	//--Defino Variables...
	var objeto_idioma = {
							"idioma_inicio": "",
							"titulo": "",
							"pie" : "",
							"url_menu" : "",
							"btn":""
						}
	//--titulos
	var titulo_menu_es={
							"menu1":"INICIO",
							"menu2":"NOSOTROS",
							"menu3":"PRODUCTOS",
							"menu4":"NOTICIAS",
							"menu5":"CONTÁCTANOS",
							"menu5":"CONTÁCTANOS",
							"menu_items":"Productos en su carrito",
							"menu_carritos":"CARRITO",
							"menu_productos":"ORDENES",
							"footer":"2019 © ZOUG ZOUG TODOS LOS DERECHOS RESERVADOS",
							"menu_iniciar":"Iniciar",
							"menu_salir":"Salir",
							"titulo_inicio_sesion":"Inicio de Sesión",
							"registro":"Registrarse",
							"iniciar":"Inciar Sesión",
							"registrarse_email":"Registrarse",
							"contrasena":"¿Olvidaste tu contraseña?",
							"correo":"Correo electrónico",
							"clave":"Contraseña",
							"nombres_apellidos":"Nombre",
							"clave_confirm":"Confirmar contraseña",
							"change_clave":"Cambiar contraseña",
							"ing_correo":"Ingrese el correo electrónico",
							"todas_opciones":"Opciones de inicio de sesión",
							"todas_opciones2":"Opciones de inicio",
							"envio_activacion":"Se le ha enviado un correo con el enlace de activación.",
							"envio_cambio_contra":"Se le ha enviado un correo de activación para el cambio de contraseña.",
							"incio_facebook":"INGRESA CON FACEBOOK",
							"incio_google":"INGRESA CON GOOGLE",
							"no_tienes_cuenta":"¿No tienes cuenta todavía?",




	}
	var titulo_menu_uk={
							"menu1":"HOME",
							"menu2":"ABOUT US",
							"menu3":"PRODUCTS",
							"menu4":"NEWS",
							"menu5":"CONTACT US",
							"menu_items":"Item(s) in your cart",
							"menu_carritos":"CART",
							"menu_productos":"ORDERS",
							"footer":"2019 © ZOUG ZOUG ALL RIGHTS RESERVED",
							"menu_iniciar":"Sign In",
							"menu_salir":"LogOut",
							"titulo_inicio_sesion":"Sign In",
							"registro":"SIGN UP",
							"iniciar":"SIGN IN",
							"registrarse_email":"SIGN UP",
							"contrasena":"FORGOT PASSWORD?",
							"correo":"Email",
							"clave":"Password",
							"nombres_apellidos":"Name",
							"clave_confirm":"Confirm Password",
							"change_clave":"Change Password",
							"todas_opciones":"All sign in option",
							"todas_opciones2":"All sign in option",
							"envio_activacion":"You have been sent an email with the activation link.",
							"envio_cambio_contra":"You have been sent an activation email for the password change.",
							"incio_facebook":"SIGN IN WITH FACEBOOK",
							"incio_google":"SIGN IN WITH GOOGLE",
							"no_tienes_cuenta":"Don’t have an account yet?",

							
				}
	//-------------------------------------------------------------------------------------
	//--TITULOS HOME
	var titulos_home = {}
	var titulos_home_es = {
								"somos1":"Zoug Zoug",
								"somos2":"Kitchen Clothing",
								"nosotros":"NOSOTROS",
								"productos":"PRODUCTOS",
								"productos2":"Nuestros Productos",
								"noticias2":"Publicaciones Recientes",
								"redes_sociales":"REDES SOCIALES",
								"noticias":"NOTICIAS",
								"redes_sociales2":"Síguenos En Las Redes Sociales",
								"contactanos":"CONTÁCTANOS",
								"contactanos2":"Dejanos Tu Mensaje",
								"noticias":"NOTICIAS",
								"nuestros_productos":"Nuestros productos",
								"otras_noticias":"Otras noticias",
								"mision":"Misión",
								"vision":"Visión",
								"producto_nombre":"Producto",
								"producto_precio":"Precio",
								"producto_cantidad":"Cantidad",
								"producto_cantidad_reducido":"Cant",
								"vista_orden":"Vista de Orden",
								"resumen_orden":"Resumen de Orden",
								"producto_metodo_pago":"Método de Pago",
								"gracias_compra":"Gracias Por Su Compra",
								"info_email":"Recibirá un email con los detalles de su orden",
								"info_orden1":"Haz solicitado productos de la siguiente orden: ",
								"info_orden2":"La confirmación de su pedido y el recibo se envían a: ",
								"checkout":"ORDEN RECIBIDA",
								"cart":"CARRITO DE COMPRAS",
								"order_titulo":"ORDEN",
								"order_usuario_titulo":"SUS ORDENES",
								"vista_orden_usuario":"Lista de Ordenes",
								"datos_orden":"Datos de Orden",
								"usuarioOrden":"Usuario",
								"usuarioEmail":"Email",
								"orderOrder":"Orden",
								"fechaOrder":"Fecha",
								"payment_id":"ID del pago paypal",
								"payer_id":"ID cliente paypal",
								"detalles_productos":"Detalles del producto",
								"detalles_productos_categoria":"Categoría",
								"detalles_productos_tipo_producto":"Tipo producto",
								"detalles_productos_talla":"Talla",
								"detalles_productos_color":"Color",
								"redes_sociales3":"Algunas de nuestras publicaciones en instagram",
								"mas_productos":"Productos relacionados",
								"filtros":"Filtros",
								"buscar":"Buscar productos…",
								"registro_exitoso":"ACTIVAR CUENTA",
								"recuperar_cuenta":"RECUPERAR CUENTA",
								"gracias_registro":"Gracias Por Registrarse",
								"la_cuenta":"La cuenta perteneciente al correo: ",
								"activada":"Ha sido activada exitosamente",
								"error_nada":"Se Ha Producido Un Error",
								"caducado":"Link Caducado",
								"nueva_clave":"Nueva contraseña",
								"nueva_clave_r":"Repite contraseña",
								"recuperar":"Recuperar contraseña",
								"cambio_realizado":"Cambio Exitoso",
								"detalles_productos_cantidad":"Cantidad",
								"detalles_productos_agregar":"Agregar",
								"mensaje_redirigir":"EROOR DE INICIO DE SESIÓN",
								"mensaje_email":"Debes iniciar sesión por correo electrónico",
								"mensaje_facebook":"Debes iniciar sesión por Facebook",
								"mensaje_google":"Debes iniciar sesión por Google",
	}							
	
	var titulos_home_uk ={
								"nosotros":"ABOUT US",
								"somos1":"ZOUG ZOUG",
								"somos2":"Kitchen Clothing",
								"productos":"PRODUCTS",
								"productos2":"Our Products",
								"noticias":"NEWS",
								"noticias2":"Recents Posts ",
								"redes_sociales":"SOCIAL MEDIA",
								"redes_sociales2":"Follow us on social media",
								"contactanos":"CONTACT US",
								"contactanos2":"Give us a Message",
								"noticias":"NEWS",
								"nuestros_productos":"Our products",
								"otras_noticias":"Others news",
								"mision":"Mision",
								"vision":"Vision",
								"producto_nombre":"Product",
								"producto_precio":"Price",
								"producto_cantidad":"Quantity",
								"producto_cantidad_reducido":"Quant",
								"vista_orden":"Order Review",
								"resumen_orden":"Order Sumary",
								"producto_metodo_pago":"Payment Method",
								"gracias_compra":"Thank You For Your Order",
								"info_email":"You will receive an email of your order details",
								"info_orden1":"You have requested products of the following order: ",
								"info_orden2":"Your order confirmation and receipt is sent to: ",
								"checkout":"CHECKOUT STEP",
								"cart":"CART",
								"order_titulo":"ORDER",
								"order_usuario_titulo":"YOUR ORDERS",
								"vista_orden_usuario":"Order List",
								"datos_orden":"Info of Order",
								"usuarioOrden":"User",
								"usuarioEmail":"Email",
								"orderOrder":"Order",
								"fechaOrder":"Date",
								"payment_id":"Payment ID",
								"payer_id":"Payer ID",
								"detalles_productos":"Product's details",
								"detalles_productos_categoria":"Category",
								"detalles_productos_tipo_producto":"Type",
								"detalles_productos_talla":"Size",
								"detalles_productos_color":"Colors",
								"redes_sociales3":"Some instagram post",
								"mas_productos":"Related products",
								"filtros":"Filters",
								"buscar":"Search Items…",
								"registro_exitoso":"ACTIVATE ACCOUNT",
								"recuperar_cuenta":"RECOVER ACCOUNT",
								"gracias_registro":"Thank you for register",
								"recuperar":"Password recovery							",
								"la_cuenta":"The email account: ",
								"activada":"It has been successfully activated",
								"error_nada":"An Error Has Occurred",
								"caducado":"Link Caducado1",
								"nueva_clave":"New Password",
								"nueva_clave_r":"Repeat new Password",
								"cambio_realizado":"Successful Change",
								"detalles_productos_cantidad":"Quantity",
								"detalles_productos_agregar":"Add",
								"mensaje_redirigir":"LOGIN EROOR",
								"mensaje_email":"You must log in by email",
								"mensaje_facebook":"You must log in by Facebook",
								"mensaje_google":"You must be logged in by Google",

	}
	//-------------------------------------------------------------------------------------
	var btn_es = {
								"ver_mas":"VER MÁS",
								"leer_mas":"LEER MÁS",
								"enviar":"ENVIAR",
								"contacto":"Contacto",
								"place_h1":"Ingrese nombre",
								"place_h2":"Ingrese email",
								"place_h3":"Ingrese su número de teléfono",
								"place_h4":"Ingrese su Mensaje",
								"ver_servicios":"VER SERVICIOS",
								"datos_proyecto":"Datos del proyecto",
								"descripcion_proyecto":"Descripcion",
								"otros_proyectos":"Otros proyectos",
								"otras_noticias":"Otras noticias",
								"otros_servicios":"Otros servicios",
								"footer":"2019 © ZOUG ZOUG Todos los derechos reservados.",
								"navegacion":"NAVEGACIÓN",
								"pagos":"Métodos de pagos",
								"ver_producto":"Ver producto",
								"limpiar_filtro":"Limpiar filtro",
								"volver_tienda":"Volver a tienda",
								"ver_producto":"Ver producto",
								"update_cart":"ACTUALIZAR",
								"checkout":"SIGUIENTE",
								"privacy":" Política de Privacidad |",
								"terms":"Términos y Condiciones ",
								"volver_inicio":"Volver a inicio",
								"cambiar_clave":"Cambiar contraseña",
								
	}
	var btn_uk = {
								"ver_mas":"VIEW MORE",
								"leer_mas":"READ MORE",
								"enviar":"SEND",
								"contacto":"Contact",
								"place_h1":"Our name",
								"place_h2":"Our email",
								"place_h3":"Our telephone number",
								"place_h4":"Our message",
								"ver_servicios":"VIEW SERVICES",
								"datos_proyecto":"Project data",
								"descripcion_proyecto":"Description",
								"otros_proyectos":"Others projects",
								"otras_noticias":"Others news",
								"otros_servicios":"Others services",
								"footer":"2019 © ZOUG ZOUG All rigths reserved.",
								"navegacion":"NAVIGATION",
								"pagos":"Payment methods",
								"ver_producto":"Views products",
								"limpiar_filtro":"Clean Filters",
								"volver_tienda":"Back to shop",
								"ver_producto":"View product",
								"update_cart":"UPDATE CART",
								"checkout":"CHECKOUT",
								"privacy":" Privacy Policy | ",
								"terms":"Terms & Conditions ",
								"volver_inicio":"Go back to start",
								"cambiar_clave":"Change Password",
								"cantidad":"Quantity",
								"agregar":"Add",

							}
	//-------------------------------------------------------------------------------------
	//Urls
	var url_menu_es = {
								"menu1":"inicio",
								"menu2":"nosotros",
								"menu3":"productos",
								"menu4":"noticias",
								"menu5":"contactanos",
								"menu6":"carrito",
								"menu7":"politicas_privacidad",
								"menu8":"terminos",
	}
	var url_menu_uk = {
								"menu1":"home",
								"menu2":"about",
								"menu3":"products",
								"menu4":"news",
								"menu5":"contact_us",
								"menu6":"cart",
								"menu7":"privacy_policy",
								"menu8":"terms",
	}
	//---Para carga de inicio en ingles
	this.cargar_inicio_espain = function(base_url){
		objeto_idioma.titulo = titulo_menu_es;
		objeto_idioma.url = url_menu_es;
		objeto_idioma.btn = btn_es

		$("#bandera").attr("src",base_url+"assets/web/img/flags/uk.png").attr("title","Traducir a Ingles")
		$("#idioma").addClass("esta_espaniol").removeClass("esta_ingles");
		objeto_idioma.idioma_inicio = 1
		//deferred.resolve(objeto_idioma);
		return objeto_idioma
	}
	//---Para carga de inicio en español
	this.cargar_inicio_uk = function(base_url){
		objeto_idioma.titulo = titulo_menu_uk;
		objeto_idioma.url = url_menu_uk;
		objeto_idioma.btn = btn_uk

		$("#bandera").attr("src",base_url+"assets/web/img/flags/spain.png").attr("title","Translate to Spanish")

		$("#idioma").addClass("esta_ingles").removeClass("esta_espaniol");
		objeto_idioma.idioma_inicio = 2
		//deferred.resolve(objeto_idioma);
		return objeto_idioma
	}
	//--Defino el idioma
	this.definir_idioma = function(base_url){

		if ($("#idioma").hasClass("esta_ingles")==true){
			this.cargar_inicio_uk(base_url);
		}else if($("#idioma").hasClass("esta_espaniol")==true){
			this.cargar_inicio_espain(base_url);
		}
		return objeto_idioma
	}
	//--Cambiar idioma
	this.cambiar_idioma = function(base_url){
		if ($("#idioma").hasClass("esta_ingles")==true){
			$("#idioma").addClass("esta_espaniol").removeClass("esta_ingles")
			this.cargar_inicio_espain(base_url);
		}else if($("#idioma").hasClass("esta_espaniol")==true){
			$("#idioma").addClass("esta_ingles").removeClass("esta_espaniol")
			this.cargar_inicio_uk(base_url);
		}
		return objeto_idioma
	}
	//--Cambiar titulos idioma Home ---
	this.cambiar_idioma_home = function(){
		mostrar_preloader()
		if ($("#idioma").hasClass("esta_ingles")==true){
			titulos_home = titulos_home_uk
			idioma=2
		}else if($("#idioma").hasClass("esta_espaniol")==true){
			titulos_home = titulos_home_es
			idioma=1
		}

		return titulos_home
	}
	//--
}])
.service('upload', ["$http", "$q", function ($http, $q)
{
	this.uploadFile = function(file, id_empleo,nombre_archivo)
	{
		var deferred = $q.defer();
		var formData = new FormData();

		formData.append("id_empleo", id_empleo);
		formData.append("file", file);
		/*formData.append("categoria", categoria);
		formData.append("file", file);*/
		formData.append("nombre_archivo",nombre_archivo);
		return $http.post("./controladores/archivosController.php", formData, {
			headers: {
				"Content-type": undefined
			},
			transformRequest: angular.identity
		})
		.success(function(res)
		{
			deferred.resolve(res);
		})
		.error(function(msg, code)
		{
			deferred.reject(msg);
		})
		return deferred.promise;
	}
}])
.factory("portafoliosLimitFactory",['$http', function($http){
	return{
		asignar_valores : function (id_idioma,offset_producto,limit_producto){
			if(id_idioma!="" || id_idioma != undefined)
				idioma = id_idioma;
			else
				idioma = "";
			if(offset_producto != "" || offset_producto != undefined)
				offset = offset_producto;
			else
				offset = "";
			if(limit_producto != "" || limit_producto != undefined)
				limit = limit_producto;
			else
				limit = "";
		},

		cargar_productos_limit : function(callback){
			$http.post("./controladores/portafolioController.php",
				{	accion:'consultar_protafolio_filtros',
					id_idioma:idioma,
					offset:offset,
					limit:limit
				})
				.success(callback);
		}
	}
}])
.factory("tallaProdFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				
				if(id!="")
					id_prod = id;
				else
					id_prod = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_talla: function(callback){
				$http.post(base_url+"WebProductos/consultarTallasProd", {id_prod:id_prod}).success(callback);
			}
	}
}])
.factory("colorProdFactory",['$http', function($http){
	return{
			asignar_valores : function (talla,id,url){
				if(talla!="")
					talla_prod = talla;
				else
					talla_prod = "";
				if(id!="")
					id_prod = id;
				else
					id_prod = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_color: function(callback){
				$http.post(base_url+"WebProductos/consultarColorProd", {talla_prod:talla_prod,id_prod:id_prod}).success(callback);
			}
	}
}])
.factory("cantidadPFactory",['$http', function($http){
	return{
			asignar_valores : function (color,talla,id,url){
				if(color!="")
					color_prod = color;
				else
					color_prod = "";
				if(talla!="")
					talla_prod = talla;
				else
					talla_prod = "";
				if(id!="")
					id_prod = id;
				else
					id_prod = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_cantidad: function(callback){
				$http.post(base_url+"WebProductos/consultarCantidadProd", {color_prod:color_prod,talla_prod:talla_prod,id_prod:id_prod}).success(callback);
			}
	}
}]);

