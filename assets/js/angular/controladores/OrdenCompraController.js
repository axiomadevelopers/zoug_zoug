angular.module("ContentManagerApp")
    .controller("OrdenCompraController", function($scope,$http,$location,serverDataMensajes,categoriasFactory,sesionFactory,idiomaFactory,OrdenCompraFactory){
       
		$(".li-menu").removeClass("active");
        $("#li_productos").addClass("active");
        $(".a-menu").removeClass("active");
        $("#orden_pago").addClass("active");   
        $scope.titulo_pagina = "Consulta de Ordenes de Compra";
        $scope.activo_img = "inactivo";
        $scope.base_url = $("#base_url").val();
        
        //---------------------------------------------
        $scope.contactos = {
                                'id':'',
                                'nombres':'',
                                'email':'',
                                'telefono':'',
                                'mensaje':''
        }
        $scope.detalle = {
                                'id':'',
                                'nombres':'',
                                'email':'',
                                'telefono':'',
                                'mensaje':''
        }
        $scope.titulo_mensaje = [];
        //-----------------------------------------------
      
		

        $scope.iniciar_datatable = function(){
			//--Iniciar DataTable
			$('#myTable').DataTable({
			    language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Entradas",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    },
                "order": [[ 0, "desc" ]]
			});
		}
        $scope.consultar_contactos = function(){
            OrdenCompraFactory.asignar_valores($scope.base_url)
            OrdenCompraFactory.cargar_ordenes(function(data){
                $scope.ordenCompra=data;
                //console.log($scope.ordenCompra)
            });
        }
        //--Para visualizar resumen
        $scope.ver_detalle = function(index){
            $("#modal_mensaje").modal("show");
            $scope.titulo_mensaje = "Detalles de la Orden"
            $scope.detalle = $scope.ordenCompra[index].detalles;
            $scope.total = $scope.ordenCompra[index];

            //console.log($scope.total.total)

        }
        
		//----------------------------------------------------------------
        setTimeout(function(){
        	$scope.iniciar_datatable();
        },500);

        $scope.consultar_contactos();

        //----------------------------------------------------------------
    });
