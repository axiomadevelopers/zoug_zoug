angular.module("ContentManagerApp")
	.controller("tallaConsultasController", function($scope,$http,$location,serverDataMensajes,sesionFactory,idiomaFactory,tallasFactory){
        $(".li-menu").removeClass("active");
		$("#li_productos").addClass("active");
		$(".a-menu").removeClass("active");
		$("#tallas").addClass("active");

		$scope.titulo_pagina = "Consulta de Tallas";
		$scope.activo_img = "inactivo";

		$scope.tallas = {
						'id': '',
						'idioma': '',
						'id_idioma' : '',
						'titulo' : '',
						'descripcion' : '',
						'id_imagen' : '',
						'imagen' : ''
		}
		$scope.id_talla = "";
		$scope.categorias_menu = "2";
		$scope.id_marca = "";
		$scope.base_url = $("#base_url").val();

		$scope.iniciar_datatable = function(){
			//--Iniciar DataTable
			$('#myTable').DataTable({
			    language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Entradas",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    },
			    "order": [[ 0, "desc" ]]

			});
			//console.log($scope.talla);
		}
		//////////////////////////////////////////////////////////////////////////////////////
		$scope.consultar_tallas = function(){
			tallasFactory.asignar_valores("","",$scope.base_url)
			tallasFactory.cargar_tallas(function(data){
				$scope.talla=data;
				//console.log($scope.talla);
			});
		}
		
		//////////////////////////////////////////////////////////////////////////////////////
		$scope.ver_tallas = function(valor){
			var id = $("#ver"+valor).attr("data");
			$scope.id_talla = id;
			//console.log($scope.id_talla);
			$("#id_talla").val($scope.id_talla)
			let form = document.getElementById('formConsultaTallas');
			form.action = "./tallasVer";
			form.submit();
		}
		//////////////////////////////////////////////////////////////////////////////////////
		$scope.activar_registro = function(event){
			$scope.id_seleccionado_tallas = []
			$scope.estatus_seleccionado_tallas = []
			var caja = event.currentTarget.id;
			var atributos = $("#"+caja).attr("data");
			var arreglo_atributos = atributos.split("|");
			$scope.id_seleccionado_tallas = arreglo_atributos[0];
			$scope.estatus_seleccionado_tallas = arreglo_atributos[1];
			$("#cabecera_mensaje").text("Información:");
			//--
			if ($scope.estatus_seleccionado_tallas==0){
				mensaje = "Desea modificar el estatus de este registro a publicado? ";
				$scope.estatus_seleccionado_tallas=1;
			}else{
				mensaje = "Desea modificar el estatus de este registro a inactivo? ";
				$scope.estatus_seleccionado_tallas=0
			}
			//($scope.estatus_seleccionado_categorias==1) ? $scope.estatus_seleccionado_categorias=0:$scope.estatus_seleccionado_categorias=1;

			//--
			$scope.modificar_estatus("activar_inactivar",mensaje)
		}
		//----------------------------------------------------------------
		$scope.modificar_estatus = function(opcion,mensaje){
			 swal({
				  title: 'Esta seguro?',
				  text: mensaje,
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Si!',
				  cancelButtonText: 'No',
				}).then((result) => {
					  if (result.value) {
						$scope.accion_estatus()
					  }
				})
		}
		//----------------------------------------------------------------
		$scope.accion_estatus = function(){

			$http.post($scope.base_url+"/Tallas/modificarTallasEstatus",
			{
				 'id':$scope.id_seleccionado_tallas,
				 'estatus':$scope.estatus_seleccionado_tallas,

			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					Swal(
						  'Realizado!',
						  'El proceso fue ejecutado.',
						  'success'
					).then((result) => {
						  if (result.value) {
								let form = document.getElementById('formConsultaTallas');
								form.action = "./consultar_tallas";
								form.submit();
						  }

					});
				}else{
					Swal(
						  'No realizado!',
						  'El proceso no pudo ser ejecutado.',
						  'warning'
					)
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}
		//////////////////////////////////////////////////////////////////////////////////////
		$scope.eliminar_registro = function(event){
			$scope.id_seleccionado_tallas = []
			$scope.estatus_seleccionado_tallas = []
			var caja = event.currentTarget.id;
			//alert(caja);
			var atributos = $("#"+caja).attr("data");
			var arreglo_atributos = atributos.split("|");
			$scope.id_seleccionado_tallas = arreglo_atributos[0];
			$scope.estatus_seleccionado_tallas = arreglo_atributos[1];
			//console.log($scope.estatus_seleccionado_tallas);
			$("#cabecera_mensaje").text("Información:");
			if ($scope.estatus_seleccionado_tallas==2){
				mensaje = "Desea eliminar este registro? ";
			}
			$scope.modificar_estatus("eliminar",mensaje)
		}
		//////////////////////////////////////////////////////////////////////////////////////
		setTimeout(function(){
				$scope.iniciar_datatable();
		},500);
		$scope.consultar_tallas();
	})
