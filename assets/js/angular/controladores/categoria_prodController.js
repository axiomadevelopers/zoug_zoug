angular.module("ContentManagerApp")
	.controller("categoria_prodController" , function($scope,$http,$location,serverDataMensajes,categoriasProdFactory,sesionFactory,idiomaFactory){
		//Cuerpo declaraciones
		$(".li-menu").removeClass("active");
        $("#li_productos").addClass("active");
        $(".a-menu").removeClass("active");
        $("#cat_prod").addClass("active");

		$scope.titulo_pagina = "Categoría de Producto";
		$scope.subtitulo_pagina  = "Registrar";
		$scope.activo_img = "inactivo";
		$scope.titulo_registrar = "Registrar";
		$scope.titulo_cons = "Consultar";
		$scope.base_url = $("#base_url").val();
		$scope.titulo_text = "Pulse aquí para ingresar el contenido"
		//////////////////////////////////////////////////////////////////////////////
		$scope.categoria_prod  = {
									'id' : '',
									'titulo' : '',
									'descripcion' : '',
									'id_idioma' : '',
									'idioma' : ''
		}
		//////////////////////////////////////////////////////////////////////////////
		$scope.consultar_idioma = function(){
			idiomaFactory.asignar_valores("",$scope.base_url)
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data;
				//console.log($scope.idioma);
			});
		}
		//////////////////////////////////////////////////////////////////////////////
		$scope.agregarWisi = function(){
			$scope.categoria_prod.descripcion = $(".textarea_editor").val()
			$("#div_descripcion").html($scope.categoria_prod.descripcion)
			$("#cerrarModal").click();
		}
		$scope.wisi_modal = function(){
			$("#wisiModal").modal("show")
			$(".textarea_editor").data("wysihtml5").editor.setValue($scope.categoria_prod.descripcion)
		}
		/////////////////////////////////////////////////////////////////////////////////////
		$scope.registrarCategoriasProd = function(){
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			if($scope.validar_form()==true){
				//Para guardar
				$scope.categoria_prod.id = $scope.id_categoria_prod;
				if(($scope.categoria_prod.id!="")&&($scope.categoria_prod.id!=undefined)){
					$scope.modificar_categoria_prod();
				}else{
					$scope.insertar_categoria_prod();
				}

			}

			//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}
		//////////////////////////
		$scope.validar_form = function(){
			//console.log($scope.categoria_prod)
			if(($scope.categoria_prod.id_idioma=="")||($scope.categoria_prod.id_idioma=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar un idioma","warning");
				return false;
			}else if(($scope.categoria_prod.titulo=="")||($scope.categoria_prod.titulo=="")){
				mostrar_notificacion("Campos no validos","Debe registrar un título","warning");
				return false;
			}else if(($scope.categoria_prod.descripcion=="NULL")||($scope.categoria_prod.descripcion=="")){
				mostrar_notificacion("Campos no validos","Debe registrar una descripción","warning");
				return false;
			}
			else{
				return true;
			}
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.limpiar_cajas_categorias = function(){
			$scope.categoria_prod  = {
										'id' : '',
										'titulo' : '',
										'descripcion' : '',
										'id_idioma' : '',
										'idioma' : ''
			}
			$("#div_descripcion").html($scope.titulo_text);
			$('#textarea_editor').data("wysihtml5").editor.clear();
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.insertar_categoria_prod = function(){
			$http.post($scope.base_url+"/Categoria_prod/registrarCategoriasProd",
			{
				'titulo' : $scope.categoria_prod.titulo,
				'descripcion': $scope.categoria_prod.descripcion,
				'id_idioma' : $scope.categoria_prod.id_idioma.id,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cajas_categorias();
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe una categoría con esa descripción","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultarCategoriaProdIndividual = function(){
			
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			//console.log($scope.id_categoria_prod);
			categoriasProdFactory.asignar_valores("",$scope.id_categoria_prod,$scope.base_url)
			categoriasProdFactory.cargar_categorias_prod(function(data){
				$scope.categoria_prod=data[0];
				//console.log(data[0]);

				$("#div_descripcion").html($scope.categoria_prod.descripcion)
				setTimeout(function(){
					$('#idioma > option[value="'+$scope.categoria_prod.id_idioma+'"]').prop('selected', true);
				},300);
				$("#idioma").prop('disabled', true);

				$scope.titulo_registrar = "Modificar";
				$scope.subtitulo_pagina  = "Modificar categorías";
			});
			desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.modificar_categoria_prod = function(){
			//console.log($scope.categoria_prod.id_idioma);
			$http.post($scope.base_url+"/Categoria_prod/modificarCategoriasProd",
			{
				'id' 	 	 : $scope.categoria_prod.id,
				'titulo' 	 : $scope.categoria_prod.titulo,
				'descripcion': $scope.categoria_prod.descripcion,
				'id_idioma'  : $scope.categoria_prod.id_idioma,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
					//$scope.limpiar_cajas_categorias();
				}else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}
		$scope.consultar_idioma();
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.id_categoria_prod  = $("#id_categoria_prod").val();
		//console.log($scope.id_categoria_prod);
			if($scope.id_categoria_prod){
				$scope.consultarCategoriaProdIndividual();
			}
	})
