angular.module("ContentManagerApp")
	.controller("AdministracionCantidadController", function($scope,$http,$location,serverDataMensajes,sesionFactory,idiomaFactory,colorFactory,tallaFactory,productosFactory,existenteFactory){
		//Cuerpo declaraciones
		$(".li-menu").removeClass("active");
        $("#li_productos").addClass("active");	
        $(".a-menu").removeClass("active");
        $("#adcantidad").addClass("active");	
		$scope.titulo_pagina = "Administración de cantidades de producto";
		$scope.subtitulo_pagina  = "Administrar inventario";
		$scope.activo_img = "inactivo";
		$scope.administra = {
							"productos":"",
							"talla":"",
							"color":"",
							"cantidad":"",
							"existente":"",
		}
		$scope.titulo_registrar = "Registrar";
		$scope.categorias_menu = "1";
		$scope.base_url = $("#base_url").val();
		$scope.currentTab = 'datos_basicos'
		
		$("#existente").prop('disabled', true);

		$scope.TraerExistentes = function(){
			if(($scope.administra.productos!="")&&($scope.administra.talla!="")&&($scope.administra.color!="")){
				//console.log($scope.administra.productos);

				existenteFactory.asignar_valores("",$scope.administra.productos.id,$scope.administra.color.id,$scope.administra.talla.id,$scope.base_url)
				existenteFactory.cargar_existentes(function(data){
					$scope.administra.existente=data;
				});
				
			}
		}

		//Cuerpo de metodos
		//--
		$scope.ConsultaProductos = function(){
			productosFactory.asignar_valores("","",$scope.base_url)
			productosFactory.cargar_productos(function(data){
				$scope.productos=data;
			});
		}
		$scope.cargar_color_idioma = function(){
			//colorFactory.asignar_valor('1',$scope.detalle_prod.id_idioma.id);//ya que es galeria de imagenes
			colorFactory.asignar_valor('1','1');//ya que es galeria de imagenes
 	       	colorFactory.cargar_color(function(data){
 	            $scope.color=data;
 	            $.each( $scope.color, function( indice, elemento ){
				  	agregarOptions("#color", elemento.id, elemento.descripcion)
				});
				$('#color > option[value=""]').prop('selected', true);
		    });
		}
		 $scope.consultar_talla = function(){
			tallaFactory.asignar_valores("")
			tallaFactory.cargar_talla(function(data){
				$scope.talla=data;
				$.each( $scope.talla, function( indice, elemento ){
				  	agregarOptions("#talla", elemento.id, elemento.descripcion)
				});
				$('#talla > option[value=""]').prop('selected', true);

				
				//console.log($scope.talla);
			});
		} 
		
		
		
		$("#formDropZone").dropzone({ url: $scope.base_url+"/GaleriaMultimedia/Upload" });
		//--
		$scope.validar_form = function(){
			if($scope.administra.productos==""){
				mostrar_notificacion("Campos no validos","Debe Seleccionar un producto","warning");
				return false;
			}else if($scope.administra.color==""){
				mostrar_notificacion("Campos no validos","Debe seleccionar un color","warning");
				return false;
			}else if($scope.administra.talla==""){
				mostrar_notificacion("Campos no validos","Debe seleccionar una talla","warning");
				return false;
			}else if($scope.administra.cantidad==""){
				mostrar_notificacion("Campos no validos","Debe ingresar la cantidad de productos","warning");
				return false;
			}
			else{
				return true;
			}
		}
		//--
		$scope.registrar = function(){

			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			if($scope.validar_form()==true){
				//Para guardar
				//alert("id_personas:"+$scope.doctor.id_personas);
				//alert("id_doctor:"+$scope.doctor.id);
				$scope.administra.id = $scope.id_administra;
				if(($scope.administra.id!=undefined)&&($scope.administra.id!="")){
					$scope.modificar_cantidad();	
				}else{
					$scope.insertar_cantidad();
				}		
			}

			//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}
		//--
		$scope.insertar_cantidad = function (){

			$http.post($scope.base_url+"/AdministracionCantidad/registrarCantidad",
			{
				'id_producto' 	     : $scope.administra.productos.id,
				'cantidad'  : $scope.administra.cantidad,
				'id_color': $scope.administra.color.id,
				'id_talla': $scope.administra.talla.id,
							
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "cambio_cantidad"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
					$scope.limpiar_cantidad();
				}else if($scope.mensajes.mensaje == "registro_cantidad"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cantidad();
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe ese color","warning");
				}else if($scope.mensajes.mensaje == "menor_a_cero"){
					mostrar_notificacion("Mensaje","La existencia total no puede ser menor a cero","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
				$mensajes["mensaje"] = "no_registro";

			}).error(function(data,estatus){
				console.log(data);
			});		}
		//---
		//---------------------------------------
		//--Metodo para subir acrhivos
		$scope.uploadFile = function(){
			var file = $scope.file;
			var categoria = $scope.galeria.categoria.id;
			var nombre_archivo = $scope.galeria.descripcion;
			var base_url = $scope.base_url;
			//-
			upload.uploadFile(file,categoria,nombre_archivo,base_url).then(function(res){
				//console.log(res.data.mensaje);
				if(res.data.mensaje=="registro_procesado"){
					mostrar_notificacion("Mensaje","La imagen fue cargada de manera exitosa!","info");
					//$scope.consultar_galeria();
					$scope.limpiar_imagen();
				}else{
					mostrar_notificacion("Mensaje"," Error al subir tipo de archivo, solo puede subir imagenes .jpg con un peso menor a 1 mb con medidas entre w:5000 h:5000	","warning");
				}
				//--------------------------------
			});
			//-
		}
		//--
		$scope.limpiar_cantidad = function(){
			$scope.administra = {
							"productos":'',
							"color":"",
							"talla":"",
							"cantidad":"",
							"existente":""
			}
			$scope.productos = "";
			//console.log($scope.administra.productos);
			//console.log($scope.productos);
			$scope.ConsultaProductos();

			$("#previa").css("display","block");
			$(".imgbiblioteca_principal").css("display","none");
			$("#file").val("");
			$("#categorias").val("");
			//$("#select_categoria").selectpicker('refresh');
		}
		//---------------------------------------
		//---
		//Cuerpo de llamados
		$scope.ConsultaProductos();
		$scope.cargar_color_idioma();
		$scope.consultar_talla(); 

		$("#producto").select2({
			allowClear: true,
			placeholder: "Seleccionar producto",
        	theme: "material"
		});
	});	

	