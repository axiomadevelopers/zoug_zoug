angular.module("ContentManagerApp")
	.controller("Marca_Controller", function($scope,$http,$location,serverDataMensajes,sesionFactory,galeriaFactory,idiomaFactory,marcasFactory){

		$(".li-menu").removeClass("active");
		$("#li_productos").addClass("active");
		$(".a-menu").removeClass("active");
		$("#marcas").addClass("active");

		$scope.titulo_pagina = "Línea";
		$scope.subtitulo_pagina = "Registro";
		$scope.titulo_registrar = "Registrar";
		$scope.titulo_cons = "Consultar";
		$scope.id_marca = "";
		$scope.base_url = $("#base_url").val();
		$scope.titulo_text = "Pulse aquí para ingresar el contenido"
		$scope.activo_img = "inactivo";
		$scope.searchMarcas = []
		$scope.borrar_imagen = []

		$scope.marcas = {
						'id': '',
						'idioma': '',
						'id_idioma' : '',
						'titulo' : '',
						'descripcion' : '',
						'id_imagen' : '',
						'imagen' : ''
		}

		$scope.consultar_idioma = function(){
			idiomaFactory.asignar_valores("",$scope.base_url)
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data;
				//console.log($scope.idioma);
			});
		}
		///////////////////////////////////////////////////////////////
		$scope.agregarWisi = function(){
			//$('#text_editor').data("wysihtml5").editor.clear();
			$scope.marcas.descripcion = $(".textarea_editor").val()
			$("#div_descripcion").html($scope.marcas.descripcion)
			$("#cerrarModal").click();
		}
		$scope.wisi_modal = function(){
			$("#wisiModal").modal("show")
			$(".textarea_editor").data("wysihtml5").editor.setValue($scope.marcas.descripcion)
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultar_galeria_img = function(){
			galeriaFactory.asignar_valores('2','',$scope.base_url);//ya que es galeria de imagenes
			galeriaFactory.cargar_galeria_fa(function(data){
				$scope.galery=data;
				//console.log($scope.galery);
			});
		}

		$scope.seleccione_img_principal = function(){
			$("#modal_img1").modal("show");
		}
		//PARA SELECCIONAR UNA IMAGEN Y MOSTRARLA EN EL CAMPO DE IMG
		$scope.seleccionar_imagen = function(event){
				var imagen = event.target.id;//Para capturar id
				//console.log(imagen);
				var vec = $("#"+imagen).attr("data");
				var vector_data = vec.split("|")
				var id_imagen = vector_data[0];
				var ruta = vector_data[1];

				$scope.borrar_imagen.push(id_imagen);
				$scope.activo_img = "activo"
				$scope.marcas.id_imagen = id_imagen
				$scope.marcas.imagen = ruta
				//alert($scope.marcas.id_imagen);
				//--
				$("#modal_img1").modal("hide");
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.limpiar_cajas_marcas = function(){
			$scope.marcas = {
							'id': '',
							'idioma': '',
							'id_idioma' : '',
							'titulo' : '',
							'descripcion' : '',
							'id_imagen' : '',
							'imagen' : ''
			}

			$scope.activo_img = "inactivo";
			$("#div_descripcion").html($scope.titulo_text);
			$('#textarea_editor').data("wysihtml5").editor.clear();
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.registrarMarcas = function(){
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			if($scope.validar_form()==true){
				//Para guardar
				if(($scope.marcas.id!="")&&($scope.marcas.id!=undefined)){
					$scope.modificar_marcas();
				}else{
					$scope.insertar_marcas();
				}

			}

			//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.validar_form = function(){
			//console.log($scope.marcas)
			if(($scope.marcas.id_idioma=="")||($scope.marcas.id_idioma=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
				return false;
			}else if($scope.marcas.titulo==""){
				mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
				return false;
			}else if($scope.marcas.descripcion==""){
				mostrar_notificacion("Campos no validos","Debe ingresar la descripción","warning");
				return false;
			}//else if(($scope.marcas.id_imagen=="NULL")||($scope.marcas.id_imagen=="")){
			// 	mostrar_notificacion("Campos no validos","Debe seleccionar la imagen","warning");
			// 	return false;
			// }
			else{
				return true;
			}
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.insertar_marcas = function(){
			$http.post($scope.base_url+"/Marcas/registrarMarcas",
			{
				'id' 	     : $scope.marcas.id,
				'id_idioma'  : $scope.marcas.id_idioma.id,
				'titulo'     : $scope.marcas.titulo,
				'descripcion': $scope.marcas.descripcion,
				'id_imagen'  : $scope.marcas.id_imagen
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cajas_marcas();
					desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe una noticia con ese titulo","warning");
					desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
					desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultarMarcaIndividual = function(){
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			marcasFactory.asignar_valores("",$scope.id_marca,$scope.base_url)
			marcasFactory.cargar_marcas(function(data){
				$scope.marcas=data[0];
				//console.log(data[0]);
				$("#div_descripcion").html($scope.marcas.descripcion)
				$scope.borrar_imagen.push($scope.marcas.id_imagen);
				$scope.activo_img = "activo"
				$scope.marcas.imagen = $scope.marcas.ruta
				$scope.titulo_registrar = "Modificar";
				$scope.subtitulo_pagina  = "Modificar slider";
				setTimeout(function(){
					$('#idioma > option[value="'+$scope.marcas.id_idioma+'"]').prop('selected', true);
				},300);
				$("#idioma").prop('disabled', true);
			});
			desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.modificar_marcas = function(){
			$http.post($scope.base_url+"/Marcas/modificarMarcas",
			{
				'id' 	     : $scope.marcas.id,
				'id_idioma'  : $scope.marcas.id_idioma,
				'titulo'     : $scope.marcas.titulo,
				'descripcion': $scope.marcas.descripcion,
				'id_imagen'  : $scope.marcas.id_imagen
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
					//$scope.limpiar_cajas_marcas();
					desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
				}else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
					desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
				}
			}).error(function(data,estatus){
				//console.log(data);
			});
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultar_idioma();
		$scope.consultar_galeria_img();

		$scope.id_marca = $("#id_marca").val();
			if($scope.id_marca){
				$scope.consultarMarcaIndividual();
			}else{
				$("#idioma").removeAttr("disabled");
			}
	})
