angular.module("ContentManagerApp")
	.controller("Tipo_prodController", function($scope,$http,$location,serverDataMensajes,sesionFactory,idiomaFactory,categoriaProdFactory,tipoProdFactory){
		$(".li-menu").removeClass("active");
        $("#li_productos").addClass("active");
        $(".a-menu").removeClass("active");
        $("#tipo_prod").addClass("active");

		$scope.titulo_pagina = "Tipo de Productos";
		$scope.subtitulo_pagina = "Registro";
		$scope.titulo_registrar = "Registrar";
		$scope.titulo_cons = "Consultar";
		$scope.base_url = $("#base_url").val();
		$scope.titulo_text = "Pulse aquí para ingresas el contenido"

		$scope.tipo_prod = {
						'id': '',
						'idioma': '',
						'id_idioma' : '',
						'categoria_prod': {
							"id":"",
							"titulo":"",
							"descripcion":""
						},
						'titulo' : '',
						'descripcion' : '',
		}
		//Cuerpo de metodos
		$scope.consultar_idioma = function(){
			idiomaFactory.asignar_valores("",$scope.base_url)
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data;
				//console.log($scope.idioma);
			});
		}
		///////////////////////////////////////////////////////////////
		$scope.cargar_catprod_idioma = function(){
			categoriaProdFactory.asignar_valor('1',$scope.tipo_prod.id_idioma.id);//ya que es galeria de imagenes
			categoriaProdFactory.cargar_catprod(function(data){
				$scope.catprod=data;
				//console.log($scope.catprod);
			});
		}
		$scope.cargar_catprod_idioma_edit = function(){
			categoriaProdFactory.asignar_valor('1',$scope.tipo_prod.id_idioma);//ya que es galeria de imagenes
			categoriaProdFactory.cargar_catprod(function(data){
				$scope.catprod=data;
				//console.log($scope.catprod);
			});
		}
		///////////////////////////////////////////////////////////////
		//WISIMODAL
		$scope.agregarWisi = function(){
			//$('#text_editor').data("wysihtml5").editor.clear();
			$scope.tipo_prod.descripcion = $(".textarea_editor").val()
			$("#div_descripcion").html($scope.tipo_prod.descripcion)
			$("#cerrarModal").click();
		}
		$scope.wisi_modal = function(){
			$("#wisiModal").modal("show")
			$(".textarea_editor").data("wysihtml5").editor.setValue($scope.tipo_prod.descripcion)
		}
		///////////////////////////////////////////////////////////////
		$scope.registrarTipoProd = function(){
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			if($scope.validar_form()==true){
				//Para guardar
				if(($scope.tipo_prod.id!="")&&($scope.tipo_prod.id!=undefined)){
					$scope.modificar_tipoProd();
				}else{
					$scope.insertar_tipoProd();
				}

			}
			//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}
		///////////////////////////////////////////////////////////////
		$scope.validar_form = function(){
			//console.log($scope.tipo_prod)
			if(($scope.tipo_prod.id_idioma=="")||($scope.tipo_prod.id_idioma=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
				return false;
			}/*else if(($scope.tipo_prod.categoria_prod.id=="")||($scope.tipo_prod.categoria_prod.id=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar una Categoría de Producto","warning");
				return false;
			}*/else if($scope.tipo_prod.titulo==""){
				mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
				return false;
			}else if($scope.tipo_prod.descripcion==""){
				mostrar_notificacion("Campos no validos","Debe ingresar la descripción","warning");
				return false;
			}
			else{
				return true;
			}
		}
		///////////////////////////////////////////////////////////////
		$scope.limpiar_cajas_tipoProd = function(){
			$scope.tipo_prod = {
							'id': '',
							'idioma': '',
							'id_idioma' : '',
							'categoria_prod': {
								"id":"",
								"titulo":"",
								"descripcion":""
							},
							'titulo' : '',
							'descripcion' : '',
			}

			$("#div_descripcion").html($scope.titulo_text);
			$('#textarea_editor').data("wysihtml5").editor.clear();
		}
		///////////////////////////////////////////////////////////////
		$scope.insertar_tipoProd = function(){
			$http.post($scope.base_url+"/tipo_prod/insertar_tipoProd",
			{
				'id' 	     		: $scope.tipo_prod.id,
				'id_idioma'  		: $scope.tipo_prod.id_idioma.id,
				'categoria_prod' 	: $scope.tipo_prod.categoria_prod.id,
				'titulo'     		: $scope.tipo_prod.titulo,
				'descripcion'		: $scope.tipo_prod.descripcion,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cajas_tipoProd();
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe una noticia con ese titulo","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}
		///////////////////////////////////////////////////////////////
		$scope.consultarTipoProdIndividual = function(){
			//console.log($scope.id_tipo_prod);
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			tipoProdFactory.asignar_valores("",$scope.id_tipo_prod,$scope.base_url)
			tipoProdFactory.cargar_tipoProd(function(data){
				$scope.tipo_prod=data[0];
				//console.log(data[0]);
				$("#div_descripcion").html($scope.tipo_prod.descripcion)
				$scope.idioma2 = $scope.tipo_prod.id_idioma
				if ($scope.idioma2) {
						$scope.cargar_catprod_idioma_edit();
				}
				setTimeout(function(){
					$('#idioma').val($scope.tipo_prod.id_idioma);
					$('#idioma > option[value="'+$scope.tipo_prod.id_idioma+'"]').prop('selected', true);
					$('#categoria_prod > option[value="'+$scope.tipo_prod.categoria_prod+'"]').prop('selected', true);
				},300);
				$("#idioma").prop('disabled', true);
				$scope.titulo_registrar = "Modificar";
				$scope.subtitulo_pagina  = "Modificar categorías";
			});
			desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.modificar_tipoProd = function(){
			$scope.categoria_producto = $("#categoria_prod").val();
			$http.post($scope.base_url+"/tipo_prod/modificar_tipoProd",
			{
				'id' 	     		: $scope.tipo_prod.id,
				'id_idioma'  		: $scope.tipo_prod.id_idioma,
				'categoria_prod' 	: $scope.categoria_producto,
				'titulo'     		: $scope.tipo_prod.titulo,
				'descripcion'		: $scope.tipo_prod.descripcion,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
					//$scope.limpiar_cajas_tipoProd();
				}else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultar_idioma();
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.id_tipo_prod  = $("#id_tipo_prod").val();
			if($scope.id_tipo_prod){
				$scope.consultarTipoProdIndividual();
			}
	})
