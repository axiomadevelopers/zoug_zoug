angular.module("ContentManagerApp")
    .controller("ConsultaCarritoController", function($scope,$http,$location,serverDataMensajes,categoriasFactory,sesionFactory,idiomaFactory,ConsultaCarritoFactory){
       
		$(".li-menu").removeClass("active");
        $("#li_productos").addClass("active");
        $(".a-menu").removeClass("active");
        $("#consulta_carrito").addClass("active");   
        $scope.titulo_pagina = "Consulta de Carrito de Compra";
        $scope.activo_img = "inactivo";
        //---------------------------------------------
        $scope.contactos = {
                                'id':'',
                                'nombres':'',
                                'email':'',
                                'telefono':'',
                                'mensaje':''
        }
        $scope.detalle = {
                                'id':'',
                                'nombres':'',
                                'email':'',
                                'telefono':'',
                                'mensaje':''
        }
        $scope.titulo_mensaje = [];
        //-----------------------------------------------
        $scope.nosotros_menu = "1";
        $scope.id_nosotros = "";
        $scope.base_url = $("#base_url").val();

        $scope.iniciar_datatable = function(){
			//--Iniciar DataTable
			$('#myTable').DataTable({
			    language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Entradas",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    },
                "order": [[ 0, "desc" ]]
			});
		}
        $scope.consultar_contactos = function(){
            ConsultaCarritoFactory.asignar_valores($scope.base_url)
            ConsultaCarritoFactory.cargar_contactos(function(data){
                $scope.carritoCompra=data;
                //console.log($scope.carritoCompra)
            });
        }
        //--Para visualizar resumen
        $scope.ver_detalle = function(index){
            $("#modal_mensaje").modal("show");
            $scope.titulo_mensaje = "Detalles del carrito"
            $scope.detalle = $scope.carritoCompra[index].detalles
            $scope.total = $scope.carritoCompra[index];
            //console.log($scope.total)
        }
        
		//----------------------------------------------------------------
        setTimeout(function(){
        	$scope.iniciar_datatable();
        },500);

        $scope.consultar_contactos();

        //----------------------------------------------------------------
    });
