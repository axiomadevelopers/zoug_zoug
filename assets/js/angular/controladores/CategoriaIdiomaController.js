angular.module("ContentManagerApp")
	.controller("CategoriaIdiomaController", function($scope,$http,$location,serverDataMensajes,sesionFactory,galeriaFactory,idiomaFactory,marcasFactory,coloresFactory,categprodESFactory,categprodENFactory,coloresESfactory,coloresENfactory,marcasESfactory,marcasENfactory,tiposESfactory,tiposENfactory){

		$(".li-menu").removeClass("active");
		$("#li_productos").addClass("active");
		$(".a-menu").removeClass("active");
		$("#categoria_idiomas").addClass("active");

		$scope.titulo_pagina = "Registrar Categorías Idiomas";
		$scope.subtitulo_pagina = "Registro";
		$scope.titulo_registrar = "Registrar";
		$scope.titulo_cons = "Consultar";
		$scope.base_url = $("#base_url").val();
		$scope.titulo_text = "Pulse aquí para ingresar el contenido"
		$scope.activo_img = "inactivo";
		$scope.searchMarcas = []
		$scope.borrar_imagen = []

			$scope.type = '';
			$scope.thing1 = '';
			$scope.thing2 = '';
			$scope.types = [{value:'Colores',label:'Colores',}, {value:'Marcas',label:'Marcas'}, {value:'Categoria Productos',label:'Categoría Productos'}, {value:'Tipo Producto',label:'Tipo Producto'}];
			$scope.things1 = [];
			$scope.things2 = [];

			$scope.putThings = function() {
				if($scope.type === 'Colores') {
					$scope.thing1 = '';
					$scope.thing2 = '';
					$scope.things1 = $scope.ColorES;
					$scope.things2 = $scope.ColorEN ;
				}
				if($scope.type === 'Marcas') {
					$scope.thing1 = '';
					$scope.thing2 = '';
					$scope.things1 = $scope.marcasES;
					$scope.things2 = $scope.marcasEN ;
				}
				if($scope.type === 'Categoria Productos') {
					$scope.thing1 = '';
					$scope.thing2 = '';
					$scope.things1 = $scope.categprodES;
					$scope.things2 = $scope.categprodEN ;
				}
				if($scope.type === 'Tipo Producto') {
					$scope.thing1 = '';
					$scope.thing2 = '';
					$scope.things1 = $scope.tiposES;
					$scope.things2 = $scope.tiposEN ;
				}

			}

			//	Categoria Producto //
		$scope.consultar_categoria_producto_es = function(){
			categprodESFactory.asignar_valores("",$scope.base_url)
			categprodESFactory.cargar_categprod(function(data){
				$scope.categprodES=data;
				//console.log($scope.categprodES);
			});
		}
		$scope.consultar_categoria_producto_en = function(){
			categprodENFactory.asignar_valores("",$scope.base_url)
			categprodENFactory.cargar_categprod(function(data){
				$scope.categprodEN=data;
				//console.log($scope.categprodEN);
			});
		}
		
		//	Colores //
		$scope.consultar_colores_es = function(){
			coloresESfactory.asignar_valores("",$scope.base_url)
			coloresESfactory.cargar_color(function(data){
				$scope.ColorES=data;
				//console.log($scope.ColorES);
			});
		}
		$scope.consultar_colores_en = function(){
			coloresENfactory.asignar_valores("",$scope.base_url)
			coloresENfactory.cargar_color(function(data){
				$scope.ColorEN=data;
				//console.log($scope.ColorEN);
			});
		}
		//	Marcas //
		$scope.consultar_marcas_es = function(){
			marcasESfactory.asignar_valores("",$scope.base_url)
			marcasESfactory.cargar_marcas(function(data){
				$scope.marcasES=data;
				//console.log($scope.marcasES);
			});
		}
		$scope.consultar_marcas_en = function(){
			marcasENfactory.asignar_valores("",$scope.base_url)
			marcasENfactory.cargar_marcas(function(data){
				$scope.marcasEN=data;
				//console.log($scope.marcasEN);
			});
		}
		//	Tipos Producto //
		$scope.consultar_tipos_es = function(){
			tiposESfactory.asignar_valores("",$scope.base_url)
			tiposESfactory.cargar_tipos(function(data){
				$scope.tiposES=data;
				//console.log($scope.tiposES);
			});
		}
		$scope.consultar_tipos_en = function(){
			tiposENfactory.asignar_valores("",$scope.base_url)
			tiposENfactory.cargar_tipos(function(data){
				$scope.tiposEN=data;
				//console.log($scope.tiposEN);
			});
		}
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.limpiar_cajas_color = function(){
			$scope.thing1='';
			$scope.thing2='';

			$scope.activo_img = "inactivo";
			$("#div_descripcion").html($scope.titulo_text);
			$('#textarea_editor').data("wysihtml5").editor.clear();
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.registrar = function(){

			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			if($scope.validar_form()==true){
				
					$scope.insertar();
			}
			//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
		}
			
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.validar_form = function(){
			//console.log($scope.color)
			if(($scope.type=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar una tabla","warning");
				return false;
			}else if($scope.thing1==""){
				mostrar_notificacion("Campos no validos","Debe seleccionar el equivalente en español","warning");
				return false;
			}else if($scope.thing2==""){
				mostrar_notificacion("Campos no validos","Debe seleccionar el equivalente en ingles","warning");
				return false;
			}//else if(($scope.color.id_imagen=="NULL")||($scope.color.id_imagen=="")){
			// 	mostrar_notificacion("Campos no validos","Debe seleccionar la imagen","warning");
			// 	return false;
			// }
			else{
				return true;
			}
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.insertar = function(){
			$http.post($scope.base_url+"/Categoria_idiomas/registrar",
			{
				'nombre_tabla' 	     : $scope.type,
				'id_categoria_es'  : $scope.thing1,
				'id_categoria_en': $scope.thing2,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cajas_color();
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe un registro con este item en ingles","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
				$mensajes["mensaje"] = "no_registro";

			}).error(function(data,estatus){
				console.log(data);
			});
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultarColorIndividual = function(){

			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			coloresFactory.asignar_valores("",$scope.id_color,$scope.base_url)
			coloresFactory.cargar_colores(function(data){
				$scope.color=data[0];
				//console.log(data[0]);
				$("#div_descripcion").html($scope.color.descripcion)
				$scope.borrar_imagen.push($scope.color.id_imagen);
				$scope.activo_img = "activo"
				$scope.color.imagen = $scope.color.ruta
				$scope.titulo_pagina = "Modificar color";
				$scope.subtitulo_pagina  = "Modificar";
				setTimeout(function(){
					$('#idioma > option[value="'+$scope.color.id_idioma+'"]').prop('selected', true);
				},300);
				$("#idioma").prop('disabled', true);
			});
			desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.modificar_colores = function(){
			$http.post($scope.base_url+"/Colores/modificarColores",
			{
                'id' 	     : $scope.color.id,
				'id_idioma'  : $scope.color.id_idioma.id,
				'descripcion': $scope.color.descripcion,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
					//$scope.limpiar_cajas_color();
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe ese color","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
				$mensajes["mensaje"] = "no_registro";

			}).error(function(data,estatus){
				//console.log(data);
			});
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultar_categoria_producto_es();
		$scope.consultar_categoria_producto_en();
		$scope.consultar_colores_es();
		$scope.consultar_colores_en();
		$scope.consultar_marcas_es();
		$scope.consultar_marcas_en();
		$scope.consultar_tipos_es();
		$scope.consultar_tipos_en();
		
        $scope.id_color = $("#id_color").val();

        if($scope.id_color){
              $scope.consultarColorIndividual();
		}
	})
