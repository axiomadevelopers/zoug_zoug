-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 02-04-2020 a las 00:03:46
-- Versión del servidor: 10.1.38-MariaDB-0+deb9u1
-- Versión de PHP: 7.0.33-0+deb9u7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_cms_ci`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auditoria`
--

CREATE TABLE `auditoria` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `modulo` int(11) NOT NULL,
  `accion` text NOT NULL,
  `ip` varchar(200) NOT NULL,
  `fecha_hora` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `auditoria`
--

INSERT INTO `auditoria` (`id`, `id_usuario`, `modulo`, `accion`, `ip`, `fecha_hora`) VALUES
(1, 8, 2, 'Agrego al carrito producto: Zoug Zoug Masculino Talla:S Color:BICOLOR', '190.103.28.234', '2019-10-24 09:53:00'),
(2, 8, 2, 'Agrego al carrito producto: Zoug Zoug Masculino Talla:L Color:BICOLOR', '190.103.28.234', '2019-10-24 09:59:00'),
(3, 8, 2, 'Se modifico la cantidad del producto: Zoug Zoug Masculino Talla:S Color:BICOLOR cantidad ingresada:1 cantidad anterior:2 cantidad en tabla: 98 cantidad nueva calculo: 1 cantidad total: 99', '190.103.28.234', '2019-10-24 10:00:00'),
(4, 8, 2, 'Se modifico la cantidad del producto: Zoug Zoug Masculino Talla:L Color:BICOLOR cantidad ingresada:2 cantidad anterior:3 cantidad en tabla: 197 cantidad nueva calculo: 1 cantidad total: 198', '190.103.28.234', '2019-10-24 10:00:00'),
(5, 8, 2, 'Creación de orden #id:86 con los siguientes items:  producto: Zoug Zoug Masculino Talla:L Color:BICOLOR, producto: Zoug Zoug Masculino Talla:S Color:BICOLOR,', '190.103.28.234', '2019-10-24 10:02:00'),
(6, 8, 2, 'Agrego al carrito producto: Zoug Zoug Masculino Talla:S Color:BICOLOR', '190.103.28.234', '2019-10-24 10:15:00'),
(7, 8, 2, 'Agrego al carrito producto: Zoug Zoug Masculino Talla:L Color:BICOLOR', '190.103.28.234', '2019-10-24 10:16:00'),
(8, 8, 2, 'Agrego al carrito producto: Zoug Zoug Femenino Talla:M Color:BICOLOR', '190.103.28.234', '2019-10-24 10:19:00'),
(9, 8, 2, 'Creación de orden #id:87 con los siguientes items:  producto: Zoug Zoug Femenino Talla:M Color:BICOLOR, producto: Zoug Zoug Masculino Talla:L Color:BICOLOR, producto: Zoug Zoug Masculino Talla:S Color:BICOLOR,', '190.103.28.234', '2019-10-24 10:20:00'),
(10, 4, 2, 'Agrego al carrito producto: Premium Masculino Talla:S Color:BICOLOR', '190.103.28.234', '2019-10-24 10:23:00'),
(11, 4, 2, 'Agrego al carrito producto: Premium Femenino Talla:S Color:BICOLOR', '190.103.28.234', '2019-10-24 10:24:00'),
(12, 4, 2, 'Creación de orden #id:88 con los siguientes items:  producto: Premium Femenino Talla:S Color:BICOLOR, producto: Premium Masculino Talla:S Color:BICOLOR,', '190.103.28.234', '2019-10-24 10:25:00'),
(13, 1, 1, 'Inicio de sesion', '190.103.28.234', '2019-10-24 10:28:00'),
(14, 1, 1, 'Inactivar cantidad producto: 199', '190.103.28.234', '2019-10-24 10:28:00'),
(15, 1, 1, 'Activar cantidad producto: 199', '190.103.28.234', '2019-10-24 10:28:00'),
(16, 4, 2, 'Agrego al carrito producto: Zoug Zoug Femenino II Talla:S Color:BICOLOR', '190.103.28.234', '2019-10-24 10:55:00'),
(17, 1, 1, 'Inicio de sesion', '190.103.28.234', '2019-10-24 11:10:00'),
(18, 4, 2, 'Agrego al carrito producto: Zoug Zoug Femenino II Talla:S Color:BICOLOR', '190.79.77.173', '2019-11-01 16:11:00'),
(19, 4, 2, 'Elimino producto Zoug Zoug Femenino II de talla:S , color: BICOLOR del carrito con id:65', '190.79.77.173', '2019-11-01 16:11:00'),
(20, 4, 2, 'Agrego al carrito producto: Zoug Zoug Femenino II Talla:S Color:BICOLOR', '190.79.77.173', '2019-11-01 16:12:00'),
(21, 4, 2, 'Creación de orden #id:89 con los siguientes items:  producto: Zoug Zoug Femenino II Talla:S Color:BICOLOR,', '190.79.77.173', '2019-11-01 16:13:00'),
(22, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-01 16:22:00'),
(23, 1, 1, 'Registro cantidad producto id: 200', '190.79.77.173', '2019-11-01 16:25:00'),
(24, 1, 1, 'Inactivar cantidad producto: 200', '190.79.77.173', '2019-11-01 16:25:00'),
(25, 1, 1, 'Eliminar cantidad producto: 200', '190.79.77.173', '2019-11-01 16:25:00'),
(26, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-11-01 16:26:00'),
(27, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-11 17:19:00'),
(28, 1, 1, 'Actualizacion de empresa nosotros id: 20', '190.79.77.173', '2019-11-11 17:22:00'),
(29, 1, 1, 'Inactivar empresa nosotros id: 20', '190.79.77.173', '2019-11-11 17:23:00'),
(30, 1, 1, 'Activar empresa nosotros id: 20', '190.79.77.173', '2019-11-11 17:23:00'),
(31, 1, 1, 'Actualizacion de slider id: 264', '190.79.77.173', '2019-11-11 17:23:00'),
(32, 1, 1, 'Actualizacion de slider id: 264', '190.79.77.173', '2019-11-11 17:23:00'),
(33, 1, 1, 'Inactivar slider id: 264', '190.79.77.173', '2019-11-11 17:24:00'),
(34, 1, 1, 'Activar slider id: 264', '190.79.77.173', '2019-11-11 17:24:00'),
(35, 1, 1, 'Actualizacion de marca id: 14', '190.79.77.173', '2019-11-11 17:24:00'),
(36, 1, 1, 'Inactivar marca id: 14', '190.79.77.173', '2019-11-11 17:25:00'),
(37, 1, 1, 'Activar marca id: 14', '190.79.77.173', '2019-11-11 17:25:00'),
(38, 1, 1, 'Actualizacion de categoría producto id: 11', '190.79.77.173', '2019-11-11 17:25:00'),
(39, 1, 1, 'Inactivar categoría producto id: 11', '190.79.77.173', '2019-11-11 17:25:00'),
(40, 1, 1, 'Activar categoría producto id: 11', '190.79.77.173', '2019-11-11 17:25:00'),
(41, 1, 1, 'Activar tipo de producto id: 45', '190.79.77.173', '2019-11-11 17:26:00'),
(42, 1, 1, 'Inactivar tipo de producto id: 45', '190.79.77.173', '2019-11-11 17:26:00'),
(43, 1, 1, 'Actualizacion tipo de producto id: 45', '190.79.77.173', '2019-11-11 17:27:00'),
(44, 1, 1, 'Inactivar color id: 11', '190.79.77.173', '2019-11-11 17:27:00'),
(45, 1, 1, 'Activar color id: 11', '190.79.77.173', '2019-11-11 17:27:00'),
(46, 1, 1, 'Actualizacion de color id: 11', '190.79.77.173', '2019-11-11 17:27:00'),
(47, 1, 1, 'Actualizacion de talla id: 8', '190.79.77.173', '2019-11-11 17:47:00'),
(48, 1, 1, 'Inactivar talla id: 8', '190.79.77.173', '2019-11-11 17:47:00'),
(49, 1, 1, 'Activar talla id: 8', '190.79.77.173', '2019-11-11 17:47:00'),
(50, 1, 1, 'Inactivar categorías idiomas id: 28', '190.79.77.173', '2019-11-11 17:47:00'),
(51, 1, 1, 'Inactivar categorías idiomas id: 39', '190.79.77.173', '2019-11-11 17:47:00'),
(52, 1, 1, 'Activar categorías idiomas id: 39', '190.79.77.173', '2019-11-11 17:47:00'),
(53, 1, 1, 'Activar categorías idiomas id: 28', '190.79.77.173', '2019-11-11 17:48:00'),
(54, 1, 1, 'Actualizacion de producto id: 122', '190.79.77.173', '2019-11-11 17:51:00'),
(55, 1, 1, 'Actualizacion de producto id: 122', '190.79.77.173', '2019-11-11 17:59:00'),
(56, 1, 1, 'Inactivar producto: 122', '190.79.77.173', '2019-11-11 17:59:00'),
(57, 1, 1, 'Activar producto: 122', '190.79.77.173', '2019-11-11 17:59:00'),
(58, 1, 1, 'Inactivar cantidad producto: 199', '190.79.77.173', '2019-11-11 18:00:00'),
(59, 1, 1, 'Activar cantidad producto: 199', '190.79.77.173', '2019-11-11 18:00:00'),
(60, 1, 1, 'Actualizar noticia id: 15 titulo: PRUEBA 4', '190.79.77.173', '2019-11-11 18:00:00'),
(61, 1, 1, 'Inactivar noticia: 15', '190.79.77.173', '2019-11-11 18:01:00'),
(62, 1, 1, 'Activar noticia: 15', '190.79.77.173', '2019-11-11 18:01:00'),
(63, 1, 1, 'Inactivar noticia: 15', '190.79.77.173', '2019-11-11 18:01:00'),
(64, 1, 1, 'Activar noticia: 15', '190.79.77.173', '2019-11-11 18:01:00'),
(65, 1, 1, 'Actualizacion redes sociales id: 11', '190.79.77.173', '2019-11-11 18:02:00'),
(66, 1, 1, 'Actualizar footer id: 4', '190.79.77.173', '2019-11-11 18:03:00'),
(67, 1, 1, 'Inactivar footer: 3', '190.79.77.173', '2019-11-11 18:03:00'),
(68, 1, 1, 'Activar footer: 3', '190.79.77.173', '2019-11-11 18:03:00'),
(69, 1, 1, 'Modificar categtorias cms id: 22', '190.79.77.173', '2019-11-11 18:03:00'),
(70, 1, 1, 'Inactivar categorias: 22', '190.79.77.173', '2019-11-11 18:03:00'),
(71, 1, 1, 'Activar categorias: 22', '190.79.77.173', '2019-11-11 18:03:00'),
(72, 1, 1, 'Modificar categtorias cms id: 22', '190.79.77.173', '2019-11-11 18:06:00'),
(73, 1, 1, 'Inactivar categorias: 22', '190.79.77.173', '2019-11-11 18:06:00'),
(74, 1, 1, 'Activar categorias: 22', '190.79.77.173', '2019-11-11 18:06:00'),
(75, 1, 1, 'Eliminar galeria  id: 113', '190.79.77.173', '2019-11-11 18:08:00'),
(76, 1, 1, 'Modificar usuarios id: 1', '190.79.77.173', '2019-11-11 18:08:00'),
(77, 1, 1, 'Inactivar usuarios: 1', '190.79.77.173', '2019-11-11 18:08:00'),
(78, 1, 1, 'Activar usuarios: 1', '190.79.77.173', '2019-11-11 18:08:00'),
(79, 1, 1, 'Modificar descripcion id: 1', '190.79.77.173', '2019-11-11 18:12:00'),
(80, 1, 1, 'Registro palabras claves id: 103', '190.79.77.173', '2019-11-11 18:14:00'),
(81, 1, 1, 'Modificar palabras claves id: 66', '190.79.77.173', '2019-11-11 18:28:00'),
(82, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-11-11 18:28:00'),
(83, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-12 09:11:00'),
(84, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-20 11:13:00'),
(85, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-11-20 12:38:00'),
(86, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-20 12:38:00'),
(87, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-20 12:46:00'),
(88, 1, 1, 'Inicio de sesion', '75.51.30.41', '2019-11-20 12:53:00'),
(89, 1, 1, 'Cerrar de sesion', '75.51.30.41', '2019-11-20 13:32:00'),
(90, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-21 09:03:00'),
(91, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-11-21 09:14:00'),
(92, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-21 09:17:00'),
(93, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-11-21 09:24:00'),
(94, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-21 09:24:00'),
(95, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-11-21 09:40:00'),
(96, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-21 09:41:00'),
(97, 1, 1, 'Registro usuarios id: 10', '190.79.77.173', '2019-11-21 09:46:00'),
(98, 1, 1, 'Registro usuarios id: 11', '190.79.77.173', '2019-11-21 09:47:00'),
(99, 1, 1, 'Modificar usuarios id: 11', '190.79.77.173', '2019-11-21 09:47:00'),
(100, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-11-21 09:47:00'),
(101, 11, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-21 09:48:00'),
(102, 11, 1, 'Activar tipo de producto id: 45', '190.79.77.173', '2019-11-21 09:49:00'),
(103, 11, 1, 'Activar tipo de producto id: 42', '190.79.77.173', '2019-11-21 09:49:00'),
(104, 11, 1, 'Cerrar de sesion', '190.79.77.173', '2019-11-21 09:54:00'),
(105, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-21 09:54:00'),
(106, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-11-21 10:12:00'),
(107, 11, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-21 10:13:00'),
(108, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-21 10:13:00'),
(109, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-11-21 10:15:00'),
(110, 11, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-21 10:15:00'),
(111, 8, 2, 'Agrego al carrito producto: Zoug Zoug Masculino Talla:S Color:BICOLOR', '190.79.77.173', '2019-11-21 10:23:00'),
(112, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-21 10:24:00'),
(113, 1, 2, 'Agrego al carrito producto: Zoug Zoug Masculino Talla:S Color:BICOLOR', '190.79.77.173', '2019-11-21 10:28:00'),
(114, 1, 2, 'Creación de orden #id:90 con los siguientes items:  producto: Zoug Zoug Masculino Talla:S Color:BICOLOR,', '190.79.77.173', '2019-11-21 10:30:00'),
(115, 1, 2, 'Agrego al carrito producto: Zoug Zoug Femenino II Talla:S Color:BICOLOR', '190.79.77.173', '2019-11-21 10:35:00'),
(116, 1, 2, 'Se modifico la cantidad del producto: Zoug Zoug Femenino II Talla:S Color:BICOLOR cantidad ingresada:1 cantidad anterior:2 cantidad en tabla: 93 cantidad nueva calculo: 1 cantidad total: 94', '190.79.77.173', '2019-11-21 10:35:00'),
(117, 1, 2, 'Agrego al carrito producto: Zoug Zoug Femenino Talla:S Color:BICOLOR', '190.79.77.173', '2019-11-21 10:36:00'),
(118, 1, 2, 'Se modifico la cantidad del producto: Zoug Zoug Femenino II Talla:S Color:BICOLOR cantidad ingresada:2 cantidad anterior:1 cantidad en tabla: 94 cantidad nueva calculo: 1 cantidad total: 93', '190.79.77.173', '2019-11-21 10:36:00'),
(119, 1, 2, 'Se modifico la cantidad del producto: Zoug Zoug Femenino Talla:S Color:BICOLOR cantidad ingresada:3 cantidad anterior:2 cantidad en tabla: 96 cantidad nueva calculo: 1 cantidad total: 95', '190.79.77.173', '2019-11-21 10:36:00'),
(120, 1, 2, 'Elimino producto Zoug Zoug Femenino II de talla:S , color: BICOLOR del carrito con id:69', '190.79.77.173', '2019-11-21 10:37:00'),
(121, 1, 2, 'Creación de orden #id:91 con los siguientes items:  producto: Zoug Zoug Femenino Talla:S Color:BICOLOR,', '190.79.77.173', '2019-11-21 10:44:00'),
(122, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-21 10:45:00'),
(123, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-27 09:43:00'),
(124, 4, 2, 'Agrego al carrito producto: Zoug Zoug Femenino II Talla:S Color:BICOLOR', '190.79.77.173', '2019-11-28 07:02:00'),
(125, 4, 2, 'Creación de orden #id:92 con los siguientes items:  producto: Zoug Zoug Femenino II Talla:S Color:BICOLOR,', '190.79.77.173', '2019-11-28 07:03:00'),
(126, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-03 16:20:00'),
(127, 1, 1, 'Actualizacion de slider id: 264', '190.79.77.173', '2019-12-03 16:21:00'),
(128, 1, 1, 'Actualizacion de slider id: 264', '190.79.77.173', '2019-12-03 16:25:00'),
(129, 1, 1, 'Actualizacion de slider id: 268', '190.79.77.173', '2019-12-03 16:25:00'),
(130, 1, 1, 'Actualizacion de slider id: 267', '190.79.77.173', '2019-12-03 16:25:00'),
(131, 1, 1, 'Actualizacion de slider id: 269', '190.79.77.173', '2019-12-03 16:26:00'),
(132, 1, 1, 'Actualizacion de slider id: 265', '190.79.77.173', '2019-12-03 16:26:00'),
(133, 1, 1, 'Actualizacion de slider id: 266', '190.79.77.173', '2019-12-03 16:26:00'),
(134, 0, 1, 'Actualizacion de slider id: 266', '190.79.77.173', '2019-12-03 16:35:00'),
(135, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-04 08:22:00'),
(136, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-04 08:28:00'),
(137, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-04 08:31:00'),
(138, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-04 08:37:00'),
(139, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-04 08:40:00'),
(140, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-04 08:56:00'),
(141, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-04 09:01:00'),
(142, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-04 09:01:00'),
(143, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-04 09:01:00'),
(144, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-12-04 09:02:00'),
(145, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-04 09:03:00'),
(146, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-12-04 09:04:00'),
(147, 11, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-04 09:04:00'),
(148, 10, 2, 'Agrego al carrito producto: Liqui Liqui Masculino Talla:S Color:BICOLOR', '190.79.77.173', '2019-12-04 10:19:00'),
(149, 10, 2, 'Creación de orden #id:93 con los siguientes items:  producto: Liqui Liqui Masculino Talla:S Color:BICOLOR,', '190.79.77.173', '2019-12-04 10:22:00'),
(150, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-05 19:28:00'),
(151, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-12-05 19:31:00'),
(152, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-05 19:31:00'),
(153, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-12-05 19:32:00'),
(154, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-05 19:32:00'),
(155, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-09 12:15:00'),
(156, 1, 1, 'Actualizacion de empresa nosotros id: 20', '190.79.77.173', '2019-12-09 12:17:00'),
(157, 1, 1, 'Actualizacion de empresa nosotros id: 21', '190.79.77.173', '2019-12-09 12:18:00'),
(158, 0, 1, 'Cerrar de sesion', '190.79.77.173', '2019-12-09 13:31:00'),
(159, 8, 2, 'Agrego al carrito producto: Liqui Liqui Masculino Talla:S Color:BICOLOR', '190.79.77.173', '2019-12-11 15:43:00'),
(160, 8, 2, 'Se modifico la cantidad del producto: Liqui Liqui Masculino Talla:S Color:BICOLOR cantidad ingresada:1 cantidad anterior:2 cantidad en tabla: 96 cantidad nueva calculo: 1 cantidad total: 97', '190.79.77.173', '2019-12-11 15:44:00'),
(161, 8, 2, 'Se modifico la cantidad del producto: Zoug Zoug Masculino Talla:S Color:BICOLOR cantidad ingresada:0 cantidad anterior:1 cantidad en tabla: 94 cantidad nueva calculo: 1 cantidad total: 94', '190.79.77.173', '2019-12-11 15:44:00'),
(162, 8, 2, 'Creación de orden #id:94 con los siguientes items:  producto: Liqui Liqui Masculino Talla:S Color:BICOLOR, producto: Zoug Zoug Masculino Talla:S Color:BICOLOR,', '190.79.77.173', '2019-12-11 15:45:00'),
(163, 8, 2, 'Agrego al carrito producto: Zoug Zoug Femenino II Talla:S Color:BICOLOR', '190.79.77.173', '2019-12-11 16:38:00'),
(164, 8, 2, 'Creación de orden #id:95 con los siguientes items:  producto: Zoug Zoug Femenino II Talla:S Color:BICOLOR,', '190.79.77.173', '2019-12-11 16:40:00'),
(165, 4, 2, 'Agrego al carrito producto: Premium Masculino Talla:S Color:BICOLOR', '190.79.77.173', '2019-12-11 16:49:00'),
(166, 4, 2, 'Se modifico la cantidad del producto: Premium Masculino Talla:S Color:BICOLOR cantidad ingresada:0 cantidad anterior:1 cantidad en tabla: 98 cantidad nueva calculo:  cantidad total: 98', '190.79.77.173', '2019-12-11 16:49:00'),
(167, 4, 2, 'Creación de orden #id:96 con los siguientes items:  producto: Premium Masculino Talla:S Color:BICOLOR,', '190.79.77.173', '2019-12-11 16:51:00'),
(168, 8, 2, 'Agrego al carrito producto: Liqui Liqui Femenino Talla:M Color:BICOLOR', '190.79.77.173', '2019-12-11 16:59:00'),
(169, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-11 17:02:00'),
(170, 1, 2, 'Agrego al carrito producto: Liqui Liqui Femenino Talla:M Color:BICOLOR', '190.79.77.173', '2019-12-11 17:22:00'),
(171, 1, 2, 'Se modifico la cantidad del producto: Liqui Liqui Femenino Talla:M Color:BICOLOR cantidad ingresada:1 cantidad anterior:2 cantidad en tabla: 98 cantidad nueva calculo: 1 cantidad total: 99', '190.79.77.173', '2019-12-11 17:23:00'),
(172, 1, 2, 'Agrego al carrito producto: Liqui Liqui Femenino Talla:M Color:BICOLOR', '190.79.77.173', '2019-12-11 17:30:00'),
(173, 1, 2, 'Se modifico la cantidad del producto: Liqui Liqui Femenino Talla:M Color:BICOLOR cantidad ingresada:0 cantidad anterior:3 cantidad en tabla: 248 cantidad nueva calculo:  cantidad total: 248', '190.79.77.173', '2019-12-11 17:32:00'),
(174, 1, 2, 'Creación de orden #id:97 con los siguientes items:  producto: Liqui Liqui Femenino Talla:M Color:BICOLOR,', '190.79.77.173', '2019-12-11 17:52:00'),
(175, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-12-11 18:00:00'),
(176, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-12 06:20:00'),
(177, 1, 1, 'Registro galeria  id: 143', '190.79.77.173', '2019-12-12 06:21:00'),
(178, 1, 1, 'Registro galeria  id: 144', '190.79.77.173', '2019-12-12 06:22:00'),
(179, 1, 1, 'Registro galeria  id: 145', '190.79.77.173', '2019-12-12 06:23:00'),
(180, 1, 1, 'Actualizacion de slider id: 264', '190.79.77.173', '2019-12-12 06:24:00'),
(181, 1, 1, 'Actualizacion de slider id: 267', '190.79.77.173', '2019-12-12 06:24:00'),
(182, 0, 1, 'Actualizacion de slider id: 265', '190.79.77.173', '2019-12-12 06:25:00'),
(183, 0, 1, 'Actualizacion de slider id: 265', '190.79.77.173', '2019-12-12 06:25:00'),
(184, 0, 1, 'Actualizacion de slider id: 264', '190.79.77.173', '2019-12-12 06:26:00'),
(185, 0, 1, 'Actualizacion de slider id: 267', '190.79.77.173', '2019-12-12 06:28:00'),
(186, 0, 1, 'Actualizacion de slider id: 265', '190.79.77.173', '2019-12-12 06:29:00'),
(187, 0, 1, 'Actualizacion de slider id: 265', '190.79.77.173', '2019-12-12 06:30:00'),
(188, 0, 1, 'Actualizacion de slider id: 265', '190.79.77.173', '2019-12-12 06:32:00'),
(189, 0, 1, 'Actualizacion de slider id: 264', '190.79.77.173', '2019-12-12 06:34:00'),
(190, 0, 1, 'Actualizacion de slider id: 267', '190.79.77.173', '2019-12-12 06:34:00'),
(191, 0, 1, 'Actualizacion de slider id: 267', '190.79.77.173', '2019-12-12 06:35:00'),
(192, 0, 1, 'Actualizacion de slider id: 267', '190.79.77.173', '2019-12-12 06:38:00'),
(193, 0, 1, 'Actualizacion de slider id: 267', '190.79.77.173', '2019-12-12 06:56:00'),
(194, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-12 08:41:00'),
(195, 1, 1, 'Actualizacion de slider id: 268', '190.79.77.173', '2019-12-12 08:42:00'),
(196, 1, 1, 'Inicio de sesion', '190.202.241.91', '2019-12-12 12:53:00'),
(197, 1, 1, 'Registro galeria  id: 146', '190.202.241.91', '2019-12-12 12:55:00'),
(198, 1, 1, 'Inicio de sesion', '190.202.241.91', '2019-12-12 13:06:00'),
(199, 1, 1, 'Actualizacion de slider id: 268', '190.202.241.91', '2019-12-12 13:07:00'),
(200, 1, 1, 'Actualizacion de slider id: 264', '190.202.241.91', '2019-12-12 13:08:00'),
(201, 1, 1, 'Actualizacion de slider id: 265', '190.202.241.91', '2019-12-12 13:08:00'),
(202, 1, 1, 'Actualizacion de slider id: 266', '190.202.241.91', '2019-12-12 13:08:00'),
(203, 1, 1, 'Actualizacion de slider id: 267', '190.202.241.91', '2019-12-12 13:09:00'),
(204, 1, 1, 'Actualizacion de slider id: 269', '190.202.241.91', '2019-12-12 13:09:00'),
(205, 1, 1, 'Eliminar galeria  id: 146', '190.202.241.91', '2019-12-12 13:10:00'),
(206, 1, 1, 'Eliminar galeria  id: 145', '190.202.241.91', '2019-12-12 13:11:00'),
(207, 1, 1, 'Eliminar galeria  id: 144', '190.202.241.91', '2019-12-12 13:11:00'),
(208, 1, 1, 'Eliminar galeria  id: 143', '190.202.241.91', '2019-12-12 13:11:00'),
(209, 1, 1, 'Eliminar galeria  id: 111', '190.202.241.91', '2019-12-12 13:11:00'),
(210, 1, 1, 'Eliminar galeria  id: 110', '190.202.241.91', '2019-12-12 13:11:00'),
(211, 1, 1, 'Eliminar galeria  id: 109', '190.202.241.91', '2019-12-12 13:11:00'),
(212, 1, 1, 'Registro galeria  id: 147', '190.202.241.91', '2019-12-12 13:12:00'),
(213, 1, 1, 'Registro galeria  id: 148', '190.202.241.91', '2019-12-12 13:12:00'),
(214, 1, 1, 'Registro galeria  id: 149', '190.202.241.91', '2019-12-12 13:13:00'),
(215, 1, 1, 'Actualizacion de slider id: 267', '190.202.241.91', '2019-12-12 13:14:00'),
(216, 1, 1, 'Actualizacion de slider id: 269', '190.202.241.91', '2019-12-12 13:14:00'),
(217, 1, 1, 'Actualizacion de slider id: 265', '190.202.241.91', '2019-12-12 13:14:00'),
(218, 1, 1, 'Actualizacion de slider id: 266', '190.202.241.91', '2019-12-12 13:14:00'),
(219, 1, 1, 'Actualizacion de slider id: 264', '190.202.241.91', '2019-12-12 13:14:00'),
(220, 1, 1, 'Actualizacion de slider id: 268', '190.202.241.91', '2019-12-12 13:15:00'),
(221, 0, 1, 'Actualizacion de slider id: 267', '190.202.241.91', '2019-12-12 13:15:00'),
(222, 0, 1, 'Actualizacion de slider id: 269', '190.202.241.91', '2019-12-12 13:15:00'),
(223, 0, 1, 'Actualizacion de slider id: 269', '190.202.241.91', '2019-12-12 13:16:00'),
(224, 0, 1, 'Actualizacion de slider id: 264', '190.202.241.91', '2019-12-12 13:16:00'),
(225, 0, 1, 'Actualizacion de slider id: 266', '190.202.241.91', '2019-12-12 13:16:00'),
(226, 0, 1, 'Actualizacion de slider id: 265', '190.202.241.91', '2019-12-12 13:17:00'),
(227, 0, 1, 'Actualizacion de slider id: 266', '190.202.241.91', '2019-12-12 13:18:00'),
(228, 0, 1, 'Actualizacion de slider id: 264', '190.202.241.91', '2019-12-12 13:18:00'),
(229, 0, 1, 'Cerrar de sesion', '190.103.28.234', '2019-12-12 15:24:00'),
(230, 1, 1, 'Inicio de sesion', '190.103.28.234', '2019-12-12 15:24:00'),
(231, 1, 1, 'Inicio de sesion', '190.103.28.234', '2019-12-13 08:08:00'),
(232, 1, 1, 'Inicio de sesion', '190.103.28.234', '2019-12-13 08:22:00'),
(233, 1, 1, 'Registro galeria  id: 150', '190.103.28.234', '2019-12-13 08:22:00'),
(234, 1, 1, 'Eliminar galeria  id: 149', '190.103.28.234', '2019-12-13 08:23:00'),
(235, 1, 1, 'Actualizacion de slider id: 265', '190.103.28.234', '2019-12-13 08:23:00'),
(236, 1, 1, 'Actualizacion de slider id: 266', '190.103.28.234', '2019-12-13 08:24:00'),
(237, 1, 1, 'Inicio de sesion', '190.103.28.234', '2019-12-13 16:31:00'),
(238, 1, 1, 'Actualizacion de slider id: 267', '190.103.28.234', '2019-12-13 16:32:00'),
(239, 1, 1, 'Actualizacion de slider id: 269', '190.103.28.234', '2019-12-13 16:32:00'),
(240, 1, 1, 'Actualizacion de slider id: 269', '190.103.28.234', '2019-12-13 16:32:00'),
(241, 1, 1, 'Registro de slider id:272,titulo:prueba', '190.103.28.234', '2019-12-13 16:33:00'),
(242, 1, 1, 'Inactivar slider id: 0', '190.103.28.234', '2019-12-13 16:33:00'),
(243, 1, 1, 'Actualizacion de slider id: 0', '190.103.28.234', '2019-12-13 16:33:00'),
(244, 1, 1, 'Inicio de sesion', '190.103.28.234', '2019-12-13 16:54:00'),
(245, 1, 1, 'Actualizacion de slider id: 267', '190.103.28.234', '2019-12-13 16:56:00'),
(246, 1, 1, 'Eliminar slider id: 0', '190.103.28.234', '2019-12-13 16:57:00'),
(247, 1, 1, 'Actualizacion de empresa nosotros id: 20', '190.103.28.234', '2019-12-13 17:02:00'),
(248, 1, 1, 'Actualizacion de empresa nosotros id: 20', '190.103.28.234', '2019-12-13 17:02:00'),
(249, 1, 1, 'Actualizacion de empresa nosotros id: 20', '190.103.28.234', '2019-12-13 17:02:00'),
(250, 1, 1, 'Actualizacion de marca id: 14', '190.103.28.234', '2019-12-13 17:08:00'),
(251, 1, 1, 'Actualizacion de categoría producto id: 11', '190.103.28.234', '2019-12-13 17:13:00'),
(252, 1, 1, 'Actualizacion de categoría producto id: 11', '190.103.28.234', '2019-12-13 17:13:00'),
(253, 1, 1, 'Actualizacion de categoría producto id: 11', '190.103.28.234', '2019-12-13 17:13:00'),
(254, 1, 1, 'Actualizacion de categoría producto id: 11', '190.103.28.234', '2019-12-13 17:13:00'),
(255, 1, 1, 'Actualizacion de categoría producto id: 11', '190.103.28.234', '2019-12-13 17:13:00'),
(256, 1, 1, 'Actualizacion de categoría producto id: 11', '190.103.28.234', '2019-12-13 17:13:00'),
(257, 1, 1, 'Actualizacion de categoría producto id: 11', '190.103.28.234', '2019-12-13 17:13:00'),
(258, 1, 1, 'Actualizacion de marca id: 14', '190.103.28.234', '2019-12-13 17:13:00'),
(259, 1, 1, 'Actualizacion tipo de producto id: 45', '190.103.28.234', '2019-12-13 17:16:00'),
(260, 1, 1, 'Actualizacion tipo de producto id: 45', '190.103.28.234', '2019-12-13 17:16:00'),
(261, 1, 1, 'Actualizacion tipo de producto id: 42', '190.103.28.234', '2019-12-13 17:16:00'),
(262, 1, 1, 'Actualizacion de categoría producto id: 11', '190.103.28.234', '2019-12-13 17:17:00'),
(263, 1, 1, 'Actualizacion de categoría producto id: 11', '190.103.28.234', '2019-12-13 17:18:00'),
(264, 1, 1, 'Actualizacion de categoría producto id: 11', '190.103.28.234', '2019-12-13 17:18:00'),
(265, 1, 1, 'Actualizacion tipo de producto id: 45', '190.103.28.234', '2019-12-13 17:18:00'),
(266, 1, 1, 'Actualizacion tipo de producto id: 45', '190.103.28.234', '2019-12-13 17:18:00'),
(267, 1, 1, 'Actualizacion tipo de producto id: 45', '190.103.28.234', '2019-12-13 17:18:00'),
(268, 1, 1, 'Actualizacion de color id: 11', '190.103.28.234', '2019-12-13 17:19:00'),
(269, 1, 1, 'Actualizacion de color id: 11', '190.103.28.234', '2019-12-13 17:29:00'),
(270, 1, 1, 'Actualizacion tipo de producto id: 45', '190.103.28.234', '2019-12-13 17:29:00'),
(271, 1, 1, 'Modificar descripcion id: 1', '190.103.28.234', '2019-12-13 18:08:00'),
(272, 1, 1, 'Modificar descripcion id: 1', '190.103.28.234', '2019-12-13 18:08:00'),
(273, 1, 1, 'Modificar descripcion id: 1', '190.103.28.234', '2019-12-13 18:08:00'),
(274, 1, 1, 'Inicio de sesion', '190.103.28.234', '2019-12-16 13:41:00'),
(275, 1, 1, 'Registro galeria  id: 151', '190.103.28.234', '2019-12-16 13:43:00'),
(276, 1, 1, 'Registro galeria  id: 152', '190.103.28.234', '2019-12-16 13:45:00'),
(277, 1, 1, 'Registro galeria  id: 153', '190.103.28.234', '2019-12-16 13:48:00'),
(278, 1, 1, 'Eliminar galeria  id: 148', '190.103.28.234', '2019-12-16 13:50:00'),
(279, 1, 1, 'Eliminar galeria  id: 150', '190.103.28.234', '2019-12-16 13:50:00'),
(280, 1, 1, 'Eliminar galeria  id: 147', '190.103.28.234', '2019-12-16 13:50:00'),
(281, 1, 1, 'Actualizacion de slider id: 267', '190.103.28.234', '2019-12-16 13:52:00'),
(282, 1, 1, 'Actualizacion de slider id: 267', '190.103.28.234', '2019-12-16 13:52:00'),
(283, 1, 1, 'Actualizacion de slider id: 269', '190.103.28.234', '2019-12-16 13:53:00'),
(284, 1, 1, 'Actualizacion de slider id: 265', '190.103.28.234', '2019-12-16 13:53:00'),
(285, 1, 1, 'Actualizacion de slider id: 266', '190.103.28.234', '2019-12-16 13:53:00'),
(286, 1, 1, 'Actualizacion de slider id: 264', '190.103.28.234', '2019-12-16 13:53:00'),
(287, 1, 1, 'Actualizacion de slider id: 268', '190.103.28.234', '2019-12-16 13:54:00'),
(288, 44, 2, 'Agrego al carrito producto: Zoug Zoug Masculino Talla:S Color:BICOLOR', '190.103.28.234', '2020-01-02 16:09:00'),
(289, 46, 2, 'Agrego al carrito producto: Zoug Zoug Femenino Talla:S Color:BICOLOR', '190.103.28.234', '2020-01-20 13:31:00'),
(290, 46, 2, 'Creación de orden #id:98 con los siguientes items:  producto: Zoug Zoug Femenino Talla:S Color:BICOLOR,', '190.103.28.234', '2020-01-20 13:33:00'),
(291, 46, 2, 'Agrego al carrito producto: Zoug Zoug Masculino Talla:S Color:BICOLOR', '190.103.28.234', '2020-01-20 13:34:00'),
(292, 46, 2, 'Creación de orden #id:99 con los siguientes items:  producto: Zoug Zoug Masculino Talla:S Color:BICOLOR,', '190.103.28.234', '2020-01-20 13:36:00'),
(293, 44, 2, 'Agrego al carrito producto: Premium Masculino Talla:S Color:BICOLOR', '186.167.248.187', '2020-01-20 21:30:00'),
(294, 44, 2, 'Creación de orden #id:100 con los siguientes items:  producto: Premium Masculino Talla:S Color:BICOLOR, producto: Zoug Zoug Masculino Talla:S Color:BICOLOR,', '186.167.248.187', '2020-01-20 21:32:00'),
(295, 1, 1, 'Inicio de sesion', '190.103.28.234', '2020-01-27 19:51:00'),
(296, 1, 1, 'Registro de slider id:272,titulo:Prueba slider 4', '190.103.28.234', '2020-01-27 20:02:00'),
(297, 1, 1, 'Eliminar slider id: 0', '190.103.28.234', '2020-01-27 20:03:00'),
(298, 1, 1, 'Inicio de sesion', '190.103.28.234', '2020-02-05 09:12:00'),
(299, 1, 1, 'Registro galeria  id: 154', '190.103.28.234', '2020-02-05 09:13:00'),
(300, 1, 1, 'Registro galeria  id: 155', '190.103.28.234', '2020-02-05 09:13:00'),
(301, 1, 1, 'Registro galeria  id: 156', '190.103.28.234', '2020-02-05 09:14:00'),
(302, 1, 1, 'Registro galeria  id: 157', '190.103.28.234', '2020-02-05 09:15:00'),
(303, 1, 1, 'Registro galeria  id: 158', '190.103.28.234', '2020-02-05 09:15:00'),
(304, 1, 1, 'Registro galeria  id: 159', '190.103.28.234', '2020-02-05 09:16:00'),
(305, 1, 1, 'Registro galeria  id: 160', '190.103.28.234', '2020-02-05 09:19:00'),
(306, 1, 1, 'Registro galeria  id: 161', '190.103.28.234', '2020-02-05 09:19:00'),
(307, 1, 1, 'Registro galeria  id: 162', '190.103.28.234', '2020-02-05 09:20:00'),
(308, 1, 1, 'Registro galeria  id: 163', '190.103.28.234', '2020-02-05 09:21:00'),
(309, 1, 1, 'Registro galeria  id: 164', '190.103.28.234', '2020-02-05 09:21:00'),
(310, 1, 1, 'Registro galeria  id: 165', '190.103.28.234', '2020-02-05 09:21:00'),
(311, 1, 1, 'Registro galeria  id: 166', '190.103.28.234', '2020-02-05 09:22:00'),
(312, 1, 1, 'Registro galeria  id: 167', '190.103.28.234', '2020-02-05 09:22:00'),
(313, 1, 1, 'Inicio de sesion', '190.103.28.234', '2020-02-05 11:18:00'),
(314, 1, 1, 'Inicio de sesion', '190.103.28.234', '2020-02-05 11:50:00'),
(315, 1, 1, 'Inicio de sesion', '190.103.28.234', '2020-02-05 18:33:00'),
(316, 1, 1, 'Inicio de sesion', '190.103.28.234', '2020-02-05 18:43:00'),
(317, 1, 1, 'Actualizacion de producto id: 122', '190.103.28.234', '2020-02-05 18:50:00'),
(318, 1, 1, 'Actualizacion de producto id: 121', '190.103.28.234', '2020-02-05 18:51:00'),
(319, 1, 1, 'Actualizacion de producto id: 120', '190.103.28.234', '2020-02-05 18:52:00'),
(320, 1, 1, 'Actualizacion de producto id: 120', '190.103.28.234', '2020-02-05 18:56:00'),
(321, 1, 1, 'Actualizacion de producto id: 119', '190.103.28.234', '2020-02-05 18:56:00'),
(322, 1, 1, 'Actualizacion de producto id: 118', '190.103.28.234', '2020-02-05 18:57:00'),
(323, 1, 1, 'Actualizacion de producto id: 118', '190.103.28.234', '2020-02-05 18:57:00'),
(324, 1, 1, 'Actualizacion de producto id: 121', '190.103.28.234', '2020-02-05 18:58:00'),
(325, 1, 1, 'Actualizacion de producto id: 117', '190.103.28.234', '2020-02-05 18:58:00'),
(326, 1, 1, 'Actualizacion de producto id: 118', '190.103.28.234', '2020-02-05 18:59:00'),
(327, 1, 1, 'Actualizacion de producto id: 116', '190.103.28.234', '2020-02-05 18:59:00'),
(328, 1, 1, 'Actualizacion de producto id: 115', '190.103.28.234', '2020-02-05 18:59:00'),
(329, 1, 1, 'Actualizacion de producto id: 116', '190.103.28.234', '2020-02-05 19:00:00'),
(330, 1, 1, 'Actualizacion de producto id: 115', '190.103.28.234', '2020-02-05 19:00:00'),
(331, 1, 1, 'Actualizacion de producto id: 114', '190.103.28.234', '2020-02-05 19:01:00'),
(332, 1, 1, 'Actualizacion de producto id: 113', '190.103.28.234', '2020-02-05 19:02:00'),
(333, 1, 1, 'Actualizacion de producto id: 112', '190.103.28.234', '2020-02-05 19:02:00'),
(334, 1, 1, 'Actualizacion de producto id: 111', '190.103.28.234', '2020-02-05 19:03:00'),
(335, 1, 1, 'Registro galeria  id: 168', '190.103.28.234', '2020-02-05 19:05:00'),
(336, 1, 1, 'Actualizacion de producto id: 110', '190.103.28.234', '2020-02-05 19:29:00'),
(337, 1, 1, 'Actualizacion de producto id: 109', '190.103.28.234', '2020-02-05 19:30:00'),
(338, 1, 1, 'Inicio de sesion', '190.103.28.234', '2020-02-05 19:42:00'),
(339, 1, 1, 'Inicio de sesion', '::1', '2020-02-05 20:57:00'),
(340, 1, 1, 'Registro cantidad producto id: 201', '::1', '2020-02-05 20:57:00'),
(341, 1, 1, 'Registro cantidad producto id: 202', '::1', '2020-02-05 20:57:00'),
(342, 1, 1, 'Registro cantidad producto id: 203', '::1', '2020-02-05 20:57:00'),
(343, 1, 1, 'Registro cantidad producto id: 204', '::1', '2020-02-05 20:57:00'),
(344, 1, 1, 'Registro cantidad producto id: 205', '::1', '2020-02-05 20:58:00'),
(345, 1, 1, 'Registro cantidad producto id: 206', '::1', '2020-02-05 20:58:00'),
(346, 1, 1, 'Registro cantidad producto id: 207', '::1', '2020-02-05 20:58:00'),
(347, 1, 1, 'Registro cantidad producto id: 208', '::1', '2020-02-05 20:58:00'),
(348, 1, 1, 'Registro cantidad producto id: 209', '::1', '2020-02-05 20:58:00'),
(349, 1, 1, 'Registro cantidad producto id: 210', '::1', '2020-02-05 20:59:00'),
(350, 1, 1, 'Registro cantidad producto id: 211', '::1', '2020-02-05 20:59:00'),
(351, 1, 1, 'Registro cantidad producto id: 212', '::1', '2020-02-05 20:59:00'),
(352, 1, 1, 'Registro cantidad producto id: 213', '::1', '2020-02-05 20:59:00'),
(353, 1, 1, 'Registro cantidad producto id: 214', '::1', '2020-02-05 21:00:00'),
(354, 1, 1, 'Registro cantidad producto id: 215', '::1', '2020-02-05 21:00:00'),
(355, 1, 1, 'Registro cantidad producto id: 216', '::1', '2020-02-05 21:00:00'),
(356, 1, 1, 'Registro cantidad producto id: 217', '::1', '2020-02-05 21:00:00'),
(357, 1, 1, 'Registro cantidad producto id: 218', '::1', '2020-02-05 21:00:00'),
(358, 1, 1, 'Registro cantidad producto id: 219', '::1', '2020-02-05 21:00:00'),
(359, 1, 1, 'Registro cantidad producto id: 220', '::1', '2020-02-05 21:01:00'),
(360, 1, 1, 'Registro cantidad producto id: 221', '::1', '2020-02-05 21:01:00'),
(361, 1, 1, 'Registro cantidad producto id: 222', '::1', '2020-02-05 21:01:00'),
(362, 1, 1, 'Registro cantidad producto id: 223', '::1', '2020-02-05 21:02:00'),
(363, 1, 1, 'Registro cantidad producto id: 224', '::1', '2020-02-05 21:02:00'),
(364, 1, 1, 'Registro cantidad producto id: 225', '::1', '2020-02-05 21:02:00'),
(365, 1, 1, 'Registro cantidad producto id: 226', '::1', '2020-02-05 21:02:00'),
(366, 1, 1, 'Registro cantidad producto id: 227', '::1', '2020-02-05 21:02:00'),
(367, 1, 1, 'Registro cantidad producto id: 228', '::1', '2020-02-05 21:02:00'),
(368, 1, 1, 'Registro cantidad producto id: 229', '::1', '2020-02-05 21:06:00'),
(369, 1, 1, 'Registro cantidad producto id: 230', '::1', '2020-02-05 21:06:00'),
(370, 1, 1, 'Registro cantidad producto id: 231', '::1', '2020-02-05 21:06:00'),
(371, 1, 1, 'Registro cantidad producto id: 232', '::1', '2020-02-05 21:06:00'),
(372, 1, 1, 'Inicio de sesion', '::1', '2020-02-06 14:04:00'),
(373, 1, 1, 'Registro cantidad producto id: 233', '::1', '2020-02-06 14:04:00'),
(374, 1, 1, 'Registro cantidad producto id: 234', '::1', '2020-02-06 14:04:00'),
(375, 1, 1, 'Registro cantidad producto id: 235', '::1', '2020-02-06 14:04:00'),
(376, 1, 1, 'Registro cantidad producto id: 236', '::1', '2020-02-06 14:04:00'),
(377, 1, 1, 'Registro cantidad producto id: 237', '::1', '2020-02-06 14:05:00'),
(378, 1, 1, 'Registro cantidad producto id: 238', '::1', '2020-02-06 14:05:00'),
(379, 1, 1, 'Registro cantidad producto id: 239', '::1', '2020-02-06 14:05:00'),
(380, 1, 1, 'Registro cantidad producto id: 240', '::1', '2020-02-06 14:06:00'),
(381, 1, 1, 'Registro cantidad producto id: 241', '::1', '2020-02-06 14:06:00'),
(382, 1, 1, 'Registro cantidad producto id: 242', '::1', '2020-02-06 14:06:00'),
(383, 1, 1, 'Registro cantidad producto id: 243', '::1', '2020-02-06 14:06:00'),
(384, 1, 1, 'Registro cantidad producto id: 244', '::1', '2020-02-06 14:06:00'),
(385, 1, 1, 'Registro cantidad producto id: 245', '::1', '2020-02-06 14:07:00'),
(386, 1, 1, 'Registro cantidad producto id: 246', '::1', '2020-02-06 14:07:00'),
(387, 1, 1, 'Registro cantidad producto id: 247', '::1', '2020-02-06 14:07:00'),
(388, 1, 1, 'Registro cantidad producto id: 248', '::1', '2020-02-06 14:07:00'),
(389, 1, 1, 'Registro cantidad producto id: 249', '::1', '2020-02-06 14:07:00'),
(390, 1, 1, 'Registro cantidad producto id: 250', '::1', '2020-02-06 14:07:00'),
(391, 1, 1, 'Registro cantidad producto id: 251', '::1', '2020-02-06 14:09:00'),
(392, 1, 1, 'Registro cantidad producto id: 252', '::1', '2020-02-06 14:09:00'),
(393, 1, 1, 'Registro cantidad producto id: 253', '::1', '2020-02-06 14:09:00'),
(394, 44, 2, 'Agrego al carrito producto: Premium Femenino Talla:S Color:BICOLOR', '::1', '2020-02-06 14:18:00'),
(395, 44, 2, 'Se modifico la cantidad del producto: Premium Femenino Talla:S Color:BICOLOR cantidad ingresada:0 cantidad anterior:2 cantidad en tabla: 98 cantidad nueva calculo:  cantidad total: 98', '::1', '2020-02-06 14:18:00'),
(396, 44, 2, 'Creación de orden #id:101 con los siguientes items:  producto: Premium Femenino Talla:S Color:BICOLOR,', '::1', '2020-02-06 14:29:00'),
(397, 44, 2, 'Agrego al carrito producto: Zoug Zoug Femenino Talla:S Color:BICOLOR', '::1', '2020-02-07 19:16:00'),
(398, 44, 2, 'Creación de orden #id:102 con los siguientes items:  producto: Zoug Zoug Femenino Talla:S Color:BICOLOR,', '::1', '2020-02-07 19:17:00'),
(399, 44, 2, 'Agrego al carrito producto: Zoug Zoug Masculino Talla:S Color:BICOLOR', '::1', '2020-02-09 13:10:00'),
(400, 44, 2, 'Creación de orden #id:103 con los siguientes items:  producto: Zoug Zoug Masculino Talla:S Color:BICOLOR,', '::1', '2020-02-09 13:15:00'),
(401, 1, 1, 'Inicio de sesion', '::1', '2020-02-11 12:00:00'),
(402, 1, 1, 'Registro galeria  id: 169', '::1', '2020-02-11 12:02:00'),
(403, 1, 1, 'Eliminar galeria  id: 169', '::1', '2020-02-11 12:03:00'),
(404, 1, 1, 'Registro galeria  id: 170', '::1', '2020-02-11 12:05:00'),
(405, 1, 1, 'Registro de producto id:125,titulo:panatalones', '::1', '2020-02-11 12:08:00'),
(406, 1, 1, 'Eliminar galeria  id: 170', '::1', '2020-02-11 12:13:00'),
(407, 1, 1, 'Actualizacion de producto id: 125', '::1', '2020-02-11 12:21:00'),
(408, 1, 1, 'Inicio de sesion', '::1', '2020-02-11 13:46:00'),
(409, 1, 1, 'Registro galeria  id: 172', '::1', '2020-02-11 13:48:00'),
(410, 1, 1, 'Eliminar galeria  id: 172', '::1', '2020-02-11 13:49:00'),
(411, 1, 1, 'Registro galeria  id: 173', '::1', '2020-02-11 13:52:00'),
(412, 1, 1, 'Registro noticia id: 16 titulo: STAND UP DE JOSÉ RAFAEL GUZMAN', '::1', '2020-02-11 13:52:00'),
(413, 1, 1, 'Eliminar galeria  id: 173', '::1', '2020-02-11 13:53:00'),
(414, 1, 1, 'Actualizar noticia id: 16 titulo: STAND UP DE JOSÉ RAFAEL GUZMAN', '::1', '2020-02-11 13:57:00'),
(415, 1, 1, 'Cerrar de sesion', '::1', '2020-02-11 14:11:00'),
(416, 1, 1, 'Inicio de sesion', '::1', '2020-02-17 15:25:00'),
(417, 1, 1, 'Actualizacion de empresa nosotros id: 20', '::1', '2020-02-17 15:27:00'),
(418, 1, 1, 'Registro galeria  id: 174', '::1', '2020-02-17 15:29:00'),
(419, 1, 1, 'Registro galeria  id: 175', '::1', '2020-02-17 15:29:00'),
(420, 1, 1, 'Registro galeria  id: 176', '::1', '2020-02-17 15:29:00'),
(421, 1, 1, 'Actualizacion de slider id: 267', '::1', '2020-02-17 15:30:00'),
(422, 1, 1, 'Actualizacion de slider id: 269', '::1', '2020-02-17 15:30:00'),
(423, 1, 1, 'Actualizacion de slider id: 265', '::1', '2020-02-17 15:30:00'),
(424, 1, 1, 'Actualizacion de slider id: 266', '::1', '2020-02-17 15:30:00'),
(425, 1, 1, 'Actualizacion de slider id: 264', '::1', '2020-02-17 15:31:00'),
(426, 1, 1, 'Actualizacion de slider id: 268', '::1', '2020-02-17 15:31:00'),
(427, 44, 2, 'Agrego al carrito producto: Premium Masculino Talla:S Color:BICOLOR', '::1', '2020-02-17 15:32:00'),
(428, 44, 2, 'Creación de orden #id:104 con los siguientes items:  producto: Premium Masculino Talla:S Color:BICOLOR,', '::1', '2020-02-17 15:36:00'),
(429, 1, 1, 'Actualizacion de marca id: 14', '::1', '2020-02-17 15:37:00'),
(430, 1, 1, 'Inactivar marca id: 14', '::1', '2020-02-17 15:37:00'),
(431, 1, 1, 'Activar marca id: 14', '::1', '2020-02-17 15:37:00'),
(432, 1, 1, 'Actualizacion de categoría producto id: 11', '::1', '2020-02-17 15:38:00'),
(433, 1, 1, 'Inactivar categoría producto id: 11', '::1', '2020-02-17 15:38:00'),
(434, 1, 1, 'Activar categoría producto id: 11', '::1', '2020-02-17 15:38:00'),
(435, 1, 1, 'Actualizacion tipo de producto id: 45', '::1', '2020-02-17 15:38:00'),
(436, 1, 1, 'Inactivar tipo de producto id: 45', '::1', '2020-02-17 15:38:00'),
(437, 1, 1, 'Activar tipo de producto id: 45', '::1', '2020-02-17 15:38:00'),
(438, 1, 1, 'Inactivar color id: 11', '::1', '2020-02-17 15:38:00'),
(439, 1, 1, 'Activar color id: 11', '::1', '2020-02-17 15:38:00'),
(440, 1, 1, 'Actualizacion de color id: 11', '::1', '2020-02-17 15:38:00'),
(441, 0, 1, 'Inactivar talla id: 8', '::1', '2020-02-17 15:39:00'),
(442, 0, 1, 'Actualizacion de talla id: 8', '::1', '2020-02-17 15:39:00'),
(443, 0, 1, 'Actualizacion de talla id: 8', '::1', '2020-02-17 15:39:00'),
(444, 1, 1, 'Actualizacion de talla id: 8', '::1', '2020-02-17 15:42:00'),
(445, 1, 1, 'Inactivar talla id: 8', '::1', '2020-02-17 15:42:00'),
(446, 1, 1, 'Activar talla id: 8', '::1', '2020-02-17 15:42:00'),
(447, 1, 1, 'Inactivar categorías idiomas id: 39', '::1', '2020-02-17 15:43:00'),
(448, 1, 1, 'Activar categorías idiomas id: 39', '::1', '2020-02-17 15:43:00'),
(449, 1, 1, 'Actualizacion de producto id: 125', '::1', '2020-02-17 15:43:00'),
(450, 1, 1, 'Eliminar producto: 125', '::1', '2020-02-17 15:43:00'),
(451, 1, 1, 'Registro de producto id:126,titulo:PAntalones', '::1', '2020-02-17 15:44:00'),
(452, 1, 1, 'Registro cantidad producto id: 254', '::1', '2020-02-17 15:44:00'),
(453, 1, 1, 'Inactivar cantidad producto: 254', '::1', '2020-02-17 15:44:00'),
(454, 1, 1, 'Eliminar cantidad producto: 254', '::1', '2020-02-17 15:44:00'),
(455, 1, 1, 'Inactivar noticia: 16', '::1', '2020-02-17 16:01:00'),
(456, 1, 1, 'Actualizar noticia id: 16 titulo: STAND UP DE JOSÉ RAFAEL GUZMAN', '::1', '2020-02-17 16:01:00'),
(457, 1, 1, 'Registro noticia id: 17 titulo: PRUEBA DE NOTICIA', '::1', '2020-02-17 16:02:00'),
(458, 1, 1, 'Actualizacion redes sociales id: 11', '::1', '2020-02-17 16:02:00'),
(459, 1, 1, 'Actualizar footer id: 4', '::1', '2020-02-17 16:02:00'),
(460, 1, 1, 'Inactivar categorias: 21', '::1', '2020-02-17 16:02:00'),
(461, 0, 1, 'Modificar categtorias cms id: 22', '::1', '2020-02-17 16:02:00'),
(462, 0, 1, 'Modificar categtorias cms id: 22', '::1', '2020-02-17 16:02:00'),
(463, 0, 1, 'Modificar categtorias cms id: 22', '::1', '2020-02-17 16:02:00'),
(464, 1, 1, 'Modificar categtorias cms id: 22', '::1', '2020-02-17 16:03:00'),
(465, 1, 1, 'Registro usuarios id: 12', '::1', '2020-02-17 16:07:00'),
(466, 1, 1, 'Inactivar usuarios: 13', '::1', '2020-02-17 16:07:00'),
(467, 1, 1, 'Modificar usuarios id: 13', '::1', '2020-02-17 16:07:00'),
(468, 1, 1, 'Modificar descripcion id: 1', '::1', '2020-02-17 16:07:00'),
(469, 44, 2, 'Agrego al carrito producto: Zoug Zoug Femenino Talla:S Color:BICOLOR', '::1', '2020-02-17 16:08:00'),
(470, 1, 1, 'Registro noticia id: 18 titulo: UNA NOTICIA MAS', '::1', '2020-02-17 16:08:00'),
(471, 44, 2, 'Creación de orden #id:105 con los siguientes items:  producto: Zoug Zoug Femenino Talla:S Color:BICOLOR,', '::1', '2020-02-17 16:10:00'),
(472, 1, 1, 'Inicio de sesion', '::1', '2020-02-17 16:43:00'),
(473, 1, 1, 'Inicio de sesion', '::1', '2020-02-17 16:45:00'),
(474, 1, 1, 'Inicio de sesion', '::1', '2020-02-17 16:46:00'),
(475, 1, 1, 'Inicio de sesion', '::1', '2020-02-17 16:52:00'),
(476, 1, 1, 'Inicio de sesion', '::1', '2020-02-18 12:45:00'),
(477, 1, 1, 'Actualizacion de empresa nosotros id: 20', '::1', '2020-02-18 12:45:00'),
(478, 1, 1, 'Inicio de sesion', '::1', '2020-03-31 15:32:00'),
(479, 1, 1, 'Cerrar de sesion', '::1', '2020-03-31 15:34:00'),
(480, 1, 1, 'Inicio de sesion', '::1', '2020-04-01 17:16:00'),
(481, 1, 1, 'Registro categtorias cms id: 23', '::1', '2020-04-01 17:16:00'),
(482, 1, 1, 'Inicio de sesion', '::1', '2020-04-01 17:17:00'),
(483, 1, 1, 'Registro galeria  id: 177', '::1', '2020-04-01 17:21:00'),
(484, 1, 1, 'Registro galeria  id: 178', '::1', '2020-04-01 17:21:00'),
(485, 1, 1, 'Modificar usuarios id: 13', '::1', '2020-04-01 17:23:00'),
(486, 1, 1, 'Modificar usuarios id: 13', '::1', '2020-04-01 17:24:00'),
(487, 1, 1, 'Registro usuarios id: 14', '::1', '2020-04-01 17:24:00'),
(488, 1, 1, 'Modificar usuarios id: 14', '::1', '2020-04-01 17:25:00'),
(489, 1, 1, 'Modificar usuarios id: 1', '::1', '2020-04-01 17:25:00'),
(490, 1, 1, 'Eliminar galeria  id: 177', '::1', '2020-04-01 17:36:00'),
(491, 1, 1, 'Registro galeria  id: 179', '::1', '2020-04-01 17:38:00'),
(492, 1, 1, 'Modificar usuarios id: 1', '::1', '2020-04-01 17:38:00'),
(493, 1, 1, 'Inicio de sesion', '::1', '2020-04-01 18:04:00'),
(494, 1, 1, 'Inicio de sesion', '::1', '2020-04-01 19:13:00'),
(495, 1, 1, 'Cerrar de sesion', '::1', '2020-04-01 19:17:00'),
(496, 1, 1, 'Inicio de sesion', '::1', '2020-04-01 19:17:00'),
(497, 1, 1, 'Eliminar galeria  id: 179', '::1', '2020-04-01 19:18:00'),
(498, 1, 1, 'Registro galeria  id: 180', '::1', '2020-04-01 19:18:00'),
(499, 1, 1, 'Modificar usuarios id: 1', '::1', '2020-04-01 19:19:00'),
(500, 1, 1, 'Inicio de sesion', '::1', '2020-04-01 19:19:00'),
(501, 1, 1, 'Inicio de sesion', '::1', '2020-04-01 19:32:00'),
(502, 1, 1, 'Inicio de sesion', '::1', '2020-04-01 19:38:00'),
(503, 1, 1, 'Cerrar de sesion', '::1', '2020-04-01 20:26:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cambio_contraseña`
--

CREATE TABLE `cambio_contraseña` (
  `id` int(100) NOT NULL,
  `id_usuario` int(100) NOT NULL,
  `codigo` varchar(100) NOT NULL,
  `id_identificador` varchar(100) NOT NULL,
  `estatus` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cambio_contraseña`
--

INSERT INTO `cambio_contraseña` (`id`, `id_usuario`, `codigo`, `id_identificador`, `estatus`) VALUES
(10, 44, '', '98fbc42faedc02492397cb5962ea3a3ffc0a9243', 2),
(11, 47, '', '827bfc458708f0b442009c9c9836f7e4b65557fb', 2),
(12, 48, '', '64e095fe763fc62418378753f9402623bea9e227', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cantidad_producto`
--

CREATE TABLE `cantidad_producto` (
  `id` int(11) NOT NULL,
  `id_det_prod` int(11) NOT NULL,
  `cantidad` text NOT NULL,
  `id_color` int(11) NOT NULL,
  `id_talla` int(11) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cantidad_producto`
--

INSERT INTO `cantidad_producto` (`id`, `id_det_prod`, `cantidad`, `id_color`, `id_talla`, `estatus`) VALUES
(254, 126, '100', 10, 2, 2),
(253, 121, '100', 10, 2, 1),
(252, 121, '100', 10, 3, 1),
(251, 121, '100', 10, 4, 1),
(250, 119, '100', 10, 2, 1),
(249, 119, '100', 10, 3, 1),
(248, 119, '100', 10, 4, 1),
(247, 117, '100', 10, 2, 1),
(246, 117, '100', 10, 3, 1),
(245, 117, '100', 10, 4, 1),
(244, 115, '100', 10, 2, 1),
(243, 115, '100', 10, 3, 1),
(242, 115, '100', 10, 4, 1),
(241, 113, '100', 10, 2, 1),
(240, 113, '100', 10, 3, 1),
(239, 113, '100', 10, 4, 1),
(238, 111, '100', 10, 2, 1),
(237, 111, '100', 10, 3, 1),
(236, 111, '100', 10, 4, 1),
(235, 109, '100', 10, 2, 1),
(234, 109, '100', 10, 3, 1),
(233, 109, '100', 10, 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrito_encabezado`
--

CREATE TABLE `carrito_encabezado` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `carrito_encabezado`
--

INSERT INTO `carrito_encabezado` (`id`, `id_usuario`, `estatus`) VALUES
(81, 44, 2),
(80, 44, 2),
(79, 44, 2),
(82, 44, 2),
(83, 44, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrito_productos`
--

CREATE TABLE `carrito_productos` (
  `id` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `monto` float NOT NULL,
  `monto_total` float NOT NULL,
  `fecha` date NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_carrito` int(11) NOT NULL,
  `id_cantidad_producto` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `carrito_productos`
--

INSERT INTO `carrito_productos` (`id`, `id_producto`, `cantidad`, `monto`, `monto_total`, `fecha`, `id_usuario`, `id_idioma`, `estatus`, `id_carrito`, `id_cantidad_producto`) VALUES
(118, 109, 2, 300, 600, '2020-02-17', 44, 1, 2, 82, 235),
(117, 111, 2, 100, 200, '2020-02-09', 44, 1, 2, 81, 238),
(116, 119, 3, 100, 300, '2020-02-07', 44, 1, 2, 80, 250),
(115, 115, 2, 200, 400, '2020-02-06', 44, 2, 2, 79, 244),
(119, 119, 3, 100, 300, '2020-02-17', 44, 1, 2, 83, 250);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `descripcion`, `estatus`) VALUES
(1, 'SLIDER', 0),
(2, 'Marcas', 0),
(3, 'quienes_somos', 1),
(4, 'noticias', 1),
(5, 'Detalles negocios', 1),
(6, 'imagenes', 1),
(7, 'servicios', 1),
(8, 'Portafolio', 1),
(9, 'Clientes', 1),
(18, 'PRUEBA DE DESCRIPCION', 2),
(19, 'PRUEBA SLIDER DOS', 1),
(20, 'Nosotros', 1),
(21, '123', 0),
(22, 'PRODUCTOS', 1),
(23, 'USUARIOS', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias_productos_idiomas`
--

CREATE TABLE `categorias_productos_idiomas` (
  `id` int(11) NOT NULL,
  `nombre_tabla` text NOT NULL,
  `id_categoria_es` int(11) NOT NULL,
  `id_categoria_en` int(11) NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categorias_productos_idiomas`
--

INSERT INTO `categorias_productos_idiomas` (`id`, `nombre_tabla`, `id_categoria_es`, `id_categoria_en`, `estatus`) VALUES
(28, 'Colores', 1, 3, 1),
(29, 'Colores', 2, 4, 1),
(30, 'Colores', 8, 9, 1),
(31, 'Marcas', 7, 9, 1),
(32, 'Marcas', 8, 10, 1),
(33, 'Categoria Productos', 9, 11, 1),
(34, 'Tipo Producto', 42, 45, 1),
(36, 'Categoria Productos', 8, 10, 1),
(37, 'Marcas', 13, 14, 1),
(38, 'Colores', 10, 11, 1),
(39, 'Marcas', 11, 12, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_producto`
--

CREATE TABLE `categoria_producto` (
  `id` int(11) NOT NULL,
  `titulo` varchar(250) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria_producto`
--

INSERT INTO `categoria_producto` (`id`, `titulo`, `descripcion`, `id_idioma`, `estatus`) VALUES
(8, 'MASCULINO', 'ROPA MASCULINA', 1, 1),
(9, 'FEMENINO', 'ROPA FEMENINA', 1, 1),
(10, 'MALE', '<U></U><DIV><SPAN>MEN\'S CLOTHES</SPAN><U><BR></U></DIV>', 2, 1),
(11, 'FEMALE', '<DIV><BR></DIV><DIV><SPAN>WOMENSWEAR</SPAN><BR></DIV>', 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `descripcion`, `id_idioma`, `estatus`) VALUES
(2, '<p><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; line-height: 20px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fermentum libero libero, quis iaculis est venenatis quis. Quisque sodales id ligula eget gravida. Nulla scelerisque arcu at eleifend maximus. Cras cursus, quam at lacinia iaculis, ex mauris pretium nibh, sed cursus urna augue vitae nisi. Nulla nulla augue, placerat a magna vel, semper placerat turpis. Ut ut velit ornare, mollis odio nec, fringilla tortor. Praesent vitae ligula bibendum, accumsan ante sed, pretium magna.</span></p>', 1, 1),
(3, '<p><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; line-height: 20px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fermentum libero libero, quis iaculis est venenatis quis. Quisque sodales id ligula eget gravida. Nulla scelerisque arcu at eleifend maximus. Cras cursus, quam at lacinia iaculis, ex mauris pretium nibh, sed cursus urna augue vitae nisi. Nulla nulla augue, placerat a magna vel, semper placerat turpis. Ut ut velit ornare, mollis odio nec, fringilla tortor. Praesent vitae ligula bibendum, accumsan ante sed, pretium magna.</span></p>', 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colores`
--

CREATE TABLE `colores` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `colores`
--

INSERT INTO `colores` (`id`, `descripcion`, `id_idioma`, `estatus`) VALUES
(1, 'BLANCO', 1, 1),
(2, 'NEGRO', 1, 1),
(3, 'WHITE', 2, 1),
(4, 'BLACK', 2, 1),
(8, 'GRIS', 1, 1),
(7, 'AZUL', 1, 2),
(9, 'GREY', 2, 1),
(10, 'BICOLOR', 1, 1),
(11, 'BI COLOR', 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactos`
--

CREATE TABLE `contactos` (
  `id` int(11) NOT NULL,
  `nombres` text NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mensaje` text NOT NULL,
  `razon` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contactos`
--

INSERT INTO `contactos` (`id`, `nombres`, `telefono`, `email`, `mensaje`, `razon`) VALUES
(12, 'miguel', '04122473595', 'mporro@cantv.net', 'mensaje de prueba de contacto', ''),
(13, 'miguel', '04122473595', 'mporro@cantv.net', 'mensaje de prueba de contacto', ''),
(14, 'prueba', '04124567788', 'prueba@gmail.com', 'Prueba', ''),
(15, 'prueba', '04124567788', 'prueba@gmail.com', 'Prueba', ''),
(16, 'prueba', '04167485648', 'asdf@gmail.com', 'prueba', ''),
(17, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(18, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(19, 'GIanni', '04167485648', 'gianni.d.santucci@gmail.com', 'Prueba de envio', ''),
(20, 'GIanni', '04167485648', 'gianni.d.santucci@gmail.com', 'Prueba de envio', ''),
(21, 'GIanni', '04167485648', 'gianni.d.santucci@gmail.com', 'Prueba de envio', ''),
(22, 'Gianni Santucci', '04167485648', 'gianni.d.santucci@gmail.com', 'prueba', ''),
(23, 'Gianni Santucci', '04167485648', 'gianni.d.santucci@gmail.com', 'prueba', ''),
(24, 'Gianni Santucci', '04167485648', 'gianni.d.santucci@gmail.com', 'prueba', ''),
(25, 'Gianni Santucci', '04167485648', 'gianni.d.santucci@gmail.com', 'prueba', ''),
(26, 'Gianni Santucci', '04167485648', 'gianni.d.santucci@gmail.com', 'prueba', ''),
(27, 'Gianni Santucci', '04167485648', 'gianni.d.santucci@gmail.com', 'prueba', ''),
(28, 'Carlos Herrera', '04126372219', 'carlosdhm6@gmail.com', 'Prueba', ''),
(29, 'Gianni Santucci', '04167485648', 'gianni.d.santucci@gmail.com', 'prueba', ''),
(30, 'Carlos Herrera', '04126372219', 'carlosdhm6@gmail.com', 'Prueba', ''),
(31, 'Carlos Herrera', '04126372219', 'carlosdhm6@gmail.com', 'Prueba', ''),
(32, 'Prueba', '04167485648', 'prueba@gmail.com', 'Prueba', ''),
(33, 'prueba', '04167485648', 'asdf@gmail.com', 'prueba', ''),
(34, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(35, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(36, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(37, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(38, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(39, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(40, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(41, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(42, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(43, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(44, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(45, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(46, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(47, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(48, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(49, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(50, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(51, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(52, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(53, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(54, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(55, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(56, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(57, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(58, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(59, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(60, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(61, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(62, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(63, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(64, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(65, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(66, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(67, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(68, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(69, 'prueba', '04167485648', 'prueba@gmail.co', 'Prueba', ''),
(70, 'prueba', '04167485648', 'prueba@gmail.co', 'Prueba', ''),
(71, 'prueba', '04167485648', 'prueba@gmail.co', 'Prueba', ''),
(72, 'prueba', '04167485648', 'prueba@gmail.co', 'Prueba', ''),
(73, 'prueba', '04167485648', 'prueba@gmail.co', 'Prueba', ''),
(74, 'prueba', '04167485648', 'prueba@gmail.co', 'Prueba', ''),
(75, 'prueba', '04167485648', 'prueba@gmail.co', 'Prueba', ''),
(76, 'prueba', '04167485648', 'prueba@gmail.co', 'Prueba', ''),
(77, 'prueba', '04167485648', 'prueba@gmail.co', 'Prueba', ''),
(78, 'prueba', '04167485648', 'prueba@gmail.co', 'Prueba', ''),
(79, 'prueba', '04167485648', 'prueba@gmail.co', 'Prueba', ''),
(80, 'prueba', '04167485648', 'prueba@gmail.co', 'Prueba', ''),
(81, 'prueba', '04167485648', 'prueba@gmail.co', 'Prueba', ''),
(82, 'prueba', '02128723999', 'prueba@gmail.com', 'prueba', ''),
(83, 'prueba', '02128723999', 'prueba@gmail.com', 'prueba', ''),
(84, 'prueba de email', '02125555555', 'gianni.d.santucci@gmail.com', 'prueba', ''),
(85, 'Gianni santucci', '04167485648', 'prueba@gmail.com', 'prueba de mensaje', ''),
(86, 'prueba', '02126799999', 'prueba@gmail.com', 'prueba', ''),
(87, 'Gianni', '04167888888', 'paginaweb@itssca.net', 'prueba', ''),
(88, 'Gianni Santucci', '02127777777', 'gianni.d.santucci@gmail.com', 'Prueba de que la web de itssca si envia emails!', ''),
(89, 'Prueba', '02120000000', 'prueba@gmail.com', 'prueba desde sección contactanos', ''),
(90, 'prueba', '02123455555', 'prueba@gmail.com', 'prueba', ''),
(91, 'prueba viernes', '04167485648', 'prueba_viernes@gmail.com', 'Prueba de mensaje viernes 17112017', ''),
(92, 'Gianni', '04165674455', 'Gianni@gmail.com', 'Prueba', ''),
(93, 'carlos', '04126372219', 'carlos.herrera@itssca.net', 'sssss', ''),
(94, 'carlos', '04126372219', 'carlos.herrera@itssca.net', 'carlos hod', ''),
(95, 'carlos', '04126372219', 'carlosdhm6@gmail.com', 'ndsnaod', ''),
(96, 'carlos', '04126372219', 'carlosdhm6@gmail.com', 'noisanodnasd', ''),
(97, 'carlos', '04126372219', 'carlosdhm6@gmail.com', 'ndsadnald', ''),
(98, 'Prueba', '04164444444', 'prueba@gmail.com', 'Prueba de envio de email gianni santucci 15:52 pm', ''),
(99, 'carlos', '04125324219', 'carlosdhm6@gmail.como', 'sssss', ''),
(100, 'Prueba', '04167485648', 'prueba.gianni@gmail.com', 'Prueba gianni santucci', ''),
(101, 'Prueba', '04167485648', 'prueba.gianni@gmail.com', 'Prueba gianni santucci', ''),
(102, 'Prueba', '04167485648', 'prueba.gianni@gmail.com', 'Prueba gianni santucci', ''),
(103, 'Prueba', '04167485648', 'prueba.gianni@gmail.com', 'Prueba gianni santucci', ''),
(104, 'Prueba', '04164444444', 'prueba@gmail.com', 'prueba', ''),
(105, 'prueba', '04169777777', 'pruyeba@gmail.com', 'prueba', ''),
(106, 'carlos herrera', '04126372219', 'carlosdhm6@gmail.com', 'midsdbaosdad', ''),
(107, 'gustavo', '04126379299', 'gustavo.w@hotmail.com', 'qwerty', ''),
(108, 'prueba gianni', '04167485648', 'gianni.d.santucci@gmail.com', 'prueba gianni', ''),
(109, 'prueba gianni', '04167485648', 'gianni.d.santucci@gmail.com', 'prueba gianni', ''),
(110, 'prueba', '04167485648', 'prueba.gianni@gmail.com', 'prueba', ''),
(111, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba de mensaje', ''),
(112, 'Gianni', '04167485648', 'gianni.santucci@hotmail.com', 'prueba', ''),
(113, 'Gianni', '04167485648', 'gianni.d.santucci@gmail.com', 'prueba', ''),
(114, 'Gianni Santucci', '04124555555', 'prueba@gmail.com', 'prueba', ''),
(115, 'asdwnoq', '04124567890', 'carlos@algo.com', 'sdabiasndkc\nEnvio 5', ''),
(116, 'asdwnoq', '04124567890', 'carlos@algo.com', 'sdabiasndkc\nEnvio 5', ''),
(117, 'maria vinals', '04246789024', 'maria@gmail.com', 'misterdnsodnso\nenvio 6', ''),
(118, 'carlos', '04123456789', 'yupi@itssca.net', 'wertyuiomnbvfrtyujm', ''),
(119, 'Trino Gonzlez', '04249383987', 'trinoinocente01@gmail.com', 'Muy buenas, quiero saber como contrato o compro la aplicación para usar el servicio P2P', ''),
(120, 'Gianni Santucci', '04167485648', 'gianni.d.santucci@gmail.com', 'Prueba de envío página web', ''),
(121, 'prueba', '4167485648', 'prueba@gmail.com', 'pruebas', ''),
(122, 'pueba de contactos', '4167485648', '', 'p?ueba de mensaje', ''),
(123, 'prueba de contactos', '4167485677', 'prueba@gmail.com', 'mensaje de contacto', ''),
(124, 'Otro ejem', '5841256667777', 'ejem@gmail.com', 'prueba de mensaje', ''),
(125, 'prueba', '4167485648', 'prueba@gmail.com', 'prueba de mensaje', ''),
(126, 'prueba de contactos', '4167485648', 'prueba@gmail.com', 'prueba de mensaje', ''),
(127, 'prueba mil', '4167485648', 'prueba@gmail.com', 'Mensaje mil', ''),
(128, 'nelly moreno', '4167485648', 'nlm.diaz@gmail.com', 'prueba de mensaje', ''),
(129, 'Luis', '4127584466', 'lp@gmail.com', 'Mensaje prueba desde movil', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactos_empleo`
--

CREATE TABLE `contactos_empleo` (
  `id` int(11) NOT NULL,
  `nombres` varchar(400) NOT NULL,
  `email` varchar(400) NOT NULL,
  `area` varchar(400) NOT NULL,
  `mensaje` text NOT NULL,
  `cv` text NOT NULL,
  `telefono` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contactos_empleo`
--

INSERT INTO `contactos_empleo` (`id`, `nombres`, `email`, `area`, `mensaje`, `cv`, `telefono`) VALUES
(36, 'gianni', 'gianni.d.santucci@gmail.com', 'prueba', 'prueba de registro', '../administrador/site_media/images/archivos/cv/cv_gianni.pdf', '04167485648'),
(47, 'registro', 'registro@gmail.com', 'prueba', 'prueba', '../administrador/site_media/images/archivos/cv/cv_registro.pdf', '04167485648'),
(48, 'registrodos', 'registrodos@gmail.com', 'prueba', 'prueba', '../administrador/site_media/images/archivos/cv/cv_registrodos.pdf', '04167485648'),
(49, 'Miguel', 'mporro@cantv.net', 'desarrollo', 'mensaje de prueba', '../administrador/site_media/images/archivos/cv/cv_Miguel.pdf', '04122473595'),
(50, 'Gianni santucci', 'asdf@gmail.com', 'prueba', 'esto es una prueba de envío de email desde itssca.net', '../administrador/site_media/images/archivos/cv/cv_Gianni_santucci.pdf', '04129877777'),
(51, 'Gianni Santucci', 'gianni.d.santucci@gmail.com', 'prueba', 'Prueba de envio de email. desde la web.', '../administrador/site_media/images/archivos/cv/cv_Gianni_Santucci.pdf', '04126666666'),
(52, 'Prueba de cv', 'cv@gmail.com', 'Prueba', 'Prueba de cv desde seccion contactanos', '../administrador/site_media/images/archivos/cv/cv_Prueba_de cv.pdf', '02129999999'),
(53, 'Gustavo Ocanto', 'gjocanto@gmail.com', 'vendedor de', 'sos chevere', '../administrador/site_media/images/archivos/cv/cv_Gustavo_Ocanto.pdf', '02125455122'),
(54, 'Prueba de cv', 'prueba@gmail.com', 'Prueba', 'Prueba adjuntando archivos', '../administrador/site_media/images/archivos/cv/cv_Prueba_de cv.pdf', '02123333333'),
(55, 'prueba', 'prueba@gmail.com', 'prueba', 'prueba', '../administrador/site_media/images/archivos/cv/cv_prueba.pdf', '02121444444'),
(56, 'prueba ultima del dia', 'asdf@gmail.com', 'prueba', 'prueba', '../administrador/site_media/images/archivos/cv/cv_prueba_ultima del dia.pdf', '04125555555'),
(57, 'Gianni Snatucci', 'prueba@gmail.com', 'prueba', 'Prueba de viernes 17-11-2017', '../administrador/site_media/images/archivos/cv/cv_Gianni_Snatucci.pdf', '04167415588'),
(58, 'Prueba', 'prueba@gmail.com', 'prueba', 'Prueba viernes 17 11 2014', '../administrador/site_media/images/archivos/cv/cv_Prueba.pdf', '04167485648'),
(59, 'Gianni Santucci', 'gianni.d.santucci@gmail.com', 'Prueba', 'Prueba de registro de cv página web', '../administrador/site_media/images/archivos/cv/cv_Gianni_Santucci.pdf', '04167485648'),
(60, 'Abimael Milano', 'abi_milano10@hotmail.com', 'Telecomunicaciones', 'Buenos días, me interesaría conocerlos mas a fondo y tener la oportunidad de crecer junto a la empre', '../administrador/site_media/images/archivos/cv/cv_Abimael_Milano.pdf', '04241752935'),
(61, 'Luis Eduardo Carpintero', 'luise2612@gmail.com', 'Consultor de redes y telecomunicaciones', 'Proactivo, creativo y disciplinado; Luis Carpintero es un consultor de redes que disfruta de encontrar soluciones creativas para redes complejas, utilizando las mejores prácticas. Con una comprensión rápida, solución y viabilidad en el aprendizaje de nuevas tecnologías', '../administrador/site_media/images/archivos/cv/cv_Luis_Eduardo Carpintero.pdf', '04241671246'),
(62, 'Luigi Toro', 'toroluigi123@gmail.com', 'soporte', 'Actualmente soy analista de soporte en venevision plus, me gustaría ingresar en una área relacionado', '../administrador/site_media/images/archivos/cv/cv_Luigi_Toro.pdf', '04140203600');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correos`
--

CREATE TABLE `correos` (
  `id` int(11) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `titulo` text NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `posicion` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `correos`
--

INSERT INTO `correos` (`id`, `correo`, `titulo`, `estatus`, `id_idioma`, `posicion`) VALUES
(1, 'ventas@itssca.net', 'Asistencia Comercial', 1, 1, 4),
(2, 'soporte@itssca.net', 'Asistencia Técnica', 1, 1, 1),
(3, 'mercadeo@itssca.net', 'Mercadeo', 1, 1, 2),
(4, 'rrhh@itssca.net', 'Empleos', 1, 1, 3),
(5, 'webmaster@itssca.net', 'Webmaster', 0, 1, 1),
(6, 'prueba@gmail.com', 'prueba', 0, 1, 2),
(7, 'support@itssca.net', 'Technical Support', 1, 2, 0),
(8, 'sales@itssca.net', 'Sales', 1, 2, 0),
(9, 'marketing@itssca.net', 'Marketing', 1, 2, 0),
(10, 'rrhh@itssca.net', 'Work with us', 1, 2, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `descripcion`
--

CREATE TABLE `descripcion` (
  `id` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `descripcion`
--

INSERT INTO `descripcion` (`id`, `descripcion`, `estatus`) VALUES
(1, 'prueba de registros descripcion', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_productos`
--

CREATE TABLE `detalle_productos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descripcion` varchar(500) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `id_categoria_prod` int(11) NOT NULL,
  `id_tipo_prod` int(11) NOT NULL,
  `id_marca` int(11) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1',
  `slug` varchar(300) NOT NULL,
  `id_original_clonado` int(11) NOT NULL DEFAULT '0',
  `clonado` int(11) NOT NULL DEFAULT '0',
  `codigo` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `detalle_productos`
--

INSERT INTO `detalle_productos` (`id`, `titulo`, `descripcion`, `id_idioma`, `id_categoria_prod`, `id_tipo_prod`, `id_marca`, `precio`, `estatus`, `slug`, `id_original_clonado`, `clonado`, `codigo`) VALUES
(109, 'Premium Masculino', 'Chaqueta manga larga con cinta ajustable con botón para uso manga corta, combinación de dos telas, solapa intercambiable y bolsillos en cintura.', 1, 8, 42, 11, '300.00', 1, 'premium-masculino', 0, 1, 'PRM-1'),
(110, 'Premium Male', 'Long sleeve with adjustable strap with button for short sleeve use, combination of two fabrics, interchangeable flap, waist pockets', 2, 10, 45, 12, '300.00', 1, 'premium-male', 109, 1, 'PRM-1'),
(111, 'Zoug Zoug Masculino', 'Chaqueta manga corta, combinación de 2 telas y botones en broche.', 1, 8, 42, 8, '100.00', 1, 'zoug-zoug-masculino', 0, 1, 'ZZM-1'),
(112, 'Zoug Zoug Male', 'Short sleeve jacket, combination of 2 fabrics and brooch buttons', 2, 10, 45, 10, '100.00', 1, 'zoug-zoug-male', 111, 1, 'ZZM-1'),
(113, 'Liqui Liqui Masculino', 'Chaqueta Manga larga a rayas, combinación en 2 telas, solapa intercambiable, botones en tela, puños con botón y 4 bolsillos.', 1, 8, 42, 7, '150.00', 1, 'liqui-liqui-masculino', 0, 1, 'LLM-1'),
(114, 'Liqui Liqui Male', 'Striped long sleeve jacket, combination in 2 fabrics, interchangeable flap, buttons in fabric, cuffs with button and 4 pockets.', 2, 10, 45, 9, '150.00', 1, 'liqui-liqui-male', 113, 1, 'LLM-1'),
(115, 'Premium Femenino', 'Chaqueta manga larga con cinta ajustable con botón para uso manga corta, combinación de dos telas, solapa intercambiable y&nbsp; bolsillos en cintura.<br>', 1, 9, 42, 11, '200.00', 1, 'premium-femenino', 0, 1, 'PRF-1'),
(116, 'Premium Female', 'Long sleeve jacket with adjustable strap with button for short sleeve use, combination of two fabrics, interchangeable flap and waist pockets.<br>', 2, 11, 45, 12, '200.00', 1, 'premium-female', 115, 1, 'PRF-1'),
(117, 'Liqui Liqui Femenino', 'Chaqueta manga larga, combinación en 2 telas, solapa intercambiable, botones en tela, puños con boton y 4 bolsillos.<br>', 1, 9, 42, 7, '140.00', 1, 'liqui-liqui-femenino', 0, 1, 'LLF-1'),
(118, 'Liqui Liqui Female', 'Long sleeve jacket, combination in 2 fabrics, interchangeable flap, buttons in fabric, cuffs with button and&nbsp; 4 pockets.<br><br>', 2, 11, 45, 9, '140.00', 1, 'liqui-liqui-female', 117, 1, 'LLF-1'),
(119, 'Zoug Zoug Femenino', 'Chaqueta manga corta, combinación de 2 telas y botones en broche', 1, 9, 42, 8, '100.00', 1, 'zoug-zoug-femenino', 0, 1, 'ZZF-1'),
(120, 'Zoug Zoug Female', '<span>S</span>hort sleeve jacket, combination of 2 fabrics and brooch buttons.<br><br>', 2, 11, 45, 10, '100.00', 1, 'zoug-zoug-female', 119, 1, 'ZZF-1'),
(121, 'Zoug Zoug Femenino II', 'Chaqueta manga corta, combinación de 2 telas y botones en broche.<br>', 1, 9, 42, 8, '150.00', 1, 'zoug-zoug-femenino-ii', 0, 1, 'ZZF-2'),
(122, 'Zoug Zoug Female II', 'Short sleeve<span> Jacket</span>, combination of 2 fabrics and brooch buttons.<br>', 2, 11, 45, 10, '150.00', 1, 'zoug-zoug-female-ii', 121, 1, 'ZZF-2'),
(123, 'prueba', 'prueba', 1, 8, 42, 13, '200.00', 2, 'prueba', 0, 1, 'PR-1'),
(124, 'prueba en', 'prueba en', 2, 10, 45, 14, '200.00', 2, 'prueba-en', 123, 1, 'PR-1'),
(125, 'panatalones', 'Pantalones de jose rafael guzman', 1, 8, 42, 13, '200.00', 2, 'panatalones', 0, 0, 'PANTSJ'),
(126, 'PAntalones', 'Pantalon de cocina', 1, 8, 42, 13, '200.00', 1, 'pantalones', 0, 0, 'PMRC');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_productos_imag`
--

CREATE TABLE `detalle_productos_imag` (
  `id` int(11) NOT NULL,
  `id_det_prod` int(11) NOT NULL,
  `id_imagen` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `detalle_productos_imag`
--

INSERT INTO `detalle_productos_imag` (`id`, `id_det_prod`, `id_imagen`) VALUES
(367, 109, 168),
(366, 109, 164),
(365, 110, 168),
(364, 110, 164),
(363, 111, 167),
(362, 111, 166),
(361, 112, 167),
(360, 112, 166),
(359, 113, 163),
(358, 113, 162),
(357, 114, 163),
(356, 114, 162),
(355, 115, 157),
(354, 115, 156),
(353, 116, 157),
(352, 116, 156),
(345, 117, 159),
(344, 117, 158),
(347, 118, 159),
(346, 118, 158),
(337, 119, 161),
(336, 119, 160),
(335, 120, 161),
(334, 120, 160),
(343, 121, 155),
(342, 121, 154),
(329, 122, 155),
(328, 122, 154),
(322, 123, 141),
(323, 124, 141),
(370, 125, 168),
(371, 126, 120);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_tipo_negocio`
--

CREATE TABLE `detalle_tipo_negocio` (
  `id` int(11) NOT NULL,
  `titulo` text NOT NULL,
  `descripcion1` text NOT NULL,
  `descripcion2` text NOT NULL,
  `descripcion3` text NOT NULL,
  `id_tipo_negocio` int(11) NOT NULL,
  `id_imagen` int(50) NOT NULL,
  `estatus` int(11) NOT NULL,
  `slug` text NOT NULL,
  `folleto` text NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `posicion` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detalle_tipo_negocio`
--

INSERT INTO `detalle_tipo_negocio` (`id`, `titulo`, `descripcion1`, `descripcion2`, `descripcion3`, `id_tipo_negocio`, `id_imagen`, `estatus`, `slug`, `folleto`, `id_idioma`, `posicion`) VALUES
(3, 'SAMF', '<p>Es una plataforma de gestión de identidad ultra segura, de nueva generación , orientada al sector financiero. Autentica y gestiona claves estáticas y dinámicas, posee facilidades para autenticación positiva, es fácil de integrar con cualquier canal electrónico de atención de clientes del banco en un ambiente SOA. </p><p>Aplicado correctamente, SAMF fortalece la autenticación a través de los distintos canales tales como banca en línea, banca móvil, cajeros automáticos,&nbsp; Pagos Móviles P2P y P2B,&nbsp; call center y otro tipo de canales.&nbsp; SAMF es una solución de autenticación multicanal. Esta permite implementar mecanismos avanzados de&nbsp; autenticación usando múltiples factores de forma centralizada. </p><p>Permite la creación y administración de las políticas de autenticación de cada canal a través de una sola interfaz unificada de administración. Facilita la administración de perfiles y usuarios del sistema, incluye una poderosa herramienta forensica con registro, almacenamiento y visualización de logs de auditoria. </p>', '<h4 style=\"text-align: center;\"><br></h4><h4><ul style=\"color: rgb(118, 118, 118); font-size: 14px;\"><li><sup>Total cumplimiento de Resolución 641-10 en la República Bolivariana de Venezuela.</sup><br></li><li><sup>Administración centralizada del proceso de autenticación </sup></li><li><sup>Comunicación segura entre SAMF y Canales Fácil integración con canales electrónicos de atención al cliente</sup><br></li><li><sup>Escalabilidad ilimitada</sup><br></li><li><sup>Rápida adaptación a cambios de normativas y políticas</sup><br></li><li><sup>Poderosa herramienta de auditoria y forensica</sup></li></ul></h4>', '<h4 style=\"text-align: center;\"><br></h4>', 2, 24, 1, 'samf', '../administrador/site_media/images/archivos/folletos/folleto_SAMF.pdf', 1, 1),
(4, 'SERVICIOS DE SEGURIDAD ADMINISTRADA', '<h4><span style=\"text-align: justify;\"><sup>IT Security Solutions a través de su gerencia de Servicios Profesionales pone a su disposición un avanzado servicio de Seguridad Administrada con la finalidad de Administrar y Monitorear en tiempo real 7x24x365 su infraestructura de Telecomunicaciones y Seguridad.</sup></span><br></h4><br>', '<h4><strong>LOS SERVICIOS DE SEGURIDAD ADMINISTRADA PUEDEN SER EN LAS ÁREAS DE:&nbsp;</strong></h4><p style=\"text-align: justify; \">Monitoreo de Disponibilidad Administración y Monitoreo de Seguridad Detección de Vulnerabilidades</p><br>', '<h4 style=\"text-align: justify;\"><sup>Con este servicio, nuestros clientes se garantizan que su infraestructura va a ser monitoreada de forma pro-activa permitiéndoles enfocar sus esfuerzos a su ámbito de negocio.&nbsp;<br></sup></h4><h4 style=\"text-align: justify;\"><sup>Este servicio está especialmente diseñado para organizaciones que carecen de recursos humanos con experiencia y/o capacidades de monitoreo y seguridad, no disponen de suficiente personal, o no desean realizar gastos en soluciones de seguridad, bajo esta última premisa, ponemos a su disposición el servicio de Servicio Administrado de Seguridad Total, mediante el cual IT Security Solutions instala en las facilidades del cliente los sistemas necesarios los cuales contaran con pólizas de soporte post-venta por un canon mensual.</sup></h4><br>', 3, 19, 1, 'servicios-de-seguridad-administrada', '', 1, 0),
(5, 'SATM', '<blockquote><h4><span style=\"text-align: justify;\">Es una solución de monitoreo de seguridad proactiva para redes de cajeros automáticos, ayuda a mitigar el fraude electrónico, monitorea los ATM en forma permanente y alerta ante la presencia de actividades sospechosas.&nbsp;</span><br></h4></blockquote><blockquote><h4>SATM consiste de tres elemento básicos: OSSIM, S-Server, S-Agente</h4></blockquote><br>', '<p><h4 style=\"text-align: center;\"><strong>FUNCIONALIDADES</strong></h4><ul><li style=\"text-align: justify;\">Administración Centralizada&nbsp;<br></li></ul></p><p><ul><li style=\"text-align: justify;\">Monitoreo en Tiempo Real (HW y SW del ATM)<br></li><li style=\"text-align: justify;\">Recopilación de Logs de Auditoria<br></li><li style=\"text-align: justify;\">&nbsp;Auto Protección de Agente del ATM&nbsp;<br></li><li style=\"text-align: justify;\">Análisis de Vulnerabilidades Herramientas Forenses&nbsp;<br></li><li style=\"text-align: justify;\">Cumplimiento Normas PCI<br></li></ul></p>', '<h4 style=\"text-align: center;\"><span style=\"text-align: justify;\"><strong>Características Clave del SATM&nbsp;</strong></span></h4><p style=\"text-align: center; \"></p><ul><li style=\"text-align: left;\"><span style=\"text-align: justify;\">Acceso seguro vía HTTPS&nbsp;</span><br></li><li style=\"text-align: left;\"><span style=\"text-align: justify;\">Administración de Usuarios y Perfiles</span><br></li><li style=\"text-align: left;\"><span style=\"text-align: justify;\">Logs de actividad de usuarios</span><br></li><li style=\"text-align: left;\"><span style=\"text-align: justify;\">Análisis de Vulnerabilidades de los ATM</span><br></li><li style=\"text-align: left;\"><span style=\"text-align: justify;\">Visor de Disponibilidad de ATM y Servicios Configuración de Acciones ante la presencia de actividad sospechosa&nbsp;</span><br></li><li style=\"text-align: left;\"><span style=\"text-align: justify;\">Visor de Alarmas, Trafico, Riesgo y Eventos&nbsp;</span><br></li><li style=\"text-align: left;\"><span style=\"text-align: justify;\">Reportes Forenses Edición de Directivas de Correlación Dashboard del Sistema</span><br></li></ul><p></p><br>', 2, 25, 1, 'satm', '../administrador/site_media/images/archivos/folletos/folleto_SATM.pdf', 1, 4),
(6, 'SOPORTE POST-VENTA Y ASISTENCIA', '<h4 style=\"text-align: left;\"><span style=\"text-align: justify; color: rgb(118, 118, 118); font-size: 14px; line-height: 23px;\">A través de nuestra Gerencia de Servicios Profesionales ponemos a su disposición servicios de soporte y asistencia técnica al software desarrollado por nuestra empresa, y a productos de terceras partes comercializados que incluyen soporte técnico de software, hardware, mantenimiento de equipos y reemplazo de equipos (RMA).</span><br></h4><p><strong>Características Clave del Servicio de Asistencia Técnica &nbsp;</strong> </p><ul><li>Soporte ilimitado a incidentes vía teléfono, email y website &nbsp; </li><li>El Cliente puede designar hasta 2 personas (coordinadores de soporte) que pueden contactar a nuestro personal para soporte y asistencia durante la vigencia del contrato, estos serán dotados de una cuenta para acceso al SAT – Sistema de Asistencia Técnica</li><li>Actualizaciones y mejoras para todos los productos comercializados por nuestra organización y que son de libre uso, es decir, que sean sin costo alguno o que estén amparados por contratos de mantenimiento del fabricante &nbsp;</li><li>Soporte remoto para todos los productos comercializados por nuestra organización. &nbsp;&nbsp;</li></ul><br>', '<p>Ofrecemos polizas de soporte y asistencia tecnica en las siguientes modalidades:</p><ul><li>Standard 8x5&nbsp;<br></li><li>Standard 12x7&nbsp;<br></li><li>Premium 12x7&nbsp;<br></li><li>Premium 24x7<br></li></ul><br>', '<br>', 3, 23, 1, 'soporte-post-venta-y-asistencia', '', 1, 0),
(7, 'DESARROLLO DE SOFTWARE', '<p><span style=\"text-align: justify;\">Contamos con un equipo de desarrollo especializado en aplicaciones de seguridad, web y de pago electrónico que posee las habilidades para crear software de calidad colocando a la seguridad siempre como prioridad.</span><br></p><p>Nuestro departamento de Desarrollo IT cuenta con experiencia en Servicios Web seguros, aplicaciones de autenticación robusta, criptografía avanzada, aplicaciones móviles blindadas y todo un conjunto de soluciones con las cuales puedes complementar tus aplicaciones actuales o crear toda una red de aplicaciones y telecomunicaciones segura para tu empresa. &nbsp; Toda empresa requiere seguridad a nivel de infraestructura y software utilizado y en nuestro departamento de Desarrollo IT encontrarás el aliado perfecto para conseguirlo. Además, ofrecemos soporte post producción y acompañamiento en todo el proceso de implementación de nuestros productos complementado con soporte 24/7 con especialistas dedicados a atender las solicitudes favoreciendo los tiempos de respuesta y calidad lograda.</p>', '<p><span style=\"text-align: justify;\">Todos nuestros desarrollos son realizados bajo las mejores practicas internacionales, y cada trabajo es gerenciado por un gerente de proyectos que asegura la calidad y tiempo acordado &nbsp;</span></p>', '<p><br></p><br>', 3, 18, 1, 'desarrollo-de-software', '', 1, 0),
(8, 'SERVICIOS DE CONSULTORÍA', '<p style=\"text-align: justify; \">Con la experiencia ganada a lo largo de nuestra trayectoria hemos logrado un elevado nivel de conocimientos en las áreas de seguridad, telecomunicaciones y servicios, la cual ponemos a disposición de nuestros clientes a través de nuestros Servicios de Consultoría</p><p style=\"text-align: justify; \">Contamos con profesionales debidamente entrenados, de amplia experiencia, en las áreas de arquitecturas de seguridad y comunicaciones, sistemas operativos, análisis de vulnerabilidades, análisis y administración de riesgo, monitoreo de seguridad, definición e implementación de políticas de seguridad informática y capacitación, todo ello con la finalidad de proteger los activos digitales de nuestros apreciados clientes. &nbsp;&nbsp;</p><p style=\"text-align: justify; \">Tenemos asociaciones estratégicas con las principales empresas internacionales fabricantes de software y hardware de seguridad e inteligencia informática, y nuestros ingenieros se encuentran constantemente explorando, evaluando y analizando nuevos productos, a fin de lograr asociaciones que le garanticen a nuestros clientes que tienen a su disposición productos y servicios de clase mundial.</p>', '<blockquote><p>Garantizamos total confidencialidad a nuestros clientes en cuanto al diseño, recomendaciones, e implantación de las soluciones de seguridad y comunicaciones.&nbsp;</p></blockquote><br>', '<p><br></p>', 3, 20, 1, 'servicios-de-consultoria', '', 1, 0),
(9, 'Desarrollo', 'Contamos con un área especializada en el desarrollo de soluciones de seguridad IT y financiera, en ella trabajamos a diario para ayudar a la banca a satisfacer las necesidades de sus clientes, acercándolos a ellos gracias a la tecnología. Nuestro portafolio incluye software de monitoreo de seguridad, generadores de tokens para smartphones y sistemas de pago P2P y P2B.', 'Contamos con un área especializada en el desarrollo de soluciones de seguridad IT y financiera, en ella trabajamos a diario para ayudar a la banca a satisfacer las necesidades de sus clientes, acercándolos a ellos gracias a la tecnología. Nuestro portafolio incluye software de monitoreo de seguridad, generadores de tokens para smartphones y sistemas de pago P2P y P2B.', 'Contamos con un área especializada en el desarrollo de soluciones de seguridad IT y financiera, en ella trabajamos a diario para ayudar a la banca a satisfacer las necesidades de sus clientes, acercándolos a ellos gracias a la tecnología. Nuestro portafolio incluye software de monitoreo de seguridad, generadores de tokens para smartphones y sistemas de pago P2P y P2B.', 2, 43, 1, 'desarrollo', '../administrador/site_media/images/archivos/folletos/folleto_Desarrollo.pdf', 1, 0),
(10, 'SAMF Pago Móvil P2P Interbancario', '<p class=\"MsoNormal\"><span lang=\"ES-VE\" style=\"font-size: 10.5pt; line-height: 107%; font-family: &quot;Open Sans&quot;; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Solución\nque</span>&nbsp;<span lang=\"ES\" style=\"font-size: 10.5pt; line-height: 107%; font-family: &quot;Open Sans&quot;; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">permite realizar transacciones financieras&nbsp;de pagos P2P Persona a\nPersona haciendo uso de teléfonos móviles inteligentes. Facilita pagos de\nproductos y servicios, y transferencia de dinero de persona a persona en\ncuestión de segundos, única aplicación del mercado que incorpora seguridad a través\nde claves dinámicas. Disponible para sistemas Android y IOS.</span><span lang=\"ES\"><o:p></o:p></span></p>', '<p class=\"MsoNormal\" style=\"margin-bottom:0cm;margin-bottom:.0001pt;line-height:\nnormal\"><ul><li>Soporta autenticación con clava Estática y Dinámica (OTP)<br></li><li>Integrable a cualquier Switch Transaccional<br></li><li>Aplicación móvil nativa para IOS y Android<br></li><li>Cumple normas 641.10 de SUDEBAN<br></li><li>Pago rápido mediante código QR cifrado<br></li></ul></p>\n\n\n\n\n\n', '<p><span style=\"font-family: &quot;Open Sans&quot;;\">SAMF Pago Móvil P2P Interbancario se adapta a los requerimientos e imagen corporativa de la institución bancaria, cumple al 100% con la normativa 641.10 de SUDEBAN y a los requerimientos de ASOBANCA.</span></p>', 2, 43, 1, 'samf-pago-movil-p2p-interbancario', '../administrador/site_media/images/archivos/folletos/folleto_SAMF_Pago Móvil P2P Interbancario.pdf', 1, 3),
(11, 'SAMF Token Móvil', '<p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 10.5pt; font-family: &quot;Open Sans&quot;;\">Generador de Tokens (OTPs) para dispositivos móviles\nAndroid y IOS que se integra al sistema de gestión de identidad SAMF para\nasegurar el ingreso a portales de Internet Bankng y para asegurar transacciones\nfinancieras mediante un proceso único de aseguramiento de transacciones\nmediante OTP compuestas que evita ataques de hombre en el medio\n(Man-in-the-middle) y hombre en el Browser (Man-in-the-Browser).<o:p></o:p></span></p>\n\n<p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 10.5pt; font-family: &quot;Open Sans&quot;;\">Se adapta a la imagen corporativa de la institución\nbancaria.<o:p></o:p></span></p>', '<p style=\"margin-bottom: 0.0001pt; line-height: 17.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><ul><li>Seguridad\nsuperior a contraseña estática<br></li><li>Código de\nautenticación dinámico propio de la aplicación móvil vinculado a los\ncomponentes del dispositivo<br></li><li>Se vincula\nel usuario con un PIN de desbloqueo<br></li><li>Las\naplicaciones no pueden ser duplicadas en otro teléfono o dispositivo móvil.<br></li><li>Protocolo de\naprovisionamiento usando claves asimétricas de forma Online u Offline<br></li><li>Disponible para sistemas Android y IOS<br></li></ul></p>\n\n\n\n\n\n\n\n', '', 2, 18, 1, 'samf-token-movil', '../administrador/site_media/images/archivos/folletos/folleto_SAMF_Token Móvil.pdf', 1, 2),
(12, 'SAMF P2P Mobile Payment', '<p>Solution that allows financial transactions of P2P payments Person to Person using smart mobile phones. It facilitates payments of products and services, and transfer of money from person to person in a matter of seconds, the only market application that incorporates security through dynamic keys. Available for Android and IOS systems.</p>', '<ul><li>Supports authentication with Static Password and Dynamic OTP</li><li>Integrable to any Transactional Switch&nbsp; </li><li>Native mobile application for IOS and Android &nbsp;&nbsp; </li><li>Fast payment by encrypted QR code</li></ul>', '<p>SAMF Mobile Payment P2P Interbancario adapts to the requirements and corporate image of the banking institution, meet strict 2FA security requirements.</p>', 5, 43, 1, 'samf-p2p-mobile-payment', '../administrador/site_media/images/archivos/folletos/folleto_SAMF_P2P Mobile Payment.pdf', 2, 3),
(13, 'MANAGED SECURITY SERVICES', '<p>IT Security Solutions through its management of Professional Services puts at your disposal an advanced service of Managed Security with the purpose of Manage and Monitor in real time 7x24x365 its infrastructure of Telecommunications and Security.</p>', '<p>With this service, our clients guarantee that their infrastructure will be proactively monitored, allowing them to focus their efforts on their business scope. This service is specially designed for organizations that lack human resources with experience and / or monitoring and security capabilities, do not have enough personnel, or do not want to spend on security solutions, under this last premise, we offer the service of Managed Total Security Service, through which IT Security Solutions installs in the client&quot;s facilities the necessary systems which will have post-sale support policies for a monthly fee.</p>', '', 6, 23, 1, 'managed-security-services', '', 2, 4),
(14, 'SAMF - Multifactor Autentication Engine', '<p>It is an ultra-secure, next-generation identity management platform, geared towards the financial sector. Authentic and manages static and dynamic keys, has facilities for positive authentication, it is easy to integrate with any electronic channel of customer service of the bank in an SOA environment. &nbsp;&nbsp;</p><p> Applied correctly, SAMF strengthens the authentication through the different channels such as online banking, mobile banking, ATMs, P2P and P2B Mobile Payments, call center and other types of channels. SAMF is a multichannel authentication solution. This allows to implement advanced authentication mechanisms using multiple factors in a centralized manner. &nbsp;&nbsp; </p><p>It allows the creation and administration of the authentication policies of each channel through a single unified administration interface. It facilitates the administration of profiles and users of the system, it includes a powerful forensic tool with registration, storage and visualization of audit logs.</p>', '<ul><li>Centralized administration of the authentication process&nbsp; </li><li>Secure communication between SAMF and Channels&nbsp; </li><li>Easy integration with electronic customer service channels&nbsp; </li><li>Unlimited scalability&nbsp; </li><li>Rapid adaptation to changes in regulations and policies&nbsp; </li><li>Powerful audit and forensic tool</li></ul>', '', 5, 24, 1, 'samf---multifactor-autentication-engine', '../administrador/site_media/images/archivos/folletos/folleto_SAMF_-_Multifactor_Autentication_Engine.pdf', 2, 1),
(15, 'SAMF Token Mobile', '<p>Token Generator (OTPs) for Android and IOS mobile devices that is integrated into the SAMF identity management system to ensure access to Internet Bankng portals and to ensure financial transactions through a single process of securing transactions through composite OTPs that avoid attacks of man in the middle (Man-in-the-middle) and man in the Browser (Man-in-the-Browser). </p><p>It adapts to the corporate image of the banking institution. &nbsp;</p>', '<ul><li>Security above static password</li><li>Dynamic authentication code of the mobile application linked to the components of the device</li><li>The user is linked with an unlock PIN </li><li>Applications can not be duplicated on another phone or mobile device</li><li>Provisioning protocol using asymmetric keys Online or Offline</li><li>Available for Android and IOS systems</li></ul>', '', 5, 18, 1, 'samf-token-mobile', '../administrador/site_media/images/archivos/folletos/folleto_SAMF_Token Mobile.pdf', 2, 2),
(16, 'SATM - ATM Security Monitoring', '<p>It is a proactive security monitoring solution for ATMs networks, helps mitigate electronic fraud, monitors ATMs permanently and alerts to the presence of suspicious activities. </p><p>SATM consists of three basic elements: OSSIM, S-Server, S-Agent</p>', '<p>FUNCTIONALITIES&nbsp; </p><ul><li>Centralized Management </li><li>Real-Time Monitoring (HW and ATM SW) </li><li>Compilation of Audit Logs </li><li>Auto Protection of ATM Agent </li><li>Vulnerability Analysis </li><li>Forensic Tools </li><li>Compliance PCI Standards</li></ul>', '<p>KEY FEAUTURES</p><ul><li>Secure access via HTTPS </li><li>Administration of Users and Profiles</li><li>Logs of user activity</li><li>Vulnerability Analysis of ATM</li><li>Visor of Availability of ATM and Services </li><li>Configuration of Actions in the presence of suspicious activity </li><li>Viewer of Alarms, Traffic, Risk and Events </li><li>Forensic Reports Edition of Correlation Directives Dashboard of the system</li></ul>', 5, 25, 1, 'satm---atm-security-monitoring', '', 2, 4),
(17, 'CONSULTING SERVICES', '<p>In the experience gained throughout our career we have achieved a high level of knowledge in the areas of security, telecommunications and services, which we make available to our customers through our Consulting Services. </p><p>We have trained professionals with extensive experience in the areas of security and communications architectures, operating systems, vulnerability analysis, risk analysis and management, security monitoring, definition and implementation of IT security policies and training, all of which in order to protect the digital assets of our valued customers. </p><p>We have strategic partnerships with leading international software and hardware security and information technology companies, and our engineers are constantly exploring, evaluating and analyzing new products, in order to achieve partnerships that guarantee our customers that they have products at their disposal. and world class services.</p>', '<p>We guarantee total confidentiality to our clients regarding the design, recommendations, and implementation of security and communications solutions.</p>', '', 6, 20, 1, 'consulting-services', '', 2, 1),
(18, 'SOFTWARE DEVELOPMENT', '<p>We have a development team specialized in security, web and electronic payment applications that possess the skills to create quality software, always placing security as a priority. &nbsp;&nbsp; </p><p>Our IT Development group has experience in secure Web Services, robust authentication applications, advanced cryptography, shielded mobile applications and a whole set of solutions with which you can complement your current applications or create a secure network of applications and telecommunications for your company. &nbsp;&nbsp; </p><p>Every company requires security at the level of infrastructure and software used, in our IT Development department you will find the perfect partner to achieve it. In addition, we offer post production support and accompaniment throughout the implementation process of our products supplemented with 24/7 support with specialists dedicated to answering requests, favoring response times and achieved quality.</p>', '<p>All our developments are carried out under the best international practices, and each job is managed by a project manager who ensures the quality and time agreed.</p>', '', 6, 23, 1, 'software-development', '', 2, 2),
(19, 'TECHNICAL SUPPORT', '<p>Through our Professional Services Management, we offer support services and technical assistance to the software developed by our company, and third-party products marketed, including technical support for software, hardware, equipment maintenance and equipment replacement (RMA).</p><p><strong>Key Characteristics of the Technical Assistance Service</strong>: &nbsp;&nbsp; </p><ul><li>Unlimited support to incidents via phone, email and website&nbsp; </li><li>The Client can designate up to 2 people (support coordinators) who can contact our staff for support and assistance during the term of the contract they will be provided with an account for access to the SAT - Technical Assistance System&nbsp; </li><li>Updates and improvements for all products marketed by our organization and that are free to use, that is, free of charge or that are covered by manufacturer maintenance contracts&nbsp; </li><li>Remote support for all products marketed by our organization</li></ul>', '<p>We offer support policies and technical assistance in the following ways:&nbsp; </p><ul><li>Standard 8x5&nbsp; </li><li>Standard 12x7&nbsp; </li><li>Premium 12x7&nbsp; </li><li>Premium 24x7</li></ul>', '', 6, 23, 1, 'technical-support', '', 2, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direcciones`
--

CREATE TABLE `direcciones` (
  `id` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `titulo` text NOT NULL,
  `posicion` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `direcciones`
--

INSERT INTO `direcciones` (`id`, `descripcion`, `estatus`, `id_idioma`, `titulo`, `posicion`) VALUES
(1, 'Av. Francisco de Miranda, Centro Empresarial Miranda, Piso 4, Oficina 4B, Urb. Los Ruices. \nCaracas - Venezuela\nCódigo Postal 1071', 1, 1, 'OFICINA VENEZUELA', 1),
(2, '6101	Blue Lagoon Dr, Suite 150,\n      Miami, FL, 33126', 1, 1, 'OFICINA EEUU', 2),
(3, 'Prueba', 0, 1, 'OFICINA SHANGAY', 0),
(4, 'Av. Francisco de Miranda, Centro Empresarial Miranda, Piso 4, Oficina 4B, Urb. Los Ruices. Caracas - Venezuela Código Postal 1071', 1, 2, 'VENEZUELA OFFICE', 2),
(5, '6101 Blue Lagoon Dr, Suite 150, Miami, FL, 33126', 1, 2, 'USA OFFICE', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direccion_mapas`
--

CREATE TABLE `direccion_mapas` (
  `id` int(11) NOT NULL,
  `latitud` text NOT NULL,
  `longitud` text NOT NULL,
  `id_direccion` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `direccion_mapas`
--

INSERT INTO `direccion_mapas` (`id`, `latitud`, `longitud`, `id_direccion`) VALUES
(2, '25.961511', '-80.39116', 2),
(3, '10.4914821', '-66.8294758', 1),
(4, '53.397291', '-2.171772', 3),
(5, '10.4914821', '-66.8294758', 4),
(6, '25.961511', '-80.39116', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa_nosotros`
--

CREATE TABLE `empresa_nosotros` (
  `id` int(11) NOT NULL,
  `somos` text NOT NULL,
  `mision` text NOT NULL,
  `id_imagen` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  `vision` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empresa_nosotros`
--

INSERT INTO `empresa_nosotros` (`id`, `somos`, `mision`, `id_imagen`, `id_idioma`, `estatus`, `vision`) VALUES
(7, 'RFLLL', 'HHHH', 105, 2, 2, ''),
(8, 'III', 'IUIU', 105, 2, 2, ''),
(9, 'FDFD', 'DFDF', 105, 2, 2, ''),
(11, 'HHGH', 'GHGH', 105, 2, 2, ''),
(14, 'BBB', 'HHH', 105, 1, 2, ''),
(15, '<B><I><U>PRUEBA</U></I></B>', '<B><I>CON BOLD</I></B>', 105, 1, 2, ''),
(16, '<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean condimentum nisl quis libero mattis ultrices sed sed metus. Curabitur sagittis arcu est, id lacinia nisl egestas non. Duis in cursus turpis. Suspendisse ut quam nunc. In ultricies pulvinar mi, vitae ornare libero pharetra nec.&nbsp;<br></span><br><ul><li>Nulla ullamcorper nulla arcu.</li><li>blandit cursus sem pharetra efficitur</li><li>In vulputate dolor vitae diam.</li></ul>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean condimentum nisl quis libero mattis ultrices sed sed metus. Curabitur sagittis arcu est, id lacinia nisl egestas non. Duis in cursus turpis. Suspendisse ut quam nunc. In ultricies pulvinar mi, vitae ornare libero pharetra nec.&nbsp;<br>', 112, 1, 2, ''),
(17, '<B>PRUEBA</B>', '<I>PRUBEAAA</I>', 105, 2, 2, ''),
(18, 'HHH', 'HHH', 105, 1, 2, ''),
(19, '<span>Lorem Ingles dolor sit amet, consectetur adipiscing elit. Aenean condimentum nisl quis libero mattis ultrices sed sed metus. Curabitur sagittis arcu est, id lacinia nisl egestas non. Duis in cursus turpis. Suspendisse ut quam nunc. In ultricies pulvinar mi, vitae ornare libero pharetra nec.&nbsp;<br></span><ul><li>Nulla ullamcorper nulla arcu,</li><li><span>blandit cursus sem pharetra efficitur.</span></li><li><span>In vulputate dolor vitae diam&nbsp;</span></li></ul><span><br></span>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean condimentum nisl quis libero mattis ultrices sed sed metus. Curabitur sagittis arcu est, id lacinia nisl egestas non. Duis in cursus turpis. Suspendisse ut quam nunc. In ultricies pulvinar mi, vitae ornare libero pharetra nec.&nbsp;', 112, 2, 2, ''),
(20, '<span>En Zoug Zoug &nbsp;nos dedicamos a la confección, diseño y distribución de indumentaria como: chaquetas, filipinas, pantalones, etc. para el personal del departamento de alimentos y bebidas como chef, cocineros, baristas, sommeliers etc. de hoteles, restaurantes y emprendimientos en general dirigido al ramo de la gastronomía.</span>', 'Fabricar y confeccionar ropa de cocina a nivel nacional e internacional. Y ofrecerles a nuestros clientes un producto de calidad, cumplir con sus necesidades ofreciéndoles mayor confort y comodidad.<br>', 112, 1, 1, 'Ser una empresa reconocida a nivel mundial por nuestra calidad en telas y diseños, a su vez del buen servicio que ofrecemos a nuestros clientes.<br>'),
(21, '<div><div><div><div><div><div>In Zoug Zoug we are dedicated to the preparation, design and distribution of clothing such as: jackets, philippines, trousers, etc. for food and beverage department staff such as chefs, baristas, sommeliers, etc. of hotels, restaurants and businesses in general to the gastronomy branch.<div><span><br></span></div></div><div></div><div></div></div></div></div></div><div></div><div></div></div>', 'Manufacture and make kitchen clothes nationally and internationally. And offer our customers a quality product, meet their needs by offering greater comfort and convenience.<br>', 112, 2, 1, 'To be a company recognized worldwide for our quality fabrics and designs, as well as the good service we offer to our customers.<br>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estatus`
--

CREATE TABLE `estatus` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estatus`
--

INSERT INTO `estatus` (`id`, `descripcion`) VALUES
(1, 'Activo'),
(2, 'Inactivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `footer`
--

CREATE TABLE `footer` (
  `id` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `correo` varchar(200) NOT NULL,
  `rif` varchar(30) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `footer`
--

INSERT INTO `footer` (`id`, `descripcion`, `correo`, `rif`, `id_idioma`, `estatus`) VALUES
(1, 'NUESTRAS SOLUCIONES DE GESTION DE IDENTIDAD Y MEDIOS DE PAGO DISRUPTIVOS ULTRA SEGUROS PERMITEN A LAS INSTITUCIONES FINANCIERAS REDUCIR EL RIESGO , ASEGURAR LAS OPERACIONES, OPTIMIZAR EL RENDIMIENTO DE SUS APLICACIONES, LLEGAR A NUEVOS CLIENTES, MANTENER LA FIDELIDAD DE LOS ACTUALES, GENERAR NUEVOS INGRESOS Y POSICIONARSE COMO LIDERES DE LA BANCA DIGITAL.', 'CONTACTO@ZOUGZOUG.COM', 'J-31119865-2', 0, 1),
(2, 'Information of fotter, intsert this copy text', 'info@zougzoug.com', '35-2509419', 2, 1),
(3, 'Información referencial del footer', 'info@zougzoug.com', '', 1, 1),
(4, '<B>PRUEBA INGLES&NBSP;</B>', 'PRUEBA_INGLES@GMAIL.COM', '', 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeria`
--

CREATE TABLE `galeria` (
  `id` int(11) NOT NULL,
  `ruta` varchar(500) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `galeria`
--

INSERT INTO `galeria` (`id`, `ruta`, `id_categoria`, `estatus`, `titulo`) VALUES
(96, 'assets/images/archivos/slider/laptop.jpeg', 1, 2, 'LAPTOP 2'),
(97, 'assets/images/archivos/marcas/dos.jpeg', 2, 2, 'dos'),
(98, 'assets/images/archivos/quienes_somos/imagen.jpeg', 3, 2, 'imagen'),
(99, 'assets/images/archivos/quienes_somos/imagen.jpeg', 3, 2, 'imagen'),
(100, 'assets/images/archivos/quienes_somos/dos.jpeg', 3, 2, 'dos'),
(101, 'assets/images/archivos/marcas/cofee.jpeg', 2, 2, 'cofee'),
(102, 'assets/images/archivos/noticias/noticias1.jpeg', 4, 2, 'noticias1'),
(103, 'assets/images/archivos/noticias/noticias2.jpeg', 4, 2, 'noticias2'),
(104, 'assets/images/archivos/noticias/noticias3.jpeg', 4, 2, 'noticias3'),
(105, 'assets/images/archivos/nosotros/Nosotros.jpeg', 20, 2, 'Nosotros'),
(106, 'assets/images/archivos/noticias/noticias1.jpeg', 4, 1, 'noticias1'),
(107, 'assets/images/archivos/noticias/noticias2.jpeg', 4, 1, 'noticias2'),
(108, 'assets/images/archivos/noticias/noticias3.jpeg', 4, 1, 'noticias3'),
(109, 'assets/images/archivos/slider/slider_kitche1.jpeg', 1, 2, 'slider_kitche1'),
(110, 'assets/images/archivos/slider/slider_kitchen2.jpeg', 1, 2, 'slider_kitchen2'),
(111, 'assets/images/archivos/slider/slider_kitchen3.jpeg', 1, 2, 'slider_kitchen3'),
(112, 'assets/images/archivos/nosotros/nosotros1.jpeg', 20, 1, 'nosotros1'),
(113, 'assets/images/archivos/nosotros/nosotros2.jpeg', 20, 2, 'nosotros2'),
(114, 'assets/images/archivos/marcas/prueba.jpeg', 2, 2, 'prueba'),
(119, 'assets/images/archivos/productos/Zoug_Zoug_male_Front01_MG_9736.png', 22, 1, 'Producto 1'),
(120, 'assets/images/archivos/productos/Producto_2.jpeg', 22, 1, 'Producto 2'),
(121, 'assets/images/archivos/productos/Producto_3.jpeg', 22, 1, 'Producto 3'),
(122, 'assets/images/archivos/productos/Producto_4.jpeg', 22, 1, 'Producto 4'),
(123, 'assets/images/archivos/productos/Producto_5.jpeg', 22, 1, 'Producto 5'),
(124, 'assets/images/archivos/productos/Producto_6.jpeg', 22, 1, 'Producto 6'),
(125, 'assets/images/archivos/productos/Producto_7.jpeg', 22, 1, 'Producto 7'),
(126, 'assets/images/archivos/productos/Producto_8.jpeg', 22, 1, 'Producto 8'),
(127, 'assets/images/archivos/productos/Producto_9.jpeg', 22, 1, 'Producto 9'),
(128, 'assets/images/archivos/productos/Zoug_Zoug_male_Front01_MG_9736.png', 22, 2, 'Zoug_Zoug_male_Front01_MG_9736'),
(135, 'assets/images/archivos/productos/Zoug_Zoug_female_Front01_MG_9701.png', 22, 1, 'Zoug_Zoug_female_Front01_MG_9701'),
(130, 'assets/images/archivos/productos/Premium_male_Front01_MG_9723.png', 22, 1, 'Premium_male_Front01_MG_9723'),
(131, 'assets/images/archivos/productos/Premium_male_Back02_MG_9724.png', 22, 1, 'Premium_male_Back02_MG_9724'),
(132, 'assets/images/archivos/productos/Liqui_Liqui_male_Front01_MG_9711.png', 22, 1, 'Liqui_Liqui_male_Front01_MG_9711'),
(133, 'assets/images/archivos/productos/Liqui_Liqui_male_Back02_MG_9712.png', 22, 1, 'Liqui_Liqui_male_Back02_MG_9712'),
(134, 'assets/images/archivos/productos/Zoug_Zoug_male_Back02_MG_9737.png', 22, 1, 'Zoug_Zoug_male_Back02_MG_9737.jpg'),
(136, 'assets/images/archivos/productos/Zoug_Zoug_female_back02_MG_9702.png', 22, 1, 'Zoug_Zoug_female_back02_MG_9702'),
(137, 'assets/images/archivos/productos/Zoug_Zoug_female_front01_MG_9687.png', 22, 1, 'Zoug_Zoug_female_front01_MG_9687'),
(138, 'assets/images/archivos/productos/Zoug-Zoug-female-back02_MG_9689.png', 22, 1, 'Zoug-Zoug-female-back02_MG_9689'),
(139, 'assets/images/archivos/productos/Premium-female-front01_MG_9691.png', 22, 1, 'Premium-female-front01_MG_9691'),
(140, 'assets/images/archivos/productos/Premium_female_back02_MG_9692..png', 22, 1, 'Premium_female_back02_MG_9692'),
(141, 'assets/images/archivos/productos/Liqui_Liqui_female_front01_MG_9695.png', 22, 1, 'Liqui_Liqui_female_front01_MG_9695'),
(142, 'assets/images/archivos/productos/Liqui-Liqui-female-front01_MG_9696.png', 22, 1, 'Liqui-Liqui-female-front01_MG_9696'),
(143, 'assets/images/archivos/slider/slider_cambio_orientacion1.jpeg', 1, 2, 'slider_cambio_orientacion1'),
(144, 'assets/images/archivos/slider/slider_orientacion_2.jpeg', 1, 2, 'slider_orientacion_2'),
(145, 'assets/images/archivos/slider/slider_orientacion3.jpeg', 1, 2, 'slider_orientacion3'),
(146, 'assets/images/archivos/slider/slider_1_1980_853.jpeg', 1, 2, 'slider_1_1980_853'),
(147, 'assets/images/archivos/slider/slider_principal.jpeg', 1, 2, 'slider_principal'),
(148, 'assets/images/archivos/slider/slider_tercero.jpeg', 1, 2, 'slider_tercero'),
(149, 'assets/images/archivos/slider/slider_segundo.jpeg', 1, 2, 'slider_segundo'),
(150, 'assets/images/archivos/slider/slider_nuevo3.jpeg', 1, 2, 'slider_nuevo3'),
(151, 'assets/images/archivos/slider/slide1800.jpeg', 1, 1, 'slide1800'),
(152, 'assets/images/archivos/slider/slider2800.jpeg', 1, 1, 'slider2800'),
(153, 'assets/images/archivos/slider/slider3800.jpeg', 1, 1, 'slider3800'),
(154, 'assets/images/archivos/productos/zoug_zoug_femenino_front_nuevo_formato.jpeg', 22, 1, 'zoug_zoug_femenino_front_nuevo_formato'),
(155, 'assets/images/archivos/productos/zoug_zoug_femenino_back_nuevo_formato.jpeg', 22, 1, 'zoug_zoug_femenino_back_nuevo_formato'),
(156, 'assets/images/archivos/productos/premium_femenino_front_nuevo_formato.jpeg', 22, 1, 'premium_femenino_front_nuevo_formato'),
(157, 'assets/images/archivos/productos/premium_femenino_back_nuevo_formato.jpeg', 22, 1, 'premium_femenino_back_nuevo_formato'),
(158, 'assets/images/archivos/productos/liquiliqui_femenino_front_nuevo_formato.jpeg', 22, 1, 'liquiliqui_femenino_front_nuevo_formato'),
(159, 'assets/images/archivos/productos/liquiliqui_femenino_back_nuevo_formato.jpeg', 22, 1, 'liquiliqui_femenino_back_nuevo_formato'),
(160, 'assets/images/archivos/productos/zoug_zoug_femenino1_front_nuevo_formato.jpeg', 22, 1, 'zoug_zoug_femenino1_front_nuevo_formato'),
(161, 'assets/images/archivos/productos/zoug_zoug_femenino1_back_nuevo_formato.jpeg', 22, 1, 'zoug_zoug_femenino1_back_nuevo_formato'),
(162, 'assets/images/archivos/productos/liquiliqui_masculino_front_nuevo_formato.jpeg', 22, 1, 'liquiliqui_masculino_front_nuevo_formato'),
(163, 'assets/images/archivos/productos/liquiliqui_masculino_back_nuevo_formato.jpeg', 22, 1, 'liquiliqui_masculino_back_nuevo_formato'),
(164, 'assets/images/archivos/productos/premium_masculino_front_nuevo_formato.jpeg', 22, 1, 'premium_masculino_front_nuevo_formato'),
(165, 'assets/images/archivos/productos/liquiliqui_masculino_back_nuevo_formato.jpeg', 22, 1, 'liquiliqui_masculino_back_nuevo_formato'),
(166, 'assets/images/archivos/productos/zougzoug_masculino_front_nuevo_formato.jpeg', 22, 1, 'zougzoug_masculino_front_nuevo_formato'),
(167, 'assets/images/archivos/productos/zougzoug_masculino_back_nuevo_formato.jpeg', 22, 1, 'zougzoug_masculino_back_nuevo_formato'),
(168, 'assets/images/archivos/productos/PREMIUM_MASCULINO_BACK_NUEVO_FORMATO.jpeg', 22, 1, 'PREMIUM_MASCULINO_BACK_NUEVO_FORMATO'),
(174, 'assets/images/archivos/slider/slider1.jpeg', 1, 1, 'slider1'),
(0, 'assets/images/delete-trash.png', 0, 1, ''),
(175, 'assets/images/archivos/slider/slider2.jpeg', 1, 1, 'slider2'),
(176, 'assets/images/archivos/slider/slider3.jpeg', 1, 1, 'slider3'),
(180, 'assets/images/archivos/usuarios/usuario_admin.png', 23, 1, 'usuario_admin'),
(178, 'assets/images/archivos/usuarios/rod_montana.jpeg', 23, 1, 'rod_montana');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeria_clientes`
--

CREATE TABLE `galeria_clientes` (
  `id` int(11) NOT NULL,
  `id_galeria` int(11) NOT NULL,
  `id_clientes` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `galeria_clientes`
--

INSERT INTO `galeria_clientes` (`id`, `id_galeria`, `id_clientes`) VALUES
(15, 70, 2),
(16, 70, 3),
(17, 71, 3),
(18, 72, 3),
(19, 73, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idioma`
--

CREATE TABLE `idioma` (
  `id` int(11) NOT NULL,
  `descripcion` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `idioma`
--

INSERT INTO `idioma` (`id`, `descripcion`) VALUES
(1, 'Español'),
(2, 'Inglés');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario_carrito`
--

CREATE TABLE `inventario_carrito` (
  `id` int(11) NOT NULL,
  `id_det_prod` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `id_color` int(11) NOT NULL,
  `id_talla` int(50) NOT NULL,
  `estatus` int(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `inventario_carrito`
--

INSERT INTO `inventario_carrito` (`id`, `id_det_prod`, `cantidad`, `id_color`, `id_talla`, `estatus`) VALUES
(254, 126, 100, 10, 2, 2),
(253, 121, 100, 10, 2, 1),
(252, 121, 100, 10, 3, 1),
(251, 121, 100, 10, 4, 1),
(250, 119, 94, 10, 2, 1),
(249, 119, 100, 10, 3, 1),
(248, 119, 100, 10, 4, 1),
(247, 117, 100, 10, 2, 1),
(246, 117, 100, 10, 3, 1),
(245, 117, 100, 10, 4, 1),
(244, 115, 98, 10, 2, 1),
(243, 115, 100, 10, 3, 1),
(242, 115, 100, 10, 4, 1),
(241, 113, 100, 10, 2, 1),
(240, 113, 100, 10, 3, 1),
(239, 113, 100, 10, 4, 1),
(238, 111, 98, 10, 2, 1),
(237, 111, 100, 10, 3, 1),
(236, 111, 100, 10, 4, 1),
(235, 109, 98, 10, 2, 1),
(234, 109, 100, 10, 3, 1),
(233, 109, 100, 10, 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario_ordenes`
--

CREATE TABLE `inventario_ordenes` (
  `id` int(20) NOT NULL,
  `id_det_prod` int(20) NOT NULL,
  `cantidad` int(20) NOT NULL,
  `id_color` int(20) NOT NULL,
  `id_talla` int(20) NOT NULL,
  `estatus` int(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `inventario_ordenes`
--

INSERT INTO `inventario_ordenes` (`id`, `id_det_prod`, `cantidad`, `id_color`, `id_talla`, `estatus`) VALUES
(254, 126, 100, 10, 2, 2),
(253, 121, 100, 10, 2, 1),
(252, 121, 100, 10, 3, 1),
(251, 121, 100, 10, 4, 1),
(250, 119, 94, 10, 2, 1),
(249, 119, 100, 10, 3, 1),
(248, 119, 100, 10, 4, 1),
(247, 117, 100, 10, 2, 1),
(246, 117, 100, 10, 3, 1),
(245, 117, 100, 10, 4, 1),
(244, 115, 98, 10, 2, 1),
(243, 115, 100, 10, 3, 1),
(242, 115, 100, 10, 4, 1),
(241, 113, 100, 10, 2, 1),
(240, 113, 100, 10, 3, 1),
(239, 113, 100, 10, 4, 1),
(238, 111, 98, 10, 2, 1),
(237, 111, 100, 10, 3, 1),
(236, 111, 100, 10, 4, 1),
(235, 109, 98, 10, 2, 1),
(234, 109, 100, 10, 3, 1),
(233, 109, 100, 10, 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marcas`
--

CREATE TABLE `marcas` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descripcion` varchar(500) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `id_imagen` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  `orden` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `marcas`
--

INSERT INTO `marcas` (`id`, `titulo`, `descripcion`, `id_idioma`, `id_imagen`, `estatus`, `orden`) VALUES
(7, 'LIQUI LIQUI', 'LIQUI LIQUI', 1, 0, 1, 3),
(8, 'ZOUG ZOUG', 'ZOUG ZOUG', 1, 0, 1, 4),
(9, 'LIQUI LIQUI', 'LIQUI LIQUI', 2, 0, 1, 3),
(10, 'ZOUG ZOUG', 'ZOUG ZOUG', 2, 0, 1, 4),
(11, 'PREMIUM', 'PREMIUM', 1, 0, 1, 2),
(12, 'PREMIUM', 'PREMIUM', 2, 0, 1, 2),
(13, 'PANTALONES', 'PANTALONES', 1, 0, 1, 1),
(14, 'PANTS', 'PANTS', 2, 0, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulos`
--

CREATE TABLE `modulos` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `modulos`
--

INSERT INTO `modulos` (`id`, `descripcion`, `estatus`) VALUES
(1, 'cms', 1),
(2, 'web', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `negocios`
--

CREATE TABLE `negocios` (
  `id` int(11) DEFAULT NULL,
  `objetivo_negocio` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orden_compra`
--

CREATE TABLE `orden_compra` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `numero_orden_compra` text NOT NULL,
  `fecha` date NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_carrito` int(11) NOT NULL,
  `payment_id` text NOT NULL,
  `payer_id` text NOT NULL,
  `payment_token` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `orden_compra`
--

INSERT INTO `orden_compra` (`id`, `id_usuario`, `numero_orden_compra`, `fecha`, `estatus`, `id_carrito`, `payment_id`, `payer_id`, `payment_token`) VALUES
(103, 44, 'aec3ed80ba7405d980fa93673152e1a1', '2020-02-09', 1, 81, 'PAYID-LZAFTRQ00066176PN2855221', 'VKD9GMWBRNE84', 'EC-1EW08940BT426340U'),
(102, 44, 'ccf80d51216742552cd02d5336c2c592', '2020-02-07', 1, 80, 'PAYID-LY7AX7Q5LY61777465270419', 'VKD9GMWBRNE84', 'EC-5WW3567021519844N'),
(101, 44, 'f0a5d0b4f5ceff00c44284a02b12585e', '2020-02-06', 1, 79, 'PAYID-LY6HOEI41P5257089117605S', 'VKD9GMWBRNE84', 'EC-4JL59682J4300362A'),
(104, 44, '0896978c498a88d2dc9d7a4e450b8bf3', '2020-02-17', 1, 82, 'PAYID-LZFQNTQ0R239421N95610939', 'VKD9GMWBRNE84', 'EC-4KD81401L8827110X'),
(105, 44, 'c20ca033ca887555273e109b9fa50be4', '2020-02-17', 1, 83, 'PAYID-LZFQ6BY59B54602UL270335X', 'VKD9GMWBRNE84', 'EC-32V571631A367820D');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orden_compra_detalle`
--

CREATE TABLE `orden_compra_detalle` (
  `id` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `monto` float NOT NULL,
  `monto_total` float NOT NULL,
  `id_orden` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_cantidad_producto` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `orden_compra_detalle`
--

INSERT INTO `orden_compra_detalle` (`id`, `id_producto`, `cantidad`, `monto`, `monto_total`, `id_orden`, `estatus`, `id_cantidad_producto`) VALUES
(129, 109, 2, 300, 600, 104, 1, 235),
(128, 111, 2, 100, 200, 103, 1, 238),
(127, 119, 3, 100, 300, 102, 1, 250),
(126, 115, 2, 200, 400, 101, 1, 244),
(130, 119, 3, 100, 300, 105, 1, 250);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `palabras_claves`
--

CREATE TABLE `palabras_claves` (
  `id` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `palabras_claves`
--

INSERT INTO `palabras_claves` (`id`, `descripcion`, `estatus`) VALUES
(66, 'pago movil', 1),
(67, ' qr', 1),
(68, ' pago con qr', 1),
(69, ' token movil', 1),
(70, ' P2P', 1),
(71, ' pago movil p2p', 1),
(72, ' pago movil sms', 1),
(73, ' pago movil p2p sms', 1),
(74, ' pago movil interbancario', 1),
(75, ' pago movil p2p interbancario', 1),
(76, ' p2c', 1),
(77, ' pago movil p2c', 1),
(78, '  pago movil p2c interbancario', 1),
(79, ' p2b', 1),
(80, ' pago p2b qr', 1),
(81, ' pago qr', 1),
(82, ' mobile payment', 1),
(83, ' qr payment', 1),
(84, ' p2p payment', 1),
(85, ' p2c payment', 1),
(86, ' p2b payment', 1),
(87, ' secure payment', 1),
(88, ' secure qr payment', 1),
(89, ' interbank payment', 1),
(90, ' p2p mobile payment', 1),
(91, ' p2c mobile payment', 1),
(92, ' p2b mobile payment', 1),
(93, ' secure mobile payment', 1),
(94, ' ultra secure mobile payment', 1),
(95, ' soft token', 1),
(96, ' soft token android', 1),
(97, ' soft token ios', 1),
(98, ' identity management', 1),
(99, ' identitity management financial', 1),
(100, ' media payment', 1),
(101, ' media payment secure solutions', 1),
(102, ' mobile media payment', 1),
(103, 'ejemplo', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `id` int(11) NOT NULL,
  `cedula` int(11) NOT NULL,
  `nombres_apellidos` varchar(100) NOT NULL,
  `estatus` int(11) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`id`, `cedula`, `nombres_apellidos`, `estatus`, `telefono`, `email`) VALUES
(1, 9999999, 'ADMINISTRADOR', 1, '04164561122', 'content.manager@gmail.com'),
(12, 2000000, 'USUARIO COMUN', 1, '04161112223', 'usuario_comun@gmail.com'),
(11, 3000000, 'EDITOR', 1, '04164564445', 'editor@gmail.com'),
(13, 40000000, 'RODRIGO MONTANA', 0, '04164564561', 'rdmontana@gmail.com'),
(14, 12345678, 'NORMAN PEREZ', 1, '04167485648', 'zorman_videos@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `portafolio`
--

CREATE TABLE `portafolio` (
  `id` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `id_servicio` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `fecha_creacion` date NOT NULL,
  `url` varchar(155) NOT NULL,
  `cliente` varchar(255) NOT NULL,
  `id_imagen` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  `slug` text NOT NULL,
  `posicion` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `portafolio`
--

INSERT INTO `portafolio` (`id`, `id_idioma`, `id_servicio`, `titulo`, `descripcion`, `fecha_creacion`, `url`, `cliente`, `id_imagen`, `estatus`, `slug`, `posicion`) VALUES
(1, 1, 11, 'Página web euroglobalrsi', '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; line-height: 20px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus diam arcu, ullamcorper sit', '2018-11-17', 'www.euroglobalrsi.com', 'euroglobalrsi', 74, 1, 'pgina-web-euroglobalrsi', 2),
(2, 1, 11, 'Aktive', '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; line-height: 20px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus diam arcu, ullamcorper sit', '2018-11-17', '', 'aktive', 66, 1, 'aktive', 1),
(3, 1, 11, 'Esaica', '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; line-height: 20px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus diam arcu, ullamcorper sit', '2018-11-25', 'www.esaica.esy.es', 'esaica', 77, 1, 'esaica', 3),
(4, 2, 12, 'Euroglobalrsi', '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; line-height: 20px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus diam arcu, ullamcorper sit', '2018-11-25', 'www.euroglobalrsi.com', 'euroglobalrsi', 74, 1, 'euroglobalrsi', 2),
(5, 2, 12, 'Aktive', '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; line-height: 20px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus diam arcu, ullamcorper sit', '2018-11-25', 'www.aktive.com', 'Aktive', 66, 1, 'aktive', 1),
(6, 1, 5, 'Marca empresa', '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; line-height: 20px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus diam arcu, ullamcorper sit', '2018-11-25', '', 'Etc', 68, 1, 'marca-empresa', 1),
(7, 2, 12, 'Esaica', '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; line-height: 20px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus diam arcu, ullamcorper sit', '2018-11-25', 'www.esaica.esy.es', 'Esiaca', 77, 1, 'esaica', 3),
(8, 1, 6, 'Logos', '<p><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; line-height: 20px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam in blandit libero. Proin vitae egestas risus. Nulla consequat', '2018-11-30', '', 'Mcdonalds', 76, 1, 'logos', 0),
(9, 2, 9, 'Logos', '<p><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; line-height: 20px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam in blandit libero. Proin vitae egestas risus. Nulla consequat', '2018-11-28', '', 'Mcdonalds', 76, 1, 'logos', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_clonados`
--

CREATE TABLE `productos_clonados` (
  `id` int(11) NOT NULL,
  `id_det_prod` int(11) NOT NULL COMMENT 'id del producto clonado',
  `id_producto_original` int(11) NOT NULL COMMENT 'id del producto original'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `productos_clonados`
--

INSERT INTO `productos_clonados` (`id`, `id_det_prod`, `id_producto_original`) VALUES
(8, 93, 92),
(9, 95, 94),
(10, 98, 97),
(11, 99, 97),
(12, 102, 101),
(13, 104, 103),
(14, 106, 105),
(15, 108, 107),
(16, 110, 109),
(17, 112, 111),
(18, 114, 113),
(19, 116, 115),
(20, 118, 117),
(21, 120, 119),
(22, 122, 121),
(23, 124, 123);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `redes`
--

CREATE TABLE `redes` (
  `id` int(11) NOT NULL,
  `enlace` varchar(200) NOT NULL,
  `id_tipo_redes` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `red_social`
--

CREATE TABLE `red_social` (
  `id` int(11) NOT NULL,
  `url_red` varchar(100) NOT NULL,
  `id_tipo_red` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `red_social`
--

INSERT INTO `red_social` (`id`, `url_red`, `id_tipo_red`) VALUES
(5, 'HTTPS://WWW.INSTAGRAM.COM/ZOUG.ZOUG/', 3),
(6, 'HTTPS://WWW.FACEBOOK.COM/CHEFPAOLARODRIGUEZ', 1),
(11, 'HTTPS://TWITTER.COM/ZOUG_ZOUG', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seccion_galeria`
--

CREATE TABLE `seccion_galeria` (
  `id` int(11) NOT NULL,
  `id_imagen` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seccion_noticias`
--

CREATE TABLE `seccion_noticias` (
  `id` int(11) NOT NULL,
  `titulo` varchar(300) NOT NULL,
  `id_imagen` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `estatus` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `slug` text NOT NULL,
  `id_idioma` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `seccion_noticias`
--

INSERT INTO `seccion_noticias` (`id`, `titulo`, `id_imagen`, `descripcion`, `estatus`, `fecha`, `id_usuario`, `slug`, `id_idioma`) VALUES
(9, 'NOTICIA PRINCIPAL', 106, '<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras a porttitor risus. Pellentesque a nibh elementum libero vehicula molestie. Suspendisse sed odio facilisis, eleifend purus eget, suscipit lorem. Donec scelerisque nisi sed sem laoreet semper. Ut aliquet rhoncus mi ac hendrerit. Suspendisse sodales est dui, non consequat libero pellentesque malesuada. Etiam vel justo risus. Sed mollis condimentum nunc sit amet dignissim. In enim eros, molestie eu elementum a, feugiat quis ligula.</span><br>', 1, '2019-02-28', 1, 'noticia-principal', 1),
(10, 'NOTICIA DOS', 104, '<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras a porttitor risus. Pellentesque a nibh elementum libero vehicula molestie. Suspendisse sed odio facilisis, eleifend purus eget, suscipit lorem. Donec scelerisque nisi sed sem laoreet semper. Ut aliquet rhoncus mi ac hendrerit. Suspendisse sodales est dui, non consequat libero pellentesque malesuada. Etiam vel justo risus. Sed mollis condimentum nunc sit amet dignissim. In enim eros, molestie eu elementum a, feugiat quis ligula.</span><br>', 1, '2019-02-28', 1, 'noticia-dos', 1),
(11, 'NOTICIA 3', 108, '<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras a porttitor risus. Pellentesque a nibh elementum libero vehicula molestie. Suspendisse sed odio facilisis, eleifend purus eget, suscipit lorem. Donec scelerisque nisi sed sem laoreet semper. Ut aliquet rhoncus mi ac hendrerit. Suspendisse sodales est dui, non consequat libero pellentesque malesuada. Etiam vel justo risus. Sed mollis condimentum nunc sit amet dignissim. In enim eros, molestie eu elementum a, feugiat quis ligula.</span><br>', 1, '2019-02-28', 1, 'noticia-3', 1),
(12, 'NEWS 1', 106, '<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras a porttitor risus. Pellentesque a nibh elementum libero vehicula molestie. Suspendisse sed odio facilisis, eleifend purus eget, suscipit lorem. Donec scelerisque nisi sed sem laoreet semper. Ut aliquet rhoncus mi ac hendrerit. Suspendisse sodales est dui, non consequat libero pellentesque malesuada. Etiam vel justo risus. Sed mollis condimentum nunc sit amet dignissim. In enim eros, molestie eu elementum a, feugiat quis ligula</span>', 1, '2019-02-28', 1, 'news-1', 2),
(13, 'NEWS 2', 107, '<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras a porttitor risus. Pellentesque a nibh elementum libero vehicula molestie. Suspendisse sed odio facilisis, eleifend purus eget, suscipit lorem. Donec scelerisque nisi sed sem laoreet semper. Ut aliquet rhoncus mi ac hendrerit. Suspendisse sodales est dui, non consequat libero pellentesque malesuada. Etiam vel justo risus. Sed mollis condimentum nunc sit amet dignissim. In enim eros, molestie eu elementum a, feugiat quis ligula.</span>', 1, '2019-02-28', 1, 'news-2', 2),
(14, 'NEWS 3', 108, '<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras a porttitor risus. Pellentesque a nibh elementum libero vehicula molestie. Suspendisse sed odio facilisis, eleifend purus eget, suscipit lorem. Donec scelerisque nisi sed sem laoreet semper. Ut aliquet rhoncus mi ac hendrerit. Suspendisse sodales est dui, non consequat libero pellentesque malesuada. Etiam vel justo risus. Sed mollis condimentum nunc sit amet dignissim. In enim eros, molestie eu elementum a, feugiat quis ligula.</span>', 1, '2019-02-28', 1, 'news-3', 2),
(15, 'PRUEBA 4', 107, '<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ac ornare dui. Sed libero sapien, commodo non accumsan non, egestas a sem. Etiam ac diam in nibh efficitur pellentesque id quis eros. Cras rhoncus libero quis cursus dignissim. Praesent suscipit, magna quis malesuada consectetur, risus ante faucibus purus, nec pharetra odio ex tempor dui. Fusce sed velit tincidunt, hendrerit lacus suscipit, bibendum massa. Phasellus elit ante, pulvinar vel mauris et, aliquet euismod erat. Aliquam suscipit vestibulum lacus eget gravida. Etiam interdum tincidunt ligula, vel eleifend tortor malesuada eget. Nulla sit amet enim et magna hendrerit dictum. Fusce tortor massa, porta at dignissim ut, tempus sit amet arcu. Suspendisse ornare, ante in sollicitudin vestibulum, nisi est ullamcorper mauris, quis lacinia ex magna quis sem. Sed et enim dolor. In eget tempus lorem. Pellentesque quis vehicula est, vel congue felis.</span>', 1, '2019-11-11', 1, 'prueba-4', 1),
(16, 'STAND UP DE JOSÉ RAFAEL GUZMAN', 108, 'Ejemplo de noticia', 1, '2020-02-17', 1, 'stand-up-de-josro-rafael-guzman', 1),
(17, 'PRUEBA DE NOTICIA', 106, 'Otra noticia', 1, '2020-02-17', 1, 'prueba-de-noticia', 1),
(18, 'UNA NOTICIA MAS', 106, 'Una noticia mas', 1, '2020-02-17', 1, 'una-noticia-mas', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

CREATE TABLE `servicios` (
  `id` int(11) NOT NULL,
  `id_imagen` int(11) NOT NULL,
  `titulo` varchar(250) NOT NULL,
  `descripcion` text NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `posicion` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `servicios`
--

INSERT INTO `servicios` (`id`, `id_imagen`, `titulo`, `descripcion`, `estatus`, `id_idioma`, `posicion`) VALUES
(1, 56, 'Primera', 'Primera Prueba', 2, 1, 0),
(2, 55, 'Prueba', '2', 2, 1, 0),
(3, 57, 'Prueba Mod', 'sdsds', 2, 1, 0),
(4, 57, 'prueba 4', 'prueba hoy', 2, 1, 0),
(5, 61, 'MARKETING DIGITAL', '<h3><sub>Nuestras estrategias permiten alcanzar los objetivos de negocio e impulsar las marcas.</sub></h3><h3><sub><br></sub><ul><li><sub>Estrategia de Marketing Digital</sub></li></ul><ul><li><sub>Marketing de Contenidos</sub></li></ul><ul><li><sub>Estrategia de Redes Sociales</sub></li></ul><ul><li><sub>Posicionamiento SEO &amp; SEM</sub></li></ul><ul><li><sub>E-Mail Marketing</sub></li></ul><ul><li><sub>Publicidad Display</sub></li></ul></h3>', 1, 1, 6),
(6, 60, 'DISEÑO GRÁFICO', '<h3><sub>Ofrecemos soluciones innovadoras e impactantes para una comunicación visual efectiva de su empresa.<br></sub><h3><sub>Nuestro servicio incluye:</sub></h3><h3><br><ul><li><sub>Diseño de Logotipos</sub></li></ul><ul><li><sub>Identidad Corporativa</sub></li></ul><ul><li><sub>Creación de Marca</sub></li></ul><ul><li><sub>Diseño Editorial</sub></li></ul><ul><li><sub>Folletos y Catálogos</sub></li></ul><ul><li><sub>Diseño de Empaques</sub></li></ul></h3></h3>', 1, 1, 1),
(7, 63, 'REDES SOCIALES', '<h3 style=\"text-align: justify;\"><sub>GESTIÓN DE REDES SOCIALES:<br></sub></h3><h3 style=\"text-align: justify;\"><sub>Construimos y gestionamos comunidades alrededor de los negocios en las plataformas de Redes Sociales.&nbsp;<br>Nuestro servicio ayuda a su empresa a mejorar la presencia en línea, fortalecer la relación con sus clientes, aumentar el tráfico web y obtener reconocimiento de la marca.</sub></h3><h3 style=\"text-align: justify;\"><sub><br></sub><ul><li><sub>Creación de Perfiles Sociales</sub></li></ul><ul><li><sub>Gestión Estratégica de Redes Sociales</sub></li></ul><ul><li><sub>Dinamización de Comunidades Online&nbsp;</sub></li></ul><ul><li><sub>Creación de Contenido en las Plataformas Sociales</sub></li></ul><ul><li><sub>Informe de Gestión</sub></li></ul></h3>', 1, 1, 3),
(8, 63, 'SOCIAL MEDIA', '<h3><sub>SOCIAL MEDIA MANAGEMENT:<br></sub></h3><h3><sub>We build and manage communities around business in the different Social Media platforms.&nbsp;<br></sub></h3><h3></h3><h3><sub>Our service help your company to enhance online presence, strengthen relationship with your clients, increase web traffic and gain brand awareness.</sub></h3><span style=\"font-size: 18px; line-height: 0;\"><sub><br></sub></span><h3><ul><li><sub><span style=\"font-size: 18px; line-height: 0;\">Social Profiles Creation</span></sub></li></ul><ul><li><sub><span style=\"font-size: 18px; line-height: 0;\">Strategic Social Media Management</span></sub></li></ul><ul><li><sub><span style=\"font-size: 18px; line-height: 0;\">Dynamizing Online Communities</span></sub></li></ul><ul><li><sub><span style=\"font-size: 18px; line-height: 0;\">Content Creation on Social platforms</span></sub></li></ul></h3><h3><ul><li><sub>Social Media Management Reporting</sub></li></ul></h3>', 1, 2, 2),
(9, 60, 'GRAPHIC DESIGN', '<h3><sub>We offer innovative and impactful solutions for an effective visual communication of your company.<br></sub><h3><sub>Our service includes:</sub><h3><h3><h3><ul><li><sub>Logo Design</sub></li></ul><ul><li><sub>Corporate Identity</sub></li></ul><ul><li><sub>Branding</sub></li></ul><ul><li><sub>Editorial Design</sub></li></ul><ul><li><sub>Brochures and catalogs</sub></li></ul><ul><li><sub>Packaging Design</sub></li></ul></h3></h3></h3></h3></h3><p><sub></sub></p>', 1, 2, 1),
(10, 61, 'DIGITAL MARKETING', '<h3><sub>Our strategies allow to achieve the business objectives and boost the brands</sub></h3><h3><sub><br></sub><ul><li><sub>Digital Marketing Strategy</sub></li></ul><ul><li><sub>Content Marketing</sub></li></ul><ul><li><sub>Social Media Strategy</sub></li></ul><ul><li><sub>SEO &amp; SEM</sub></li></ul><ul><li><sub>E-Mail Marketing</sub></li></ul><ul><li><sub>Display Advertising</sub></li></ul></h3>', 1, 2, 3),
(11, 62, 'DESARROLLO WEB', '<h3></h3><h3><sub>DISEÑO Y DESARROLLO WEB:<br></sub></h3><h3><sub>Creamos sitios web profesionales, funcionales y optimizados para múltiples dispositivos.</sub></h3><h3><sub><br></sub></h3><h3 style=\"text-align: justify;\"></h3><h3><ul><li><sub>Sitios Web Corporativos</sub></li></ul><ul><li><sub>Diseño Web Responsive</sub></li></ul><ul><li><sub>Soluciones Ecommerce</sub></li></ul><ul><li><sub>Sitios Web Personalizados</sub></li></ul><ul><li><sub>Blogs y Landing Pages</sub></li></ul><ul><li><sub>Mantenimiento de Sitios Web</sub></li></ul></h3>', 1, 1, 2),
(12, 62, 'WEB DESIGN', '<h3 style=\"text-align: justify;\"><sub>WEB DESIGN &amp; DEVELOPMENT:<br></sub></h3><h3 style=\"text-align: justify;\"><sub>We build and manage communities around business in the different Social Media platforms.&nbsp;<br></sub><sub>Our service help your company to enhance online presence, strengthen relationship with your clients, increase web traffic and gain brand awareness.</sub></h3><h3 style=\"text-align: justify;\"><br><ul><li><sub>Social Profiles Creation</sub></li></ul><ul><li><sub>Strategic Social Media Management</sub></li></ul><ul><li><sub>Dynamizing Online Communities</sub></li></ul><ul><li><sub>Content Creation on Social platforms</sub></li></ul><ul><li><sub>Social Media Management Reporting</sub></li></ul></h3>', 1, 2, 4),
(13, 59, 'SEO', '<h3><sub>SEARCH ENGINE OPTIMIZATION (SEO):<br></sub></h3><h3><sub>We increase the visibility of your business website in the most popular search engines in order to generate quality organic traffic, improve brand image and attract more potential customers.</sub></h3><h3><sub><br>Our SEO service includes:</sub></h3><h3><sub><br></sub><ul><li><sub>Web Technical Audit</sub></li></ul><ul><li><sub>Keyword Research</sub></li></ul><ul><li><sub>Content Strategy&nbsp;</sub></li></ul><ul><li><sub>On/Off-Page Optimization</sub></li></ul><ul><li><sub>Report and Monitoring</sub></li></ul></h3>', 1, 2, 5),
(14, 59, 'SEO', '<h3><sub>OPTIMIZACIÓN DE MOTORES DE BÚSQUEDA (SEO):<span style=\"font-size: 18px; line-height: 0px;\">&nbsp;</span></sub></h3><h3><sub>Aumentamos la visibilidad del sitio web de su negocio en los motores de búsqueda más populares con el objeto de generar tráfico orgánico de calidad, mejorar la imagen de marca y atraer más clientes potenciales.</sub></h3><h3><sub><br></sub></h3><h3><sub>Nuestro servicio SEO incluye:</sub></h3><p><sub><br></sub></p><h3><ul><li><sub>Auditoria Técnica Web<br></sub></li><li><sub>Investigación de Palabras Claves<br></sub></li><li><sub>Estrategia de Contenido&nbsp;<br></sub></li><li><sub>Optimización On/Off del Sitio Web<br></sub></li><li><sub>Reporte y Seguimiento</sub><br></li></ul></h3>', 1, 1, 4),
(15, 64, 'CONSULTING', '<h3 style=\"text-align: justify;\"><sub>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sem eros, ullamcorper vel convallis a, convallis a quam. Nullam porta mauris augue, a imperdiet elit tempus vitae. Quisque venenatis nunc id nisl varius hendrerit. Nullam sit amet auctor m</sub></h3>', 1, 2, 6),
(16, 64, 'CONSULTORÍA', '<h3 style=\"text-align: justify;\"><sub>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sem eros, ullamcorper vel convallis a, convallis a quam. Nullam porta mauris augue, a imperdiet elit tempus vitae. Quisque venenatis nunc id nisl varius hendrerit. Nullam sit amet auctor m</sub></h3>', 1, 1, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio_texto`
--

CREATE TABLE `servicio_texto` (
  `id` int(11) NOT NULL,
  `titulo_text` varchar(150) NOT NULL,
  `texto` varchar(250) NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `posicion` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `servicio_texto`
--

INSERT INTO `servicio_texto` (`id`, `titulo_text`, `texto`, `estatus`, `id_idioma`, `posicion`) VALUES
(8, 'roxi', 'prueba', 2, 1, 1),
(9, 'dsdsds', 'sdsdsd', 0, 2, 1),
(10, 'CUÁL ES NUESTRO TRABAJO', '<p><span style=\"color: rgb(119, 119, 119); font-family: Roboto-Light; font-size: 20px; line-height: 35px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sem eros, ullamcorper vel convallis a, convallis a quam. Nul', 1, 1, 1),
(11, 'WHAT WE DO', '<h3><span style=\"text-align: justify;\"><sub>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sem eros, ullamcorper vel convallis a, convallis a quam. Nullam porta mauris augue, a imperdiet elit tempus vitae. Quisque venenatis nunc id nis', 1, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `titulo` text NOT NULL,
  `descripcion` text NOT NULL,
  `imagen` varchar(300) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `boton` varchar(200) NOT NULL,
  `id_imagen` int(200) NOT NULL,
  `estatus` int(11) NOT NULL,
  `url` varchar(300) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `orden` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `slider`
--

INSERT INTO `slider` (`id`, `titulo`, `descripcion`, `imagen`, `id_idioma`, `boton`, `id_imagen`, `estatus`, `url`, `direccion`, `orden`) VALUES
(264, 'TRABAJA BIEN, SIÉNTETE BIEN,  LUCE BIEN', '', '96', 1, 'VER PRODUCTOS', 176, 1, 'PRODUCTOS', '600', 3),
(265, 'DISEÑO FUNCIONAL, CASUAL Y VANGUARDISTA', '', '', 1, 'COMPRAR', 175, 1, 'PRODUCTOS', '700', 2),
(266, 'FUNCTIONAL, CASUAL AND AVANT-GARDE DESIGN', '', '', 2, 'SHOP NOW', 175, 1, 'PRODUCTS', '700', 2),
(267, 'NUEVA TENDENCIA EN INDUMENTARIA GASTRONÓMICA', '', '', 1, 'COMPRAR', 174, 1, 'PRODUCTOS', '600', 1),
(268, 'WORK GOOD, FEEL GOOD, LOOK GOOD', '', '', 2, 'VIEW PRODUCTS', 176, 1, 'PRODUCTS', '600', 3),
(269, 'NEW TRENDY IN KITCHEN CLOTHING', '', '', 2, 'SHOP NOW', 174, 1, 'PRODUCTS', '600', 1),
(270, 'UNO', 'DESC UNO', '', 1, 'UNO', 96, 2, 'UNO', '600', 2),
(271, 'NUEVO TITULO', 'NUEVO TITULO', '', 1, 'NUEVO TITULO BTN', 96, 2, 'BTN', '600', 7),
(272, 'PRUEBA', 'PRUEBA', '', 1, 'TITULO', 96, 2, 'TITULO', '[15,15,42,15]', 6),
(0, 'PRUEBA', 'ESTO ES UNA PRUEBA', '', 1, 'PRUEBA', 147, 2, 'PRUEBA', '500', 3),
(0, 'PRUEBA SLIDER 4', 'PRUEBA SLIDER 4', '', 1, '', 152, 2, '', '500', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `social_login`
--

CREATE TABLE `social_login` (
  `id` int(200) NOT NULL,
  `nombre_apellido` varchar(200) NOT NULL,
  `correo` varchar(200) NOT NULL,
  `clave` varchar(50) NOT NULL,
  `codigo` varchar(100) NOT NULL,
  `tp_login` int(10) NOT NULL,
  `estatus` int(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `social_login`
--

INSERT INTO `social_login` (`id`, `nombre_apellido`, `correo`, `clave`, `codigo`, `tp_login`, `estatus`) VALUES
(44, 'nelly moreno', 'nlmdiaz@gmail.com', '7c222fb2927d828af22f592134e8932480637c0d', 'uLRPt9CnXAkl', 1, 1),
(45, 'axioma developers', 'contacto.axioma.dvlp@gmail.com', '', '', 2, 1),
(46, 'gianni santucci', 'gianni.d.santucci@gmail.com', '', '', 2, 1),
(47, 'dario carrillo', 'gdsc120987@hotmail.com', '7c222fb2927d828af22f592134e8932480637c0d', 'ZqT1bp9WvRKc', 1, 2),
(48, 'Pedro perez', 'pperez@gmail.com', '7c222fb2927d828af22f592134e8932480637c0d', 'nXBRGahstuy4', 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tallas`
--

CREATE TABLE `tallas` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tallas`
--

INSERT INTO `tallas` (`id`, `descripcion`, `estatus`) VALUES
(1, 'XS', 0),
(2, 'S', 1),
(3, 'M', 1),
(4, 'L', 1),
(5, 'XL', 0),
(6, 'TRTR', 2),
(7, 'AZUL', 2),
(8, 'U', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefonos`
--

CREATE TABLE `telefonos` (
  `id` int(11) NOT NULL,
  `id_direccion` int(11) NOT NULL,
  `telefono` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `telefonos`
--

INSERT INTO `telefonos` (`id`, `id_direccion`, `telefono`) VALUES
(15, 1, '+58-212-234-24-44'),
(16, 1, '+58-412-219-48-77'),
(23, 3, '+5609998877'),
(26, 4, '+58-212-234-24-44'),
(27, 4, '+58-412-219-48-77'),
(32, 5, '+1-786-288-01-75'),
(33, 5, '+1-305-262-28-15'),
(34, 2, '+1-786-288-01-75'),
(35, 2, '+1-305-262-28-15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_usuarios`
--

CREATE TABLE `tipos_usuarios` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipos_usuarios`
--

INSERT INTO `tipos_usuarios` (`id`, `descripcion`) VALUES
(1, 'administrador'),
(2, 'editor'),
(3, 'usuario_comun');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_negocio`
--

CREATE TABLE `tipo_negocio` (
  `id` int(11) NOT NULL,
  `titulo` text NOT NULL,
  `descripcion` text NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `posicion` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_negocio`
--

INSERT INTO `tipo_negocio` (`id`, `titulo`, `descripcion`, `estatus`, `id_idioma`, `posicion`) VALUES
(1, 'Integración de Soluciones', '<p style=\"margin-bottom: 0.0001pt; line-height: 17.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size:10.5pt;font-family:&quot;Open Sans&quot;;\nmso-bidi-font-family:Helvetica;color:#333333;mso-ansi-language:ES\" lang=\"ES\">Contamos con\namplia experiencia en el área de integraci</span><span style=\"font-size:11.0pt;font-family:&quot;Calibri&quot;,sans-serif;color:black;\nmso-ansi-language:ES\" lang=\"ES\">ó</span><span style=\"font-size:10.5pt;font-family:\n&quot;Open Sans&quot;;mso-bidi-font-family:Helvetica;color:#333333;mso-ansi-language:\nES\" lang=\"ES\">n de soluciones de seguridad y telecomunicaciones, a la fecha hemos\nrealizado innumerables implementaciones en los sectores de Finanzas, Gobierno,\nTelecomunicaciones, Seguros, Industria y Comercio en general.<o:p></o:p></span></p>\n\n<span style=\"font-size:10.5pt;font-family:&quot;Open Sans&quot;;\nmso-bidi-font-family:Helvetica;color:#333333;mso-ansi-language:ES\" lang=\"ES\">&nbsp;</span>\n\n<p style=\"margin-bottom: 0.0001pt; line-height: 17.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size:10.5pt;font-family:&quot;Open Sans&quot;;\nmso-bidi-font-family:Helvetica;color:#333333;mso-ansi-language:ES\" lang=\"ES\">Nuestra\nfortaleza se basa en un equipo humano integrado por ingenieros debidamente\nentrenados y certificados en las marcas que comercializamos, que tienen como misión\nprestar servicios de preventa y postventa de <strong>excelencia.</strong><o:p></o:p></span></p>', 1, 1, 3),
(2, 'Portafolio de Soluciones', '<p>Contamos con un área especializada en el desarrollo de soluciones de seguridad IT y financiera, en ella trabajamos a diario para ayudar a la banca a satisfacer las necesidades de sus clientes, acercándolos a ellos gracias a la tecnología. Nuestro portafolio incluye software de gestión de identidad, generadores de tokens para smartphones y sistemas de pago P2P, P2C y P2B ultra seguros.</p>', 1, 1, 1),
(3, 'Servicios Profesionales', '<p>Ponemos a disposición de nuestros clientes una amplia gama de servicios de soporte técnico, entrenamiento, consultoría, control de proyectos y pólizas de soporte en variadas modalidades que se ajustan según sus necesidades.</p>', 1, 1, 2),
(4, 'Ejemplo de modificación', ' Lorem ipsum dolor', 0, 1, 0),
(5, 'Solutions Portfolio', '<p>We have an area specialized in the development of IT and financial security solutions, in which we work daily to help banks meet the needs of their customers, bringing them closer to them thanks to technology. Our portfolio includes identity management software, token generators for smartphones and P2P, P2C and P2B ultra secure payment systems.</p>', 1, 2, 0),
(6, 'Professional Services', '<p>We offer our clients a wide range of technical support services, training, consulting, project control and support policies in various ways that are adjusted according to your needs.</p>', 1, 2, 0),
(7, 'Integration Services', '<p>We have extensive experience in the area of integration of security and telecommunications solutions, to date we have made countless implementations in the sectors of Finance, Government, Telecommunications, Insurance, Industry and Commerce in general. &nbsp; &nbsp;&nbsp; Our strength is based on a human team made up of engineers duly trained and certified in the brands we market, whose mission is to provide pre-sales and after-sales services of excellence. &nbsp; &nbsp; &nbsp; &nbsp;</p>', 1, 2, 0),
(8, 'prueba de registro cargar  modificar', 'Esto es una prueba de registro de modificación, el registro ha sido modificado<br>', 0, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_producto`
--

CREATE TABLE `tipo_producto` (
  `id` int(11) NOT NULL,
  `titulo` varchar(500) NOT NULL,
  `descripcion` varchar(500) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `id_categoria_prod` int(11) NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_producto`
--

INSERT INTO `tipo_producto` (`id`, `titulo`, `descripcion`, `id_idioma`, `id_categoria_prod`, `estatus`) VALUES
(42, 'ROPA DE COCINA', 'ROPA DE COCINA', 1, 0, 1),
(45, 'KITCHEN CLOTHES', 'KITCHEN CLOTHES', 2, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_red`
--

CREATE TABLE `tipo_red` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_red`
--

INSERT INTO `tipo_red` (`id`, `descripcion`) VALUES
(1, 'Facebook'),
(2, 'Twitter'),
(3, 'Instagram'),
(4, 'Linkedin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_redes`
--

CREATE TABLE `tipo_redes` (
  `id` int(11) NOT NULL,
  `descripcion` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_redes`
--

INSERT INTO `tipo_redes` (`id`, `descripcion`) VALUES
(1, 'Facebook'),
(2, 'Twitter');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `id_persona` int(11) NOT NULL,
  `id_tipo_usuario` int(11) NOT NULL,
  `login` varchar(150) NOT NULL,
  `clave` varchar(150) NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_imagen` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `id_persona`, `id_tipo_usuario`, `login`, `clave`, `estatus`, `id_imagen`) VALUES
(1, 1, 1, 'administrador', '54fcaf4b9a819766aff69755f649d5ea3fbe8468', 1, 180),
(11, 12, 3, 'user', 'a0320a043f8b41c9a3d29dd8dc947efdf0980fb1', 1, 0),
(10, 11, 2, 'editor', '489c6ca4fa44d6733a85a2f158a19e35491b264a', 1, 0),
(13, 13, 1, 'rmontana', '7c4a8d09ca3762af61e59520943dc26494f8941b', 0, 178),
(14, 14, 1, 'zorman', '7c4a8d09ca3762af61e59520943dc26494f8941b', 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

CREATE TABLE `videos` (
  `id` int(11) NOT NULL,
  `titulo` text NOT NULL,
  `link` text NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `videos`
--

INSERT INTO `videos` (`id`, `titulo`, `link`, `estatus`, `id_idioma`) VALUES
(1, 'SAMF Pago movil, noticias Globovisión tecnología', 'https://www.youtube.com/watch?v=8QmIyF7q3vI', 1, 1),
(2, 'Pago P2P Noticias globovisión tecnología', 'https://www.youtube.com/watch?v=NcS4g3lN_eU&t=53s', 0, 1),
(3, 'BanescoPagoMovil', 'https://youtu.be/jBFZXhpvYKo', 0, 1),
(4, 'Pago Móvil Banesco', 'https://www.youtube.com/watch?v=jBFZXhpvYKo', 1, 1),
(5, 'Entrevista a  Deiniel Cardenas Banesco Pago Móvil', 'https://www.youtube.com/watch?v=z33RIoHKKBY&feature=youtu.be', 1, 1),
(6, 'BANESCO PAGO MOVIL', 'https://youtu.be/JVUT2YBJTw0', 0, 1),
(7, 'BANESCO PAGO MÓVIL', 'https://www.youtube.com/watch?v=JVUT2YBJTw0&feature=youtu.be', 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `auditoria`
--
ALTER TABLE `auditoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cambio_contraseña`
--
ALTER TABLE `cambio_contraseña`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cantidad_producto`
--
ALTER TABLE `cantidad_producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `carrito_encabezado`
--
ALTER TABLE `carrito_encabezado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `carrito_productos`
--
ALTER TABLE `carrito_productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias_productos_idiomas`
--
ALTER TABLE `categorias_productos_idiomas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoria_producto`
--
ALTER TABLE `categoria_producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `colores`
--
ALTER TABLE `colores`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contactos`
--
ALTER TABLE `contactos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contactos_empleo`
--
ALTER TABLE `contactos_empleo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `correos`
--
ALTER TABLE `correos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `descripcion`
--
ALTER TABLE `descripcion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detalle_productos`
--
ALTER TABLE `detalle_productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detalle_productos_imag`
--
ALTER TABLE `detalle_productos_imag`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detalle_tipo_negocio`
--
ALTER TABLE `detalle_tipo_negocio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `direcciones`
--
ALTER TABLE `direcciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `direccion_mapas`
--
ALTER TABLE `direccion_mapas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `empresa_nosotros`
--
ALTER TABLE `empresa_nosotros`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estatus`
--
ALTER TABLE `estatus`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `footer`
--
ALTER TABLE `footer`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `galeria`
--
ALTER TABLE `galeria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `galeria_clientes`
--
ALTER TABLE `galeria_clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `idioma`
--
ALTER TABLE `idioma`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inventario_carrito`
--
ALTER TABLE `inventario_carrito`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inventario_ordenes`
--
ALTER TABLE `inventario_ordenes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `marcas`
--
ALTER TABLE `marcas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `modulos`
--
ALTER TABLE `modulos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `negocios`
--
ALTER TABLE `negocios`
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `orden_compra`
--
ALTER TABLE `orden_compra`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orden_compra_detalle`
--
ALTER TABLE `orden_compra_detalle`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `palabras_claves`
--
ALTER TABLE `palabras_claves`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `portafolio`
--
ALTER TABLE `portafolio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos_clonados`
--
ALTER TABLE `productos_clonados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `redes`
--
ALTER TABLE `redes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `red_social`
--
ALTER TABLE `red_social`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `seccion_galeria`
--
ALTER TABLE `seccion_galeria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `seccion_noticias`
--
ALTER TABLE `seccion_noticias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `servicio_texto`
--
ALTER TABLE `servicio_texto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `social_login`
--
ALTER TABLE `social_login`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tallas`
--
ALTER TABLE `tallas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `telefonos`
--
ALTER TABLE `telefonos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipos_usuarios`
--
ALTER TABLE `tipos_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_negocio`
--
ALTER TABLE `tipo_negocio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_producto`
--
ALTER TABLE `tipo_producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_redes`
--
ALTER TABLE `tipo_redes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `auditoria`
--
ALTER TABLE `auditoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=504;

--
-- AUTO_INCREMENT de la tabla `cambio_contraseña`
--
ALTER TABLE `cambio_contraseña`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `cantidad_producto`
--
ALTER TABLE `cantidad_producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=255;

--
-- AUTO_INCREMENT de la tabla `carrito_encabezado`
--
ALTER TABLE `carrito_encabezado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT de la tabla `carrito_productos`
--
ALTER TABLE `carrito_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `categorias_productos_idiomas`
--
ALTER TABLE `categorias_productos_idiomas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `categoria_producto`
--
ALTER TABLE `categoria_producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `colores`
--
ALTER TABLE `colores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `contactos`
--
ALTER TABLE `contactos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT de la tabla `contactos_empleo`
--
ALTER TABLE `contactos_empleo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT de la tabla `correos`
--
ALTER TABLE `correos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `descripcion`
--
ALTER TABLE `descripcion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `detalle_productos`
--
ALTER TABLE `detalle_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT de la tabla `detalle_productos_imag`
--
ALTER TABLE `detalle_productos_imag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=372;

--
-- AUTO_INCREMENT de la tabla `detalle_tipo_negocio`
--
ALTER TABLE `detalle_tipo_negocio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `direcciones`
--
ALTER TABLE `direcciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `direccion_mapas`
--
ALTER TABLE `direccion_mapas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `empresa_nosotros`
--
ALTER TABLE `empresa_nosotros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `estatus`
--
ALTER TABLE `estatus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `footer`
--
ALTER TABLE `footer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `galeria`
--
ALTER TABLE `galeria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=181;

--
-- AUTO_INCREMENT de la tabla `galeria_clientes`
--
ALTER TABLE `galeria_clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `idioma`
--
ALTER TABLE `idioma`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `marcas`
--
ALTER TABLE `marcas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `modulos`
--
ALTER TABLE `modulos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `orden_compra`
--
ALTER TABLE `orden_compra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT de la tabla `orden_compra_detalle`
--
ALTER TABLE `orden_compra_detalle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT de la tabla `palabras_claves`
--
ALTER TABLE `palabras_claves`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `portafolio`
--
ALTER TABLE `portafolio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `productos_clonados`
--
ALTER TABLE `productos_clonados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `redes`
--
ALTER TABLE `redes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `red_social`
--
ALTER TABLE `red_social`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `seccion_galeria`
--
ALTER TABLE `seccion_galeria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `seccion_noticias`
--
ALTER TABLE `seccion_noticias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `servicios`
--
ALTER TABLE `servicios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `servicio_texto`
--
ALTER TABLE `servicio_texto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `social_login`
--
ALTER TABLE `social_login`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT de la tabla `tallas`
--
ALTER TABLE `tallas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `telefonos`
--
ALTER TABLE `telefonos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de la tabla `tipos_usuarios`
--
ALTER TABLE `tipos_usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipo_negocio`
--
ALTER TABLE `tipo_negocio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `tipo_producto`
--
ALTER TABLE `tipo_producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT de la tabla `tipo_redes`
--
ALTER TABLE `tipo_redes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
